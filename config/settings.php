<?php

return [

    'zahtjev' => [
        'statuses' => [
            1 => 'U izradi',
            2 => 'Predan',
            3 => 'Završen',
        ]
    ],

    'ovjera' => [
        'statuses' => [
            1 => 'Prihvaćen',
            2 => 'Odbijen',
            3 => 'Odustao',
        ]
    ],

];
