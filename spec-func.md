# Funkcionalna specifikacija
### Aplikacija Državnog zavoda za mjeriteljstvo

---
**Naziv aplikacije** DZM Admin  
**Korisnik** Državni zavod za mjeriteljstvo  
**Razvoj** AGmedia d.o.o.  
**Privremeni URL** http://mapp.selectpo.lin48.host25.com/
---

## 1. Namjena aplikacije

---
Aplikacija Državnog zavoda za mjeriteljstvo (u daljnjem tekstu DZM aplikacija) je programski proizvod koji omogućava upravljanje nad radom 
Državnog zavoda za mjeriteljstvo i ovlaštenih tijela za obavljanje
poslova ovjeravanja zakonitih mjerila, vođenje evidencije svih zakonitih mjerila na području
Republike Hrvatske, provođenje nadzora nad ovlaštenim tijelima za poslove ovjeravanja
zakonitih mjerila te transparentnije provođenje inspekcijskog nadzora od strane mjeriteljske
inspekcije nad radom ovlaštenih tijela za obavljanje poslova ovjeravanja zakonitih mjerila i
transparentnost javno dostupnih podataka o zakonitim mjerilima svim zainteresiranim
stranama.

## 2. Izrada aplikacije

---
### 2.2. Osnovne značajke 

DZM aplikacija je izrađena u PHP serverskom jeziku, koristi MySql bazu podataka bez troškova za godišnje licence. 
Prednja strana aplikacije (u daljnjem takstu frontend, koja je dostupna javnosti) i administracijska strana aplikacije (u daljnjem tekstu backend) aplikacije 
je responsive, odnosno oba su prilagođena za internet preglednike i kao takvi dostupni za rad
na osobnom računalu, tablet računalima i pametnim telefonima (Android/iOS). 
DZM aplikacija također je prilagođena GDPR pravilima koja omogućava brisanje osobnih podataka korisnika prema njihovom zahtjevu. 
Brisanje bilo kakvih osobnih podataka bilo kojeg korisnika neće imati negativan utjecaj na DZM aplikaciju.
DZM aplikaciji će se moći pristupiti sa bilo kojeg mjesta na svijetu uz pristup internetu. 
Backend-u uz uvjet da imate ispravne korisničke podatke, frontend-u bez uvjeta.

### 2.3. Izgled aplikacije

Kada govorimo o opisu izgleda DZM aplikacije uglavnom se misli na izgled desktop verzije (na računalu). 
Na mobilnoj i na tablet verziji izgled je zbog oblika ekrana uglavnom natjeran da bude jedan objekt ispod drugoga do zadnjega na pojedinoj stranici.

#### 2.3.1. Backend
Backend se sastoji od navigacije koja se nalazi sa lijeve strane i sadrži u kategorijskom obliku poveznice na sve glavne stranice aplikacije.
Glavni ekran desno ad navigacije služi za pojedine radnje aplikacije i kao takav bit će objašnjen svaki pojednini dio u nastavku specifikacije.
Backend također sadrži takozvani topbar(malo zaglavlje) gdje se sa desne strane nalazi ime registriranog korisnika uz padajuči meni 
za brzi pristup svom profilu, odlogiravanju iz aplikacije i poveznicu za frontend.
U mobilnoj i tablet verziji topbar također sadrži i gumb za izvlačenje navigacije na ekran.

#### 2.3.2. Frontend
Frontend se sastoji od topbara koji sadrži logo Državnog zavoda za mjeriteljstvo sa lijeve strane i 
brze padajuće poveznice za prijavljivanje, registraciju i/ili administraciju ako je korisnik već prijavljen sa desne.
Središnji glavni dio sadrži kratak opisni tekst i polje za tražilicu ovlaštenih mjerila. Nakon početka upisa automatski se počinju prikazivati rezultati
pretrage u tabličnom ispisu ispod tražilice.

## 3. Uloge aplikacije

---
### 3.1. Superadmin

Super-administrator je uloga koja služi za informatičare i ima sve ovlasti koje ima DZM aplikacija.

### 3.2. Administracija DZM

Administracija Državnog zavoda za mjeriteljstvo ima potpuna čitaj/piši (read/write) ovlaštenja svih sastavnica aplikacije. 
Može kreirati i uređivati sve grupe, šifre, tipove i mjerila kataloga. 
Može dodjeljivati i mijenjati korisnicima ogovarajuće uloge i dodjeljivati i uređivati prema ulogama njihove institucionalne oznake poput 
broja mjeritelja, pripadajučeg voditelja ili datuma do kojeg su ovlašteni...
Može vidjeti i uređivati sve zahtjeve i ovjere. Može vidjeti sve mjesečne obračune svih mjeritelja.
Niti jedna druga uloga osim Administracije DZM i Super-admina ne može dodjeljivati ili mijenjati korisnicima uloge.

### 3.3. Inspekcija DZM

Inspekcija Državnog zavoda za mjeriteljstvo ima potpuna čitaj (read) ovlaštenja svih sastavnica aplikacije.
Inspekcija DZM može vidjeti sav katalog, sve korisnike, sve zahtjeve, sve ovjere i sve obračune. ne može ništa mijenjati niti uređivati u njima. 

### 3.4. Voditelj DZM

Voditelj Državnog zavoda za mjeriteljstvo ima djelomična čitaj/piši (read/write) ovlaštenja.
Voditelj DZM može vidjeti samo grupe koje su mu dodijeljene od strane Administracije DZM.
Voditelj DZM može vidjeti sve tipove i šifre kataloga.
Voditelj DZM može kreirati nova mjerila, vidjeti i uređivati mjerila svojih mjeritelja, 
kao i mjerila mjeritelja svojih pod-ovlaštenih tijela (pod-voditelja) koji su podređeni navedenom voditelju DZM.
Pod korisnicima Voditelj DZM može vidjeti i uređivati sebe, može vidjeti sve svoje mjeritelje i 
mjeritelje svojih pod-ovlaštenih tijela.
Voditelj DZM može kreirati nove zahtjeve i ovjere i dodjeljivati im ovlaštenog mjeritelja, 
može uređivati zahtjeve i ovjere svojih mjeritelja i mjeritelja svojih pod-ovlaštenih tijela.
Voditelj DZM može vidjeti obračune svojih mjeritelja i mjeritelja svojih pod-ovlaštenih tijela.

### 3.5. Voditelj

Voditelj ovlaštenog tijela (u daljnjem takstu OT) ima djelomična čitaj/piši (read/write) ovlaštenja.
OT može vidjeti samo grupe koje su mu dodijeljene od strane Administracije DZM.
Posljedično tome vidi samo određene tipove i šifre koje pripadaju tim grupama.
OT može kreirati nova mjerila, vidjeti mjerila svojih mjeritelja.
Pod korisnicima OT može vidjeti i uređivati sebe, može vidjeti sve svoje mjeritelje i sve korisnike sa javnim profilom.
OT može vidjeti i do statusa predaje uređivati zahtjeve i ovjere svojih mjeritelja.

### 3.6. Mjeritelj DZM

Mjeritelj Državnog zavoda za mjeriteljstvo ima djelomična čitaj/piši (read/write) ovlaštenja.
Mjeritelj DZM može vidjeti samo grupe koje su mu dodijeljene od strane Administracije DZM.
Mjeritelj DZM može vidjeti sve tipove i šifre kataloga.
Mjeritelj DZM može kreirati nova mjerila i vidjeti svoja mjerila.
Pod korisnicima Mjeritelj DZM može vidjeti i uređivati sebe, vidjeti svoje ovlašteno tijelo i voditelja DZM.
Mjeritelj DZM može kreirati nove zahtjeve i ovjere, može vidjeti i do statusa predaje uređivati svoje zahtjeve i ovjere.
Mjeritelj DZM može vidjeti svoj obračun.

### 3.7. Mjeritelj

Mjeritelj ima djelomična čitaj/piši (read/write) ovlaštenja.
Mjeritelj može vidjeti samo grupe koje su mu dodijeljene od strane Administracije DZM.
Posljedično tome vidi samo određene tipove i šifre koje pripadaju tim grupama.
Mjeritelj može kreirati nova mjerila i vidjeti sva mjerila.
Pod korisnicima Mjeritelj DZM može vidjeti i uređivati sebe, vidjeti svoje ovlašteno tijelo i vidjeti voditelja DZM.
Mjeritelj DZM može kreirati nove zahtjeve i ovjere, može vidjeti i do statusa predaje uređivati svoje zahtjeve i ovjere.
Mjeritelj DZM može vidjeti svoj obračun.

### 3.8. Servis

Servis ima ista prava kao i mjeritelj. 
Jedina razlika je u tome što kod kreiranja zahtjeva može proizvoljno odabrati Voditelja DZM ili ovlašteno tijelo kao nadređenu instituciju predanog zahtjeva.

### 3.9. Javni profil

Javni profil je početni profil svakog registriranog korisnika. Jedina prava koja javni profil ima su pregled, uređivanje i brisanje svog profila.
Jedinstveno pravo koje ima samo javni profil je brisanje svog korisničkog računa, svojih sesija, kolačića i povjesti čime se ostvaruje kompatibilnost sa GDPR-om.

Ako je korisnik bio mjeritelj, voditelj ili imao bilo koju drugu ulogu, nakon naloga za brisanje korisničkih podataka 
jedini podatak koji ostaje u sustavu je eventualno broj ovlaštenja istog dodjeljenog od administratora DZM. Svi osobni podaci biti će izbrisani.

Kako odlučite, javni profili se mogu brzo onemogučiti.

## 4. Funkcionalne značajke aplikacije

---

Kada pričamo o pojedinim djelovima DZM aplikacije uvijek razmišljamo kroz uloge kroz koje gledamo. 
Ako pričamo o pregledu ili uređivanju npr. grupa kataloga, zapitamo se može li naša uloga vidjeti sve grupe ili može li uopće uređivati ili kreirati grupe.
Npr. javni profil na navigaciji ima samo poveznicu za profil. Ne vidi katalog, ne vidi ni korisnike, ni ovjere ni zahtjeve ni obračune.
Mjeritelj od korisnika vidi sebe i voditelja institucije, od institucije samo svoju.
Sve liste i pregledi bilo kataloga, zahtjeva, ovjera, obračuna ili korisnika gledaju se kroz uloge i ovlaštenja registriranog korisnika.
kada u listama nema rezultata prikazuje se poruka o nepostojanju rezultata, bilo pretrage ili cijele liste. 
Svi iznosi koji se upisuju u DZM aplikaciju, ako su decimalni decimala se može upisivati točkom ili zarezom, a tisuće se ne označavaju.
Obvezna polja na svim formama DZM aplikacije su označena crvenom zvjezdicom prije naslova polja. 
Ako se ne upiše obvezno polje pojavljuje se greška za obvezan upis polja koje nije ispunjeno. 
Greška se označava crvenim obrubom oko polja i kratkom porukom ispod polja.

### 4.1. Navigacija (sidebar)

Glavna navigacija aplikacije nalazi se sa lijeve strane ekrana i sadrži kategorijski prikaz poveznica na glavne stranice aplikacije. 
Katalog, korisnici i report ne ponašaju se kao poveznice nego kao grupacije drugih poveznica, pa kao takve neće biti posebno objašnjene.

Sastavnice navigacije:

- Nadzorna ploča
- *Katalog*
    - Grupe
    - Mjerila
    - Šifre
    - Tipovi
- *Korisnici*
    - Institucije
    - Korisnici
- Zahtjevi
- Ovjernice
- *Report*
    - Obračuni
    - Izvještaji
- Profil

Kada detaljno u daljnjem tekstu govorimo o pojedinoj sastavnici navigacije misli se na pregled što vidimo na glavnom ekranu DZM aplikacije desno od navigacije.
Sama navigacija (sidebar) i zaglavlje (topbar) se uvijek vide isto, na svakoj pojedinoj stranici administracije DZM aplikacije na kojoj se nalazimo.

### 4.2. Nadzorna ploča

Nadzorna ploča se sastoji od prikaza zahtjeva sa statusom predano.

### 4.3. Grupe kataloga

Klikom na grupe dolazimo na listu grupa i podgrupa kataloga koji su u prikazani nazivom, oznakom je li grupa ili podgrupa, 
oznakom statusa je li aktivna ili neaktivna, redosljedom u listi i gumbom uredi. Gore desno se nalazi gumb za kreiranje novih grupa.

Klikom na gumb za kreiranje ili uređivanje grupe dolazimo na formu za kreiranje ili uređivanje grupe. 
Forma se sastoji od polja za upis naziva grupe, polja za upis opisa, odabira i prekidača je li grupa ili podgrupa, tj. ima svoju krovnu grupu ili je ona krovna grupa,
prekidačem za status grupe, je li aktivna ili neaktivna i poljem za upis redosljeda u listama u kojima se grupe i podgrupe prikazuju.

Klikom na gumb snimi grupa sa upisanim podacima snima se u bazu.

### 4.4. Mjerila kataloga

Klikom na mjerila dolazimo na listu mjerila kataloga koji su u prikazani serijskim brojem, proizvođačem, grupom kojoj pripada, tipnom oznakom i gumbom uredi.
Gore desno na listi se nalazi polje za pretragu mjerila koje se automatski uključuje kako pišemo u njemo i filtrira rezultate u listi.
Pretraga mjerila se vrši prema serijskom broja mjerila, tipu mjerila, šifri ili grupi kojoj mjerilo pripada.
Također gore desno se nalazi gumb za kreiranje novih mjerila. Broj pored naslova liste označava ukupan broj mjerila u katalogu.
Klikom na gumb za kreiranje ili uređivanje mjerila dolazimo na formu za kreiranje ili uređivanje mjerila.

Forma se sastoji od polja za upis serijskog broja, polja za odabir kojoj šifri pripada mjerilo i polja za odabir kojoj tipnoj oznaci pripada.

Klikom na gumb snimi mjerilo sa upisanim podacima snima se u bazu.

### 4.5. Šifre kataloga

Klikom na šifre kataloga dolazimo na listu šifri koji su u prikazani nazivom šifre, oznakom šifre, tarifom, ovjernim razdobljem i gumbom uredi.
Gore desno na listi se nalazi polje za pretragu šifri koje se automatski uključuje kako pišemo u njemo i filtrira rezultate u listi.
Pretraga šifre se vrši prema nazivu ili oznaci šifre.
Također gore desno se nalazi gumb za kreiranje nove šifre. Broj pored naslova liste označava ukupan broj šifri u katalogu.
Klikom na gumb za kreiranje ili uređivanje šifri dolazimo na formu za kreiranje ili uređivanje šifri.

Forma se sastoji od polja za upis naziva šifre, polja za upis šifre, polja za upis ovjernog razdoblja u godinama, 
polja za upis iznosa tarife ovjere, polja za odabir grupe kojoj šifra pripada i prekidača za status šifre je li aktivna ili neaktivna.

Klikom na gumb snimi šifra sa upisanim podacima snima se u bazu.

### 4.6. Tipovi kataloga

Klikom na tipove kataloga dolazimo na listu tipova koji su u prikazani tipom mjerila, svojom tipnom oznakom, 
proizvođačem, gumbom za pregled PDF tipnog odobrenja ako ga ima i gumbom uredi.
Gore desno na listi se nalazi polje za pretragu tipova koje se automatski uključuje kako pišemo u njemo i filtrira rezultate u listi.
Pretraga tipova se vrši prema oznaci, tipu mjerila ili proizvođaču tipa.
Također gore desno se nalazi gumb za kreiranje novog tipa. Broj pored naslova liste označava ukupan broj tipova u katalogu.
Klikom na gumb za kreiranje ili uređivanje tipova dolazimo na formu za kreiranje ili uređivanje tipova.

Forma se sastoji od polja za upis tipa mjerila, polja za upis oznake tipa, polja za upis ili odabir proizvođača, polja za odabir grupe kojoj tip pripada i
gumba za učitavanje PDF dokumenta. Lista u odabiru proizvođača je lista unikatnih proizvođača u listi svih tipova. 

Ako želite dodati novog proizvođača učinite to na način da upišete novog proizvođača u polje za pretraživanje proizvođača i kliknete enter da odabir potvrdi novi upis.
Bez izričitog klika na enter nakon novog upisa može se desiti da se novi proizvođač ne kreira u bazi i da se pojavi greška u obliku potrebe za ispunjenjem obveznog polja.

Dokumenti koji se učitavaju mogu biti samo PDF dokumenti i ne smiju biti veči od 10 MB. 
Ako već postoji dokument tipnog odobrenja u formi, ispod gumba za učitavanje vidi se poveznica na postojeći.

Klikom na gumb snimi tip sa upisanim podacima snima se u bazu.

### 4.7. Institucije

Klikom na institucje dolazima na listu upisanih institucija s dodatnim prikazom osnovnih informacija pojedine institucije.
Lista je prikazana nazivom, gradom, brojem ovlaštenja institucije, brojem njezinih članova ili pripadnika i oznakom statusa.
Uz listu klikom na gumb filter otvara nam se dodatni filter za napredno pretraživanje institucija.
Dodatni prikaz još je upotpunjen adresom, emailom, telefonom, datumom ovlaštenja, gumbom za detaljni prikaz i uređivanje podataka institucije,
i osnovnom listom članova pojedine institucije.
Klikom na pojedinu instituciju na listi prikazuju se njegovi podaci na osnovnom prikazu uz dodatak spomenutih linkova.
Gore desno se nalazi gumb za kreiranje nove institucije. Prikaz gumba za kreiranje ili uređivanje zavisi od uloge korisnika DZM aplikacije.

Klikom na gumb za kreiranje ili uređivanje institucije prelazimo na stranicu forme za kreiranje ili uređivanje institucije.
Forma se sastoji od polja za upis naziva institucije, polja za upis ovlaštenog broja institucije,
polja za upis adrese, poštanskog broja, grada, županije, emaila, telefona, odabira datuma ovlaštenja od i do, te statusa institucije.
Klikom na gumb snimi institucija sa upisanim podacima snima se u bazu.

Klikom na gumb za detaljan pregled odabrane institucije prelazimo na stranicu detaljnog pregleda podataka iste.
Na detaljnom pregledu se nalazi lista osnovnih informacija institucije kao i lista korisnika koji pripadaju instituciji.

### 4.8. Korisnici

Klikom na Korisnike dolazima na listu prijavljenih korisnika s dodatnim prikazom osnovnih informacija pojedinog korisnika.
Lista je prikazana imenom korisnika, emailom, njegovom ulogom unutar DZM aplikacije, institucijom kojoj pripada i statusom korisnika.
Uz listu klikom na gumb filter otvara nam se dodatni filter za napredno pretraživanje korisnika.
Dodatni prikaz još je upotpunjen telefonom, gradom i gumbom za detaljan pregled korisnika.

Klikom na gumb za detaljan pregled odabranog korisnika prelazimo na stranicu detaljnog pregleda podataka istog.
Na detaljnom pregledu se nalazi lista osnovnih informacija korisnika. 
Uz listu imamo odabir uloge korisnika kao i formu za odabir kojoj instituciji pripada, unos njegovog broja ovlaštenja, 
ima li položen ispit i upis datum do kojeg je ovlašten. Ako datum ostavimo praznim datum ovlaštenja je neograničen.

Ispod liste i unosa institucije, a zavisno od uloge korisnika nalazi se lista grupa za odabir. Tj, lista grupa koje odabrani korisnik smije koristiti.
Ako je riječ o Mjeritelju, Voditelju ili Serviseru.

*Svaki korisnik mora pripadati nekoj instituciji u kojoj će imati određenu ulogu.*

### 4.9. Zahtjevi

Klikom na Zahtjeve dolazima na listu upisanih zahtjeva s dodatnim prikazom osnovnih informacija pojedinog zahtjeva.
Lista je prikazana klasom ili oznakom zahtjeva, mjeriteljem koji je napravio zahtjev, brojem ovjera, datumom pregleda i statusom zahtjeva.
Uz listu klikom na gumb filter otvara nam se dodatni filter za napredno pretraživanje zahtjeva.
Dodatni prikaz još je upotpunjen institucijom, datumom upisa zahtjeva i napomenom.

Ispod detaljnog prikaza pojedinog zahtjeva nalaze se gumbi. Prikaz koji gumbi su prisutni zavisi od statusa zahtjeva ili uloge korisnika. 
Imamo gumb za detaljni prikaz zahtjeva, za uređivanje zahtjeva, za dodavanje ovjere ili ovjeru zahtjeva. 
Pri vrhu detaljnog prikaza ako je zahtjev u statusu "U Izradi" nalazi se gumb predaja zahtjeva kojim se, 
kako i naziv kaže, zahtjev predaje, tj. mijenja mu se status u "Predan".
Gore desno se nalazi gumb za kreiranje novog zahtjeva.

*Zahtjev da bi mogao biti predan mora imati barem jednu ovjeru mjerila. 
Nakon predaje zahtjeva mjeritelj više ne može uređivati zahtjev ili dodavati nove ovjere, može jedino ovjeriti predani zahtjev.
Zahtjev da bi se ovjerio mora biti predan. Nakon zadnje ovjere unutar zahtjeva, ako ih ima više, status zahtjeva se mijenja u "Završen".*

Klikom na gumb za kreiranje ili uređivanje zahtjeva prelazimo na stranicu forme za kreiranje ili uređivanje zahtjeva.
Forma se sastoji od polja za upis klase ili oznake zahtjeva, polja za upis mjeritelja (ako korisnik nije mjeritelj), predloženi datum pregleda, napomene ako je potrebna i statusa zahtjeva.
Klikom na gumb snimi zahtjev sa upisanim podacima snima se u bazu. 
Klikom na gumb "snimi i idi na ovjere zahtjeva" zahtjev sa upisanim podacima snima se u bazu i prelazimo na stranicu ovjera po zahtjevu gdje možemo dodavati ovjere (točka 4.10). 
Na istu stranicu idemo i ako kliknemo na gumb Dodaj ovjeru na dodatnom prikazu zahtjeva.

Klikom na gumb za detaljan pregled odabranog zahtjeva prelazimo na stranicu detaljnog pregleda podataka istog.
Na detaljnom pregledu se nalazi lista osnovnih informacija zahtjeva. 
Uz info se nalazi lista ovjera po tom zahtjevu uz dodatne gumbe za dodavanje ovjera ili ovjeravanje istih, zavisno od statusa zahtjeva.

### 4.12. Profil

Klikom na Profil dolazimo na stranicu za editiranje profila. Stranica se sastoji od više pod-stranica.
Prva je stranica sa korisničkim računom gdje može korisnik promijeniti svoje podatke, poput imena, adrese, telefona...
Druga je stranica Lozinke gdje korisnik može promijeniti svoju lozinku. 
Treća stranica je privatnost i sigurnost korisnika gdje korisnik može omogučiti dvofaktorsku autentifikaciju i 
gdje može izbrisati sesije ako se spajao na DZM aplikaciju preko drugih uređaja.
Kada je omogućena dvofaktorska autentifikacija, tijekom autentifikacije od vas će se zatražiti siguran, slučajni token. 
Ovaj token možete preuzeti iz aplikacije Google Authenticator na svom pametnom telefonu.
Četvrta stranica je postavka teme DZM aplikacije za pojedinog korisnika, odabir izgleda aplikacije (svijetla ili tamna), položaj i vidljivost navigacije.
Peta stranica je stranica brisanja računa.











