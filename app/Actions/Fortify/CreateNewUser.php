<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;
use Bouncer;

class CreateNewUser implements CreatesNewUsers
{

    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param array $input
     *
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'terms'    => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['required', 'accepted'] : '',
        ])->validate();

        $public_user = User::create([
            'name'     => $input['name'],
            'email'    => $input['email'],
            'role'     => 'public',
            'status'   => 1,
            'password' => Hash::make($input['password']),
        ]);

        Bouncer::assign('public')->to($public_user);

        UserProfile::create([
            'user_id'                  => $public_user->id,
            'fname'                    => '',
            'lname'                    => '',
            'address'                  => '',
            'zip'                      => '',
            'city'                     => '',
            'region'                   => '',
            'state'                    => '',
            'bio'                      => '',
            'phone'                    => '',
            'admin_color_scheme'       => 'default',
            'admin_sidebar_position'   => 'left',
            'admin_sidebar_behavior'   => 'sticky',
            'admin_layout'             => 'fluid',
            'admin_sidebar_visibility' => 'default',
            'ovjere_view'              => 'list'
        ]);

        return $public_user;
    }
}
