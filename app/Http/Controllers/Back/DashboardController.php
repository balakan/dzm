<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Back\Catalog\Mjerilo;
use App\Models\Back\Chart;
use App\Models\Back\Ovjera;
use App\Models\Back\Zahtjev;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    //
    public function index(Request $request)
    {
        $zahtjevi = Zahtjev::listSearch('', $request->toArray())->where('status', 2)->paginate(20);
        /*$ovjera   = new Ovjera();
        $ovjere   = $ovjera->filterByRoles((new Ovjera())->newQuery())->paginate(5);
        $mjerila  = (new Mjerilo())->listSearch()->paginate(5);*/

        // Chart and total data
        /*$chart        = new Chart();
        $data         = auth()->user()->totalByMonth($chart->setQueryParams());
        $months_array = $chart->setDataByMonth($data);
        $months       = json_encode($chart->setDataByMonth($data));
        $total        = $chart->total($months_array);*/

        return view('dashboard', compact('zahtjevi', /*'ovjere', 'mjerila', 'months', 'months_array', 'total'*/));
    }
}
