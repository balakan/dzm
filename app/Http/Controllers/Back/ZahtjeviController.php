<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Back\Zahtjev;
use App\Models\Helper;
use App\Models\Roles\Ability;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

/**
 * Class ZahtjeviController
 * @package App\Http\Controllers\Back
 */
class ZahtjeviController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        Ability::authorize(['view-all-zahtjev', 'view-group-zahtjev', 'view-own-zahtjev']);

        return view('layouts.back.zahtjevi');
    }

    //


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        Ability::authorize('create-zahtjev');

        $statuses = Helper::getAvailableZahtjevStatuses();

        return view('layouts.back.zahtjevi-edit', compact('statuses'));
    }

    //


    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(Request $request)
    {
        Ability::authorize('create-zahtjev');

        $zahtjev = new Zahtjev();

        $zahtjev->validateRequest($request);

        $stored = $zahtjev->create();

        if ($stored) {
            if ($request->submitBtn == '1') {
                return redirect()->route('ovjere.create', ['zahtjev' => $stored])->with(['success' => 'Zahtjev je uspješno snimljen.']);
            }

            return redirect()->route('zahtjevi.edit', ['zahtjev' => (new Zahtjev())->find($stored)])->with(['success' => 'Zahtjev je uspješno snimljen.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju Zahtjeva.']);
    }

    //


    /**
     * @param Request $request
     * @param Zahtjev $zahtjev
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Request $request, Zahtjev $zahtjev)
    {
        Ability::authorize(['view-all-zahtjev', 'view-group-zahtjev', 'view-own-zahtjev']);

        $zahtjev->checkPermission();

        return view('layouts.back.zahtjevi-show', compact('zahtjev'));
    }


    /**
     * @param Request $request
     * @param Zahtjev $zahtjev
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function ovjeri(Request $request, Zahtjev $zahtjev)
    {
        Ability::authorize(['view-all-zahtjev', 'view-group-zahtjev', 'view-own-zahtjev']);

        $zahtjev->checkPermission();

        return view('layouts.back.zahtjevi-ovjeri', compact('zahtjev'));
    }

    //


    /**
     * @param Zahtjev $zahtjev
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Zahtjev $zahtjev)
    {
        Ability::authorize(['edit-all-zahtjev', 'edit-group-zahtjev', 'edit-own-zahtjev']);

        $zahtjev->checkPermission('edit');

        $statuses = Helper::getAvailableZahtjevStatuses($zahtjev);

        return view('layouts.back.zahtjevi-edit', compact('zahtjev', 'statuses'));
    }

    //


    /**
     * @param Request $request
     * @param Zahtjev $zahtjev
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(Request $request, Zahtjev $zahtjev)
    {
        Ability::authorize(['edit-all-zahtjev', 'edit-group-zahtjev', 'edit-own-zahtjev']);

        $zahtjev->checkPermission('edit')->validateRequest($request);

        $updated = $zahtjev->edit();

        if ($updated) {
            if ($updated === 'status_error') {
                return redirect()->back()->with(['error' => 'Whoops..! Nemate ovlasti mijenjati status zahtjeva.']);
            }

            if ($request->submitBtn == '1') {
                return redirect()->route('ovjere.create', ['zahtjev' => $zahtjev->id])->with(['success' => 'Zahtjev je uspješno snimljen.']);
            }

            return redirect()->back()->with(['success' => 'Zahtjev je uspješno snimljen.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju Zahtjeva.']);
    }


    /**
     * @param Zahtjev $zahtjev
     *
     * @return \Illuminate\Http\Response
     */
    public function printLista(Zahtjev $zahtjev)
    {
        $count = 1;
        $data = [];
        $ovjere = [];

        $data['zahtjev'] = [
            'id' => $zahtjev->id,
            'br' => 'HR-' . $zahtjev->institucija->broj . '-' . $zahtjev->id,
            'klasa' => $zahtjev->klasa,
            'institucija' => '(' . $zahtjev->institucija->broj . '), ' . $zahtjev->institucija->naziv,
            'mjeritelj' => '(' . $zahtjev->mjeritelj->data->broj . '), ' . $zahtjev->mjeritelj->name,
            'pregled' => Carbon::make($zahtjev->datum_pregleda)->format('d.m.Y.'),
            'status' => $zahtjev->getStatus()
        ];

        foreach ($zahtjev->ovjere()->get() as $ovjera) {
            $ovjere[] = [
                'rb' => $count,
                'sifra' => $ovjera->mjerilo->sifra->sifra,
                'vrsta' => $ovjera->vrsta,
                'sb' => $ovjera->mjerilo->sn,
                'tip' => $ovjera->mjerilo->tip->naziv,
                'oznaka' => $ovjera->mjerilo->tip->oznaka,
                'mjesto' => $ovjera->mjesto_ovjere->city . ', ' . $ovjera->mjesto_ovjere->address,
                'nalj' => $ovjera->naljepnica ?: $ovjera->zig,
                'status' => $ovjera->getStatus()
            ];

            $count++;
        }

        $data['ovjere'] = $ovjere;

        $pdf = Pdf::loadView('layouts.print.zahtjev-lista', ['data' => $data]);

        return $pdf->stream('lista.pdf');
    }
}
