<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Back\Ovjera;
use App\Models\Back\Zahtjev;
use App\Models\Roles\Ability;
use Illuminate\Http\Request;

/**
 * Class OvjereController
 * @package App\Http\Controllers\Back
 */
class OvjereController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        Ability::authorize(['view-ovjera', 'view-group-ovjera', 'view-own-ovjera']);

        $ovjere = Ovjera::orderBy('created_at', 'desc')->paginate(config('view.admin.pagination'));

        $mjeritelj = $request->has('mjeritelj') ? $request->input('mjeritelj') : false;
        $zahtjev   = $request->has('zahtjev') ? $request->input('zahtjev') : false;

        return view('layouts.back.ovjere', compact('ovjere', 'mjeritelj', 'zahtjev'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Zahtjev $zahtjev)
    {
        Ability::authorize('create-ovjera');

        return view('layouts.back.ovjere-edit', compact('zahtjev'));
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(Request $request)
    {
        Ability::authorize('create-ovjera');

        $ovjera = new Ovjera();

        $stored = $ovjera->validateRequest($request)->create();

        if ($stored) {
            $ovjera = Ovjera::find($stored);

            if ($request->submitBtn == '1') {
                return redirect()->route('ovjere.create', ['zahtjev' => $ovjera->zahtjev()->first()])->with(['success' => 'Ovjera je uspješno snimljena.']);
            }

            if (auth()->user()->can(['edit-ovjera', 'edit-group-ovjera', 'edit-own-ovjera'])) {
                return redirect()->route('ovjere.edit', ['ovjera' => $ovjera, 'zahtjev' => $ovjera->zahtjev()->first()])->with(['success' => 'Ovjera je uspješno snimljena.']);
            }

            return redirect()->route('ovjere.create', ['zahtjev' => $ovjera->zahtjev()->first()])->with(['success' => 'Ovjera je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju Ovjere.']);
    }


    /**
     * @param Request $request
     * @param         $ovjera
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Request $request, Ovjera $ovjera)
    {
        Ability::authorize(['view-ovjera', 'view-group-ovjera', 'view-own-ovjera']);

        $history = Ovjera::where('mjerilo_id', $ovjera->mjerilo_id)
                         ->where('id', '!=', $ovjera->id)
                         ->paginate(config('view.admin.pagination'));

        return view('layouts.back.ovjere-show', compact('ovjera', 'history'));
    }


    /**
     * @param Ovjera $ovjera
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Ovjera $ovjera, Zahtjev $zahtjev)
    {
        Ability::authorize(['edit-ovjera', 'edit-group-ovjera', 'edit-own-ovjera']);

        $ovjera->checkPermission('edit');

        return view('layouts.back.ovjere-edit', compact('ovjera', 'zahtjev'));
    }


    /**
     * @param Request $request
     * @param Ovjera  $ovjera
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(Request $request, Ovjera $ovjera)
    {
        Ability::authorize(['edit-ovjera', 'edit-group-ovjera', 'edit-own-ovjera']);

        $updated = $ovjera->validateRequest($request)->edit();

        if ($updated) {
            if ($request->submitBtn == '1') {
                return redirect()->route('ovjere.create', ['zahtjev' => $ovjera->zahtjev()->first()])->with(['success' => 'Ovjera je uspješno snimljena.']);
            }

            return redirect()->route('ovjere.edit', ['ovjera' => $ovjera, 'zahtjev' => $ovjera->zahtjev()->first()])->with(['success' => 'Ovjera je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju Ovjere.']);
    }
}
