<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Roles\Ability;
use Illuminate\Http\Request;

class UlogeController extends Controller
{
    //
    public function index()
    {
        Ability::authorize('view-roles');

        return view('layouts.back.user.uloge');
    }
}
