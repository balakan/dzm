<?php

namespace App\Http\Controllers\Back\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Back\Catalog\Grupa;
use App\Models\Roles\Ability;
use Illuminate\Http\Request;

class GrupeController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        Ability::authorize(['view-grupe', 'view-group-grupe', 'view-own-grupe']);

        $groups = (new Grupa())->adminList();

        return view('layouts.back.catalog.grupe', compact('groups'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        Ability::authorize('create-grupe');

        $parents = (new Grupa())->parents();
        
        return view('layouts.back.catalog.grupe-edit', compact('parents'));
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Ability::authorize('create-grupe');

        $request->validate(['name' => 'required']);

        $stored = (new Grupa())->create($request);

        if ($stored) {
            return redirect()->route('grupe.edit', ['grupa' => (new Grupa())->find($stored)])->with(['success' => 'Grupa je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju grupe.']);
    }


    /**
     * @param Grupa $grupa
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Grupa $grupa)
    {
        Ability::authorize('edit-grupe');

        $parents = $grupa->parents();

        return view('layouts.back.catalog.grupe-edit', compact('grupa', 'parents'));
    }


    /**
     * @param Request $request
     * @param Grupa   $grupa
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Grupa $grupa)
    {
        Ability::authorize('edit-grupe');

        $request->validate(['name' => 'required']);

        $updated = $grupa->edit($request);

        if ($updated) {
            return redirect()->back()->with(['success' => 'Grupa je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju grupe.']);
    }

}
