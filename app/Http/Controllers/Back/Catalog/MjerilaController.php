<?php

namespace App\Http\Controllers\Back\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Back\Catalog\Mjerilo;
use App\Models\Back\Catalog\Sifra;
use App\Models\Back\Catalog\Tip;
use App\Models\Roles\Ability;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MjerilaController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        Ability::authorize(['view-mjerila', 'view-group-mjerila', 'view-own-mjerila']);

        $search = '';

        if ($request->has('search') && $request->input('search') != '') {
            $search = $request->input('search');
        }

        return view('layouts.back.catalog.mjerila', compact('search'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {

        Ability::authorize('create-mjerila');

        $sifre = (new Sifra())->listSearch()->get();
        $tipovi = (new Tip())->listSearch('', false)->get();

        return view('layouts.back.catalog.mjerila-edit', compact('sifre', 'tipovi'));
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Ability::authorize('create-mjerila');

        $mjerilo = new Mjerilo();

        $mjerilo->validateRequest($request);

        $stored = $mjerilo->create();

        if ($stored) {
            if ($stored == 'exist') {
                return redirect()->back()->with(['error' => 'Greška..! Mjerilo sa istim karakteristikama već postoji.']);
            }

            if (auth()->user()->can(['edit-mjerila', 'edit-group-mjerila', 'edit-own-mjerila'])) {
                return redirect()->route('mjerila.edit', ['mjerilo' => (new Mjerilo())->find($stored)])->with(['success' => 'Mjerilo je uspješno snimljeno.']);
            }

            $search = '';

            return view('layouts.back.catalog.mjerila', compact('search'))->with(['success' => 'Mjerilo je uspješno snimljeno.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju mjerila.']);
    }


    /**
     * @param Mjerilo $mjerilo
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Mjerilo $mjerilo)
    {
        $ovjere = $mjerilo->history();

        return view('layouts.back.catalog.mjerila-show', compact('mjerilo', 'ovjere'));
    }


    /**
     * @param Tip $tip
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Mjerilo $mjerilo)
    {
        Ability::authorize(['edit-mjerila', 'edit-group-mjerila', 'edit-own-mjerila']);

        $sifre = (new Sifra())->listSearch()->get();
        $tipovi = (new Tip())->listSearch('', false)->get();

        return view('layouts.back.catalog.mjerila-edit', compact('mjerilo', 'sifre', 'tipovi'));
    }


    /**
     * @param Request $request
     * @param Tip     $tip
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Mjerilo $mjerilo)
    {
        Ability::authorize(['edit-mjerila', 'edit-group-mjerila', 'edit-own-mjerila']);

        $mjerilo->validateRequest($request);

        if ($mjerilo->isCopy()) {
            return redirect()->back()->with(['error' => 'Greška..! Mjerilo sa istim karakteristikama već postoji.']);
        }

        $updated = $mjerilo->edit();

        if ($updated) {
            return redirect()->back()->with(['success' => 'Mjerilo je uspješno snimljeno.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju mjerile.']);
    }
}
