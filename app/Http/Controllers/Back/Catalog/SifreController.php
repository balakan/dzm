<?php

namespace App\Http\Controllers\Back\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Back\Catalog\Grupa;
use App\Models\Back\Catalog\Sifra;
use App\Models\Roles\Ability;
use Illuminate\Http\Request;

class SifreController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        Ability::authorize(['view-sifre', 'view-group-sifre', 'view-own-sifre']);

        return view('layouts.back.catalog.sifre');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        Ability::authorize(['create-sifre']);

        $groups = (new Grupa())->adminList();

        return view('layouts.back.catalog.sifre-edit', compact('groups'));
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Ability::authorize('create-sifre');

        $sifra = new Sifra();

        $sifra->validateRequest($request);

        $stored = $sifra->create($request);

        if ($stored) {
            return redirect()->route('sifre.edit', ['sifra' => (new Sifra())->find($stored)])->with(['success' => 'Šifra je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju šifre.']);
    }


    /**
     * @param Sifra $sifra
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Sifra $sifra)
    {
        Ability::authorize('edit-sifre');

        $groups = (new Grupa())->adminList();

        return view('layouts.back.catalog.sifre-edit', compact('groups', 'sifra'));
    }


    /**
     * @param Request $request
     * @param Sifra   $sifra
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Sifra $sifra)
    {
        Ability::authorize('edit-sifre');

        $sifra->validateRequest($request);

        $updated = $sifra->edit($request);

        if ($updated) {
            return redirect()->back()->with(['success' => 'Šifra je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju šifre.']);
    }
}
