<?php

namespace App\Http\Controllers\Back\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Back\Catalog\Grupa;
use App\Models\Back\Catalog\Tip;
use App\Models\Roles\Ability;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TipoviController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        Ability::authorize(['view-tipovi', 'view-group-tipovi', 'view-own-tipovi']);

        return view('layouts.back.catalog.tipovi');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        Ability::authorize('create-tipovi');

        $groups = (new Grupa())->adminList();
        $brands = Tip::uniqueBrands();

        return view('layouts.back.catalog.tipovi-edit', compact('groups', 'brands'));
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Ability::authorize('create-tipovi');

        $tip = new Tip();

        $tip->validateRequest($request);

        $stored = $tip->create();

        if ($stored) {
            return redirect()->route('tipovi.edit', ['tip' => (new Tip())->find($stored)])->with(['success' => 'Tip je uspješno snimljen.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju tipa.']);
    }


    /**
     * @param Tip $tip
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Tip $tip)
    {
        Ability::authorize('edit-tipovi');

        $groups = (new Grupa())->adminList();
        $brands = Tip::uniqueBrands();

        return view('layouts.back.catalog.tipovi-edit', compact('tip', 'groups', 'brands'));
    }


    /**
     * @param Request $request
     * @param Tip     $tip
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Tip $tip)
    {
        Ability::authorize('edit-tipovi');

        $tip->validateRequest($request);

        $updated = $tip->edit();

        if ($updated) {
            return redirect()->back()->with(['success' => 'Tip je uspješno snimljen.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju tipa.']);
    }
}
