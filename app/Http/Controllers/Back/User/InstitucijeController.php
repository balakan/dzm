<?php

namespace App\Http\Controllers\Back\User;

use App\Http\Controllers\Controller;
use App\Models\Back\User\Institucija;
use App\Models\Back\User\Korisnik;
use App\Models\Roles\Ability;
use Illuminate\Http\Request;

class InstitucijeController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        Ability::authorize(['view-all-institucije', 'view-own-institucije']);

        return view('layouts.back.user.institucija.institucije');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        Ability::authorize('create-institucije');

        return view('layouts.back.user.institucija.institucije-edit');
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Ability::authorize('create-institucije');

        $institucija = (new Institucija())->validateRequest($request);

        $stored_id = $institucija->create();

        if ($stored_id) {
            $edit = Institucija::find($stored_id);

            return redirect()->route('institucije.edit', ['institucija' => $edit])->with(['success' => 'Institucija je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju Institucije.']);
    }


    /**
     * @param Request     $request
     * @param Institucija $institucija
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Request $request, Institucija $institucija)
    {
        Ability::authorize(['view-all-institucije', 'view-own-institucije']);

        $institucija->checkPermission();

        return view('layouts.back.user.institucija.institucije-show', compact('institucija'));
    }


    /**
     * @param Institucija $institucija
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Institucija $institucija)
    {
        Ability::authorize(['edit-all-institucije', 'edit-own-institucije']);

        $institucija->checkPermission();

        return view('layouts.back.user.institucija.institucije-edit', compact('institucija'));
    }


    /**
     * @param Request     $request
     * @param Institucija $institucija
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(Request $request, Institucija $institucija)
    {
        Ability::authorize(['edit-all-institucije', 'edit-own-institucije']);

        $institucija->checkPermission()->validateRequest($request);

        $updated_id = $institucija->edit();

        if ($updated_id) {
            return redirect()->route('institucije.edit', ['institucija' => $institucija])->with(['success' => 'Institucija je uspješno snimljena.']);
        }

        return redirect()->back()->with(['error' => 'Whoops..! Greška u snimanju Institucije.']);

        return view('layouts.back.user.institucija.institucije');
    }
}
