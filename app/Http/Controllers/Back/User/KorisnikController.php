<?php

namespace App\Http\Controllers\Back\User;

use App\Http\Controllers\Controller;
use App\Models\Roles\Ability;
use App\Models\User;
use Illuminate\Http\Request;

class KorisnikController extends Controller
{
    //
    public function index()
    {
        Ability::authorize(['view-all-korisnici', 'view-group-korisnici', 'view-own-korisnici']);

        return view('layouts.back.user.korisnik.korisnici');
    }

    //
    public function show(Request $request, User $korisnik)
    {
        return view('layouts.back.user.korisnik.korisnici-show', compact('korisnik'));
    }

    //
    public function edit(User $korisnik)
    {
        return view('layouts.back.user.korisnik.korisnici-edit', compact('korisnik'));
    }

    //
    public function update(Request $request, User $korisnik)
    {
        return view('layouts.back.user.korisnik.korisnici');
    }
}
