<?php

namespace App\Http\Controllers\Back\Report;

use App\Http\Controllers\Controller;
use App\Models\Back\Chart;
use App\Models\Back\Ovjera;
use App\Models\Back\User\Mjeritelj;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ObracuniController extends Controller
{

    //
    public function index()
    {
        return view('layouts.back.report.obracuni');
    }


    /**
     * @param User $user
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(User $user)
    {
        $chart = new Chart();

        $data = $user->totalByMonth($chart->setQueryParams());

        $months_array = $chart->setDataByMonth($data);
        $months       = json_encode($chart->setDataByMonth($data));

        $total = $chart->total($months_array);

        return view('layouts.back.report.obracun-show', compact('user', 'months', 'months_array', 'total'));
    }
}
