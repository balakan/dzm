<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Front\Ovjera;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('layouts.front.index');
    }


    /**
     * @param Ovjera $ovjera
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detail($sn)
    {
        $ovjera = (new Ovjera())->homeSearch($sn)->first();

        return view('layouts.front.result-detail', compact('ovjera'));
    }
}
