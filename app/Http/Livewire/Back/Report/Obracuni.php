<?php

namespace App\Http\Livewire\Back\Report;

use App\Models\Back\User\Korisnik;
use App\Models\Back\User\Mjeritelj;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Obracuni extends Component
{
    use WithPagination;

    /**
     * @var
     */
    public $search = '';

    /**
     * @var array
     */
    public $selected_user = [];

    /**
     * @var bool
     */
    public $show_filter = false;

    /**
     * @var array
     */
    public $state = [
        'ime'         => true,
        'email'       => true,
        'uloga'       => ['mjeritelj-dzm', 'mjeritelj', 'mjeritelj-servis'],
        'institucija' => false,
        'status'      => 'all',
    ];


    /**
     *
     */
    public function updatingSearch()
    {
        $this->render();
    }


    /**
     * @param bool $value
     */
    public function showFilterClicked(bool $value = false)
    {
        $this->show_filter = ! $value;
    }


    /**
     * @param $id
     */
    public function userSelected($id)
    {
        $this->selected_user = User::where('id', $id)->first();

        $this->fill([
            'selected_user' => $this->selected_user,
        ]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $mjeritelji = (new User())->listSearch($this->search, $this->state)
                               ->paginate(config('view.admin.pagination'));

        return view('livewire.back.report.obracuni', [
            'mjeritelji' => $mjeritelji
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }

}
