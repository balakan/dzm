<?php

namespace App\Http\Livewire\Back;

use App\Models\Back\Zahtjev;
use App\Models\User;
use Illuminate\Support\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class Zahtjevi extends Component
{

    use WithPagination;

    /**
     * @var
     */
    public $search = '';

    /**
     * @var array
     */
    public $selected_zahtjev = [];

    /**
     * @var bool
     */
    public $show_filter = false;

    /**
     * @var array
     */
    public $state = [
        'date_from'   => null,
        'date_to'     => null,
        'klasa'       => true,
        'oznaka'      => false,
        'mjeritelj'   => true,
        'institucija' => false,
        'status'      => 0,
    ];


    /**
     *
     */
    public function updatingSearch()
    {
        $this->render();
    }


    /**
     * @param string $value
     */
    public function fromSelected(string $value)
    {
        $this->state['date_from'] = $value;

        $this->render();
    }


    /**
     * @param string $value
     */
    public function toSelected(string $value)
    {
        $this->state['date_to'] = $value;

        $this->render();
    }


    /**
     * @param bool $value
     */
    public function showFilterClicked(bool $value = false)
    {
        $this->show_filter = ! $value;
    }


    /**
     * @param $id
     */
    public function zahtjevSelected($id)
    {
        $this->selected_zahtjev = Zahtjev::where('id', $id)->with('mjeritelj')->first();
    }


    /**
     *
     */
    public function predajZahtjev()
    {
        $this->selected_zahtjev->update([
            'status' => 2,
            'datum_predaje' => Carbon::now()
        ]);

        $this->emit('success_alert');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $zahtjevi = Zahtjev::listSearch($this->search, $this->state)
                           ->paginate(config('view.admin.pagination'));

        return view('livewire.back.zahtjevi', [
            'zahtjevi' => $zahtjevi
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }
}
