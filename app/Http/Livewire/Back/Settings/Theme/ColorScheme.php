<?php

namespace App\Http\Livewire\Back\Settings\Theme;

use App\Models\UserProfile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class ColorScheme extends Component
{

    /**
     * @var string
     */
    public $theme;

    /**
     * @var string
     */
    public $sidebar_position;

    /**
     * @var string
     */
    public $sidebar_behavior;

    /**
     * @var string
     */
    public $sidebar_visibility;

    /**
     * @var string
     */
    public $layout;

    /**
     * @var string
     */
    public $message;


    /**
     *
     */
    public function mount()
    {
        $this->setDefaults($this->getProfile());
    }


    /**
     * @return \Livewire\Event
     */
    public function changedColorScheme()
    {
        return $this->resolve('theme', 'theme');
    }


    /**
     *
     */
    public function changeSidebarPosition()
    {
        return $this->resolve('sidebar_position', 'sidebarPosition');
    }


    /**
     *
     */
    public function changeSidebarBehavior()
    {
        return $this->resolve('sidebar_behavior', 'sidebarBehavior');
    }


    /**
     *
     */
    public function changeSidebarVisibility()
    {
        return $this->resolve('sidebar_visibility', 'sidebarVisibility');
    }


    /**
     *
     */
    public function changeLayout()
    {
        return $this->resolve('layout', 'layout');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.settings.theme.color-scheme');
    }


    /*******************************************************************************
    *                                Copyright : AGmedia                           *
    *                              email: filip@agmedia.hr                         *
    *******************************************************************************/

    /**
     * @param string $tag
     * @param string $value
     *
     * @return \Livewire\Event
     */
    private function resolve(string $tag, string $value)
    {
        $profile = $this->getProfile();

        if ( ! isset($profile->id)) {
            $profile             = new UserProfile();
            $profile->user_id    = Auth::id();
            $profile->created_at = Carbon::now();
        }

        $admin_str = 'admin_' . ($tag == 'theme' ? 'color_scheme' : $tag);

        $profile->{$admin_str} = $this->{$tag};

        if ($this->saveProfile($profile)) {
            Cache::forget('setup');

            return $this->emit('success_alert', ['tag' => $value, 'value' => $this->{$tag}]);
        }

        $this->emit('error_alert');
    }


    /**
     * @return mixed
     */
    private function getProfile()
    {
        return UserProfile::where('user_id', Auth::id())->first();
    }


    /**
     * @param UserProfile $profile
     */
    private function setDefaults(UserProfile $profile): void
    {
        if ( ! isset($profile->id)) {
            $this->theme              = 'default';
            $this->sidebar_position   = 'left';
            $this->sidebar_behavior   = 'sticky';
            $this->sidebar_visibility = 'default';
            $this->layout             = 'fluid';
        } else {
            $this->theme              = $profile->admin_color_scheme;
            $this->sidebar_position   = $profile->admin_sidebar_position;
            $this->sidebar_behavior   = $profile->admin_sidebar_behavior;
            $this->sidebar_visibility = $profile->admin_sidebar_visibility;
            $this->layout             = $profile->admin_layout;
        }
    }


    /**
     * @param $profile
     *
     * @return mixed
     */
    private function saveProfile($profile)
    {
        $profile->updated_at = Carbon::now();

        return $profile->save();
    }
}
