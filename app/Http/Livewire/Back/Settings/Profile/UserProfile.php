<?php

namespace App\Http\Livewire\Back\Settings\Profile;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class UserProfile extends Component
{

    /**
     * @var string
     */
    public $fname;

    /**
     * @var string
     */
    public $lname;

    /**
     * @var string
     */
    public $address;

    /**
     * @var string
     */
    public $zip;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $region;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $bio;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $role;

    /**
     * @var bool
     */
    public $status;

    /**
     * @var string
     */
    public $success_message;


    /**
     *
     */
    public function mount()
    {
        $profile = \App\Models\UserProfile::where('user_id', Auth::id())->first();

        if ($profile) {
            $this->fname = $profile->fname;
            $this->lname = $profile->lname;
            $this->address = $profile->address;
            $this->zip = $profile->zip;
            $this->city = $profile->city;
            $this->region = $profile->region;
            $this->state = $profile->state;
            $this->bio = $profile->bio;
            $this->phone = $profile->phone;
        }
    }


    /**
     *
     */
    public function changeUserProfile()
    {
        $profile = \App\Models\UserProfile::where('user_id', Auth::id())->first();

        if ( ! isset($profile->id)) {
            $profile = new \App\Models\UserProfile();
        }

        $profile->user_id = Auth::id();
        $profile->fname = $this->fname;
        $profile->lname = $this->lname;
        $profile->address = $this->address;
        $profile->zip = $this->zip;
        $profile->city = $this->city;
        $profile->region = $this->region;
        $profile->state = $this->state;
        $profile->bio = $this->bio;
        $profile->phone = $this->phone;
        $profile->updated_at = Carbon::now();
        $profile->save();

        $this->success_message = '<strong>Success...</strong> Profile has been updated..!';
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.settings.profile.user-profile');
    }
}
