<?php

namespace App\Http\Livewire\Back;

use App\Models\Back\Ovjera;
use App\Models\Back\User\Mjeritelj;
use App\Models\Back\User\OvlastenoTijelo;
use App\Models\Back\Zahtjev;
use App\Models\Helper;
use App\Models\UserProfile;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Livewire\WithPagination;

/**
 * Class Ovjere
 * @package App\Http\Livewire\Back
 */
class Ovjere extends Component
{

    use WithPagination;

    /**
     * @var UserProfile
     */
    public $profile;

    /**
     * @var string
     */
    public $search;

    /**
     * @var bool
     */
    public $show_filter = false;

    /**
     * @var string
     */
    public $list_view = 'list';

    /**
     * @var bool
     */
    public $oznaka = true;

    /**
     * @var bool
     */
    public $vrsta = true;

    /**
     * @var bool
     */
    public $zahtjev = false;

    /**
     * @var bool
     */
    public $vlasnik = false;

    /**
     * @var bool
     */
    public $mjesto_postavljanja = false;

    /**
     * @var bool
     */
    public $mjesto_ovjere = false;

    /**
     * @var bool
     */
    public $mjerilo = true;

    /**
     * @var bool
     */
    public $mjeritelj = false;

    /**
     * @var bool
     */
    public $ovlasteno_tijelo = false;

    /**
     * @var string
     */
    public $naljepnice = 'sve';

    /**
     * @var string
     */
    public $zig = 'sve';

    /**
     * @var string
     */
    public $ovjernica = 'sve';


    /**
     *
     */
    public function mount()
    {
        $this->profile = UserProfile::where('user_id', Auth::id())->first();

        $this->list_view = $this->profile->ovjere_view;

        if ($this->mjeritelj || $this->zahtjev) {
            if ($this->mjeritelj) {
                $mjeritelj = Mjeritelj::find($this->mjeritelj);
                $this->search = $mjeritelj->naziv;
            }

            if ($this->zahtjev) {
                $this->search = $this->zahtjev;
            }

            $this->oznaka = false;
            $this->vrsta = false;
        }
    }


    /**
     *
     */
    public function updatingSearch()
    {
        $this->render();
    }


    /**
     * @param bool $value
     */
    public function showFilterClicked(bool $value = false)
    {
        $this->show_filter = ! $value;
    }


    /**
     * @param string $view
     */
    public function changeListView(string $view)
    {
        $this->list_view = $view;

        $this->profile->update([
            'ovjere_view' => $view
        ]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $ovjere = (new Ovjera())->newQuery();

        $ovjere = $this->filterSwitches($ovjere);
        $ovjere = $this->filterRadios($ovjere);
        $ovjere = $this->filterRoles($ovjere);

        $ovjere = $ovjere->orderBy('created_at', 'desc')
                         ->paginate($this->resolvePagination());

        return view('livewire.back.ovjere', [
            'ovjere' => $ovjere
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }


    /**
     * @param Builder $query
     *
     * @return Builder
     */
    private function filterRoles(Builder $query): Builder
    {
        if ( ! \Bouncer::can('*')) {
            // Mjeritelj
            if (\Bouncer::can('view-own-ovjera')) {
                $query->whereHas('zahtjev', function ($subquery) {
                    $subquery->where('mjeritelj_id', auth()->user()->id);
                });
            }

            // Ovlašteno tijelo
            if (\Bouncer::can('view-group-ovjera')) {
                $institucija = auth()->user()->institucija->first();
                $users = $institucija->users->whereIn('role', ['mjeritelj', 'mjeritelj-dzm', 'mjeritelj-servis'])->pluck('id');

                $query->whereHas('zahtjev', function ($subquery) use ($users) {
                    $subquery->whereIn('mjeritelj_id', $users);
                });
            }
        }

        return $query;
    }


    /**
     * @param Builder $query
     *
     * @return Builder
     */
    private function filterSwitches(Builder $query): Builder
    {
        if ( ! empty($this->search)) {
            $search = $this->search;

            $query->where(function ($query) use ($search) {
                // Oznaka
                if ($this->oznaka) {
                    $query->orWhere('id', 'like', $search . '%');
                }

                // Zahtjev
                if ($this->zahtjev) {
                    $query->orWhereHas('zahtjev', function ($subquery) use ($search) {
                        $subquery->where('oznaka', 'like', $search . '%');
                    });
                }

                // Vlasnik
                if ($this->vlasnik) {
                    $query->orWhereHas('vlasnik', function ($subquery) use ($search) {
                        $subquery->where('naziv', 'like', $search . '%');
                    });
                }

                // Vlasnik
                if ($this->mjesto_postavljanja) {
                    $query->orWhereHas('mjesto_postavljanja', function ($subquery) use ($search) {
                        $subquery->where('city', 'like', $search . '%');
                    });
                }

                // Vlasnik
                if ($this->mjesto_ovjere) {
                    $query->orWhereHas('mjesto_ovjere', function ($subquery) use ($search) {
                        $subquery->where('city', 'like', $search . '%');
                    });
                }

                // Mjerilo
                if ($this->mjerilo) {
                    $query->orWhereHas('mjerilo', function ($subquery) use ($search) {
                        $subquery->where('sn', 'like', $search . '%');
                    });
                }

                // Mjeritelj
                if ($this->mjeritelj) {
                    $query->orWhereHas('mjeritelj', function ($subquery) use ($search) {
                        $subquery->where('naziv', 'like', $search . '%')
                                 ->orWhere('broj', 'like', $search . '%');
                    });
                }

                // Ovlašteno tijelo
                if ($this->ovlasteno_tijelo) {
                    $query->orWhereHas('ovlasteno_tijelo', function ($subquery) use ($search) {
                        $subquery->where('naziv', 'like', $search . '%')
                                 ->orWhere('broj', 'like', $search . '%');
                    });
                }
            });
        }

        return $query;
    }


    /**
     * @param Builder $query
     *
     * @return Builder
     */
    private function filterRadios(Builder $query): Builder
    {
        // Naljepnice
        if ($this->naljepnice == 'da') {
            $query->where('naljepnica', 1);
        }
        if ($this->naljepnice == 'ne') {
            $query->where('naljepnica', 0);
        }

        // Žigovi
        if ($this->zig == 'da') {
            $query->where('zig', 1);
        }
        if ($this->zig == 'ne') {
            $query->where('zig', 0);
        }

        // Ovjernice
        if ($this->ovjernica == 'da') {
            $query->where('ovjernica', 1);
        }
        if ($this->ovjernica == 'ne') {
            $query->where('ovjernica', 0);
        }

        return $query;
    }


    /**
     * @return int
     */
    private function resolvePagination(): int
    {
        $pagination = config('view.admin.pagination');

        if ($this->list_view == 'list') {
            return $pagination;
        }

        if ($this->list_view == 'table') {
            return Helper::closestNumberDivisibleBy($pagination, 3);
        }
    }

}
