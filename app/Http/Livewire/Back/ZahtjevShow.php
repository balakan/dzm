<?php

namespace App\Http\Livewire\Back;

use Illuminate\Support\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class ZahtjevShow extends Component
{
    use WithPagination;

    /**
     * @var array
     */
    public $zahtjev = [];

    /**
     * @var string
     */
    public $napomena = '';


    /**
     *
     */
    public function predajZahtjev()
    {
        $this->zahtjev->update([
            'status' => 2,
            'datum_predaje' => Carbon::now()
        ]);

        $this->emit('success_alert');
    }


    /**
     *
     */
    public function napomenaZahtjeva()
    {
        $this->zahtjev->update([
            'napomena' => $this->napomena
        ]);

        $this->emit('success_alert');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $this->napomena = $this->zahtjev->napomena;

        return view('livewire.back.zahtjev-show', [
            'ovjere' => $this->zahtjev->ovjere()->paginate(config('view.admin.pagination')),
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }
}
