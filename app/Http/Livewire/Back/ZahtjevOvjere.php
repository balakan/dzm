<?php

namespace App\Http\Livewire\Back;

use App\Models\Back\Ovjera;
use Livewire\Component;
use Livewire\WithPagination;

class ZahtjevOvjere extends Component
{
    use WithPagination;

    /**
     * @var array
     */
    public $zahtjev = [];

    /**
     * @var array
     */
    public $state = [];


    /**
     *
     */
    public function mount()
    {
        //dd($this->zahtjev->ovjere()->get()->toArray());
        foreach ($this->zahtjev->ovjere()->get() as $ovjera) {
            $status = (int)$ovjera->status;

            if ($ovjera->status == '0') {
                $status = 1;
            }

            $this->state[$ovjera->id] = [
                'vrsta' => '',
                'vrsta_input' => '',
                'vrsta_input_label' => 'Odaberite potvrdu',
                'status' => $status,
                'napomena' => ''
            ];

            $declined = in_array($ovjera->status, [2, 3]); // [ Odbijen, Odustao ]

            if ($ovjera->mjerilo->sifra->naljepnica) {
                $this->state[$ovjera->id]['vrsta_input'] = $declined ? '' : $ovjera->naljepnica;
            } else {
                $this->state[$ovjera->id]['vrsta_input'] = $declined ? '' : $ovjera->zig;
            }

            if ($ovjera->mjerilo->sifra->naljepnica) {
                $this->vrstaSelected('N', $ovjera->id);
            } else {
                $this->vrstaSelected('Z', $ovjera->id);
            }
        }
    }


    /**
     * @param string $type
     * @param int    $id
     */
    public function vrstaSelected(string $type, int $id)
    {
        if ($type == 'N') {
            $this->state[$id]['vrsta_input_label'] = 'Broj Naljepnice';
        }
        if ($type == 'Z') {
            $this->state[$id]['vrsta_input_label'] = 'Broj Žiga';
        }

        $this->state[$id]['vrsta'] = $type;
    }


    /**
     * @param string $type
     * @param int    $id
     */
    public function statusSelected(string $type, int $id)
    {
        //dd($id, $type);
        $this->state[$id]['status'] = (int)$type;
    }


    /**
     * @param int $id
     *
     * @return \Livewire\Event
     */
    public function ovjeri(int $id)
    {
        //dd($id, $this->state);
        if ( ! $this->checkIfRequired($id)) {
            return $this->emit('warning_alert');
        }

        $this->resolveOvjera($id);

        $this->emit('success_alert');

        return redirect(request()->header('Referer'));
    }


    /**
     * @return \Livewire\Event
     */
    public function ovjeriSve()
    {
        //dd($this->state);
        if ( ! $this->checkIfRequired()) {
            return $this->emit('warning_alert');
        }

        foreach ($this->state as $key => $item) {
            $this->resolveOvjera($key);
        }

        $this->emit('success_alert');

        $this->render();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.zahtjev-ovjere', [
            'ovjere' => $this->zahtjev->ovjere()->where('datum_ovjere', null)->get(),
            'ovjere_ovjerene' => $this->zahtjev->ovjere()->where('datum_ovjere', '!=', null)->get(),
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }

    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/

    /**
     * @param int|null $id
     *
     * @return bool
     */
    private function checkIfRequired(int $id = null): bool
    {
        if ( ! $id) {
            foreach ($this->state as $item) {
                if ($item['vrsta'] == 'N' && $item['vrsta_input'] == '' && $item['status'] == 1) {
                    return false;
                }
            }

            return true;
        }

        if ($this->state[$id]['vrsta'] == 'N' && $this->state[$id]['vrsta_input'] == '' && $this->state[$id]['status'] == 1) {
            return false;
        }

        return true;
    }


    /**
     *
     */
    private function checkIfLast(): void
    {
        $last = true;

        foreach ($this->zahtjev->ovjere()->get() as $ovjera) {
            if ( ! $ovjera->datum_ovjere) {
                $last = false;
            }
        }

        if ($last) {
            foreach ($this->zahtjev->ovjere()->get() as $ovjera) {
                if ($ovjera->status == 1) {
                    Ovjera::where('mjerilo_id', $ovjera->mjerilo_id)->where('id', '<>', $ovjera->id)->update([
                        'status' => 0,
                        'updated_at' => now()
                    ]);
                }
            }

            $this->zahtjev->update([
                'status' => 3,
                'updated_at' => now()
            ]);
        }
    }


    /**
     * @param int $id
     */
    private function resolveOvjera(int $id)
    {
        Ovjera::ovjeri($id, $this->state[$id]);

        $this->checkIfLast();
    }
}
