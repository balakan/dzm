<?php

namespace App\Http\Livewire\Back\User;

use App\Models\Back\Catalog\Grupa;
use App\Models\Back\User\GroupToKorisnik;
use App\Models\Back\User\Institucija;
use App\Models\User;
use App\Models\Roles\Role;
use Carbon\Carbon;
use Livewire\Component;

class KorisnikShow extends Component
{

    /**
     * @var User
     */
    public $korisnik;

    /**
     * @var array
     */
    public $roles = [];

    /**
     * @var array
     */
    public $groups = [];

    /**
     * @var array
     */
    public $selected_role = [];

    /**
     * @var int
     */
    public $institucija_id = 0;

    /**
     * @var array
     */
    public $selected_institucija = [];

    /**
     * @var bool
     */
    public $date_error = false;

    /**
     * @var bool
     */
    public $show_institucija_input_window = false;

    /**
     * @var bool
     */
    public $show_group_window = false;

    /**
     * @var array
     */
    public $institucija_input_state = [
        'broj'                => '',
        'datum_ovlastenja_do' => '',
        'ispit'               => false,
    ];

    /**
     * @var string[]
     */
    protected $listeners = ["institucijaSelected" => 'setInstitucija'];


    /**
     *
     */
    public function mount()
    {
        $this->selected_role = $this->korisnik->uloga;

        $this->checkGroupWindow();

        if ($this->korisnik->institucija) {
            $this->selected_institucija          = $this->korisnik->institucija;
            $this->institucija_id                = $this->selected_institucija->id;
            $this->show_institucija_input_window = true;

            $this->institucija_input_state = [
                'institucija_id'      => $this->institucija_id,
                'broj'                => $this->korisnik->data->broj,
                'datum_ovlastenja_do' => $this->korisnik->data->datum_ovlastenja_do ? Carbon::make($this->korisnik->data->datum_ovlastenja_do)->format('d.m.Y') : '',
                'ispit'               => $this->korisnik->data->ispit,
            ];
        }
    }


    /**
     * @param Institucija $institucija
     */
    public function setInstitucija(Institucija $institucija)
    {
        $this->selected_institucija                      = $institucija;
        $this->institucija_input_state['institucija_id'] = $institucija->id;
    }


    /**
     * Ako je datum prazan ne prikazuj grešku.
     * Aje je format neispravan prikaži grešku.
     * Ako ima ispravan forma sa 0 ili bez, pridruži datum input_state.
     */
    public function checkDate()
    {
        if ($this->institucija_input_state['datum_ovlastenja_do'] == '') {
            $this->date_error = false;

            return;
        }

        // d.m.Y = 01.01.2022
        // j.n.Y = 1.1.2022
        $formated = Carbon::hasFormat($this->institucija_input_state['datum_ovlastenja_do'], 'd.m.Y') || Carbon::hasFormat($this->institucija_input_state['datum_ovlastenja_do'], 'j.n.Y');

        if ( ! $formated) {
            $this->date_error = true;

            return;
        }

        $this->institucija_input_state['datum_ovlastenja_do'] = Carbon::make($this->institucija_input_state['datum_ovlastenja_do'])->format('d.m.Y');
    }


    /**
     *
     */
    public function changeRole()
    {
        if ($this->selected_role->name != 'public') {
            if (isset($this->institucija_input_state['institucija_id']) && $this->institucija_input_state['institucija_id']) {
                $this->korisnik->addInstitutionData($this->institucija_input_state);
            }
        }

        $changed = Role::change($this->korisnik->id, $this->selected_role->name);

        if ( ! $changed) {
            return $this->emit('error_alert');
        }

        $this->checkGroupWindow();

        $this->emit('success_alert');

        $this->render();
    }


    /**
     * @param $user
     */
    public function roleSelected($value)
    {
        $this->selected_role = Role::find($value);

        $this->checkGroupWindow();

        if ($this->selected_role && $this->selected_role->name != 'public') {
            $this->show_institucija_input_window = true;
        } else {
            $this->show_institucija_input_window = false;
        }
    }


    /**
     * @param int $group_id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function disallowGroup(int $group_id)
    {
        if (GroupToKorisnik::disallow($group_id, $this->korisnik)) {
            $this->emit('success_alert');

            return $this->render();
        }

        $this->emit('error_alert');
    }


    /**
     * @param int $group_id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function allowGroup(int $group_id)
    {
        if (GroupToKorisnik::allow($group_id, $this->korisnik)) {
            $this->emit('success_alert');

            return $this->render();
        }

        $this->emit('error_alert');
    }


    /**
     *
     */
    public function allowAllGroups()
    {
        GroupToKorisnik::reflectAll($this->korisnik, 'allow');

        $this->render();
    }


    /**
     *
     */
    public function disallowAllGroups()
    {
        GroupToKorisnik::reflectAll($this->korisnik, 'disallow');

        $this->render();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $this->groups = (new Grupa())->adminList();
        $this->roles = Role::selectList();

        return view('livewire.back.user.korisnik-show');
    }


    /**
     *
     */
    private function checkGroupWindow()
    {
        $this->show_group_window = false;

        if ($this->korisnik->can('view-group-grupe') || $this->korisnik->can('view-own-grupe')) {
            $this->show_group_window = true;
        }
    }

}
