<?php

namespace App\Http\Livewire\Back\User;

use App\Models\Back\User\Mjeritelj;
use App\Models\Roles\Role;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Livewire\WithPagination;

class Korisnici extends Component
{

    use WithPagination;

    /**
     * @var
     */
    public $search = '';

    /**
     * @var array
     */
    public $roles = [];

    /**
     * @var bool
     */
    public $show_filter = false;

    /**
     * @var array
     */
    public $state = [
        'ime'         => true,
        'email'       => true,
        'uloga'       => '',
        'institucija' => false,
        'status'      => 'all',
    ];

    /**
     * @var array
     */
    public $selected_user = [];


    /**
     *
     */
    public function updatingSearch()
    {
        $this->render();
    }


    /**
     * @param bool $value
     */
    public function showFilterClicked(bool $value = false)
    {
        $this->show_filter = ! $value;
    }


    /**
     * @param string $data
     */
    public function filterRoleSelected(string $data)
    {
        $this->state['uloga'] = $data;

        $this->render();
    }


    /**
     * @param $id
     */
    public function userSelected($id)
    {
        $this->selected_user = User::where('id', $id)->with('profile', 'uloga')->first();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $users = (new User())->listSearch($this->search, $this->state)
                             ->paginate(config('view.admin.pagination'));

        $this->roles = Role::all();

        return view('livewire.back.user.korisnici', [
            'users' => $users
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }
}
