<?php

namespace App\Http\Livewire\Back\User;

use App\Models\Back\User\Institucija;
use Livewire\Component;
use Livewire\WithPagination;

class Institucije extends Component
{
    use WithPagination;

    /**
     * @var
     */
    public $search = '';

    /**
     * @var array
     */
    public $roles = [];

    /**
     * @var bool
     */
    public $show_filter = false;

    /**
     * @var array
     */
    public $state = [
        'broj'     => true,
        'naziv'    => true,
        'korisnik' => false,
        'city'     => false,
        'status'   => 'all',
    ];

    /**
     * @var array
     */
    public $selected = [];


    /**
     *
     */
    public function updatingSearch()
    {
        $this->render();
    }


    /**
     * @param bool $value
     */
    public function showFilterClicked(bool $value = false)
    {
        $this->show_filter = ! $value;
    }


    /**
     * @param $id
     */
    public function userSelected($id)
    {
        $this->selected = Institucija::where('id', $id)->first();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $institucije = Institucija::search($this->search, $this->state)
                                  ->paginate(config('view.admin.pagination'));

        return view('livewire.back.user.institucije', [
            'institucije' => $institucije
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }
}
