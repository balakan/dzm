<?php

namespace App\Http\Livewire\Back\User;

use App\Models\Back\User\Korisnik;
use App\Models\Back\User\Mjeritelj;
use App\Models\Roles\Role;
use App\Models\User;
use Illuminate\Support\Carbon;
use Livewire\Component;

class InstitucijaShow extends Component
{

    /**
     * @var Korisnik
     */
    public $institucija;

    /**
     * @var array
     */
    public $roles = [];

    /**
     * @var array
     */
    public $selected_role = [];


    /**
     *
     */
    public function changeRole()
    {
        /*$changed = Role::change($this->korisnik->id, $this->selected_role->name);

        if ( ! $changed) {
            return $this->emit('error_alert');
        }

        $this->emit('success_alert');

        if (isset($changed['mjeritelj']) && $changed['id']) {
            redirect()->route('mjeritelji.edit', ['mjeritelj' => Mjeritelj::find($changed['id'])]);
        }

        $this->render();*/
    }


    /**
     * @param $user
     */
    public function roleSelected($value)
    {
        $this->selected_role = Role::find($value);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $this->roles = Role::all();

        return view('livewire.back.user.institucija-show');
    }
}
