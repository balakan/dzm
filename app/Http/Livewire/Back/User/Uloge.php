<?php

namespace App\Http\Livewire\Back\User;

use App\Models\Roles\Ability;
use App\Models\Roles\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;
use Bouncer;

/**
 * Class Uloge
 * @package App\Http\Livewire\Back\User
 */
class Uloge extends Component
{

    /**
     * @var
     */
    public $roles;

    /**
     * @var
     */
    public $selected_role;

    /**
     * @var
     */
    public $selected_abilities;


    /**
     *
     */
    public function mount()
    {
        $this->roleSelected(1);
    }


    /**
     * @param $role
     */
    public function roleSelected($role)
    {
        $this->selected_role = Role::find($role);
        $this->selected_abilities = Role::getAbilities($role);
    }


    /**
     * @param Role    $role
     * @param Ability $ability
     *
     * @return mixed
     */
    public function disallowAbility(Role $role, Ability $ability)
    {
        $has = Bouncer::disallow($role->name)->to($ability->name);

        $has ? $this->emit('success_alert') : $this->emit('error_alert');
    }


    /**
     * @param Role    $role
     * @param Ability $ability
     *
     * @return mixed
     */
    public function allowAbility(Role $role, Ability $ability)
    {
        $has = Bouncer::allow($role->name)->to($ability->name);

        $has ? $this->emit('success_alert') : $this->emit('error_alert');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $this->roles = [
            'roles' => Role::getAllWithPermissions(),
            'abilities' => Ability::getFullList()
        ];

        return view('livewire.back.user.uloge');
    }
}
