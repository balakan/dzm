<?php

namespace App\Http\Livewire\Back\Layout\Search;

use App\Models\Back\Ovjera;
use App\Models\Back\Ovjera\Vlasnik;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

/**
 * Class VlasnikSearch
 * @package App\Http\Livewire\Back\Layout\Search
 */
class VlasnikSearch extends Component
{

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var int
     */
    public $vlasnik_id = 0;

    /**
     * @var bool
     */
    public $show_add_window = false;

    public $naziv = '';

    public $address = '';

    public $zip = '';

    public $city = '';

    public $region = '';

    public $email = '';

    public $phone = '';

    /**
     * @var string[]
     */
    protected $listeners = ['mjeriloAdded' => 'mjeriloIsAdded'];


    /**
     *
     */
    public function mount()
    {
        if ($this->vlasnik_id) {
            $this->search = Vlasnik::find($this->vlasnik_id)->naziv;
        }
    }


    /**
     * @param null $mjerilo
     */
    public function mjeriloIsAdded($mjerilo = null)
    {
        if ($mjerilo) {
            $ovjera = Ovjera::where('mjerilo_id', $mjerilo['mjerilo']['id'])->orderBy('created_at', 'desc')->first();

            if ($ovjera) {
                $this->vlasnik_id = $ovjera->vlasnik->id;
                $this->search     = $ovjera->vlasnik->naziv;
            }
        }
    }


    /**
     *
     */
    public function viewAddWindow()
    {
        $this->show_add_window = ! $this->show_add_window;
    }


    /**
     * @param $value
     */
    public function updatingSearch($value)
    {
        $this->search         = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = (new Vlasnik())->listSearch($this->search)
                                                   ->limit(config('view.admin.dropdown'))
                                                   ->get();
        }
    }


    /**
     * @param int $id
     */
    public function addVlasnik(int $id)
    {
        $vlasnik = Vlasnik::find($id);

        $this->search         = $vlasnik->naziv;
        $this->vlasnik_id     = $vlasnik->id;
        $this->search_results = [];
    }


    /**
     *
     */
    public function storeVlasnik()
    {
        if (trim($this->naziv) == '' || trim($this->address) == '' || trim($this->city) == '') {
            return $this->emit('error_required_vlasnik_alert', ['message' => 'Molimo vas da popunite potrebne podatke!']);
        }

        $id = Vlasnik::insertGetId([
            'naziv'      => trim($this->naziv),
            'address'    => trim($this->address),
            'zip'        => trim($this->zip),
            'city'       => trim($this->city),
            'region'     => trim($this->region),
            'phone'      => trim($this->phone),
            'email'      => trim($this->email),
            'status'     => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        if ($id) {
            $vlasnik = Vlasnik::find($id);

            $this->search         = $vlasnik->naziv;
            $this->vlasnik_id     = $vlasnik->id;
            $this->search_results = [];

            $this->viewAddWindow();

            return $this->emit('vlasnikStored', ['vlasnik' => $vlasnik->toArray()]);
        }

        $this->emit('errorStoring');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.vlasnik-search');
    }
}
