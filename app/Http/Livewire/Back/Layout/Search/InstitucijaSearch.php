<?php

namespace App\Http\Livewire\Back\Layout\Search;

use App\Models\Back\User\Institucija;
use App\Models\Back\User\OvlastenoTijelo;
use Livewire\Component;

class InstitucijaSearch extends Component
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var int
     */
    public $institucija_id = 0;


    /**
     *
     */
    public function mount()
    {
        if ($this->institucija_id) {
            $this->search = Institucija::find($this->institucija_id)->naziv;
        }
    }


    /**
     *
     */
    public function updatingSearch($value)
    {
        $this->search = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = Institucija::where('naziv', 'like', $this->search . '%')
                                               ->orWhere('broj', 'like', $this->search . '%')
                                               ->limit(config('view.admin.dropdown'))
                                               ->get();
        }
    }


    /**
     * @param $user_id
     */
    public function addUser($id)
    {
        $institucija = Institucija::find($id);

        $this->search = $institucija->naziv;
        $this->search_results = [];

        $this->emit('institucijaSelected', $institucija);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.institucija-search');
    }
}
