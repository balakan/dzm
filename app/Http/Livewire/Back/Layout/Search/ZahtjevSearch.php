<?php

namespace App\Http\Livewire\Back\Layout\Search;

use App\Models\Back\Zahtjev;
use Livewire\Component;

class ZahtjevSearch extends Component
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var array
     */
    public $search_state = [
        'klasa'       => true,
        'oznaka'      => true,
        'mjeritelj'   => false,
        'institucija' => false,
        'status'      => 0,
    ];


    /**
     * @param $value
     */
    public function updatingSearch(string $value)
    {
        $this->search = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = Zahtjev::listSearch($value, $this->search_state)
                                           ->limit(config('view.admin.dropdown'))
                                           ->get();
        }
    }


    public function addZahtjev(int $id)
    {
        return $this->redirectRoute('ovjere.create', ['zahtjev' => $id]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.zahtjev-search');
    }
}
