<?php

namespace App\Http\Livewire\Back\Layout\Search;

use App\Models\Back\Ovjera;
use App\Models\Back\Ovjera\MjestoPostavljanja;
use Carbon\Carbon;
use Livewire\Component;

class MjestoPostavljanjaSearch extends Component
{

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var int
     */
    public $mjesto_postavljanja_id = 0;

    /**
     * @var bool
     */
    public $show_add_window = false;

    public $address = '';

    public $zip = '';

    public $city = '';

    public $region = '';

    /**
     * @var string[]
     */
    protected $listeners = ['mjeriloAdded' => 'mjeriloIsAdded'];


    /**
     *
     */
    public function mount()
    {
        if ($this->mjesto_postavljanja_id) {
            $this->search = MjestoPostavljanja::find($this->mjesto_postavljanja_id)->address;
        }
    }


    /**
     * @param null $mjerilo
     */
    public function mjeriloIsAdded($mjerilo = null)
    {
        if ($mjerilo) {
            $ovjera = Ovjera::where('mjerilo_id', $mjerilo['mjerilo']['id'])->orderBy('created_at', 'desc')->first();

            if ($ovjera) {
                $this->mjesto_postavljanja_id = $ovjera->mjesto_postavljanja->id;
                $this->search                 = $ovjera->mjesto_postavljanja->address;
            }
        }
    }


    /**
     *
     */
    public function viewAddWindow()
    {
        $this->show_add_window = ! $this->show_add_window;
    }


    /**
     *
     */
    public function updatingSearch($value)
    {
        $this->search         = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = (new MjestoPostavljanja())->listSearch($this->search)
                                                              ->limit(config('view.admin.dropdown'))
                                                              ->get();
        }
    }


    /**
     * @param int $id
     */
    public function addMjestoPostavljanja(int $id)
    {
        $mjesto_postavljanja = MjestoPostavljanja::find($id);

        $this->search                 = $mjesto_postavljanja->address;
        $this->mjesto_postavljanja_id = $mjesto_postavljanja->id;
        $this->search_results         = [];
    }


    /**
     *
     */
    public function storeMjestoPostavljanja()
    {
        //dd($this);
        $id = MjestoPostavljanja::insertGetId([
            'address'    => trim($this->address),
            'zip'        => trim($this->zip),
            'city'       => trim($this->city),
            'region'     => trim($this->region),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        if ($id) {
            $mjesto_postavljanja = MjestoPostavljanja::find($id);

            $this->search                 = $mjesto_postavljanja->address;
            $this->mjesto_postavljanja_id = $mjesto_postavljanja->id;
            $this->search_results         = [];

            $this->viewAddWindow();

            return $this->emit('mjestoPostavljanjaStored', ['mjesto_postavljanja' => $mjesto_postavljanja->toArray()]);
        }

        $this->emit('errorStoring');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.mjesto-postavljanja-search');
    }
}
