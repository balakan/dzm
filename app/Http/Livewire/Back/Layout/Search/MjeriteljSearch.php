<?php

namespace App\Http\Livewire\Back\Layout\Search;

use App\Models\User;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class MjeriteljSearch extends Component
{

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var int
     */
    public $mjeritelj_id = 0;

    /**
     * @var bool
     */
    public $disable_input = true;

    /**
     * @var array
     */
    public $state = [
        'uloga' => ['mjeritelj-dzm', 'mjeritelj', 'mjeritelj-servis'],
        'ime' => true
    ];


    /**
     *
     */
    public function mount()
    {
        $user = auth()->user();

        if ($this->mjeritelj_id) {
            $this->search = User::find($this->mjeritelj_id)->name;
        }

        if ($user->can('select-mjeritelj-zahtjev')) {
            $this->disable_input = false;
        } else {
            if ($user->can('create-zahtjev')) {
                $this->search = $user->name;
                $this->mjeritelj_id = $user->id;
            }
        }
    }


    /**
     *
     */
    public function updatingSearch($value)
    {
        $this->search = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = (new User())->listSearch($this->search, $this->state)
                                             ->limit(config('view.admin.dropdown'))
                                             ->get()->reverse();
        }
    }


    /**
     * @param $user_id
     */
    public function addUser($id)
    {
        $mjeritelj = User::where('id', $id)->with('institucija')->first()->toArray();

        $this->mjeritelj_id = $mjeritelj['id'];
        $this->search = $mjeritelj['name'];
        $this->search_results = [];

        $this->emit('userAdded', ['mjeritelj' => $mjeritelj]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.mjeritelj-search');
    }
}
