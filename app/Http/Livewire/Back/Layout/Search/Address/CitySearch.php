<?php

namespace App\Http\Livewire\Back\Layout\Search\Address;

use App\Models\Back\Zahtjev;
use App\Models\Helper;
use Livewire\Component;

class CitySearch extends Component
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var array
     */
    public $list = [];

    /**
     * @var string[]
     */
    protected $listeners = [
        'zip_added' => 'zipAdded'
    ];


    /**
     *
     */
    public function mount()
    {
        $this->list = Helper::load_PP_Collection();
    }


    /**
     * @param $value
     */
    public function updatingSearch(string $value)
    {
        $this->search = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = $this->list->filter(function ($item, $key) use ($value) {
                if (str_contains($item['Naselje'], $value) || str_contains(strtolower($item['Naselje']), $value)) {
                    return true;
                }
            })->take(config('view.admin.dropdown'));
        }
    }


    /**
     * @param array $data
     */
    public function zipAdded(array $data)
    {
        $this->search = $data['data']['Naselje'];
    }


    /**
     * @param string $city
     */
    public function addCity(string $city)
    {
        $this->search = $city;
        $this->search_results = [];

        $data = $this->list->where('Naselje', $city)->first();

        $this->emit('city_added', ['data' => $data]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.address.city-search');
    }
}
