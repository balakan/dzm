<?php

namespace App\Http\Livewire\Back\Layout\Search\Address;

use App\Models\Back\Zahtjev;
use Livewire\Component;

class RegionSearch extends Component
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];


    /**
     * @var string[]
     */
    protected $listeners = [
        'city_added' => 'cityAdded',
        'zip_added' => 'zipAdded',
    ];


    /**
     * @param array $data
     */
    public function cityAdded(array $data)
    {
        $this->search = $data['data']['Zupanija'];
    }


    /**
     * @param array $data
     */
    public function zipAdded(array $data)
    {
        $this->search = $data['data']['Zupanija'];
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.address.region-search');
    }
}
