<?php

namespace App\Http\Livewire\Back\Layout\Search\Address;

use App\Models\Back\Zahtjev;
use App\Models\Helper;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;

class ZipSearch extends Component
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var array
     */
    public $list = [];

    /**
     * @var string[]
     */
    protected $listeners = [
        'city_added' => 'cityAdded'
    ];


    /**
     *
     */
    public function mount()
    {
        $this->list = Helper::load_PP_Collection();
    }


    /**
     * @param $value
     */
    public function updatingSearch(string $value)
    {
        $this->search = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = $this->list->filter(function ($item, $key) use ($value) {
                if (str_contains($item['BrojPu'], $value)) {
                    return true;
                }
            })->take(config('view.admin.dropdown'));
        }
    }


    /**
     * @param array $data
     */
    public function cityAdded(array $data)
    {
        $this->search = $data['data']['BrojPu'];
    }


    /**
     * @param string $zip
     */
    public function addZip(string $zip, string $naselje)
    {
        $this->search = $zip;
        $this->search_results = [];

        $data = $this->list->where('BrojPu', $zip)->where('Naselje', $naselje)->first();

        $this->emit('zip_added', ['data' => $data]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.address.zip-search');
    }
}
