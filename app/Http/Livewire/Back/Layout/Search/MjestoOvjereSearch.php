<?php

namespace App\Http\Livewire\Back\Layout\Search;

use App\Models\Back\Ovjera;
use App\Models\Back\Ovjera\MjestoOvjere;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class MjestoOvjereSearch extends Component
{

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var int
     */
    public $mjesto_ovjere_id = 0;

    /**
     * @var bool
     */
    public $show_add_window = false;

    public $address = '';

    public $zip = '';

    public $city = '';

    public $region = '';

    /**
     * @var string[]
     */
    protected $listeners = ['mjeriloAdded' => 'mjeriloIsAdded'];


    /**
     *
     */
    public function mount()
    {
        if ($this->mjesto_ovjere_id) {
            $mjesto_ovjere = MjestoOvjere::find($this->mjesto_ovjere_id);
            $this->search = $mjesto_ovjere->city . ', ' . $mjesto_ovjere->address;
        }
    }


    /**
     * @param null $mjerilo
     */
    public function mjeriloIsAdded($mjerilo = null)
    {
        if ($mjerilo) {
            $ovjera = Ovjera::where('mjerilo_id', $mjerilo['mjerilo']['id'])->orderBy('created_at', 'desc')->first();

            if ($ovjera) {
                $this->mjesto_ovjere_id = $ovjera->mjesto_ovjere->id;
                $this->search           = $ovjera->mjesto_ovjere->city . ', ' . $ovjera->mjesto_ovjere->address;
            }
        }
    }


    /**
     *
     */
    public function viewAddWindow()
    {
        $this->show_add_window = ! $this->show_add_window;
    }


    /**
     *
     */
    public function updatingSearch($value)
    {
        $this->search         = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = (new MjestoOvjere())->listSearch($this->search)
                                                        ->limit(config('view.admin.dropdown'))
                                                        ->get();
        }
    }


    /**
     * @param int $id
     */
    public function addMjestoOvjere(int $id)
    {
        $mjesto_ovjere = MjestoOvjere::find($id);

        $this->search           = $mjesto_ovjere->city . ', ' . $mjesto_ovjere->address;
        $this->mjesto_ovjere_id = $mjesto_ovjere->id;
        $this->search_results   = [];
    }


    /**
     *
     */
    public function storeMjestoOvjere()
    {
        if (trim($this->address) == '' || trim($this->city) == '') {
            return $this->emit('error_required_mjesto_ovjere_alert', ['message' => 'Molimo vas da popunite potrebne podatke!']);
        }

        $id = MjestoOvjere::insertGetId([
            'address'    => trim($this->address),
            'zip'        => trim($this->zip),
            'city'       => trim($this->city),
            'region'     => trim($this->region),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        if ($id) {
            $mjesto_ovjere = MjestoOvjere::find($id);

            $this->search           = $mjesto_ovjere->city . ', ' . $mjesto_ovjere->address;
            $this->mjesto_ovjere_id = $mjesto_ovjere->id;
            $this->search_results   = [];

            $this->viewAddWindow();

            return $this->emit('mjestoOvjereStored', ['mjesto_ovjere' => $mjesto_ovjere->toArray()]);
        }

        $this->emit('errorStoring');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.mjesto-ovjere-search');
    }
}
