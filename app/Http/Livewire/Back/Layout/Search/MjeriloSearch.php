<?php

namespace App\Http\Livewire\Back\Layout\Search;

use App\Models\Back\Catalog\Mjerilo;
use App\Models\Back\Catalog\Sifra;
use App\Models\Back\Catalog\Tip;
use App\Models\Back\User\Korisnik;
use Carbon\Carbon;
use Livewire\Component;

class MjeriloSearch extends Component
{

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $search_results = [];

    /**
     * @var int
     */
    public $mjerilo_id = 0;

    /**
     * @var null
     */
    public $zahtjev = null;

    /**
     * @var bool
     */
    public $show_add_window = false;

    /**
     * @var array
     */
    public $new = [
        'sn'           => '',
        'max_mjerenje' => '',
        'sifra_id'     => 0,
        'tip_id'       => 0
    ];

    /**
     * @var bool
     */
    private $permission_search = false;


    /**
     *
     */
    public function mount()
    {
        if ($this->mjerilo_id) {
            $this->search = Mjerilo::find($this->mjerilo_id)->sn;
        }
    }


    /**
     *
     */
    public function viewAddWindow()
    {
        $this->show_add_window = ! $this->show_add_window;
    }


    /**
     *
     */
    public function updatingSearch($value)
    {
        $this->search         = $value;
        $this->search_results = [];

        if ($this->search != '') {
            $this->search_results = (new Mjerilo())->listSearch($this->search, false, $this->zahtjev, Korisnik::canViewAllGrupe())
                                                   ->limit(config('view.admin.dropdown'))
                                                   ->get();
        }
    }


    /**
     * @param $sn
     */
    public function addMjerilo($sn)
    {
        $mjerilo = (new Mjerilo())->listSearch($sn, false, null, Korisnik::canViewAllGrupe())->first();

        $this->search_results = [];
        $this->search         = $mjerilo->sn;
        $this->mjerilo_id     = $mjerilo->id;

        $mjerilo->last = $mjerilo->last_ovjera()->first() ?: [];

        $this->emit('mjeriloAdded', ['mjerilo' => $mjerilo->toArray()]);
    }


    /**
     * @param $id
     */
    public function sifraSelected($id)
    {
        $this->new['sifra_id'] = $id;
    }


    /**
     * @param $id
     */
    public function tipSelected($id)
    {
        $this->new['tip_id'] = $id;
    }


    /**
     *
     */
    public function makeNewMjerilo()
    {
        if ($this->new['sn'] == '' || ! $this->new['sifra_id'] || ! $this->new['tip_id']) {
            return $this->emit('error_alert', ['message' => 'Molimo vas da popunite sve podatke!']);
        }

        $exist = Mjerilo::exist(
            $this->new['sn'],
            $this->new['tip_id'],
            $this->new['sifra_id']
        );

        if ($exist) {
            return $this->emit('error_alert', ['message' => 'Greška... Mjerilo sa istim karakteristikama već postoji.! <br>Možete ga odabrati iz padajućeg izbornika.']);
        }

        $id = Mjerilo::insertGetId([
            'tip_id'       => $this->new['tip_id'],
            'sifra_id'     => $this->new['sifra_id'],
            'kreator'      => auth()->user()->id,
            'sn'           => str_replace(' ', '', $this->new['sn']),
            'max_mjerenje' => $this->new['max_mjerenje'],
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);

        if ($id) {
            $mjerilo = Mjerilo::find($id);

            $this->show_add_window = false;

            $this->mjerilo_id = $mjerilo->id;
            $this->search     = $mjerilo->sn;

            return $this->emit('mjeriloAdded', ['mjerilo' => $mjerilo->toArray()]);
        }

        return $this->emit('error_alert');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.back.layout.search.mjerilo-search', [
            'sifre'  => (new Sifra())->listSearch()->get(),
            'tipovi' => (new Tip())->listSearch('', false)->get()
        ]);
    }
}
