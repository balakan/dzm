<?php

namespace App\Http\Livewire\Back\Layout\Partials;

use Livewire\Component;

class Topbar extends Component
{
    public function render()
    {
        return view('livewire.back.layout.partials.topbar');
    }
}
