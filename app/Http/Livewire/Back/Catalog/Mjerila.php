<?php

namespace App\Http\Livewire\Back\Catalog;

use App\Models\Back\Catalog\Mjerilo;
use Livewire\Component;
use Livewire\WithPagination;

class Mjerila extends Component
{
    use WithPagination;

    /**
     * @var
     */
    public $search;


    /**
     *
     */
    public function updatingSearch()
    {
        $this->resetPage();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $mjerila = (new Mjerilo())->listSearch($this->search, false, null, true)
                                  ->paginate(config('view.admin.pagination'));

        return view('livewire.back.catalog.mjerila', [
            'mjerila' => $mjerila
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }
}
