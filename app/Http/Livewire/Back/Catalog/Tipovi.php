<?php

namespace App\Http\Livewire\Back\Catalog;

use App\Models\Back\Catalog\Tip;
use Livewire\Component;
use Livewire\WithPagination;

class Tipovi extends Component
{
    use WithPagination;

    /**
     * @var
     */
    public $search;


    /**
     *
     */
    public function updatingSearch()
    {
        $this->resetPage();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $tipovi = (new Tip())->listSearch($this->search)
                             ->paginate(config('view.admin.pagination'));

        return view('livewire.back.catalog.tipovi', [
            'tipovi' => $tipovi
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire';
    }
}
