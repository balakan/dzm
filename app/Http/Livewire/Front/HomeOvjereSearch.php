<?php

namespace App\Http\Livewire\Front;

use App\Models\Front\Ovjera;
use Livewire\Component;
use Livewire\WithPagination;

class HomeOvjereSearch extends Component
{
    use WithPagination;

    /**
     * @var string
     */
    public $search = '';


    /**
     *
     */
    public function updatingSearch($value)
    {
        $this->search = $value;

        if ($this->search != '') {
            $this->render();
        }
    }


    /**
     * @param int $id
     */
    public function link($sn)
    {
        return $this->redirectRoute('detail', ['sn' => $sn]);
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        if ($this->search != '') {
            $search_results = (new Ovjera())->homeSearch($this->search)->paginate(config('view.admin.pagination'));
        } else {
            $search_results = [];
            $this->page = 1;
        }

        return view('livewire.front.home-ovjere-search', [
            'search_results' => $search_results
        ]);
    }


    /**
     * @return string
     */
    public function paginationView()
    {
        return 'vendor.pagination.bootstrap-livewire-front';
    }
}
