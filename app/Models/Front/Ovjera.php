<?php

namespace App\Models\Front;

use App\Models\Back\Catalog\Mjerilo;
use App\Models\Back\Ovjera\MjestoOvjere;
use App\Models\Back\Ovjera\MjestoPostavljanja;
use App\Models\Back\Ovjera\Vlasnik;
use App\Models\Back\User\Mjeritelj;
use App\Models\Back\User\OvlastenoTijelo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class Ovjera
 * @package App\Models\Back
 */
class Ovjera extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'ovjere';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mjeritelj()
    {
        return $this->hasOne(Mjeritelj::class, 'id', 'mjeritelj_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ovlasteno_tijelo()
    {
        return $this->hasOne(OvlastenoTijelo::class, 'id', 'ovlasteno_tijelo_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mjerilo()
    {
        return $this->hasOne(Mjerilo::class, 'id', 'mjerilo_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vlasnik()
    {
        return $this->hasOne(Vlasnik::class, 'id', 'mjerilo_vlasnik_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mjesto_ovjere()
    {
        return $this->hasOne(MjestoOvjere::class, 'id', 'mjerilo_mjesto_ovjere_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mjesto_postavljanja()
    {
        return $this->hasOne(MjestoPostavljanja::class, 'id', 'mjerilo_mjesto_postavljanja_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function zahtjev()
    {
        return $this->hasOneThrough(Zahtjev::class, OvjeraToZahtjev::class, 'ovjera_id', 'id', 'id', 'zahtjev_id');
    }


    /**
     * @param string $search
     *
     * @return Builder
     */
    public function homeSearch(string $search = ''): Builder
    {
        $query = $this->newQuery();

        $query->where('status', '=', 1);

        $query->where(function ($query) use ($search) {
            $query->orWhere('naljepnica', 'like', $search . '%');
            $query->orWhere('zig', 'like', $search . '%');

            // Mjerilo
            $query->orWhereHas('mjerilo', function ($subquery) use ($search) {
                $subquery->where('sn', 'like', $search . '%');
            });

            /*// Vlasnik
            $query->orWhereHas('vlasnik', function ($subquery) use ($search) {
                $subquery->where('naziv', 'like', $search . '%');
            });

            // Vlasnik
            $query->orWhereHas('mjesto_postavljanja', function ($subquery) use ($search) {
                $subquery->where('city', 'like', $search . '%');
            });

            // Vlasnik
            $query->orWhereHas('mjesto_ovjere', function ($subquery) use ($search) {
                $subquery->where('city', 'like', $search . '%');
            });

            // Mjeritelj
            $query->orWhereHas('mjeritelj', function ($subquery) use ($search) {
                $subquery->where('naziv', 'like', $search . '%')
                         ->orWhere('broj', 'like', $search . '%');
            });*/
        });

        $query->where('datum_ovjere', '<=', now())
              ->where('datum_valjanosti', '>=', now());

        return $query;
    }

}
