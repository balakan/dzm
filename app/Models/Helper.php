<?php

namespace App\Models;

use App\Models\Back\Zahtjev;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\False_;

class Helper
{

    /**
     * @param string $name
     * @param string $target
     *
     * @return int
     */
    public static function resolveStatusIdByName(string $name, string $target = 'zahtjev'): int
    {
        foreach (config('settings.' . $target . '.statuses') as $key => $status) {
            if ($name == $status) {
                return $key;
            }
        }

        return 0;
    }


    /**
     * @param int    $id
     * @param string $target
     *
     * @return string
     */
    public static function resolveStatusNameById(int $id, string $target = 'zahtjev'): string
    {
        foreach (config('settings.' . $target . '.statuses') as $key => $status) {
            if ($key == $id) {
                return $status;
            }
        }

        return '';
    }


    /**
     * @param Zahtjev|null $zahtjev
     *
     * @return array
     */
    public static function getAvailableZahtjevStatuses(Zahtjev $zahtjev = null): array
    {
        // Ako nema zahtjeva znači da se novi izrađuje
        // i tada vraća samo u izradi status.
        if ( ! $zahtjev) {
            return [
                1 => config('settings.zahtjev.statuses')[1]
            ];
        }

        // Ako korisnik ne može editirati statuse vraća prazan array.
        // To se u view prikazuje kao badge tag, bez selecta.
        if ( ! \Bouncer::can('edit-zahtjev-status')) {
            return [];
        }

        // Korisnik smije editirati statuse.
        $statuses = [];

        if ($zahtjev && \Bouncer::can('edit-zahtjev-status')) {
            // Ako je status u izradi vraća dpdatno Predan.
            if ($zahtjev->status == 1) {
                $statuses = [
                    1 => config('settings.zahtjev.statuses')[1],
                    2 => config('settings.zahtjev.statuses')[2]
                ];
            }

            // Ako je status predan vraća u izradi, predan i
            // dodatno Završen ako korisnik smije ovjeravati zahtjeve.
            if ($zahtjev->status == 2) {
                $statuses = [
                    1 => config('settings.zahtjev.statuses')[1],
                    2 => config('settings.zahtjev.statuses')[2]
                ];

                if (\Bouncer::can('ovjera-zahtjev') && auth()->user()->id == $zahtjev->mjeritelj_id) {
                    $statuses[3] = config('settings.zahtjev.statuses')[3];
                }
            }

            // Ako je završen vrati sve statuse.
            if ($zahtjev->status == 3) {
                $statuses = config('settings.zahtjev.statuses');
            }
        }

        return $statuses;
    }


    /**
     * @param int $number
     * @param int $by
     *
     * @return int
     */
    public static function closestNumberDivisibleBy(int $number, int $by): int
    {
        // find the quotient
        $q = (int) ($number / $by);

        // 1st possible closest number
        $n1 = $by * $q;

        // 2nd possible closest number
        $n2 = ($number * $by) > 0 ?
            ($by * ($q + 1)) : ($by * ($q - 1));

        // if true, then n1 is the
        // required closest number
        if (abs($number - $n1) < abs($number - $n2)) {
            return $n1;
        }

        // else n2 is the required
        // closest number
        return $n2;
    }


    /**
     * Return the array od Zip Codes as collection.
     *
     * @return Collection
     */
    public static function load_PP_Collection(string $name = 'pp'): Collection
    {
        $json = file_get_contents(public_path('assets/' . $name . '.json'));

        $arr = json_decode($json, true);

        return collect($arr);
    }


    /**
     * @param string $name
     * @param array  $data
     *
     * @return string
     */
    public static function writeJson(string $name, array $data): string
    {
        $json = json_encode($data);
        $fp   = fopen(public_path('assets/' . $name . '.json'), 'w');

        fwrite($fp, $json);
        fclose($fp);

        return $json;
    }

}