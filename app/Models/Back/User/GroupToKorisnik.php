<?php

namespace App\Models\Back\User;

use App\Models\Back\Catalog\Grupa;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupToKorisnik extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'grupe_to_user_data';


    /**
     * @param int       $group
     * @param Mjeritelj $mjeritelj
     *
     * @return mixed
     */
    public static function allow(int $group_id, User $korisnik)
    {
        $group = Grupa::find($group_id);

        if ( ! $group->parent_id) {
            $groups = Grupa::where('parent_id', $group_id)->get();

            if ($groups->count()) {
                foreach ($groups as $item) {
                    self::insert([
                        'group_id' => $item->id,
                        'user_data_id' => $korisnik->id
                    ]);
                }
            }
        }

        return self::insert([
            'group_id' => $group_id,
            'user_data_id' => $korisnik->id
        ]);
    }


    /**
     * @param int       $group
     * @param Mjeritelj $mjeritelj
     *
     * @return mixed
     */
    public static function disallow(int $group_id, User $korisnik)
    {
        $group = Grupa::find($group_id);

        if ( ! $group->parent_id) {
            $groups = Grupa::where('parent_id', $group_id)->get();

            if ($groups->count()) {
                foreach ($groups as $item) {
                    self::where('group_id', $item->id)
                        ->where('user_data_id', $korisnik->id)
                        ->delete();
                }
            }
        }

        return self::where('group_id', $group_id)
                   ->where('user_data_id', $korisnik->id)
                   ->delete();
    }


    /**
     * @param User   $korisnik
     * @param string $type
     *
     * @return mixed
     */
    public static function reflectAll(User $korisnik, string $type)
    {
        if ($type == 'allow') {
            self::where('user_data_id', $korisnik->id)->delete();
        }

        $reflected = false;
        $groups = Grupa::where('parent_id', 0)->get();

        if ($groups->count()) {
            foreach ($groups as $group) {
                $reflected = $type == 'allow' ?
                    self::allow($group->id, $korisnik) :
                    self::disallow($group->id, $korisnik);
            }
        }

        return $reflected;
    }
}
