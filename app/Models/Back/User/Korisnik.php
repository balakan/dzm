<?php

namespace App\Models\Back\User;

use App\Models\Roles\Role;
use App\Models\User;
use App\Models\UserData;
use App\Models\UserProfile;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Korisnik extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    private $request = [];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(UserProfile::class, 'user_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uloga()
    {
        return $this->hasOne(Role::class, 'name', 'role');
    }


    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasOne|object|null
     */
    public function data()
    {
        return $this->hasOne(UserData::class, 'user_id', 'id')->first();
    }


    /**
     * @return mixed|null
     */
    public function institucija()
    {
        return $this->hasOneThrough(Institucija::class, UserData::class, 'user_id', 'id', 'id', 'institucija_id');
    }


    /**
     * @param array $data
     *
     * @return false
     */
    public function addInstitutionData(array $data)
    {
        if (empty($data)) {
            return false;
        }

        $exist = UserData::where('user_id', $this->id)->where('institucija_id', $this->institucija->id)->first();

        if ($exist) {
            return UserData::where('user_id', $this->id)->update([
                'institucija_id'      => $data['institucija_id'],
                'broj'                => $data['broj'],
                'datum_ovlastenja_do' => $data['datum_ovlastenja_do'] ? Carbon::make($data['datum_ovlastenja_do']) : null,
                'ispit'               => $data['ispit'] ? 1 : 0,
                'updated_at'          => Carbon::now()
            ]);
        }

        return UserData::insert([
            'user_id'             => $this->id,
            'institucija_id'      => $data['institucija_id'],
            'broj'                => $data['broj'],
            'datum_ovlastenja_od' => Carbon::now(),
            'datum_ovlastenja_do' => $data['datum_ovlastenja_do'] ? Carbon::make($data['datum_ovlastenja_do']) : null,
            'ispit'               => $data['ispit'] ? 1 : 0,
            'created_at'          => Carbon::now(),
            'updated_at'          => Carbon::now()
        ]);
    }


    /**
     * @return bool
     */
    public static function canViewAllGrupe(): bool
    {
        if (auth()->user()->can('view-grupe')) {
            return true;
        }

        return false;
    }

}
