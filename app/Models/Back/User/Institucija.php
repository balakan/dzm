<?php

namespace App\Models\Back\User;

use App\Models\Back\Ovjera;
use App\Models\Back\Zahtjev;
use App\Models\Roles\Role;
use App\Models\User;
use App\Models\UserData;
use App\Models\UserProfile;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Institucija extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'institucije';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    private $request = [];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function users()
    {
        return $this->hasManyThrough(User::class, UserData::class, 'institucija_id', 'id', 'id', 'user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function zahtjevi()
    {
        return $this->hasMany(Zahtjev::class, 'id', 'institucija_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ovjere()
    {
        return $this->hasMany(Ovjera::class, 'id', 'institucija_id');
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    public function validateRequest(Request $request)
    {
        $this->request = $request;

        $request->validate([
            'naziv' => 'required',
            'broj'  => 'required',
        ]);

        return $this;
    }


    /**
     * @return mixed
     */
    public function create()
    {
        return $this->insertGetId([
            'parent_id'           => $this->request->parent_id ?: 0,
            'broj'                => $this->request->broj,
            'naziv'               => $this->request->naziv,
            'address'             => $this->request->address,
            'zip'                 => $this->request->zip,
            'city'                => $this->request->city,
            'region'              => $this->request->region,
            'phone'               => $this->request->phone,
            'email'               => $this->request->email,
            'datum_ovlastenja_od' => $this->request->datum_ovlastenja_od ? Carbon::make($this->request->datum_ovlastenja_od) : now(),
            'datum_ovlastenja_do' => $this->request->datum_ovlastenja_do ? Carbon::make($this->request->datum_ovlastenja_do) : null,
            'status'              => (isset($this->request->status) and $this->request->status == 'on') ? 1 : 0,
            'created_at'          => Carbon::now(),
            'updated_at'          => Carbon::now()
        ]);
    }


    /**
     * @return bool
     */
    public function edit()
    {
        return $this->update([
            'parent_id'           => $this->request->parent_id ?: 0,
            'broj'                => $this->request->broj,
            'naziv'               => $this->request->naziv,
            'address'             => $this->request->address,
            'zip'                 => $this->request->zip,
            'city'                => $this->request->city,
            'region'              => $this->request->region,
            'phone'               => $this->request->phone,
            'email'               => $this->request->email,
            'datum_ovlastenja_od' => $this->request->datum_ovlastenja_od ? Carbon::make($this->request->datum_ovlastenja_od) : now(),
            'datum_ovlastenja_do' => $this->request->datum_ovlastenja_do ? Carbon::make($this->request->datum_ovlastenja_do) : null,
            'status'              => (isset($this->request->status) and $this->request->status == 'on') ? 1 : 0,
            'updated_at'          => Carbon::now()
        ]);
    }


    /**
     * @param string $search
     * @param array  $state
     *
     * @return Builder
     */
    public static function search(string $search = '', array $state = []): Builder
    {
        $query = (new Institucija())->newQuery();

        if ( ! empty($state)) {
            $query->where(function ($query) use ($state) {
                if ($state['status'] != 'all') {
                    $query->where('status', $state['status']);
                }
            });
        }

        if ( ! empty($search)) {
            if ($state['broj']) {
                $query->orWhere('broj', 'like', $search . '%');
            }

            if ($state['naziv']) {
                $query->orWhere('naziv', 'like', $search . '%');
            }

            if ($state['korisnik']) {
                $query->orWhere('korisnik', 'like', $search . '%');
            }

            if ($state['city']) {
                $query->orWhere('city', 'like', $search . '%');
            }
        }

        if ( ! \Bouncer::can('*') && \Bouncer::can('view-own-institucije')) {
            $query->where('id', auth()->user()->data->institucija_id);
        }

        $query->orderBy('broj');

        return $query;
    }


    /**
     * @return $this
     */
    public function checkPermission()
    {
        if ( ! \Bouncer::can('view-all-institucije')) {
            if (auth()->user()->data->institucija_id != $this->id) {
                abort(403);
            }
        }

        return $this;
    }
}
