<?php

namespace App\Models\Back;

use App\Models\Back\Catalog\Mjerilo;
use App\Models\Back\Catalog\Sifra;
use App\Models\Back\Ovjera\MjestoOvjere;
use App\Models\Back\Ovjera\MjestoPostavljanja;
use App\Models\Back\Ovjera\Vlasnik;
use App\Models\Back\User\Mjeritelj;
use App\Models\Back\User\OvlastenoTijelo;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 * Class Ovjera
 * @package App\Models\Back
 */
class Ovjera extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'ovjere';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var Request
     */
    public $request;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mjerilo()
    {
        return $this->hasOne(Mjerilo::class, 'id', 'mjerilo_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vlasnik()
    {
        return $this->hasOne(Vlasnik::class, 'id', 'mjerilo_vlasnik_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mjesto_ovjere()
    {
        return $this->hasOne(MjestoOvjere::class, 'id', 'mjerilo_mjesto_ovjere_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mjesto_postavljanja()
    {
        return $this->hasOne(MjestoPostavljanja::class, 'id', 'mjerilo_mjesto_postavljanja_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function zahtjev()
    {
        return $this->belongsTo(Zahtjev::class, 'zahtjev_id', 'id');
    }


    /**
     * @return mixed
     */
    public function last()
    {
        return $this->where('datum_ovjere', '!=', null)->where('mjerilo_id', $this->mjerilo_id)->orderBy('datum_ovjere', 'desc')->limit(1);
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        foreach (config('settings.ovjera.statuses') as $key => $status) {
            if ($this->status == $key) {
                return $status;
            }
        }
    }


    /**
     * @param       $query
     * @param array $params
     * @param User  $user
     *
     * @return Collection
     */
    public function scopeChartData($query, array $params, User $user): Collection
    {
        $query = $this->filterByRoles($query);

        return $query
            ->with('mjerilo')
            ->whereBetween('created_at', [$params['from'], $params['to']])
            ->orderBy('created_at')
            ->get()
            ->groupBy(function ($val) use ($params) {
                return \Illuminate\Support\Carbon::parse($val->created_at)->format($params['group']);
            });
    }


    /**
     * @param Request $request
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'zahtjev_id'                     => 'required',
            'vrsta'                          => 'required',
            'mjerilo_id'                     => 'required|not_in:0',
            'mjerilo_vlasnik_id'             => 'required|not_in:0',
            'mjerilo_mjesto_ovjere_id'       => 'required|not_in:0',
            //'mjerilo_mjesto_postavljanja_id' => 'required|not_in:0',
        ]);

        $this->request = $request;

        return $this;
    }


    /**
     * @return mixed
     */
    public function create()
    {
        $stored = $this->insertGetId([
            'zahtjev_id'                     => $this->request->zahtjev_id,
            'mjerilo_id'                     => $this->request->mjerilo_id,
            'mjerilo_vlasnik_id'             => $this->request->mjerilo_vlasnik_id,
            'mjerilo_mjesto_ovjere_id'       => $this->request->mjerilo_mjesto_ovjere_id,
            'mjerilo_mjesto_postavljanja_id' => 1,//$this->request->mjerilo_mjesto_postavljanja_id,
            'klasa'                          => '',
            'vrsta'                          => $this->request->vrsta,
            'naljepnica'                     => $this->request->naljepnica ?: '',
            'zig'                            => $this->request->zig ?: '',
            'ima_ovjernicu'                  => (isset($this->request->ima_ovjernicu) && $this->request->ima_ovjernicu == 'on') ? 1 : 0,
            'ovjernica'                      => $this->request->ovjernica ?: '',
            'datum_ovjere'                   => isset($this->request->datum_ovjere) ? Carbon::make($this->request->datum_ovjere) : null,
            'datum_valjanosti'               => isset($this->request->datum_valjanosti) ? Carbon::make($this->request->datum_valjanosti) : null,
            'status'                         => (isset($this->request->status) && $this->request->status == 'on') ? 1 : 0,
            'napomena'                       => $this->request->napomena,
            'created_at'                     => Carbon::now(),
            'updated_at'                     => Carbon::now()
        ]);

        return $stored;
    }


    /**
     * @return bool
     */
    public function edit()
    {
        if ( ! isset($this->request->naljepnica) || ! isset($this->request->zig)) {
            return $this->update([
                'zahtjev_id'                     => $this->request->zahtjev_id,
                'mjerilo_id'                     => $this->request->mjerilo_id,
                'mjerilo_vlasnik_id'             => $this->request->mjerilo_vlasnik_id,
                'mjerilo_mjesto_ovjere_id'       => $this->request->mjerilo_mjesto_ovjere_id,
                'mjerilo_mjesto_postavljanja_id' => 1,//$this->request->mjerilo_mjesto_postavljanja_id,
                'vrsta'                          => $this->request->vrsta,
                'updated_at'                     => Carbon::now()
            ]);
        }

        return $this->update([
            'zahtjev_id'                     => $this->request->zahtjev_id,
            'mjerilo_id'                     => $this->request->mjerilo_id,
            'mjerilo_vlasnik_id'             => $this->request->mjerilo_vlasnik_id,
            'mjerilo_mjesto_ovjere_id'       => $this->request->mjerilo_mjesto_ovjere_id,
            'mjerilo_mjesto_postavljanja_id' => 1,//$this->request->mjerilo_mjesto_postavljanja_id,
            'klasa'                          => '',
            'vrsta'                          => $this->request->vrsta,
            'naljepnica'                     => $this->request->naljepnica ?: '',
            'zig'                            => $this->request->zig ?: '',
            'ima_ovjernicu'                  => (isset($this->request->ima_ovjernicu) && $this->request->ima_ovjernicu == 'on') ? 1 : 0,
            'ovjernica'                      => $this->request->ovjernica ?: '',
            'datum_ovjere'                   => isset($this->request->datum_ovjere) ? Carbon::make($this->request->datum_ovjere) : null,
            'datum_valjanosti'               => isset($this->request->datum_valjanosti) ? Carbon::make($this->request->datum_valjanosti) : null,
            'status'                         => (isset($this->request->status) && $this->request->status == 'on') ? 1 : 0,
            'napomena'                       => $this->request->napomena,
            'updated_at'                     => Carbon::now()
        ]);
    }


    /**
     * @param int   $id
     * @param array $data
     *
     * @return mixed
     */
    public static function ovjeri(int $id, array $data)
    {
        $ovjera = self::where('id', $id)->first();

        if ( ! $ovjera->datum_ovjere) {
            $oznake = Mjerilo::resolveLabelsByStatus($ovjera, $data);

            return $ovjera->update([
                'naljepnica'       => $oznake['naljepnica'],
                'zig'              => $oznake['zig'],
                'ima_ovjernicu'    => 0,
                'ovjernica'        => '',
                /*'datum_ovjere'     => now(),
                'datum_valjanosti' => $oznake['valjanost'],*/
                'status'           => $data['status'],
                'napomena'         => $data['napomena']
            ]);
        }
    }


    /**
     * @param string $search
     *
     * @return Builder
     */
    public function homeSearch(string $search = ''): Builder
    {
        $query = $this->newQuery();

        $query->where(function ($query) use ($search) {
            $query->orWhere('oznaka', 'like', $search . '%');

            /*// Vlasnik
            $query->orWhereHas('vlasnik', function ($subquery) use ($search) {
                $subquery->where('naziv', 'like', $search . '%');
            });

            // Vlasnik
            $query->orWhereHas('mjesto_postavljanja', function ($subquery) use ($search) {
                $subquery->where('city', 'like', $search . '%');
            });

            // Vlasnik
            $query->orWhereHas('mjesto_ovjere', function ($subquery) use ($search) {
                $subquery->where('city', 'like', $search . '%');
            });

            // Mjerilo
            $query->orWhereHas('mjerilo', function ($subquery) use ($search) {
                $subquery->where('sn', 'like', $search . '%');
            });

            // Mjeritelj
            $query->orWhereHas('mjeritelj', function ($subquery) use ($search) {
                $subquery->where('naziv', 'like', $search . '%')
                         ->orWhere('broj', 'like', $search . '%');
            });*/
        });

        return $query;
    }


    /**
     * @param string $type
     *
     * @return $this
     */
    public function checkPermission(string $type = 'view')
    {
        if ( ! \Bouncer::can($type . '-all-ovjera')) {
            // Mjeritelj
            if (\Bouncer::can($type . '-own-ovjera')) {
                if (auth()->user()->id != $this->zahtjev->mjeritelj_id) {
                    abort(403);
                }
            }

            // Voditelj
            if (\Bouncer::can($type . '-group-ovjera')) {
                $users = auth()->user()->institucija->users->pluck('id')->toArray();

                if ( ! in_array($this->zahtjev->mjeritelj_id, $users)) {
                    abort(403);
                }
            }
        }

        return $this;
    }


    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function filterByRoles(Builder $query): Builder
    {
        if ( ! \Bouncer::can('*')) {
            // Mjeritelj
            if (\Bouncer::can('view-own-ovjera')) {
                $query->whereHas('zahtjev', function ($subquery) {
                    $subquery->where('mjeritelj_id', auth()->user()->id);
                });
            }

            // Ovlašteno tijelo
            if (\Bouncer::can('view-group-ovjera')) {
                $zahtjevi = collect();

                if (auth()->user()->institucija) {
                    $institucija = auth()->user()->institucija->first();
                    $users       = $institucija->users->whereIn('role', ['mjeritelj', 'mjeritelj-dzm', 'mjeritelj-servis'])->pluck('id');

                    $query->whereHas('zahtjev', function ($subquery) use ($users) {
                        $subquery->whereIn('mjeritelj_id', $users);
                    });
                } else {
                    $query->whereIn('mjeritelj_id', $zahtjevi);
                }
            }
        }

        return $query;
    }

}
