<?php

namespace App\Models\Back;

use App\Models\Back\User\Institucija;
use App\Models\Back\User\Mjeritelj;
use App\Models\Back\User\OvlastenoTijelo;
use App\Models\User;
use App\Models\UserData;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class Zahtjev extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'zahtjevi';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var Request
     */
    public $request;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function mjeritelj()
    {
        return $this->hasOneThrough(User::class, UserData::class, 'user_id', 'id', 'mjeritelj_id', 'user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function institucija()
    {
        return $this->hasOne(Institucija::class, 'id', 'institucija_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function ovjere()
    {
        return $this->hasMany(Ovjera::class, 'zahtjev_id', 'id');
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        foreach (config('settings.zahtjev.statuses') as $key => $status) {
            if ($this->status == $key) {
                return $status;
            }
        }
    }


    /**
     * @param Request $request
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'mjeritelj'      => 'required|not_in:0',
            'datum_pregleda' => 'required',
            'status'         => 'required|gt:0'
        ]);

        $this->request = $request;
    }


    /**
     * @return mixed
     */
    public function create()
    {
        $data = $this->resolveData();
        $napomena = $this->request->napomena;

        if (auth()->user()->isAn('mjeritelj-servis')) {
            $napomena = $this->request->napomena . ' - Mjeritelj serviser: ' . auth()->user()->name;
        }

        return $this->insertGetId([
            'mjeritelj_id'   => $data['mjeritelj_id'],
            'institucija_id' => $data['institucija_id'],
            'klasa'          => $this->request->klasa,
            'datum_pregleda' => Carbon::make($this->request->datum_pregleda),
            'status'         => $this->request->status,
            'napomena'       => $napomena,
            'created_at'     => Carbon::now(),
            'updated_at'     => Carbon::now()
        ]);
    }


    /**
     * @return bool
     */
    public function edit()
    {
        $data = $this->resolveData();

        // Provjeriti je li status mijenjan
        if ($this->status != $this->request->status) {
            // Ako se status mijenja prema gore
            if ($this->request->status > $this->status) {
                // Provjeriti smije li korisnik mijenjati status
                if ( ! \Bouncer::can('edit-zahtjev-status')) {
                    return 'status_error';
                }
            }
            // Ako se status mijenja prema dolje
            if ($this->request->status < $this->status) {
                // Provjeriti smije li korisnik vratiti status
                if ( ! \Bouncer::can('revert-zahtjev-status')) {
                    return 'status_error';
                }

                // Ako se status mijenja iz Završen u Predan
                // potreban je update ovjere.
                if ($this->status == 3 && $this->request->status == 2) {
                    Ovjera::where('zahtjev_id', $this->id)->update([
                        'datum_ovjere' => null,
                        'updated_at' => now()
                    ]);
                }
            }
        }

        return $this->update([
            'mjeritelj_id'   => $data['mjeritelj_id'],
            'institucija_id' => $data['institucija_id'],
            'klasa'          => $this->request->klasa,
            'datum_pregleda' => Carbon::make($this->request->datum_pregleda),
            'status'         => $this->request->status,
            'napomena'       => $this->request->napomena,
            'updated_at'     => Carbon::now()
        ]);
    }


    /**
     * @return array
     */
    private function resolveData()
    {
        $mjeritelj = User::find($this->request->mjeritelj);

        return [
            'mjeritelj_id'   => $mjeritelj->id,
            'institucija_id' => $mjeritelj->institucija->id,
        ];
    }


    /**
     * @param string $search
     * @param array  $state
     *
     * @return Builder
     */
    public static function listSearch(string $search = '', array $state = []): Builder
    {
        $query = (new Zahtjev())->newQuery();

        if ( ! empty($state) && isset($state['status']) && $state['status']) {
            $query->where('status', $state['status']);
        }

        if ( ! empty($state) && isset($state['date_from']) && $state['date_from']) {
            $query->whereDate('created_at', '>=', Carbon::createFromFormat('d.m.Y', $state['date_from']));
        }

        if ( ! empty($state) && isset($state['date_to']) && $state['date_to']) {
            $query->whereDate('created_at', '<=', Carbon::createFromFormat('d.m.Y', $state['date_to']));
        }

        if ($search != '') {
            $query->where(function ($query) use ($search, $state) {
                if ($state['mjeritelj'] || $state['oznaka']) {
                    $query->whereHas('mjeritelj', function ($query) use ($search, $state) {
                        $query->where('name', 'like', '%' . $search . '%');

                        if ($state['oznaka']) {
                            $query->orWhereHas('data', function ($subquery) use ($search) {
                                $subquery->where('broj', 'like', $search . '%');
                            });
                        }
                    });
                }

                if ($state['institucija']) {
                    $query->whereHas('institucija', function ($query) use ($search) {
                        $query->where('naziv', 'like', '%' . $search . '%');
                    });
                }

                /*if ($state['oznaka']) {
                    $query->whereHas('mjeritelj', function ($query) use ($search) {
                        //$query->where('name', 'like', '%' . $search . '%');

                        $query->orWhereHas('data', function ($subquery) use ($search) {
                            $subquery->where('broj', 'like', $search . '%');
                        });
                    });
                }*/

                if ($state['klasa']) {
                    $query->orWhere('klasa', 'like', '%' . $search . '%');
                }

                //dd($query->get()->toArray());
            });
        }

        if ( ! \Bouncer::can('view-all-zahtjev')) {
            // Mjeritelj
            if (\Bouncer::can('view-own-zahtjev')) {
                $query->where('mjeritelj_id', Auth::user()->id);
            }

            // Ovlašteno tijelo
            if (\Bouncer::can('view-group-zahtjev')) {
                if (Auth::user()->institucija) {
                    $users = auth()->user()->institucija->users->pluck('id');

                    $query->whereIn('mjeritelj_id', $users);
                }
            }
        }

        $query->with('ovjere')->orderBy('created_at', 'desc');

        return $query;
    }


    /**
     * @param string $type
     *
     * @return $this
     */
    public function checkPermission(string $type = 'view')
    {
        if ( ! \Bouncer::can($type . '-all-zahtjev')) {
            // Mjeritelj
            if (\Bouncer::can($type . '-own-zahtjev')) {
                if (auth()->user()->id != $this->mjeritelj_id) {
                    abort(403);
                }
            }

            // Voditelj
            if (\Bouncer::can($type . '-group-zahtjev')) {
                $users = auth()->user()->institucija->users->pluck('id')->toArray();

                if ( ! in_array($this->mjeritelj_id, $users)) {
                    abort(403);
                }
            }
        }

        return $this;
    }

}
