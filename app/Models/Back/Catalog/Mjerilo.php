<?php

namespace App\Models\Back\Catalog;

use App\Models\Back\Ovjera;
use App\Models\Back\Zahtjev;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Mjerilo extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'mjerila';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var Request
     */
    public $request;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function grupa()
    {
        return $this->hasOneThrough(Grupa::class, Sifra::class, 'id', 'id', 'sifra_id', 'group_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tip()
    {
        return $this->belongsTo(Tip::class, 'tip_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sifra()
    {
        return $this->belongsTo(Sifra::class, 'sifra_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ovjera()
    {
        return $this->belongsTo(Ovjera::class, 'mjerilo_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ovjere()
    {
        return $this->hasMany(Ovjera::class, 'mjerilo_id', 'id');
    }


    /**
     * @return mixed
     */
    public function last_ovjera()
    {
        return Ovjera::where('datum_ovjere', '!=', null)->where('mjerilo_id', $this->id)->orderBy('datum_ovjere', 'desc')->limit(1);
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'tip_id'   => 'required',
            'sifra_id' => 'required',
            'sn'       => 'required'
        ]);

        $this->request = $request;
    }


    /**
     * @return mixed
     */
    public function create()
    {
        $exist = Mjerilo::exist(
            $this->request->sn,
            $this->request->tip_id,
            $this->request->sifra_id
        );

        if ($exist) {
            return 'exist';
        }

        return $this->insertGetId([
            'tip_id'       => $this->request->tip_id,
            'sifra_id'     => $this->request->sifra_id,
            'kreator'      => auth()->user()->id,
            'sn'           => str_replace(' ', '', $this->request->sn),
            'max_mjerenje' => $this->request->max_mjerenje,
            'created_at'   => Carbon::now(),
            'updated_at'   => Carbon::now()
        ]);
    }


    /**
     * @return bool
     */
    public function edit()
    {
        return $this->update([
            'tip_id'       => $this->request->tip_id,
            'sifra_id'     => $this->request->sifra_id,
            'sn'           => str_replace(' ', '', $this->request->sn),
            'max_mjerenje' => $this->request->max_mjerenje,
            'updated_at'   => Carbon::now()
        ]);
    }


    /**
     * @param string $search
     *
     * @return Builder
     */
    public function listSearch($search = '', bool $pod_ovjerom = false, Zahtjev $zahtjev = null, bool $permission = false): Builder
    {
        $query = ($this)->newQuery();

        if ($search == '') {
            return $query->where('sn', 'empty_agmedia_remover');
        }

        if ($search != '') {
            $query->where(function ($query) use ($search, $permission) {
                $query->where('sn', 'like', $search . '%');
                      /*->orWhereHas('tip', function ($subquery) use ($search) {
                          $subquery->where('oznaka', 'like', $search . '%')
                                   ->orWhere('naziv', 'like', $search . '%')
                                   ->orWhere('brand', 'like', $search . '%');
                      })
                      ->orWhereHas('sifra', function ($subquery) use ($search) {
                          $subquery->where('sifra', 'like', $search . '%')
                                   ->orWhere('naziv', 'like', $search . '%');
                      })
                      ->orWhereHas('grupa', function ($subquery) use ($search) {
                          $subquery->where('name', 'like', $search . '%');
                      })*/
                if ( ! $permission) {
                    $query->whereHas('grupa', function ($subquery) use ($search) {
                        $subquery->whereIn('grupe.id', auth()->user()->grupe()->pluck('id'));
                    });
                }
            });
        }

        if ($pod_ovjerom) {
            $query->where(function ($query) {
                $ovjere = Ovjera::whereHas('zahtjev', function ($subquery) {
                    $subquery->where('mjeritelj_id', auth()->user()->id);
                })->where('datum_valjanosti', '>', now())
                                ->pluck('mjerilo_id')
                                ->unique();

                $query->whereNotIn('id', $ovjere);
            });
        }

        if ($zahtjev) {
            $query->whereNotIn('id', $zahtjev->ovjere()->pluck('mjerilo_id'));
        }

        if ($permission) {
            if ( ! \Bouncer::can('view-mjerila')) {
                // Korisnik
                if (\Bouncer::can('view-own-mjerila')) {
                    if (auth()->user()->data) {
                        $query->where('kreator', auth()->user()->id);
                    }
                }

                // Voditelj
                if (\Bouncer::can('view-group-mjerila')) {
                    if (auth()->user()->data) {
                        $users = auth()->user()->institucija->users()->get()->pluck('id')->flatten();

                        $query->whereIn('kreator', $users);
                    }
                }
            }
        }

        return $query->with([/*'grupa', */ 'tip', 'sifra']);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function history()
    {
        $query = ($this->ovjere())->newQuery();

        if ( ! \Bouncer::can('view-all-ovjera')) {
            $query->whereHas('zahtjev', function ($query) {
                $query->whereHas('mjeritelj', function ($sub) {
                    $sub->where('mjeritelj_id', auth()->user()->id);
                });
            });
        }

        return $query->orderBy('datum_ovjere', 'desc')->get();
    }


    /**
     * @param string $sn
     *
     * @return mixed
     */
    public static function resolveByName(string $sn)
    {
        return self::where('sn', $sn)->first();
    }


    /**
     * @return int
     */
    public function isCopy(): int
    {
        $sn = str_replace(' ', '', $this->request->sn);

        $has_sn = Mjerilo::where('sn', $sn)->first();

        if ($has_sn) {
            $exist = Mjerilo::where('sn', $sn)->where('tip_id', $this->request->tip_id)->where('sifra_id', $this->request->sifra_id)->first();

            if ($exist->id != $this->id) {
                return 1;
            }
        }

        return 0;
    }


    /**
     * @param string $sn
     * @param int    $tip_id
     * @param int    $sifra_id
     *
     * @return int
     */
    public static function exist(string $sn, int $tip_id, int $sifra_id): int
    {
        $has_sn = Mjerilo::where('sn', $sn)->first();

        if ($has_sn) {
            $exist = Mjerilo::where('sn', $sn)->where('tip_id', $tip_id)->where('sifra_id', $sifra_id)->get();

            if ($exist->count()) {
                return $exist->count();
            }
        }

        return 0;
    }


    /**
     * @param Ovjera $ovjera
     * @param array  $data
     *
     * @return array|false[]|string[]
     */
    public static function resolveLabelsByStatus(Ovjera $ovjera, array $data): array
    {
        $naljepnica = '';
        $zig        = '';

        if ($data['vrsta'] == 'N') {
            $naljepnica = self::ifStatusDenied($data['status']) ?: $data['vrsta_input'];

        } else {
            $zig_title = 'HR ' . $ovjera->zahtjev->institucija->broj . '/' . $ovjera->zahtjev->mjeritelj->data->broj;

            $zig = self::ifStatusDenied($data['status']) ?: $zig_title;
        }

        return [
            'naljepnica' => $naljepnica,
            'zig'        => $zig,
            'valjanost'  => self::ifStatusDenied($data['status']) ? null : Sifra::getValjanostMjerila($ovjera->mjerilo->sifra)
        ];
    }


    /**
     * @param $status
     *
     * @return mixed
     */
    private static function ifStatusDenied(int $status)
    {
        if (in_array($status, [2, 3])) {
            return config('settings.ovjera.statuses')[$status];
        }

        return false;
    }
}
