<?php

namespace App\Models\Back\Catalog;

use App\Models\Back\User\GroupToKorisnik;
use App\Models\Back\User\GroupToMjeritelj;
use App\Models\Back\User\GroupToOvlastenoTijelo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Sifra extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'sifre';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function grupa()
    {
        return $this->belongsTo(Grupa::class, 'group_id', 'id');
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    public function validateRequest(Request $request)
    {
        return $request->validate([
            'sifra'             => 'required',
            'naziv'             => 'required',
            'ovjerno_razdoblje' => 'required',
            'tarifa'            => 'required',
            'group_id'          => 'required'
        ]);
    }


    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function create(Request $request)
    {
        return $this->insertGetId([
            'group_id'          => intval($request->group_id),
            'sifra'             => $request->sifra,
            'naziv'             => $request->naziv,
            'ovjerno_razdoblje' => $request->ovjerno_razdoblje,
            'tarifa'            => str_replace(',', '.', $request->tarifa),
            'naljepnica'        => (isset($request->naljepnica) and $request->naljepnica == 'on') ? 1 : 0,
            'status'            => (isset($request->status) and $request->status == 'on') ? 1 : 0,
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now()
        ]);
    }


    /**
     * @param Request $request
     *
     * @return bool
     */
    public function edit(Request $request)
    {
        return $this->update([
            'group_id'          => intval($request->group_id),
            'sifra'             => $request->sifra,
            'naziv'             => $request->naziv,
            'ovjerno_razdoblje' => $request->ovjerno_razdoblje,
            'tarifa'            => str_replace(',', '.', $request->tarifa),
            'naljepnica'        => (isset($request->naljepnica) and $request->naljepnica == 'on') ? 1 : 0,
            'status'            => (isset($request->status) and $request->status == 'on') ? 1 : 0,
            'updated_at'        => Carbon::now()
        ]);
    }


    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listSearch($search = ''): Builder
    {
        $query = ($this)->newQuery();

        if ($search != '') {
            $query->where(function ($query) use ($search) {
                $query->where('sifra', 'like', $search . '%')
                      ->orWhere('naziv', 'like', $search . '%');
            });
        }

        $ids = [];

        if ( ! \Bouncer::can('*') && ! \Bouncer::can('view-sifre')) {
            if (\Bouncer::can('view-own-sifre') || \Bouncer::can('view-group-sifre')) {
                if (auth()->user()->data) {
                    $ids = GroupToKorisnik::where('user_data_id', auth()->user()->id)->pluck('group_id')->flatten();
                }

                $query->whereIn('group_id', $ids);
            }
        }

        $query->orderByRaw('CAST(sifra AS DECIMAL)')/*->orderBy('sifra')*/
              ->with('grupa');

        return $query;
    }


    /**
     * @param Sifra $sifra
     * @param null  $specific_date
     *
     * @return Carbon|\Illuminate\Support\Carbon|null
     */
    public static function getValjanostMjerila(Sifra $sifra, $specific_date = null)
    {
        if ($specific_date) {
            $date = Carbon::make($specific_date)->addYears($sifra->ovjerno_razdoblje);
        } else {
            $date = now()->addYears($sifra->ovjerno_razdoblje);
        }

        if ($sifra->naljepnica) {
            return $date->endOfMonth();
        } else {
            return $date->endOfYear();
        }
    }
}
