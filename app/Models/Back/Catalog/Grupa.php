<?php

namespace App\Models\Back\Catalog;

use App\Models\Back\User\GroupToKorisnik;
use App\Models\Back\User\GroupToMjeritelj;
use App\Models\Back\User\GroupToOvlastenoTijelo;
use App\Models\Back\User\Mjeritelj;
use App\Models\Back\User\OvlastenoTijelo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Grupa extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'grupe';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];


    /**
     * @return \Illuminate\Support\Collection
     */
    public function parents()
    {
        return static::where('parent_id', '==', '')->pluck('name', 'id');
    }


    public function parent()
    {
        return $this->hasOne(Grupa::class, 'id', 'parent_id');
    }


    /**
     * @return array
     */
    public function adminList()
    {
        $query = ($this)->newQuery();

        $query = $this->search($query, true);

        $query = $query->orderBy('sort_order')->get()->groupBy('parent_id');
        $groups = [];

        //dd($query);

        foreach ($query as $key => $_groups) {
            if ( ! $key) {
                $groups = [];

                foreach ($_groups as $index => $value) {
                    if (isset($query[$value->id])) {
                        $value->podgrupa = $query[$value->id];

                        if ($value->top) {
                            $groups[] = $value;
                        }

                    } else {
                        if ($value->top) {
                            $value->podgrupa = null;
                            $groups[]        = $value;
                        }
                    }
                }
            }
        }

        return $groups;
    }


    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function create(Request $request)
    {
        $parent = $this->isParent($request);

        return $this->insertGetId([
            'name'             => $request->name,
            'description'      => $request->description,
            'parent_id'        => $parent['parent'],
            'top'              => $parent['top'],
            'sort_order'       => $request->sort_order ?: 0,
            'status'           => (isset($request->status) and $request->status == 'on') ? 1 : 0,
            'slug'             => Str::slug($request->name),
            'created_at'       => Carbon::now(),
            'updated_at'       => Carbon::now()
        ]);
    }


    /**
     * @param Request $request
     *
     * @return bool
     */
    public function edit(Request $request)
    {
        $parent = $this->isParent($request);

        return $this->update([
            'name'             => $request->name,
            'description'      => $request->description,
            'parent_id'        => $parent['parent'],
            'top'              => $parent['top'],
            'sort_order'       => $request->sort_order,
            'status'           => (isset($request->status) and $request->status == 'on') ? 1 : 0,
            'slug'             => Str::slug($request->name),
            'updated_at'       => Carbon::now()
        ]);
    }


    /**
     * @param Request $request
     *
     * @return array
     */
    private function isParent(Request $request)
    {
        $top = (isset($request->top) and $request->top == 'on') ? 1 : 0;
        $parent = ( ! $top and isset($request->parent_id) and intval($request->parent_id)) ? intval($request->parent_id) : 0;

        return [
            'top' => $top,
            'parent' => $parent
        ];
    }


    /**
     * @param Builder $query
     *
     * @return Builder
     */
    private function search(Builder $query, bool $view = false): Builder
    {
        $ids = [];

        if ( ! \Bouncer::can('*') && ! \Bouncer::can('view-grupe')) {
            if (\Bouncer::can('view-own-grupe') || \Bouncer::can('view-group-grupe')) {
                if (auth()->user()->data) {
                    $ids = GroupToKorisnik::where('user_data_id', auth()->user()->id)->pluck('group_id')->flatten();

                    if ($view) {
                        foreach ($ids as $id) {
                            $group = $this->find($id);

                            if ($group->parent_id != 0 && ! $ids->contains($group->parent_id)) {
                                $ids->push($group->parent_id);
                            }
                        }
                    }
                }

                $query->whereIn('id', $ids);
            }
        }

        //dd($query->with('parent')->get());

        return $query;
    }
}
