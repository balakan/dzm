<?php

namespace App\Models\Back\Catalog;

use App\Models\Back\User\GroupToKorisnik;
use App\Models\Back\User\GroupToMjeritelj;
use App\Models\Back\User\GroupToOvlastenoTijelo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class Tip
 * @package App\Models\Back\Catalog
 */
class Tip extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'tipovi';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var Request 
     */
    protected $request;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function grupa()
    {
        return $this->belongsTo(Grupa::class, 'group_id', 'id');
    }


    /**
     * @param Request $request
     *
     * @return $this
     */
    public function validateRequest(Request $request)
    {
        $request->validate([
            'oznaka' => 'required',
            'naziv' => 'required',
            'brand' => 'required',
            'group_id' => 'required'
        ]);

        $this->request = $request;

        return $this;
    }


    /**
     * @return mixed
     */
    public function create()
    {
        $pdf = $this->resolvePDF();

        return $this->insertGetId([
            'group_id'   => intval($this->request->group_id),
            'oznaka'     => $this->request->oznaka,
            'naziv'      => $this->request->naziv,
            'brand'      => $this->request->brand,
            'pdf'        => $pdf,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }


    /**
     * @return bool
     */
    public function edit()
    {
        $pdf = $this->resolvePDF();

        return $this->update([
            'group_id'   => intval($this->request->group_id),
            'oznaka'     => $this->request->oznaka,
            'naziv'      => $this->request->naziv,
            'brand'      => $this->request->brand,
            'pdf'        => $pdf,
            'updated_at' => Carbon::now()
        ]);
    }


    /**
     * @return string
     */
    private function resolvePDF(): string
    {
        if ($this->request->has('pdf')) {{
            $this->request->validate([
                'pdf' => 'mimes:pdf|max:10240'
            ]);

            $file = $this->request->file('pdf');

            $name = Str::slug($file->getClientOriginalName());
            $name = Str::replaceLast('pdf', '.pdf', $name);

            $path = $file->storeAs('tipovi', $name, 'dokumenti');

            return 'dokumenti/' . $path;
        }}

        return '';
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public static function uniqueBrands()
    {
        return static::all()->unique('brand')->pluck('brand');
    }


    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listSearch($search = '', bool $permission = true): Builder
    {
        $query = ($this)->newQuery();

        if ($search == '' && $permission) {
            return $query->where('oznaka', 'empty_agmedia_remover');
        }

        $query->where(function ($query) use ($search) {
            $query->where('oznaka', 'like', $search . '%')
                  ->orWhere('naziv', 'like', $search . '%')
                  ->orWhere('brand', 'like', $search . '%');
        });

        $ids = [];

        if ( ! \Bouncer::can('*') && ! \Bouncer::can('view-tipovi')) {
            if (\Bouncer::can('view-own-tipovi') || \Bouncer::can('view-group-tipovi')) {
                if (auth()->user()->data) {
                    $ids = GroupToKorisnik::where('user_data_id', auth()->user()->id)->pluck('group_id')->flatten();
                }

                $query->whereIn('group_id', $ids);
            }
        }

        $query->orderBy('oznaka');

        return $query;
    }
}
