<?php

namespace App\Models\Back;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Chart
{
    
    /**
     * @var array
     */
    protected $request;

    /**
     * @var string[]
     */
    public $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    /**
     * @var string[]
     */
    public $month_names = ['Sij', 'Velj', 'Ožu', 'Tra', 'Svi', 'Lip', 'Srp', 'Kol', 'Ruj', 'Lis', 'Stu', 'Pro'];
    
    
    /**
     * Set chart data query params.
     *
     * @return array
     */
    public function setQueryParams(): array
    {
        return [
            'from'     => now()->startOfYear(),
            'to'       => now(),
            'iterator' => $this->months,
            'iterator_names' => $this->month_names,
            'group'    => 'm'
        ];
    }


    /**
     * @param Collection $data
     *
     * @return array
     */
    public function setDataByMonth(Collection $data):array
    {
        $response = new Collection();

        foreach ($this->months as $key => $month) {
            if ( ! $data->has($month)) {
                $response->put($month, [
                    'title' => $this->month_names[$key],
                    'value' => 0
                ]);
            } else {
                $sum = $this->sumOvjere($data[$month]);

                $response->put($month, [
                    'title' => $this->month_names[$key],
                    'value' => $sum
                ]);
            }
        }

        return array_values($response->toArray());
    }


    /**
     * @param Collection $ovjere
     *
     * @return int
     */
    public function sumOvjere(Collection $ovjere): int
    {
        $sum = 0;

        foreach ($ovjere as $month => $ovjera) {
            $sum += $ovjera->mjerilo->sifra->tarifa;
        }

        return $sum;
    }


    public function total(array $data)
    {
        $sum = 0;

        foreach ($data as $key => $month) {
            $sum += $month['value'];
        }

        return $sum;
    }
    
}
