<?php

namespace App\Models\Back\Ovjera;

use App\Models\Back\Ovjera;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MjestoOvjere extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'mjerilo_mjesto_ovjere';

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ovjera()
    {
        return $this->belongsTo(Ovjera::class, 'mjerilo_mjesto_ovjere_id', 'id');
    }


    /**
     * @param string $search
     *
     * @return Builder
     */
    public function listSearch($search = ''): Builder
    {
        $query = ($this)->newQuery();

        if ($search != '') {
            $query->where(function ($query) use ($search) {
                $query->where('address', 'like', $search . '%');
            });
        }

        return $query;
    }
}
