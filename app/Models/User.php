<?php

namespace App\Models;

use App\Models\Back\Catalog\Grupa;
use App\Models\Back\Ovjera;
use App\Models\Back\User\GroupToKorisnik;
use App\Models\Back\User\Institucija;
use App\Models\Back\User\Mjeritelj;
use App\Models\Back\User\OvlastenoTijelo;
use App\Models\Back\User\UserToMjeritelj;
use App\Models\Back\User\UserToTijelo;
use App\Models\Roles\Role;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable
{

    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(UserProfile::class, 'user_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function data()
    {
        return $this->hasOne(UserData::class, 'user_id', 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function institucija()
    {
        return $this->hasOneThrough(Institucija::class, UserData::class, 'user_id', 'id', 'id', 'institucija_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uloga()
    {
        return $this->hasOne(Role::class, 'name', 'role');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function grupe()
    {
        return $this->hasManyThrough(Grupa::class, GroupToKorisnik::class, 'user_data_id', 'id', 'id', 'group_id');
    }

    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }


    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }


    /**
     * @param        $query
     * @param string $role
     *
     * @return mixed
     */
    public function scopeRole($query, string $role = '')
    {
        return $query->where('role', $role);
    }

    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/

    /**
     * @param array $params
     *
     * @return Collection
     */
    public function totalByMonth(array $params): Collection
    {
        return Ovjera::chartData($params, $this);
    }


    /**
     * @param string $search
     * @param array  $state
     *
     * @return Builder
     */
    public function listSearch(string $search, array $state)
    {
        $query = (new User())->newQuery();

        if (isset($state['uloga']) && is_array($state['uloga']) && ! empty($state['uloga'])) {
            $query->whereIn('role', $state['uloga']);
        }

        if (isset($state['uloga']) && is_string($state['uloga']) && $state['uloga'] != '') {
            $query->orWhere('role', $state['uloga']);
        }

        if (isset($state['status']) && $state['status'] != 'all') {
            $query->orWhere('status', intval($state['status']));
        }

        $query->where(function ($query) use ($search, $state) {
            if (isset($state['ime']) && $state['ime']) {
                $query->orWhere('name', 'like', '%' . $search . '%');
                $query->orWhereHas('profile', function ($subquery) use ($search) {
                    $subquery->where('fname', 'like', $search . '%');
                    $subquery->orWhere('lname', 'like', $search . '%');
                });
            }

            if (isset($state['email']) && $state['email']) {
                $query->orWhere('email', 'like', '%' . $search . '%');
            }

            if (isset($state['institucija']) && $state['institucija']) {
                $query->orWhereHas('institucija', function ($subquery) use ($search) {
                    $subquery->where('naziv', 'like', '%' . $search . '%');
                });
            }
        });

        //dd($query->get()->toArray()); // ivo sanader, ivan cazin

        if ( ! \Bouncer::can('*') && ! \Bouncer::can('view-all-korisnici')) {
            // Mjeritelj
            if (\Bouncer::can('view-own-korisnici')) {
                $users = auth()->user()->institucija->users->where('id', auth()->user()->id)->pluck('id');

                $query->whereIn('id', $users);
            }

            // Voditelji
            if (\Bouncer::can('view-group-korisnici')) {
                $users = auth()->user()->institucija->users->pluck('id');

                $query->whereIn('id', $users);

                //dd($query->get()->toArray()); //  ivan cazin

                if (in_array(auth()->user()->role, ['voditelj', 'voditelj-dzm'])) {
                    $query->orWhere('id', auth()->user()->id);
                }

                //dd($query->get()->toArray()); //  testko testić, ivan cazin
            }
        }

        return $query->with('uloga');
    }


    /**
     * @param array $data
     *
     * @return false
     */
    public function addInstitutionData(array $data)
    {
        if (empty($data)) {
            return false;
        }

        $exist = null;

        if ($this->institucija) {
            $exist = UserData::where('user_id', $this->id)->where('institucija_id', $this->institucija->id)->first();
        }

        if ($exist) {
            return UserData::where('user_id', $this->id)->update([
                'institucija_id'      => $data['institucija_id'],
                'broj'                => $data['broj'],
                'datum_ovlastenja_do' => $data['datum_ovlastenja_do'] ? Carbon::make($data['datum_ovlastenja_do']) : null,
                'ispit'               => $data['ispit'] ? 1 : 0,
                'updated_at'          => Carbon::now()
            ]);
        }

        return UserData::insert([
            'user_id'             => $this->id,
            'institucija_id'      => $data['institucija_id'],
            'broj'                => $data['broj'],
            'datum_ovlastenja_od' => Carbon::now(),
            'datum_ovlastenja_do' => $data['datum_ovlastenja_do'] ? Carbon::make($data['datum_ovlastenja_do']) : null,
            'ispit'               => $data['ispit'] ? 1 : 0,
            'created_at'          => Carbon::now(),
            'updated_at'          => Carbon::now()
        ]);
    }


    /**
     * @return string
     */
    public static function writeAll_toJson()
    {
        $arr_users = User::all()->toArray();

        for ($i = 0; $i < count($arr_users); $i++) {
            $arr_users[$i]['password'] = User::find($arr_users[$i]['id'])->password;
            $arr_users[$i]['profile']  = User::find($arr_users[$i]['id'])->profile()->first()->toArray();
        }

        $arr['users'] = $arr_users;

        return Helper::writeJson('users', $arr);
    }

}
