<?php

namespace App\Providers;

use App\Models\UserProfile;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //
        Paginator::useBootstrap();
        //
        // App Settings - Admin setup
        view()->composer('*', function($view)
        {
            if (Auth::check()) {
                $setup = Cache::rememberForever('setup', function () {
                    return UserProfile::where('user_id', Auth::id())->first();
                });

                $view->with('setup', $setup);
            }
        });
    }
}
