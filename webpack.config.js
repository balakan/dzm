const Webpack = require("webpack");
const Path = require("path");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");
const FileManagerPlugin = require("filemanager-webpack-plugin");
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");

const opts = {
    rootDir: process.cwd(),
    devBuild: process.env.NODE_ENV !== "production"
};

module.exports = {
    entry: {
        app: "./resources/back/src/js/app.js",
        settings: "./resources/back/src/js/settings/index.js",
        light: "./resources/back/src/scss/light.scss",
        dark: "./resources/back/src/scss/dark.scss",
    },
    mode: process.env.NODE_ENV === "production" ? "production" : "development",
    devtool:
           process.env.NODE_ENV === "production" ? "none" : "inline-source-map",
    output: {
        path: Path.join(opts.rootDir, "public"),
        pathinfo: opts.devBuild,
        filename: "js/[name].js"
    },
    performance: { hints: false },
    optimization: {
        minimizer: [
            new TerserPlugin({
                parallel: true,
                terserOptions: {
                    ecma: 5
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },
    plugins: [
        // Remove empty js files from /public
        new FixStyleOnlyEntriesPlugin(),
        // Extract css files to seperate bundle
        new MiniCssExtractPlugin({
            filename: "css/[name].css",
            chunkFilename: "css/[id].css"
        }),
        // jQuery and PopperJS
        new Webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            jquery: "jquery",
            "window.$": "jquery",
            "window.jQuery": "jquery",
            Popper: ["popper.js", "default"]
        }),
        // Copy fonts and images to public
        new CopyWebpackPlugin({
            patterns: [
                { from: "resources/back/src/fonts", to: "fonts" },
                { from: "resources/back/src/img", to: "img" }
            ]
        }),
        // Speed up webpack build
        new HardSourceWebpackPlugin(),
        // Copy public folder to docs/public
        new FileManagerPlugin({
            /*onEnd: {
              copy: [{ source: "./public/!**!/!*", destination: "./docs" }]
            }*/
        }),
        // Ignore momentjs locales
        new Webpack.IgnorePlugin({
            resourceRegExp: /^\.\/locale$/,
            contextRegExp: /moment$/
        })
    ],
    module: {
        rules: [
            // Babel-loader
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: ["babel-loader?cacheDirectory=true"]
            },
            // Css-loader & sass-loader
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                    "sass-loader"
                ]
            },
            // Load fonts
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "/[name].[ext]",
                            outputPath: "fonts/",
                            publicPath: "../fonts/"
                        }
                    }
                ]
            },
            // Load images
            {
                test: /\.(png|jpg|jpeg|gif)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "img/",
                            publicPath: "../img/"
                        }
                    }
                ]
            },
            // Expose loader
            {
                test: require.resolve("jquery"),
                use: [
                    {
                        loader: "expose-loader",
                        options: "jQuery"
                    },
                    {
                        loader: "expose-loader",
                        options: "$"
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: [".js", ".scss"],
        modules: ["node_modules"],
        alias: {
            request$: "xhr"
        }
    },
    devServer: {
        contentBase: Path.join(__dirname, "docs"),
        compress: true,
        port: 8080,
        open: true
    }
};
