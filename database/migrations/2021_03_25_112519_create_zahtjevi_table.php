<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Silber\Bouncer\Database\Models;

class CreateZahtjeviTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zahtjevi', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mjeritelj_id');
            $table->unsignedBigInteger('institucija_id');
            $table->string('klasa')->nullable();
            $table->timestamp('datum_pregleda')->nullable();
            $table->unsignedInteger('status')->default(1);
            $table->text('napomena')->nullable();
            $table->timestamps();

            $table->foreign('mjeritelj_id')
                  ->references('user_id')->on('user_data');

            $table->foreign('institucija_id')
                  ->references('id')->on('institucije');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zahtjevi');
    }
}
