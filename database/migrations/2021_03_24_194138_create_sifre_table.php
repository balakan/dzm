<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSifreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sifre', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('group_id');
            $table->string('sifra');
            $table->string('naziv');
            $table->integer('ovjerno_razdoblje');
            $table->decimal('tarifa', 15, 4)->default(0);
            $table->boolean('naljepnica')->default(0); // Bitno za računanje datuma važenja...
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('group_id')
                  ->references('id')->on('grupe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sifre');
    }
}
