<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Silber\Bouncer\Database\Models;

class CreateUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('address')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('state')->nullable();
            $table->longText('bio')->nullable();
            $table->string('phone')->nullable();
            $table->string('admin_color_scheme')->default('default')->nullable();
            $table->string('admin_sidebar_position')->default('left')->nullable();
            $table->string('admin_sidebar_behavior')->default('sticky')->nullable();
            $table->string('admin_sidebar_visibility')->default('default')->nullable();
            $table->string('admin_layout')->default('fluid')->nullable();
            $table->string('ovjere_view')->default('list')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
    }
}
