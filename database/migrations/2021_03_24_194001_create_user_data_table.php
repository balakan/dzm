<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Silber\Bouncer\Database\Models;

class CreateUserDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('institucija_id');
            $table->string('broj');
            $table->timestamp('datum_ovlastenja_od');
            $table->timestamp('datum_ovlastenja_do')->nullable();
            $table->boolean('ispit')->default(0);
            $table->timestamps();

            $table->foreign('institucija_id')
                  ->references('id')->on('institucije');

            $table->foreign('user_id')
                  ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mjeritelj');
    }
}
