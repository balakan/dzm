<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Silber\Bouncer\Database\Models;

class CreateOvjereTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ovjere', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('zahtjev_id');
            $table->unsignedBigInteger('mjerilo_id');
            $table->unsignedBigInteger('mjerilo_vlasnik_id');
            $table->unsignedBigInteger('mjerilo_mjesto_ovjere_id');
            $table->unsignedBigInteger('mjerilo_mjesto_postavljanja_id');
            $table->string('klasa')->nullable();
            $table->string('vrsta');
            $table->string('naljepnica')->nullable();
            $table->string('zig')->nullable();
            $table->boolean('ima_ovjernicu')->default(0);
            $table->string('ovjernica')->nullable();
            $table->timestamp('datum_ovjere')->nullable();
            $table->timestamp('datum_valjanosti')->nullable();
            $table->unsignedInteger('status')->default(1);
            $table->text('napomena')->nullable();
            $table->timestamps();

            $table->foreign('zahtjev_id')
                  ->references('id')->on('zahtjevi');

            $table->foreign('mjerilo_id')
                  ->references('id')->on('mjerila');

            $table->foreign('mjerilo_vlasnik_id')
                  ->references('id')->on('mjerilo_vlasnik');

            $table->foreign('mjerilo_mjesto_ovjere_id')
                  ->references('id')->on('mjerilo_mjesto_ovjere');

            $table->foreign('mjerilo_mjesto_postavljanja_id')
                  ->references('id')->on('mjerilo_mjesto_postavljanja');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ovjere');
    }
}
