<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMjerilaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mjerila', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tip_id');
            $table->unsignedBigInteger('sifra_id');
            $table->unsignedBigInteger('kreator');
            $table->string('sn');
            $table->string('max_mjerenje');
            $table->timestamps();

            $table->foreign('tip_id')
                  ->references('id')->on('tipovi');

            $table->foreign('sifra_id')
                  ->references('id')->on('sifre');

            $table->foreign('kreator')
                  ->references('user_id')->on('user_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mjerila');
    }
}
