<?php

namespace Database\Seeders;

use App\Models\Back\Catalog\Tip;
use App\Models\Helper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TipoviSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipovi = Helper::load_PP_Collection('tipovi');

        foreach ($tipovi['tipovi'] as $tip) {
            Tip::insertGetId([
                'group_id'   => $tip['group_id'],
                'oznaka'     => $tip['oznaka'],
                'naziv'      => $tip['naziv'],
                'brand'      => $tip['brand'],
                'pdf'        => $tip['pdf'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
