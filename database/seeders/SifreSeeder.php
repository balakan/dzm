<?php

namespace Database\Seeders;

use App\Models\Back\Catalog\Sifra;
use App\Models\Helper;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SifreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sifre = Helper::load_PP_Collection('sifre');

        foreach ($sifre['sifre'] as $sifra) {
            Sifra::insertGetId([
                'group_id'          => intval($sifra['group_id']),
                'sifra'             => $sifra['sifra'],
                'naziv'             => $sifra['naziv'],
                'ovjerno_razdoblje' => $sifra['ovjerno_razdoblje'],
                'tarifa'            => $sifra['tarifa'],
                'naljepnica'        => 0,
                'status'            => 1,
                'created_at'        => Carbon::make($sifra['created_at']),
                'updated_at'        => Carbon::make($sifra['updated_at'])
            ]);
        }
    }
}
