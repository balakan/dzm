<?php

namespace Database\Seeders;

use App\Models\Back\User\Institucija;
use App\Models\Helper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class InstitucijeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $institucije = Helper::load_PP_Collection('institucije');

        foreach ($institucije['institucije'] as $item) {
            Institucija::insertGetId([
                'parent_id'           => $item['parent_id'],
                'broj'                => $item['broj'],
                'naziv'               => $item['naziv'],
                'address'             => $item['address'],
                'zip'                 => $item['zip'],
                'city'                => $item['city'],
                'region'              => $item['region'],
                'phone'               => $item['phone'],
                'email'               => $item['email'],
                'datum_ovlastenja_od' => $item['datum_ovlastenja_od'],
                'datum_ovlastenja_do' => $item['datum_ovlastenja_do'],
                'status'              => $item['status'],
                'created_at'          => Carbon::now(),
                'updated_at'          => Carbon::now()
            ]);
        }
    }
}
