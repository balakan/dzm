<?php

namespace Database\Seeders;

use App\Models\Back\Catalog\Grupa;
use App\Models\Back\Catalog\Mjerilo;
use App\Models\Back\Catalog\Sifra;
use App\Models\Back\Catalog\Tip;
use App\Models\Back\Ovjera;
use App\Models\Back\Ovjera\MjestoOvjere;
use App\Models\Back\Ovjera\MjestoPostavljanja;
use App\Models\Back\Ovjera\Vlasnik;
use App\Models\Back\User\Mjeritelj;
use App\Models\Back\User\OvlastenoTijelo;
use App\Models\Back\Zahtjev;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DummyUsersSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(int $number = 100)
    {

        // Šifre
        DB::insert(
            "INSERT INTO `sifre` (`id`, `group_id`, `sifra`, `naziv`, `ovjerno_razdoblje`, `tarifa`, `status`, `created_at`, `updated_at`)
                    VALUES
                        (1, 9, '001', 'Sifra 1', 1, 1200, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (2, 10, '002', 'Sifra 2', 1, 350, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (3, 11, '003', 'Sifra 3', 1, 1200, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (4, 12, '004', 'Sifra 4', 1, 500, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (5, 13, '005', 'Sifra 5', 1, 1200, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (6, 14, '006', 'Sifra 6', 1, 350, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (7, 15, '007', 'Sifra 7', 1, 200, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (8, 16, '008', 'Sifra 8', 1, 1200, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (9, 17, '009', 'Sifra 9', 1, 350, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (10, 18, '010', 'Sifra 10', 1, 500, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (11, 9, '011', 'Sifra 11', 1, 1200, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (12, 10, '012', 'Sifra 12', 1, 600, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (13, 11, '013', 'Sifra 13', 1, 1200, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (14, 12, '014', 'Sifra 14', 1, 350, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (15, 9, '015', 'Sifra 15', 1, 600, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (16, 10, '016', 'Sifra 16', 1, 700, 1, '2021-01-03 00:00:03', '2021-01-03 00:00:03');"
        );


        /*******************************************************************************
        *                                Copyright : AGmedia                           *
        *                              email: filip@agmedia.hr                         *
        *******************************************************************************/

        // KORISNICI - USERS
        $users_count = $number;

        $iter = [
            'zavod'      => abs($users_count * (1 / 50)),
            'inspekcija' => abs($users_count * (1 / 15)),
            'tijelo'     => abs($users_count * (1 / 7)),
            'mjeritelj'  => abs($users_count * (1 / 2)),
        ];

        $public = 0;

        foreach ($iter as $role => $count) {
            $public = $public + $count;

            $users = UserProfile::factory()->count($count)->create();

            foreach ($users as $user_profile) {
                $user = User::find($user_profile->user_id);

                $user->update([
                    'role' => $role
                ]);

                $user->assign($role);
            }
        }

        $public_users = UserProfile::factory()->count($users_count - $public)->create();

        foreach ($public_users as $public_user) {
            $user = User::find($public_user->user_id);

            $user->assign('public');
        }

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/

        // Ovlaštena tijela
        $users = User::where('role', 'tijelo')->get();

        foreach ($users as $key => $user) {
            if ($key < 4) {
                $ot = OvlastenoTijelo::factory()->create();
            } else {
                $ot = OvlastenoTijelo::factory()->parent(rand(1, 4))->create();
            }

            DB::table('users_to_ovlasteno_tijelo')->insert([
                'user_id'             => $user->id,
                'ovlasteno_tijelo_id' => $ot->id
            ]);
        }

        $ots = OvlastenoTijelo::all();

        // Mjeritelji
        $users = User::where('role', 'mjeritelj')->get();

        foreach ($users as $key => $user) {
            $mjeritelj = Mjeritelj::factory()
                                  ->ot($ots->random()->id)
                                  ->create();

            DB::table('users_to_mjeritelj')->insert([
                'user_id'      => $user->id,
                'mjeritelj_id' => $mjeritelj->id
            ]);
        }

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/
        // Zahtjevi

        $zahtjevi_count = floor($number / 3);

        $mjeritelji = Mjeritelj::all();

        for ($i = 0; $i < $zahtjevi_count; $i++) {
            Zahtjev::factory()
                   ->mjeritelj($mjeritelji->random()->id)
                   /*->ot($ots->random()->id)*/
                   ->create();
        }

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/
        // Tipovi

        $tipovi_count = $number;

        $grupe = Grupa::where('parent_id', '!=', 0)->get();

        for ($i = 0; $i < $tipovi_count; $i++) {
            Tip::factory()->grupa($grupe->random()->id)->create();
        }

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/
        // Mjerila

        $mjerila_count = floor($number * 1.3);

        $tipovi = Tip::all();
        $sifre  = Sifra::all();

        for ($i = 0; $i < $mjerila_count; $i++) {
            Mjerilo::factory()
                   ->tip($tipovi->random()->id)
                   ->sifra($sifre->random()->id)
                   ->create();
        }

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/
        // Ovjere

        $count = floor($number / 3);

        for ($i = 0; $i < $count; $i++) {
            MjestoPostavljanja::factory()->create();
            MjestoOvjere::factory()->create();
            Vlasnik::factory()->create();
        }

        //
        $ovjere_count = floor($number * 1.8);

        $zahtjevi        = Zahtjev::all();
        $mjerila         = Mjerilo::all();
        $vlasnici        = Vlasnik::all();
        $mj_postavljanja = MjestoPostavljanja::all();
        $mj_ovjere       = MjestoOvjere::all();

        for ($i = 0; $i < $ovjere_count; $i++) {
            $zahtjev = $zahtjevi->random();

            $ovjera = Ovjera::factory()
                            ->mjeritelj($zahtjev->mjeritelj_id)
                            /*->tijelo($zahtjev->ovlasteno_tijelo_id)*/
                            ->mjerilo($mjerila->random()->id)
                            ->vlasnik($vlasnici->random()->id)
                            ->mjestoOvjere($mj_ovjere->random()->id)
                            ->mjestoPostavljanja($mj_postavljanja->random()->id)
                            ->create();

            DB::table('ovjere_to_zahtjevi')->insert([
                'zahtjev_id' => $zahtjev->id,
                'ovjera_id'  => $ovjera->id
            ]);
        }


        /*******************************************************************************
        *                                Copyright : AGmedia                           *
        *                              email: filip@agmedia.hr                         *
        *******************************************************************************/

    }
}
