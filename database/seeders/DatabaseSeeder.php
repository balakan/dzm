<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Un Guard model
        Model::unguard();

        $this->command->call('migrate:fresh');

        $this->command->info('Refreshing database...');
        $this->command->comment('Refreshed!');
        $this->command->newLine();

        $this->call(UserSedder::class);
        $this->command->line('Users created!');
        $this->command->newLine();

        $this->call(GroupSeeder::class);
        $this->command->line('Groups created!');
        $this->command->newLine();

        $this->call(SifreSeeder::class);
        $this->command->line('Šifre created!');
        $this->command->newLine();

        $this->call(TipoviSeeder::class);
        $this->command->line('Tipovi created!');
        $this->command->newLine();

        $this->call(InstitucijeSeeder::class);
        $this->command->line('Institucije created!');
        $this->command->newLine();

        $this->call(BouncerSedder::class);
        $this->command->line('Roles and permissions created and initialized!');
        $this->command->newLine();

        /*$dummy = $this->command->askWithCompletion('Do you want dummy data with that? (y/n)', ['y', 'n'], 'n');

        if ($dummy == 'y') {
            $number = $this->command->ask('How many? Give me a number.', 100);

            $this->call(DummyUsersSeeder::class, false, compact('number'));
            $this->command->line('Dummy data seeded!');
        }*/

        $this->command->newLine();
        $this->command->line('...');
        $this->command->comment('Enjoy your app!');
        $this->command->line('...');

        // ReGuard model
        Model::reguard();
    }
}
