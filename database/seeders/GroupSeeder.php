<?php

namespace Database\Seeders;

use App\Models\Back\Catalog\Grupa;
use App\Models\Back\Catalog\Tip;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Groups
        DB::insert(
            "INSERT INTO `grupe` (`id`, `name`, `description`, `parent_id`, `top`, `sort_order`, `status`, `slug`, `created_at`, `updated_at`)
                    VALUES
                        (1, 'Duljina', '', 0, 1, 0, 1, 'duljina', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (2, 'Obujam', '', 0, 1, 1, 1, 'obujam', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (3, 'Masa', '', 0, 1, 2, 1, 'masa', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (4, 'Tlak', '', 0, 1, 3, 1, 'tlak', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (5, 'Mjerila specijalne namjere', '', 0, 1, 4, 1, 'mjerila-specijalne-namjere', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (6, 'Električna mjerila', '', 0, 1, 5, 1, 'elektricna-mjerila', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (7, 'Naknade', '', 0, 1, 6, 1, 'naknade', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (8, 'Ostalo', '', 0, 1, 7, 1, 'ostalo', '2021-01-03 00:00:03', '2021-01-03 00:00:03');"
        );

        // Subgroups
        DB::insert(
            "INSERT INTO `grupe` (`id`, `name`, `description`, `parent_id`, `top`, `sort_order`, `status`, `slug`, `created_at`, `updated_at`)
                    VALUES
                        (9, 'Spremnici', '', 2, 0, 0, 1, 'spremnici', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (10, 'Vodomjeri', '', 2, 0, 1, 1, 'vodomjeri', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (11, 'Mjerila toplinske energije', '', 2, 0, 2, 1, 'mjerila-toplinske-energije', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (12, 'Mjerila i mjerni sustavi za mjerenje količina tekučina različitih od vode', '', 2, 0, 3, 1, 'mjerila-i-mjerni-sustavi-za-mjerenje-količina-tekučina-različitih-od-vode', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (13, 'Plinomjeri', '', 2, 0, 4, 1, 'plinomjeri', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (14, 'Utezi', '', 3, 0, 0, 1, 'utezi', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (15, 'Vage', '', 3, 0, 1, 1, 'vage', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (16, 'Električna brojila', '', 6, 0, 0, 1, 'elektricna-brojila', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (17, 'Mjerni transformatori', '', 6, 0, 1, 1, 'mjerni-transformatori', '2021-01-03 00:00:03', '2021-01-03 00:00:03'),
                        (18, 'Mjerila za ispitivanje električnih instalacija', '', 6, 0, 2, 1, 'mjerila-za-ispitivanje-elektricnih-instalacija', '2021-01-03 00:00:03', '2021-01-03 00:00:03');"
        );

    }
}
