<?php

namespace Database\Seeders;

use App\Models\Helper;
use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserSedder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admins
        /*DB::insert(
            "INSERT INTO `users` (`name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `role`, `status`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
              ('Filip Jankoski', 'filip@agmedia.hr', NOW(), '" . bcrypt('majamaja001') . "', '', '', 'admin', 1, '', 1, '', NOW(), NOW()),
              ('Tomislav Jureša', 'tomislav@agmedia.hr', NOW(), '" . bcrypt('bakanal') . "', '', '', 'admin', 1, '', 1, '', NOW(), NOW()),
              ('Boris Mikić', 'boris.mikic@dzm.hr', NOW(), '" . bcrypt('bm13bm14#') . "', '', '', 'zavod', 1, '', 1, '', NOW(), NOW())"
        );

        // create admins details
        DB::insert(
            "INSERT INTO `user_profile` (`user_id`, `fname`, `lname`, `address`, `zip`, `city`, `region`, `state`, `bio`, `phone`, `admin_color_scheme`, `admin_sidebar_position`, `admin_sidebar_behavior`, `admin_sidebar_visibility`, `admin_layout`, `created_at`, `updated_at`) VALUES
              (1, 'Filip', 'Jankoski', 'Kovačića 23', '44320', 'Kutina', 'Sisačko-Moslavačka', 'Hrvatska', '', '', 'default', 'left', 'sticky', 'default', 'fluid', NOW(), NOW()),
              (2, 'Tomislav', 'Jureša', 'Malešnica bb', '10000', 'Zagreb', 'Zagrebačka', 'Hrvatska', '', '', 'default', 'left', 'sticky', 'default', 'fluid', NOW(), NOW()),
              (3, 'Boris', 'Mikić', 'Zagreb bb', '10000', 'Zagreb', 'Zagrebačka', 'Hrvatska', '', '', 'default', 'left', 'sticky', 'default', 'fluid', NOW(), NOW())"
        );*/

        $users = Helper::load_PP_Collection('users');

        foreach ($users['users'] as $user) {
            $id = User::insertGetId([
                'name'     => $user['name'],
                'email'    => $user['email'],
                'role'     => $user['role'],
                'status'   => 1,
                'password' => $user['password'],
            ]);

            UserProfile::insertGetId([
                'user_id'                  => $id,
                'fname'                    => $user['profile']['fname'],
                'lname'                    => $user['profile']['lname'],
                'address'                  => $user['profile']['address'],
                'zip'                      => $user['profile']['zip'],
                'city'                     => $user['profile']['city'],
                'region'                   => $user['profile']['region'],
                'state'                    => $user['profile']['state'],
                'bio'                      => $user['profile']['bio'],
                'phone'                    => $user['profile']['phone'],
                'admin_color_scheme'       => $user['profile']['admin_color_scheme'],
                'admin_sidebar_position'   => $user['profile']['admin_sidebar_position'],
                'admin_sidebar_behavior'   => $user['profile']['admin_sidebar_behavior'],
                'admin_layout'             => $user['profile']['admin_layout'],
                'admin_sidebar_visibility' => $user['profile']['admin_sidebar_visibility'],
                'ovjere_view'              => $user['profile']['ovjere_view']
            ]);
        }
    }
}
