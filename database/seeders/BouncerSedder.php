<?php

namespace Database\Seeders;

use App\Models\Helper;
use App\Models\User;
use Illuminate\Database\Seeder;
use Bouncer;

class BouncerSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*******************************************************************************
        *                                Copyright : AGmedia                           *
        *                              email: filip@agmedia.hr                         *
        *******************************************************************************/
        //
        // ROLES
        //
        // ADMIN
        $admin = Bouncer::role()->firstOrCreate([
            'name' => 'admin',
            'title' => 'Super-admin',
        ]);

        // ZAVOD
        $zavod = Bouncer::role()->firstOrCreate([
            'name' => 'zavod',
            'title' => 'Administrator DZM',
        ]);

        // INSPEKCIJA
        $inspekcija = Bouncer::role()->firstOrCreate([
            'name' => 'inspekcija',
            'title' => 'Inspekcija DZM',
        ]);

        // ŠEF OVLAŠTENOG TIJELA
        $voditelj_dzm = Bouncer::role()->firstOrCreate([
            'name' => 'voditelj-dzm',
            'title' => 'Voditelj DZM',
        ]);

        // OVLAŠTENO TIJELO
        $voditelj = Bouncer::role()->firstOrCreate([
            'name' => 'voditelj',
            'title' => 'Voditelj',
        ]);

        // VIP MJERITELJ
        $mjeritelj_dzm = Bouncer::role()->firstOrCreate([
            'name' => 'mjeritelj-dzm',
            'title' => 'Mjeritelj DZM',
        ]);

        // MJERITELJ
        $mjeritelj = Bouncer::role()->firstOrCreate([
            'name' => 'mjeritelj',
            'title' => 'Mjeritelj',
        ]);

        // SERVIS MJERITELJ
        $mjeritelj_servis = Bouncer::role()->firstOrCreate([
            'name' => 'mjeritelj-servis',
            'title' => 'Servis',
        ]);

        // PUBLIC
        $public = Bouncer::role()->firstOrCreate([
            'name' => 'public',
            'title' => 'Javni Profil',
        ]);

        /*******************************************************************************
        *                                Copyright : AGmedia                           *
        *                              email: filip@agmedia.hr                         *
        *******************************************************************************/
        //
        // ABILITIES
        Bouncer::allow($admin)->everything();

        /*******************************************************************************
        *                                Copyright : AGmedia                           *
        *                              email: filip@agmedia.hr                         *
        *******************************************************************************/

        // KATALOG

        /**
         * GRUPE
         */
        $view_grupe = Bouncer::ability()->firstOrCreate([
            'name' => 'view-grupe',
            'title' => 'Pregled svih GRUPA mjerila'
        ]);

        $view_group_grupe = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-grupe',
            'title' => 'Pregled GRUPA mjerila za pojedinog Voditelja'
        ]);

        $view_own_grupe = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-grupe',
            'title' => 'Pregled GRUPA mjerila za pojedinog Mjeritelja'
        ]);

        $create_grupe = Bouncer::ability()->firstOrCreate([
            'name' => 'create-grupe',
            'title' => 'Kreiranje nove GRUPE mjerila'
        ]);

        $edit_grupe = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-grupe',
            'title' => 'Uređivanje GRUPE mjerila'
        ]);

        /**
         * Mjerila
         */
        $view_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'view-mjerila',
            'title' => 'Pregled svih MJERILA'
        ]);

        $search_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'search-mjerila',
            'title' => 'Pretraživanje MJERILA'
        ]);

        $view_group_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-mjerila',
            'title' => 'Pregled MJERILA za pojedinog Voditelja'
        ]);

        $view_own_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-mjerila',
            'title' => 'Pregled MJERILA za pojedinog Mjeritelja'
        ]);

        $create_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'create-mjerila',
            'title' => 'Kreiranje novog MJERILA'
        ]);

        $edit_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-mjerila',
            'title' => 'Uređivanje svih MJERILA'
        ]);

        $edit_group_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-group-mjerila',
            'title' => 'Uređivanje MJERILA za pojedinog Voditelja'
        ]);

        $edit_own_mjerila = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-own-mjerila',
            'title' => 'Uređivanje svojih MJERILA'
        ]);


        /**
         * ŠIFRE
         */
        $view_sifre = Bouncer::ability()->firstOrCreate([
            'name' => 'view-sifre',
            'title' => 'Pregled svih ŠIFRI mjerila'
        ]);

        $view_group_sifre = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-sifre',
            'title' => 'Pregled ŠIFRI mjerila za pojedinog Voditelja'
        ]);

        $view_own_sifre = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-sifre',
            'title' => 'Pregled ŠIFRI mjerila za pojedinog Mjeritelja'
        ]);

        $create_sifre = Bouncer::ability()->firstOrCreate([
            'name' => 'create-sifre',
            'title' => 'Kreiranje nove ŠIFRE mjerila'
        ]);

        $edit_sifre = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-sifre',
            'title' => 'Uređivanje ŠIFRE mjerila'
        ]);

        /**
         * TIPOVI
         */
        $view_tipovi = Bouncer::ability()->firstOrCreate([
            'name' => 'view-tipovi',
            'title' => 'Pregled svih TIPOVA mjerila'
        ]);

        $view_group_tipovi = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-tipovi',
            'title' => 'Pregled TIPOVA mjerila za pojedinog Voditelja'
        ]);

        $view_own_tipovi = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-tipovi',
            'title' => 'Pregled TIPOVA mjerila za pojedinog Mjeritelja'
        ]);

        $create_tipovi = Bouncer::ability()->firstOrCreate([
            'name' => 'create-tipovi',
            'title' => 'Kreiranje novih TIPOVA mjerila'
        ]);

        $edit_tipovi = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-tipovi',
            'title' => 'Uređivanje TIPOVA mjerila'
        ]);

        $pdf_tipovi = Bouncer::ability()->firstOrCreate([
            'name' => 'pdf-tipovi',
            'title' => 'Pregled PDF datoteka TIPOVA mjerila'
        ]);

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/

        /**
         *
         * KORISNICI
         */
        $view_all_institucije = Bouncer::ability()->firstOrCreate([
            'name' => 'view-all-institucije',
            'title' => 'Pregled svih INSTITUCIJA'
        ]);
        
        $view_own_institucije = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-institucije',
            'title' => 'Pregled svoje INSTITUCIJE'
        ]);

        $create_institucije = Bouncer::ability()->firstOrCreate([
            'name' => 'create-institucije',
            'title' => 'Kreiranje nove INSTITUCIJE'
        ]);

        $edit_all_institucije = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-all-institucije',
            'title' => 'Uređivanje svih INSTITUCIJA'
        ]);

        $edit_own_institucije = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-own-institucije',
            'title' => 'Uređivanje svoje INSTITUCIJE'
        ]);

        /**
         *
         * KORISNICI
         */
        $view_all_korisnici = Bouncer::ability()->firstOrCreate([
            'name' => 'view-all-korisnici',
            'title' => 'Pregled svih KORISNIKA.'
        ]);

        $view_group_korisnici = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-korisnici',
            'title' => 'Pregled KORISNIKA za pojedinog Voditelja'
        ]);

        $view_own_korisnici = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-korisnici',
            'title' => 'Pregled svog KORISNIKA'
        ]);

       /* $create_korisnici = Bouncer::ability()->firstOrCreate([
            'name' => 'create-korisnici',
            'title' => 'Kreiranje novog KORISNIKA'
        ]);*/

        $edit_all_korisnici = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-korisnici-group',
            'title' => 'Uređivanje grupa mjerila svih KORISNIKA'
        ]);

        /**
         *
         * PROFIL
         */
        $view_own_profil = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-profil',
            'title' => 'Pregled svog PROFILA'
        ]);

        $delete_own_profil = Bouncer::ability()->firstOrCreate([
            'name' => 'delete-own-profil',
            'title' => 'Brisanje svog PROFILA'
        ]);

        /**
         *
         * ULOGE
         */
        $view_roles = Bouncer::ability()->firstOrCreate([
            'name' => 'view-roles',
            'title' => 'Pregled svih ULOGA i OVLAŠTENJA'
        ]);

        $edit_roles = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-roles',
            'title' => 'Uređivanje ULOGA svih korisnika'
        ]);

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/

        /**
         *
         * ZAHTJEVI
         */
        $view_all_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'view-all-zahtjev',
            'title' => 'Pregled svih ZAHTJEVA'
        ]);

        $view_group_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-zahtjev',
            'title' => 'Pregled ZAHTJEVA za pojedinog Voditelja'
        ]);

        $view_own_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-zahtjev',
            'title' => 'Pregled svojih ZAHTJEVA'
        ]);

        $create_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'create-zahtjev',
            'title' => 'Kreiranje novog ZAHTJEVA'
        ]);

        $edit_all_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-all-zahtjev',
            'title' => 'Uređivanje svih ZAHTJEVA'
        ]);

        $edit_group_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-group-zahtjev',
            'title' => 'Uređivanje ZAHTJEVA za pojedinog Voditelja'
        ]);

        $edit_own_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-own-zahtjev',
            'title' => 'Uređivanje svojih ZAHTJEVA'
        ]);

        $select_mjeritelj_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'select-mjeritelj-zahtjev',
            'title' => 'Odabir mjeritelja ZAHTJEVA'
        ]);

        $change_zahtjev_status = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-zahtjev-status',
            'title' => 'Mijenjanje statusa na ZAHTJEVIMA'
        ]);

        $revert_zahtjev_status = Bouncer::ability()->firstOrCreate([
            'name' => 'revert-zahtjev-status',
            'title' => 'Vračanje nazad statusa na ZAHTJEVIMA'
        ]);

        $ovjera_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'ovjera-zahtjev',
            'title' => 'Ovjera ZAHTJEVA'
        ]);

        $search_mjeritelj_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'search-mjeritelj-zahtjev',
            'title' => 'Pretraga ZAHTJEVA po mjeritelju.'
        ]);

        $search_institucija_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'search-institucija-zahtjev',
            'title' => 'Pretraga ZAHTJEVA po instituciji.'
        ]);

        $search_oznaka_zahtjev = Bouncer::ability()->firstOrCreate([
            'name' => 'search-oznaka-zahtjev',
            'title' => 'Pretraga ZAHTJEVA po broju ovlaštenja mjeritelja.'
        ]);

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/

        /**
         *
         * OVJERE
         */
        $view_all_ovjera = Bouncer::ability()->firstOrCreate([
            'name' => 'view-all-ovjera',
            'title' => 'Pregled svih OVJERA'
        ]);

        $view_group_ovjera = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-ovjera',
            'title' => 'Pregled OVJERA za pojedinog Voditelja'
        ]);

        $view_own_ovjera = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-ovjera',
            'title' => 'Pregled svoje OVJERE'
        ]);

        $create_ovjera = Bouncer::ability()->firstOrCreate([
            'name' => 'create-ovjera',
            'title' => 'Kreiranje nove OVJERE'
        ]);

        $edit_all_ovjera = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-all-ovjera',
            'title' => 'Uređivanje svih OVJERA'
        ]);

        $edit_group_ovjera = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-group-ovjera',
            'title' => 'Uređivanje OVJERA za pojedinog Voditelja'
        ]);

        $edit_own_ovjera = Bouncer::ability()->firstOrCreate([
            'name' => 'edit-own-ovjera',
            'title' => 'Uređivanje svoje OVJERE'
        ]);

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/

        /**
         *
         * REPORT
         */
        $view_all_obracun = Bouncer::ability()->firstOrCreate([
            'name' => 'view-all-obracun',
            'title' => 'Pregled svih OBRAČUNA'
        ]);

        $view_group_obracun = Bouncer::ability()->firstOrCreate([
            'name' => 'view-group-obracun',
            'title' => 'Pregled OBRAČUNA za pojedinog Voditelja'
        ]);

        $view_own_obracun = Bouncer::ability()->firstOrCreate([
            'name' => 'view-own-obracun',
            'title' => 'Pregled svojih OBRAČUNA'
        ]);

        /*******************************************************************************
         *                                Copyright : AGmedia                           *
         *                              email: filip@agmedia.hr                         *
         *******************************************************************************/
        //
        // PERMISSIONS

        /**
         *
         * ZAVOD
         */
        Bouncer::allow($zavod)->to($view_grupe);
        Bouncer::allow($zavod)->to($create_grupe);
        Bouncer::allow($zavod)->to($edit_grupe);

        Bouncer::allow($zavod)->to($view_mjerila);
        Bouncer::allow($zavod)->to($search_mjerila);
        Bouncer::allow($zavod)->to($create_mjerila);
        Bouncer::allow($zavod)->to($edit_mjerila);

        Bouncer::allow($zavod)->to($view_sifre);
        Bouncer::allow($zavod)->to($create_sifre);
        Bouncer::allow($zavod)->to($edit_sifre);

        Bouncer::allow($zavod)->to($view_tipovi);
        Bouncer::allow($zavod)->to($create_tipovi);
        Bouncer::allow($zavod)->to($edit_tipovi);
        Bouncer::allow($zavod)->to($pdf_tipovi);

        Bouncer::allow($zavod)->to($view_all_institucije);
        Bouncer::allow($zavod)->to($create_institucije);
        Bouncer::allow($zavod)->to($edit_all_institucije);

        Bouncer::allow($zavod)->to($view_all_korisnici);
        /*Bouncer::allow($zavod)->to($create_korisnici);*/
        Bouncer::allow($zavod)->to($edit_all_korisnici);
        Bouncer::allow($zavod)->to($view_own_profil);

        Bouncer::allow($zavod)->to($view_roles);
        Bouncer::allow($zavod)->to($edit_roles);

        Bouncer::allow($zavod)->to($view_all_zahtjev);
        Bouncer::allow($zavod)->to($create_zahtjev);
        Bouncer::allow($zavod)->to($edit_all_zahtjev);
        Bouncer::allow($zavod)->to($change_zahtjev_status);
        Bouncer::allow($zavod)->to($revert_zahtjev_status);

        Bouncer::allow($zavod)->to($view_all_ovjera);
        Bouncer::allow($zavod)->to($create_ovjera);
        Bouncer::allow($zavod)->to($edit_all_ovjera);

        Bouncer::allow($zavod)->to($view_all_obracun);

        /**
         *
         * INSPEKCIJA
         */
        Bouncer::allow($inspekcija)->to($view_grupe);
        Bouncer::allow($inspekcija)->to($view_mjerila);
        Bouncer::allow($inspekcija)->to($search_mjerila);
        Bouncer::allow($inspekcija)->to($view_sifre);
        Bouncer::allow($inspekcija)->to($view_tipovi);
        Bouncer::allow($inspekcija)->to($pdf_tipovi);
        Bouncer::allow($inspekcija)->to($view_all_institucije);
        Bouncer::allow($inspekcija)->to($view_all_korisnici);
        Bouncer::allow($inspekcija)->to($view_own_profil);
        Bouncer::allow($inspekcija)->to($view_all_zahtjev);
        Bouncer::allow($inspekcija)->to($view_all_ovjera);
        Bouncer::allow($inspekcija)->to($view_all_obracun);

        /**
         *
         * VODITELJ DZM
         */
        Bouncer::allow($voditelj_dzm)->to($view_grupe);
        Bouncer::allow($voditelj_dzm)->to($view_mjerila);
        Bouncer::allow($voditelj_dzm)->to($search_mjerila);
        Bouncer::allow($voditelj_dzm)->to($create_mjerila);
        Bouncer::allow($voditelj_dzm)->to($edit_mjerila);
        Bouncer::allow($voditelj_dzm)->to($view_sifre);
        Bouncer::allow($voditelj_dzm)->to($view_tipovi);
        Bouncer::allow($voditelj_dzm)->to($pdf_tipovi);

        Bouncer::allow($voditelj_dzm)->to($view_own_institucije);
        Bouncer::allow($voditelj_dzm)->to($edit_own_institucije);

        Bouncer::allow($voditelj_dzm)->to($view_group_korisnici);
        Bouncer::allow($voditelj_dzm)->to($view_own_profil);

        Bouncer::allow($voditelj_dzm)->to($create_zahtjev);
        Bouncer::allow($voditelj_dzm)->to($view_group_zahtjev);
        Bouncer::allow($voditelj_dzm)->to($edit_group_zahtjev);
        Bouncer::allow($voditelj_dzm)->to($change_zahtjev_status);
        Bouncer::allow($voditelj_dzm)->to($revert_zahtjev_status);
        Bouncer::allow($voditelj_dzm)->to($ovjera_zahtjev);

        Bouncer::allow($voditelj_dzm)->to($create_ovjera);
        Bouncer::allow($voditelj_dzm)->to($view_group_ovjera);
        Bouncer::allow($voditelj_dzm)->to($edit_group_ovjera);

        Bouncer::allow($voditelj_dzm)->to($view_group_obracun);

        /**
         *
         * VODITELJ
         */
        Bouncer::allow($voditelj)->to($view_group_grupe);
        Bouncer::allow($voditelj)->to($search_mjerila);
        Bouncer::allow($voditelj)->to($view_group_mjerila);
        Bouncer::allow($voditelj)->to($create_mjerila);
        Bouncer::allow($voditelj)->to($edit_group_mjerila);
        Bouncer::allow($voditelj)->to($view_group_sifre);
        Bouncer::allow($voditelj)->to($view_group_tipovi);
        Bouncer::allow($voditelj)->to($pdf_tipovi);

        Bouncer::allow($voditelj)->to($view_own_institucije);
        Bouncer::allow($voditelj)->to($edit_own_institucije);

        Bouncer::allow($voditelj)->to($view_group_korisnici);
        Bouncer::allow($voditelj)->to($view_own_profil);

        Bouncer::allow($voditelj)->to($create_zahtjev);
        Bouncer::allow($voditelj)->to($view_group_zahtjev);
        Bouncer::allow($voditelj)->to($edit_group_zahtjev);
        Bouncer::allow($voditelj)->to($ovjera_zahtjev);

        Bouncer::allow($voditelj)->to($create_ovjera);
        Bouncer::allow($voditelj)->to($view_group_ovjera);
        Bouncer::allow($voditelj)->to($edit_group_ovjera);
        Bouncer::allow($voditelj)->to($view_group_obracun);

        /**
         *
         * MJERITELJ DZM
         */
        Bouncer::allow($mjeritelj_dzm)->to($view_grupe);
        Bouncer::allow($mjeritelj_dzm)->to($view_mjerila);
        Bouncer::allow($mjeritelj_dzm)->to($search_mjerila);
        Bouncer::allow($mjeritelj_dzm)->to($create_mjerila);
        Bouncer::allow($mjeritelj_dzm)->to($edit_mjerila);
        Bouncer::allow($mjeritelj_dzm)->to($view_sifre);
        Bouncer::allow($mjeritelj_dzm)->to($view_tipovi);
        Bouncer::allow($mjeritelj_dzm)->to($pdf_tipovi);

        Bouncer::allow($mjeritelj_dzm)->to($view_own_institucije);
        Bouncer::allow($mjeritelj_dzm)->to($view_own_korisnici);
        Bouncer::allow($mjeritelj_dzm)->to($view_own_profil);

        Bouncer::allow($mjeritelj_dzm)->to($create_zahtjev);
        Bouncer::allow($mjeritelj_dzm)->to($view_own_zahtjev);
        Bouncer::allow($mjeritelj_dzm)->to($edit_own_zahtjev);
        Bouncer::allow($mjeritelj_dzm)->to($ovjera_zahtjev);

        Bouncer::allow($mjeritelj_dzm)->to($create_ovjera);
        Bouncer::allow($mjeritelj_dzm)->to($view_own_ovjera);
        Bouncer::allow($mjeritelj_dzm)->to($edit_own_ovjera);
        Bouncer::allow($mjeritelj_dzm)->to($view_own_obracun);

        /**
         *
         * MJERITELJ
         */
        Bouncer::allow($mjeritelj)->to($view_own_grupe);
        Bouncer::allow($mjeritelj)->to($search_mjerila);
        Bouncer::allow($mjeritelj)->to($view_own_mjerila);
        Bouncer::allow($mjeritelj)->to($create_mjerila);
        Bouncer::allow($mjeritelj)->to($edit_own_mjerila);
        Bouncer::allow($mjeritelj)->to($view_own_sifre);
        Bouncer::allow($mjeritelj)->to($view_own_tipovi);
        Bouncer::allow($mjeritelj)->to($pdf_tipovi);

        Bouncer::allow($mjeritelj)->to($view_own_institucije);
        Bouncer::allow($mjeritelj)->to($view_own_korisnici);
        Bouncer::allow($mjeritelj)->to($view_own_profil);

        Bouncer::allow($mjeritelj)->to($create_zahtjev);
        Bouncer::allow($mjeritelj)->to($view_own_zahtjev);
        Bouncer::allow($mjeritelj)->to($edit_own_zahtjev);
        Bouncer::allow($mjeritelj)->to($ovjera_zahtjev);

        Bouncer::allow($mjeritelj)->to($create_ovjera);
        Bouncer::allow($mjeritelj)->to($view_own_ovjera);
        Bouncer::allow($mjeritelj)->to($edit_own_ovjera);
        Bouncer::allow($mjeritelj)->to($view_own_obracun);

        /**
         *
         * MJERITELJ SERVIS
         */
        Bouncer::allow($mjeritelj_servis)->to($view_own_grupe);
        //Bouncer::allow($mjeritelj_servis)->to($view_own_mjerila);
        Bouncer::allow($mjeritelj_servis)->to($create_mjerila);
        //Bouncer::allow($mjeritelj_servis)->to($edit_own_mjerila);
        Bouncer::allow($mjeritelj_servis)->to($view_own_sifre);
        Bouncer::allow($mjeritelj_servis)->to($view_own_tipovi);
        Bouncer::allow($mjeritelj_servis)->to($pdf_tipovi);

        //Bouncer::allow($mjeritelj_servis)->to($view_own_institucije);
        Bouncer::allow($mjeritelj_servis)->to($view_own_korisnici);
        Bouncer::allow($mjeritelj_servis)->to($view_own_profil);

        Bouncer::allow($mjeritelj_servis)->to($create_zahtjev);
        Bouncer::allow($mjeritelj_servis)->to($view_own_zahtjev);
        Bouncer::allow($mjeritelj_servis)->to($edit_own_zahtjev);

        Bouncer::allow($mjeritelj_servis)->to($create_ovjera);
        Bouncer::allow($mjeritelj_servis)->to($view_own_ovjera);
        Bouncer::allow($mjeritelj_servis)->to($edit_own_ovjera);
        Bouncer::allow($mjeritelj_servis)->to($view_own_obracun);

        /**
         *
         * JAVNI PROFIL
         */
        Bouncer::allow($public)->to($view_own_profil);
        Bouncer::allow($public)->to($delete_own_profil);


        /**
         *
         *
         */
        /*$users = User::whereIn('id', [1, 2])->get();

        foreach ($users as $user) {
            $user->assign('admin');
        }

        $zavod_users = User::whereIn('id', [3])->get();

        foreach ($zavod_users as $zavod_user) {
            $zavod_user->assign($zavod);
        }*/

        $users = Helper::load_PP_Collection('users');

        foreach ($users['users'] as $user) {
            $_user = User::find($user['id']);

            $_user->assign($user['role']);
        }


        /*******************************************************************************
        *                                Copyright : AGmedia                           *
        *                              email: filip@agmedia.hr                         *
        *******************************************************************************/



    }
}
