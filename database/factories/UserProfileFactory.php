<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Log;

class UserProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserProfile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fname = $this->faker->firstName;
        $lname = $this->faker->lastName;
        $name = $fname . ' ' . $lname;

        $user = User::factory()->name($name)->create();

        return [
            'user_id' => $user->id,
            'fname' => $fname,
            'lname' => $lname,
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
            'city' => $this->faker->city,
            'region' => $this->faker->state,
            'state' => 'Hrvatska',
            'bio' => $this->faker->sentence,
            'phone' => $this->faker->phoneNumber,
        ];
    }
}
