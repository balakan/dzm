<?php

namespace Database\Factories\Back;

use App\Models\Back\Ovjera;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class OvjeraFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ovjera::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'oznaka'                         => Str::upper(Str::random(3)) . '-' . rand(1011, 1980) . '-' . Str::upper(Str::random(2)),
            'mjeritelj_id'                   => 1,
            /*'ovlasteno_tijelo_id'            => 1,*/
            'mjerilo_id'                     => 1,
            'mjerilo_vlasnik_id'             => 1,
            'mjerilo_mjesto_ovjere_id'       => 1,
            'mjerilo_mjesto_postavljanja_id' => 1,
            'vrsta'                          => $this->faker->word,
            'naljepnica'                     => rand(0,1),
            'broj_naljepnice'                => rand(1011, 1980),
            'zig'                            => rand(0,1),
            'broj_ziga'                      => rand(1011, 1980),
            'ovjernica'                      => rand(0,1),
            'broj_ovjernice'                 => rand(1011, 1980),
            'datum_prijave'                  => now()->subWeek(rand(1, 3)),
            'datum_ovjere'                   => now()->addWeek(rand(1, 3)),
            'napomena'                       => $this->faker->sentence,
        ];
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function mjeritelj($id)
    {
        return $this->state([
            'mjeritelj_id' => $id,
        ]);
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    /*public function tijelo($id)
    {
        return $this->state([
            'ovlasteno_tijelo_id' => $id,
        ]);
    }*/


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function mjerilo($id)
    {
        return $this->state([
            'mjerilo_id' => $id,
        ]);
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function vlasnik($id)
    {
        return $this->state([
            'mjerilo_vlasnik_id' => $id,
        ]);
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function mjestoOvjere($id)
    {
        return $this->state([
            'mjerilo_mjesto_ovjere_id' => $id,
        ]);
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function mjestoPostavljanja($id)
    {
        return $this->state([
            'mjerilo_mjesto_postavljanja_id' => $id,
        ]);
    }

}
