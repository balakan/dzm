<?php

namespace Database\Factories\Back;

use App\Models\Back\Zahtjev;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ZahtjevFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Zahtjev::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'oznaka' => Str::random(3) . '-' . rand(101, 980) . '-' . Str::random(2),
            'mjeritelj_id' => 1,
            /*'ovlasteno_tijelo_id' => 1,*/
            'datum_pregleda_predlozeni' => now()->addWeek(rand(1,3)),
            'datum_pregleda_stvarni' => now()->addWeek(3),
            'napomena' => $this->faker->sentence,
            'status' => rand(1,4),
        ];
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function mjeritelj($id)
    {
        return $this->state([
            'mjeritelj_id' => $id,
        ]);
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    /*public function ot($id)
    {
        return $this->state([
            'ovlasteno_tijelo_id' => $id,
        ]);
    }*/
}
