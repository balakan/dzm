<?php

namespace Database\Factories\Back\Ovjera;

use App\Models\Back\Ovjera\Vlasnik;
use Illuminate\Database\Eloquent\Factories\Factory;

class VlasnikFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vlasnik::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'naziv' => $this->faker->lastName . ' ' . $this->faker->companySuffix,
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
            'city' => $this->faker->city,
            'region' => $this->faker->state,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->safeEmail,
            'status' => 1
        ];
    }
}
