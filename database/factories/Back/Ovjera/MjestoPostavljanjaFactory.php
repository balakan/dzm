<?php

namespace Database\Factories\Back\Ovjera;

use App\Models\Back\Ovjera\MjestoPostavljanja;
use Illuminate\Database\Eloquent\Factories\Factory;

class MjestoPostavljanjaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MjestoPostavljanja::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
            'city' => $this->faker->city,
            'region' => $this->faker->state,
        ];
    }
}
