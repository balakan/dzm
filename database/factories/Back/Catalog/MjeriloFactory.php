<?php

namespace Database\Factories\Back\Catalog;

use App\Models\Back\Catalog\Mjerilo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MjeriloFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mjerilo::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tip_id'   => 0,
            'sifra_id' => 0,
            'sn'       => Str::random(3) . '-' . rand(101, 980) . '-' . rand(1001, 1980),
        ];
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function tip($id)
    {
        return $this->state([
            'tip_id' => $id,
        ]);
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function sifra($id)
    {
        return $this->state([
            'sifra_id' => $id,
        ]);
    }
}
