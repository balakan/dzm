<?php

namespace Database\Factories\Back\Catalog;

use App\Models\Back\Catalog\Tip;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TipFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tip::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->lastName;

        return [
            'group_id' => 0,
            'oznaka'   => Str::random(2) . '-' . rand(1001, 1980),
            'naziv'    => 'Tip ' . $name,
            'brand'    => $name . ' ' . $this->faker->companySuffix,
            'pdf'      => '',
        ];
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function grupa($id)
    {
        return $this->state([
            'group_id' => $id,
        ]);
    }
}
