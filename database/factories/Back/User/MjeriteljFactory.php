<?php

namespace Database\Factories\Back\User;

use App\Models\Back\User\Mjeritelj;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MjeriteljFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mjeritelj::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ovlasteno_tijelo_id' => 1,
            'broj' => rand(101, 980) . '-' . Str::random(2),
            'naziv' => $this->faker->name,
            'datum_ovlastenja_od' => now()->subDay(),
            'datum_ovlastenja_do' => now()->addYear(),
            'status' => 1,
        ];
    }


    /**
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function ot($id)
    {
        return $this->state([
            'ovlasteno_tijelo_id' => $id,
        ]);
    }
}
