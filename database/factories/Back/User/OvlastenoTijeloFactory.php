<?php

namespace Database\Factories\Back\User;

use App\Models\Back\User\OvlastenoTijelo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class OvlastenoTijeloFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OvlastenoTijelo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'parent_id' => 0,
            'broj' => rand(101, 980) . '-' . Str::random(2),
            'naziv' => $this->faker->company,
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
            'city' => $this->faker->city,
            'region' => $this->faker->state,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->safeEmail,
            'datum_ovlastenja_od' => now()->subDay(),
            'datum_ovlastenja_do' => now()->addYear(),
            'status' => 1,
        ];
    }


    /**
     * @param $parent
     *
     * @return OTFactory
     */
    public function parent($parent)
    {
        return $this->state([
            'parent_id' => $parent
        ]);
    }
}
