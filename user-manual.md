# Upute za upotrebu
### Aplikacija Državnog zavoda za mjeriteljstvo

---
**Naziv aplikacije** DZM Admin  
**Korisnik** Državni zavod za mjeriteljstvo  
**Razvoj** AGmedia d.o.o.  
**Privremeni URL** http://mapp.selectpo.lin48.host25.com/
---

### 1. Navigacija (sidebar)

Glavna navigacija aplikacije nalazi se sa lijeve strane ekrana i sadrži kategorijski prikaz poveznica na glavne stranice aplikacije. 
Katalog, korisnici i report ne ponašaju se kao poveznice nego kao grupacije drugih poveznica, pa kao takve neće biti posebno objašnjene.

Sastavnice navigacije:

- Nadzorna ploča
- *Katalog*
    - Grupe
    - Mjerila
    - Šifre
    - Tipovi
- *Korisnici*
    - Institucije
    - Korisnici
- Zahtjevi
- Ovjernice
- *Report*
    - Obračuni
    - Izvještaji
- Profil

Kada detaljno u daljnjem tekstu govorimo o pojedinoj sastavnici navigacije misli se na pregled što vidimo na glavnom ekranu DZM aplikacije desno od navigacije.
Sama navigacija (sidebar) i zaglavlje (topbar) se uvijek vide isto, na svakoj pojedinoj stranici administracije DZM aplikacije na kojoj se nalazimo.

### 2. Nadzorna ploča

Nadzorna ploča se sastoji od prikaza zahtjeva sa statusom predano.

### 3. Grupe kataloga

Klikom na grupe dolazimo na listu grupa i podgrupa kataloga koji su u prikazani nazivom, oznakom je li grupa ili podgrupa, 
oznakom statusa je li aktivna ili neaktivna, redosljedom u listi i gumbom uredi. Gore desno se nalazi gumb za kreiranje novih grupa.

Klikom na gumb za kreiranje ili uređivanje grupe dolazimo na formu za kreiranje ili uređivanje grupe. 
Forma se sastoji od polja za upis naziva grupe, polja za upis opisa, odabira i prekidača je li grupa ili podgrupa, tj. ima svoju krovnu grupu ili je ona krovna grupa,
prekidačem za status grupe, je li aktivna ili neaktivna i poljem za upis redosljeda u listama u kojima se grupe i podgrupe prikazuju.

Klikom na gumb snimi grupa sa upisanim podacima snima se u bazu.

### 4. Mjerila kataloga

Klikom na mjerila dolazimo na listu mjerila kataloga koji su u prikazani serijskim brojem, proizvođačem, grupom kojoj pripada, tipnom oznakom i gumbom uredi.
Gore desno na listi se nalazi polje za pretragu mjerila koje se automatski uključuje kako pišemo u njemo i filtrira rezultate u listi.
Pretraga mjerila se vrši prema serijskom broja mjerila, tipu mjerila, šifri ili grupi kojoj mjerilo pripada.
Također gore desno se nalazi gumb za kreiranje novih mjerila. Broj pored naslova liste označava ukupan broj mjerila u katalogu.
Klikom na gumb za kreiranje ili uređivanje mjerila dolazimo na formu za kreiranje ili uređivanje mjerila.

Forma se sastoji od polja za upis serijskog broja, polja za odabir kojoj šifri pripada mjerilo i polja za odabir kojoj tipnoj oznaci pripada.

Klikom na gumb snimi mjerilo sa upisanim podacima snima se u bazu.

### 5. Šifre kataloga

Klikom na šifre kataloga dolazimo na listu šifri koji su u prikazani nazivom šifre, oznakom šifre, tarifom, ovjernim razdobljem i gumbom uredi.
Gore desno na listi se nalazi polje za pretragu šifri koje se automatski uključuje kako pišemo u njemo i filtrira rezultate u listi.
Pretraga šifre se vrši prema nazivu ili oznaci šifre.
Također gore desno se nalazi gumb za kreiranje nove šifre. Broj pored naslova liste označava ukupan broj šifri u katalogu.
Klikom na gumb za kreiranje ili uređivanje šifri dolazimo na formu za kreiranje ili uređivanje šifri.

Forma se sastoji od polja za upis naziva šifre, polja za upis šifre, polja za upis ovjernog razdoblja u godinama, 
polja za upis iznosa tarife ovjere, polja za odabir grupe kojoj šifra pripada i prekidača za status šifre je li aktivna ili neaktivna.

Klikom na gumb snimi šifra sa upisanim podacima snima se u bazu.

### 6. Tipovi kataloga

Klikom na tipove kataloga dolazimo na listu tipova koji su u prikazani tipom mjerila, svojom tipnom oznakom, 
proizvođačem, gumbom za pregled PDF tipnog odobrenja ako ga ima i gumbom uredi.
Gore desno na listi se nalazi polje za pretragu tipova koje se automatski uključuje kako pišemo u njemo i filtrira rezultate u listi.
Pretraga tipova se vrši prema oznaci, tipu mjerila ili proizvođaču tipa.
Također gore desno se nalazi gumb za kreiranje novog tipa. Broj pored naslova liste označava ukupan broj tipova u katalogu.
Klikom na gumb za kreiranje ili uređivanje tipova dolazimo na formu za kreiranje ili uređivanje tipova.

Forma se sastoji od polja za upis tipa mjerila, polja za upis oznake tipa, polja za upis ili odabir proizvođača, polja za odabir grupe kojoj tip pripada i
gumba za učitavanje PDF dokumenta. Lista u odabiru proizvođača je lista unikatnih proizvođača u listi svih tipova. 

Ako želite dodati novog proizvođača učinite to na način da upišete novog proizvođača u polje za pretraživanje proizvođača i kliknete enter da odabir potvrdi novi upis.
Bez izričitog klika na enter nakon novog upisa može se desiti da se novi proizvođač ne kreira u bazi i da se pojavi greška u obliku potrebe za ispunjenjem obveznog polja.

Dokumenti koji se učitavaju mogu biti samo PDF dokumenti i ne smiju biti veči od 10 MB. 
Ako već postoji dokument tipnog odobrenja u formi, ispod gumba za učitavanje vidi se poveznica na postojeći.

Klikom na gumb snimi tip sa upisanim podacima snima se u bazu.

### 7. Institucije

Klikom na institucje dolazima na listu upisanih institucija s dodatnim prikazom osnovnih informacija pojedine institucije.
Lista je prikazana nazivom, gradom, brojem ovlaštenja institucije, brojem njezinih članova ili pripadnika i oznakom statusa.
Uz listu klikom na gumb filter otvara nam se dodatni filter za napredno pretraživanje institucija.
Dodatni prikaz još je upotpunjen adresom, emailom, telefonom, datumom ovlaštenja, gumbom za detaljni prikaz i uređivanje podataka institucije,
i osnovnom listom članova pojedine institucije.
Klikom na pojedinu instituciju na listi prikazuju se njegovi podaci na osnovnom prikazu uz dodatak spomenutih linkova.
Gore desno se nalazi gumb za kreiranje nove institucije. Prikaz gumba za kreiranje ili uređivanje zavisi od uloge korisnika DZM aplikacije.

Klikom na gumb za kreiranje ili uređivanje institucije prelazimo na stranicu forme za kreiranje ili uređivanje institucije.
Forma se sastoji od polja za upis naziva institucije, polja za upis ovlaštenog broja institucije,
polja za upis adrese, poštanskog broja, grada, županije, emaila, telefona, odabira datuma ovlaštenja od i do, te statusa institucije.
Klikom na gumb snimi institucija sa upisanim podacima snima se u bazu.

Klikom na gumb za detaljan pregled odabrane institucije prelazimo na stranicu detaljnog pregleda podataka iste.
Na detaljnom pregledu se nalazi lista osnovnih informacija institucije kao i lista korisnika koji pripadaju instituciji.

### 8. Korisnici

Klikom na Korisnike dolazima na listu prijavljenih korisnika s dodatnim prikazom osnovnih informacija pojedinog korisnika.
Lista je prikazana imenom korisnika, emailom, njegovom ulogom unutar DZM aplikacije, institucijom kojoj pripada i statusom korisnika.
Uz listu klikom na gumb filter otvara nam se dodatni filter za napredno pretraživanje korisnika.
Dodatni prikaz još je upotpunjen telefonom, gradom i gumbom za detaljan pregled korisnika.

Klikom na gumb za detaljan pregled odabranog korisnika prelazimo na stranicu detaljnog pregleda podataka istog.
Na detaljnom pregledu se nalazi lista osnovnih informacija korisnika. 
Uz listu imamo odabir uloge korisnika kao i formu za odabir kojoj instituciji pripada, unos njegovog broja ovlaštenja, 
ima li položen ispit i upis datum do kojeg je ovlašten. Ako datum ostavimo praznim datum ovlaštenja je neograničen.

Ispod liste i unosa institucije, a zavisno od uloge korisnika nalazi se lista grupa za odabir. Tj, lista grupa koje odabrani korisnik smije koristiti.
Ako je riječ o Mjeritelju, Voditelju ili Serviseru.

*Svaki korisnik mora pripadati nekoj instituciji u kojoj će imati određenu ulogu.*

### 9. Zahtjevi

Klikom na Zahtjeve dolazima na listu upisanih zahtjeva s dodatnim prikazom osnovnih informacija pojedinog zahtjeva.
Lista je prikazana klasom ili oznakom zahtjeva, mjeriteljem koji je napravio zahtjev, brojem ovjera, datumom pregleda i statusom zahtjeva.
Uz listu klikom na gumb filter otvara nam se dodatni filter za napredno pretraživanje zahtjeva.
Dodatni prikaz još je upotpunjen institucijom, datumom upisa zahtjeva i napomenom.

Ispod detaljnog prikaza pojedinog zahtjeva nalaze se gumbi. Prikaz koji gumbi su prisutni zavisi od statusa zahtjeva ili uloge korisnika. 
Imamo gumb za detaljni prikaz zahtjeva, za uređivanje zahtjeva, za dodavanje ovjere ili ovjeru zahtjeva. 
Pri vrhu detaljnog prikaza ako je zahtjev u statusu "U Izradi" nalazi se gumb predaja zahtjeva kojim se, 
kako i naziv kaže, zahtjev predaje, tj. mijenja mu se status u "Predan".
Gore desno se nalazi gumb za kreiranje novog zahtjeva.

*Zahtjev da bi mogao biti predan mora imati barem jednu ovjeru mjerila. 
Nakon predaje zahtjeva mjeritelj više ne može uređivati zahtjev ili dodavati nove ovjere, može jedino ovjeriti predani zahtjev.
Zahtjev da bi se ovjerio mora biti predan. Nakon zadnje ovjere unutar zahtjeva, ako ih ima više, status zahtjeva se mijenja u "Završen".*

Klikom na gumb za kreiranje ili uređivanje zahtjeva prelazimo na stranicu forme za kreiranje ili uređivanje zahtjeva.
Forma se sastoji od polja za upis klase ili oznake zahtjeva, polja za upis mjeritelja (ako korisnik nije mjeritelj), predloženi datum pregleda, napomene ako je potrebna i statusa zahtjeva.
Klikom na gumb snimi zahtjev sa upisanim podacima snima se u bazu. 
Klikom na gumb "snimi i idi na ovjere zahtjeva" zahtjev sa upisanim podacima snima se u bazu i prelazimo na stranicu ovjera po zahtjevu gdje možemo dodavati ovjere (točka 10). 
Na istu stranicu idemo i ako kliknemo na gumb Dodaj ovjeru na dodatnom prikazu zahtjeva.

Klikom na gumb za detaljan pregled odabranog zahtjeva prelazimo na stranicu detaljnog pregleda podataka istog.
Na detaljnom pregledu se nalazi lista osnovnih informacija zahtjeva. 
Uz info se nalazi lista ovjera po tom zahtjevu uz dodatne gumbe za dodavanje ovjera ili ovjeravanje istih, zavisno od statusa zahtjeva.

### 12. Profil

Klikom na Profil dolazimo na stranicu za editiranje profila. Stranica se sastoji od više pod-stranica.
Prva je stranica sa korisničkim računom gdje može korisnik promijeniti svoje podatke, poput imena, adrese, telefona...
Druga je stranica Lozinke gdje korisnik može promijeniti svoju lozinku. 
Treća stranica je privatnost i sigurnost korisnika gdje korisnik može omogučiti dvofaktorsku autentifikaciju i 
gdje može izbrisati sesije ako se spajao na DZM aplikaciju preko drugih uređaja.
Kada je omogućena dvofaktorska autentifikacija, tijekom autentifikacije od vas će se zatražiti siguran, slučajni token. 
Ovaj token možete preuzeti iz aplikacije Google Authenticator na svom pametnom telefonu.
Četvrta stranica je postavka teme DZM aplikacije za pojedinog korisnika, odabir izgleda aplikacije (svijetla ili tamna), položaj i vidljivost navigacije.
Peta stranica je stranica brisanja računa.











