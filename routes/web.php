<?php

use App\Http\Controllers\Back\Catalog\GrupeController;
use App\Http\Controllers\Back\Catalog\MjerilaController;
use App\Http\Controllers\Back\Catalog\SifreController;
use App\Http\Controllers\Back\Catalog\TipoviController;
use App\Http\Controllers\Back\DashboardController;
use App\Http\Controllers\Back\OvjereController;
use App\Http\Controllers\Back\Report\ObracuniController;
use App\Http\Controllers\Back\UlogeController;
use App\Http\Controllers\Back\User\InstitucijeController;
use App\Http\Controllers\Back\User\KorisnikController;
use App\Http\Controllers\Back\User\MjeriteljiController;
use App\Http\Controllers\Back\ZahtjeviController;
use App\Http\Controllers\Front\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * FRONT-END
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/pretraga/{sn}', [HomeController::class, 'detail'])->name('detail');

/**
 * ADMIN
 */
Route::middleware(['auth:sanctum', 'verified'])->prefix('admin')->group(function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    //
    // CATALOG
    Route::prefix('katalog')->group(function () {
        // GRUPE
        Route::prefix('grupe')->group(function () {
            Route::get('/', [GrupeController::class, 'index'])->name('grupe');
            Route::get('napravi', [GrupeController::class, 'create'])->name('grupe.create');
            Route::post('/', [GrupeController::class, 'store'])->name('grupe.store');
            Route::get('{grupa}/izmjeni', [GrupeController::class, 'edit'])->name('grupe.edit');
            Route::patch('{grupa}', [GrupeController::class, 'update'])->name('grupe.update');
        });
        // MJERILA
        Route::prefix('mjerila')->group(function () {
            Route::get('/', [MjerilaController::class, 'index'])->name('mjerila');
            Route::get('napravi', [MjerilaController::class, 'create'])->name('mjerila.create');
            Route::post('/', [MjerilaController::class, 'store'])->name('mjerila.store');
            Route::get('mjerilo/{mjerilo}', [MjerilaController::class, 'show'])->name('mjerila.show');
            Route::get('{mjerilo}/izmjeni', [MjerilaController::class, 'edit'])->name('mjerila.edit');
            Route::patch('{mjerilo}', [MjerilaController::class, 'update'])->name('mjerila.update');
        });
        // ŠIFRE
        Route::prefix('sifre')->group(function () {
            Route::get('/', [SifreController::class, 'index'])->name('sifre');
            Route::get('napravi', [SifreController::class, 'create'])->name('sifre.create');
            Route::post('/', [SifreController::class, 'store'])->name('sifre.store');
            Route::get('{sifra}/izmjeni', [SifreController::class, 'edit'])->name('sifre.edit');
            Route::patch('{sifra}', [SifreController::class, 'update'])->name('sifre.update');
        });
        // TIPOVI
        Route::prefix('tipovi')->group(function () {
            Route::get('/', [TipoviController::class, 'index'])->name('tipovi');
            Route::get('napravi', [TipoviController::class, 'create'])->name('tipovi.create');
            Route::post('/', [TipoviController::class, 'store'])->name('tipovi.store');
            Route::get('{tip}/izmjeni', [TipoviController::class, 'edit'])->name('tipovi.edit');
            Route::patch('{tip}', [TipoviController::class, 'update'])->name('tipovi.update');
        });

    });

    //
    // KORISNICI
    Route::prefix('korisnici')->group(function () {
        //
        // INSTITUCIJE
        Route::prefix('institucije')->group(function () {
            Route::get('/', [InstitucijeController::class, 'index'])->name('institucije');
            Route::get('napravi', [InstitucijeController::class, 'create'])->name('institucije.create');
            Route::post('/', [InstitucijeController::class, 'store'])->name('institucije.store');
            Route::get('institucija/{institucija}', [InstitucijeController::class, 'show'])->name('institucije.show');
            Route::get('{institucija}/izmjeni', [InstitucijeController::class, 'edit'])->name('institucije.edit');
            Route::patch('{institucija}', [InstitucijeController::class, 'update'])->name('institucije.update');
        });
        //
        // KORISNICI
        Route::prefix('svi-korisnici')->group(function () {
            Route::get('/', [KorisnikController::class, 'index'])->name('korisnici');
            Route::get('korisnik/{korisnik}', [KorisnikController::class, 'show'])->name('korisnici.show');
            Route::get('{korisnik}/izmjeni', [KorisnikController::class, 'edit'])->name('korisnici.edit');
            Route::patch('{korisnik}', [KorisnikController::class, 'update'])->name('korisnici.update');
        });
    });

    //
    // ZAHTJEVI
    Route::prefix('zahtjevi')->group(function () {
        Route::get('/', [ZahtjeviController::class, 'index'])->name('zahtjevi');
        Route::get('napravi', [ZahtjeviController::class, 'create'])->name('zahtjevi.create');
        Route::post('/', [ZahtjeviController::class, 'store'])->name('zahtjevi.store');
        Route::get('zahtjev/{zahtjev}', [ZahtjeviController::class, 'show'])->name('zahtjevi.show');
        Route::get('zahtjev/{zahtjev}/ovjere', [ZahtjeviController::class, 'ovjeri'])->name('zahtjevi.ovjeri');
        Route::get('{zahtjev}/izmjeni', [ZahtjeviController::class, 'edit'])->name('zahtjevi.edit');
        Route::patch('{zahtjev}', [ZahtjeviController::class, 'update'])->name('zahtjevi.update');
        //
        Route::get('{zahtjev}/print', [ZahtjeviController::class, 'printLista'])->name('zahtjev.print.lista');
    });

    //
    // OVJERE
    Route::prefix('ovjere')->group(function () {
        Route::get('/', [OvjereController::class, 'index'])->name('ovjere');
        Route::get('napravi/zahtjev/{zahtjev}', [OvjereController::class, 'create'])->name('ovjere.create');
        Route::post('/', [OvjereController::class, 'store'])->name('ovjere.store');
        Route::get('ovjera/{ovjera}', [OvjereController::class, 'show'])->name('ovjere.show');
        Route::get('{ovjera}/izmjeni/zahtjev/{zahtjev}', [OvjereController::class, 'edit'])->name('ovjere.edit');
        Route::patch('{ovjera}', [OvjereController::class, 'update'])->name('ovjere.update');
    });

    //
    // REPORT
    Route::prefix('report')->group(function () {
        // OBRACUNI
        Route::get('obracuni', [ObracuniController::class, 'index'])->name('obracuni');
        Route::get('obracuni/{user}', [ObracuniController::class, 'show'])->name('obracuni.show');
    });

    //
    // ULOGE
    Route::get('uloge', [UlogeController::class, 'index'])->name('uloge');
});