// Usage: https://github.com/caroso1222/notyf
import { Notyf } from "notyf";

window.notyf = new Notyf({
    duration: 5000,
    ripple: 1,
    dismissible: 1,
    position: {
        x: "right",
        y: "top"
    },
    types: [
        {
            type: "default",
            backgroundColor: "#3B7DDD",
            icon: {
                className: "notyf__icon--success",
                tagName: "i",
            }
        },
        {
            type: "success",
            backgroundColor: "#28a745",
            icon: {
                className: "notyf__icon--success",
                tagName: "i",
            }
        },
        {
            type: "warning",
            backgroundColor: "#ffc107",
            icon: {
                className: "notyf__icon--error",
                tagName: "i",
            }
        },
        {
            type: "danger",
            backgroundColor: "#dc3545",
            icon: {
                className: "notyf__icon--error",
                tagName: "i",
            }
        }
    ]
});

window.successNotify = (message = 'Uspješno snimljeno..!') => {
    window.notyf.open({
        type: 'success',
        message: message,
        duration: 3600
    });
}

window.errorNotify = (message = 'Upss..! Greška prilikom snimanja. Probajte opet ili se obratite administratoru.') => {
    window.notyf.open({
        type: 'error',
        message: message,
        duration: 4600
    });
}