<x-auth-layout>
    <div class="main d-flex justify-content-center w-100">
        <main class="content d-flex p-0">
            <div class="container d-flex flex-column">
                <div class="row h-100">
                    <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                        <div class="d-table-cell align-middle">

                            <div class="text-center">
                                <h1 class="display-1 font-weight-bold text-info">303</h1>
                                <p class="h3 font-weight-normal mt-3 mb-3">Sve je u redu, ali trenutno vam nije dodijeljena institucija kojoj pripadate. Bez nje ne možete normalno koristiti DZM aplikaciju.</p>
                                <p class="h2 font-weight-normal text-info mt-0 mb-4">Molimo vas da obavjestite administratora da vam dodjeli instituciju.</p>
                                <a href="{{ route('profile.show') }}" class="btn btn-info btn-lg">Vratite se na vaš profil</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</x-auth-layout>