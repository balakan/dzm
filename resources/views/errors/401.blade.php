<x-auth-layout>
    <div class="main d-flex justify-content-center w-100">
        <main class="content d-flex p-0">
            <div class="container d-flex flex-column">
                <div class="row h-100">
                    <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                        <div class="d-table-cell align-middle">

                            <div class="text-center">
                                <h1 class="display-1 font-weight-bold text-danger">401</h1>
                                <p class="h1">Zabranjeno područje</p>
                                <p class="h3 font-weight-normal mt-3 mb-3">Niste autorizirani za ovu stranicu. Molimo vas da se vratite i nemojte ovo više pokušavati.</p>
                                <p class="h2 font-weight-normal text-danger mt-0 mb-4">Vaš pokušaj je zabilježen</p>
                                <a href="{{ back()->getTargetUrl() }}" class="btn btn-primary btn-lg">Vratite se na stranicu</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</x-auth-layout>