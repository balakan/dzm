<x-app-layout>
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3">{{ __('Postavke') }}</h1>

        <div class="row">
            <div class="col-md-3 col-xl-2">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Postavke profila</h5>
                    </div>

                    <div class="list-group list-group-flush" role="tablist">
                        <a class="list-group-item list-group-item-action active" data-toggle="list" href="#account" role="tab">
                            Korisnički račun
                        </a>
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#password" role="tab">
                           Lozinka
                        </a>
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#privacy" role="tab">
                            Privatnost i sigurnost
                        </a>
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#theme" role="tab">
                            Postavke teme
                        </a>
                        {{--<a class="list-group-item list-group-item-action" data-toggle="list" href="#destroy" role="tab">
                            Izbriši račun
                        </a>--}}
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-xl-10">
                <div class="tab-content">
                    @livewire('profile.update-profile-information-form')

                    <div class="tab-pane fade" id="password" role="tabpanel">
                        @livewire('back.settings.profile.user-password')
                    </div>

                    <div class="tab-pane fade" id="privacy" role="tabpanel">
                        @if(\Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                            @livewire('profile.two-factor-authentication-form')
                        @endif
                        @livewire('profile.logout-other-browser-sessions-form')
                    </div>

                    <div class="tab-pane fade" id="theme" role="tabpanel">
                        @livewire('back.settings.theme.color-scheme')
                    </div>

                    <div class="tab-pane fade" id="destroy" role="tabpanel">
                        @livewire('profile.delete-user-form')
                    </div>
                </div>
            </div>
        </div>

    </div>

   {{-- <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                @livewire('profile.update-profile-information-form')

                <x-jet-section-border />
            @endif

            @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                <div class="mt-10 sm:mt-0">
                    @livewire('profile.update-password-form')
                </div>

                <x-jet-section-border />
            @endif

            @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                <div class="mt-10 sm:mt-0">
                    @livewire('profile.two-factor-authentication-form')
                </div>

                <x-jet-section-border />
            @endif

            <div class="mt-10 sm:mt-0">
                @livewire('profile.logout-other-browser-sessions-form')
            </div>

            @if (Laravel\Jetstream\Jetstream::hasAccountDeletionFeatures())
                <x-jet-section-border />

                <div class="mt-10 sm:mt-0">
                    @livewire('profile.delete-user-form')
                </div>
            @endif
        </div>
    </div>--}}
</x-app-layout>
