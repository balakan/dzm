<x-jet-action-section>
    <x-slot name="title">
        {{ __('Izbriši račun') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Trajno izbrišite svoj račun.') }}
    </x-slot>

    <x-slot name="content">
        <div class="col-md-12">
            {{ __('Jednom kada se vaš račun izbriše, svi njegovi resursi i podaci trajno će se izbrisati. Prije brisanja računa preuzmite sve podatke ili informacije koje želite zadržati.') }}
        </div>

        <div class="col-md-12 mt-5">
            <x-jet-danger-button wire:click="confirmUserDeletion" wire:loading.attr="disabled" class="btn-danger">
                {{ __('Izbriši račun') }}
            </x-jet-danger-button>
        </div>

        <!-- Delete User Confirmation Modal -->
        <x-jet-dialog-modal wire:model="confirmingUserDeletion">
            <x-slot name="title">
                {{ __('Izbriši račun') }}.
            </x-slot>

            <x-slot name="content">
                {{ __('Jeste li sigurni da želite izbrisati svoj račun? Jednom kada se vaš račun izbriše, svi njegovi resursi i podaci trajno će se izbrisati. Unesite lozinku da biste potvrdili da želite trajno izbrisati svoj račun.') }}

                <div class="mt-4" x-data="{}" x-on:confirming-delete-user.window="setTimeout(() => $refs.password.focus(), 250)">
                    <x-jet-input type="password" class="mt-1 block w-3/4"
                                placeholder="{{ __('Lozinka') }}"
                                x-ref="password"
                                wire:model.defer="password"
                                wire:keydown.enter="deleteUser" />

                    <x-jet-input-error for="password" class="mt-2" />
                </div>
            </x-slot>

            <x-slot name="footer">
                <x-jet-secondary-button wire:click="$toggle('confirmingUserDeletion')" wire:loading.attr="disabled">
                    {{ __('Odustani') }}
                </x-jet-secondary-button>

                <x-jet-danger-button class="ml-2" wire:click="deleteUser" wire:loading.attr="disabled" class="btn-danger">
                    {{ __('Izbriši račun') }}
                </x-jet-danger-button>
            </x-slot>
        </x-jet-dialog-modal>
    </x-slot>
</x-jet-action-section>
