<x-jet-action-section>
    <x-slot name="title">
        {{ __('Dvofaktorska autentifikacija') }}
    </x-slot>

    {{--<x-slot name="description">
        {{ __('Add additional security to your account using two factor authentication.') }}
    </x-slot>--}}

    <x-slot name="content">
        <h3 class="col-md-12">
            @if ($this->enabled)
                {{ __('Omogućili ste dvofaktorsku provjeru autentičnosti.') }}
            @else
                {{ __('Niste omogućili dvofaktorsku autentifikaciju.') }}
            @endif
        </h3>

        <div class="col-md-12">
            <p>
                {{ __('Kada je omogućena dvofaktorska autentifikacija, tijekom autentifikacije od vas će se zatražiti siguran, slučajni token. Ovaj token možete preuzeti iz aplikacije Google Authenticator na svom telefonu.') }}
            </p>
        </div>

        @if ($this->enabled)
            @if ($showingQrCode)
                <div class="col-md-12">
                    <p class="font-semibold">
                        {{ __('Dvofaktorska autentifikacija je sada omogućena. Skenirajte sljedeći QR kôd pomoću aplikacije za provjeru autentičnosti vašeg telefona.') }}
                    </p>
                </div>

                <div class="col-md-12">
                    {!! $this->user->twoFactorQrCodeSvg() !!}
                </div>
            @endif

            @if ($showingRecoveryCodes)
                <div class="col-md-12">
                    <p class="text-black-50">
                        {{ __('Pohranite ove kodove za oporavak u sigurnom upravitelju lozinki. Mogu se koristiti za oporavak pristupa vašem računu ako se izgubi vaš dvofaktorski uređaj za provjeru autentičnosti.') }}
                    </p>
                </div>

                <div class="col-md-12">
                    @foreach (json_decode(decrypt($this->user->two_factor_recovery_codes), true) as $code)
                        <div>{{ $code }}</div>
                    @endforeach
                </div>
            @endif
        @endif

        <div class="col-md-12 mt-5">
            @if (! $this->enabled)
                <x-jet-confirms-password wire:then="enableTwoFactorAuthentication">
                    <x-jet-button type="button" class="btn-success mt-0" wire:loading.attr="disabled">
                        {{ __('Omogući') }}
                    </x-jet-button>
                </x-jet-confirms-password>
            @else
                @if ($showingRecoveryCodes)
                    <x-jet-confirms-password wire:then="regenerateRecoveryCodes">
                        <x-jet-secondary-button class="mr-3">
                            {{ __('Obnovite kodove za oporavak') }}
                        </x-jet-secondary-button>
                    </x-jet-confirms-password>
                @else
                    <x-jet-confirms-password wire:then="showRecoveryCodes">
                        <x-jet-secondary-button class="mr-3">
                            {{ __('Prikaži kodove za oporavak') }}
                        </x-jet-secondary-button>
                    </x-jet-confirms-password>
                @endif

                <x-jet-confirms-password wire:then="disableTwoFactorAuthentication">
                    <x-jet-danger-button wire:loading.attr="disabled">
                        {{ __('Onemogući') }}
                    </x-jet-danger-button>
                </x-jet-confirms-password>
            @endif
        </div>
    </x-slot>
</x-jet-action-section>
