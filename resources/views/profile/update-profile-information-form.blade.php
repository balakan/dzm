<x-jet-form-section submit="updateProfileInformation" class="show active" id="account" role="tabpanel">
    <x-slot name="title">
        {{ __('Informacije o profilu') }}
    </x-slot>

    {{--<x-slot name="description">
        {{ __('Update your account\'s profile information and email address.') }}
    </x-slot>--}}

    <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('Photo') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20"
                          x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Select A New Photo') }}
                </x-jet-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Remove Photo') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
        @endif

        <div class="col-md-12">
            <!-- Name -->
            <div class="form-group">
                <x-jet-label for="name" value="{{ __('Ime') }}" />
                <x-jet-input id="name" type="text" wire:model.defer="state.name" autocomplete="name" />
                <x-jet-input-error for="name" class="mt-2" />
            </div>

            <!-- Email -->
            <div class="form-group">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" type="email" wire:model.defer="state.email" />
                <x-jet-input-error for="email" class="mt-2" />
            </div>

            <div class="form-group col-md-6">
                <div class="custom-control custom-switch mt-4">
                    <input type="checkbox" class="custom-control-input" wire:model.defer="state.status">
                    <label class="custom-control-label" for="status">Status korisnika</label>
                </div>
            </div>

        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="alert-success mt-3" on="saved">
            <strong>{{ __('Uspješno.') }}</strong>
        </x-jet-action-message>

        <x-jet-button class="btn-primary mt-3" wire:loading.attr="disabled" wire:target="photo">
            {{ __('Snimi') }}
        </x-jet-button>
    </x-slot>

    @livewire('back.settings.profile.user-profile')

</x-jet-form-section>


