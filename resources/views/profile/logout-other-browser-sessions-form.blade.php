<x-jet-action-section>
    <x-slot name="title">
        {{ __('Sesije preglednika') }}
    </x-slot>

    {{--<x-slot name="description">
        {{ __('Manage and log out your active sessions on other browsers and devices.') }}
    </x-slot>--}}

    <x-slot name="content">
        <div class="col-md-12">
            {{ __('Ako je potrebno, možete se odjaviti iz svih ostalih sesija preglednika na svim svojim uređajima. Neke od vaših nedavnih sesija navedene su u nastavku; međutim, ovaj popis možda nije konačan. Ako smatrate da je vaš račun ugrožen, trebali biste i ažurirati lozinku..') }}
        </div>

        @if (count($this->sessions) > 0)
            <div class="mt-5 col-md-12">
                <!-- Other Browser Sessions -->
                @foreach ($this->sessions as $session)
                    <div>
                        @if ($session->agent->isDesktop())
                            <i class="align-middle" data-feather="monitor"></i>
                        @else
                            <i class="align-middle" data-feather="tablet"></i>
                        @endif
                    </div>

                    <div class="ml-3">
                        <div class="small">
                            {{ $session->agent->platform() }} - {{ $session->agent->browser() }}
                        </div>

                        <div>
                            <div class="small">
                                {{ $session->ip_address }},

                                @if ($session->is_current_device)
                                    <span class="text-black-50">{{ __('Ovaj uređaj') }}</span>
                                @else
                                    {{ __('Posljednji put aktivan') }} {{ $session->last_active }}
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif

        <div class="col-md-12 align-items-center mt-5">
            <x-jet-button wire:click="confirmLogout" wire:loading.attr="disabled" class="btn-primary">
                {{ __('Odjavite sesije sa ostalih preglednika') }}
            </x-jet-button>

            <x-jet-action-message class="ml-3" on="loggedOut">
                {{ __('Uspješno.') }}
            </x-jet-action-message>
        </div>

        <!-- Log Out Other Devices Confirmation Modal -->
        <x-jet-dialog-modal wire:model="confirmingLogout">
            <x-slot name="title">
                {{ __('Log Out Other Browser Sessions') }}
            </x-slot>

            <x-slot name="content">
                {{ __('Please enter your password to confirm you would like to log out of your other browser sessions across all of your devices.') }}

                <div class="mt-4" x-data="{}" x-on:confirming-logout-other-browser-sessions.window="setTimeout(() => $refs.password.focus(), 250)">
                    <x-jet-input type="password" class="mt-1 block w-3/4"
                                placeholder="{{ __('Password') }}"
                                x-ref="password"
                                wire:model.defer="password"
                                wire:keydown.enter="logoutOtherBrowserSessions" />

                    <x-jet-input-error for="password" class="mt-2" />
                </div>
            </x-slot>

            <x-slot name="footer">
                <x-jet-secondary-button wire:click="$toggle('confirmingLogout')" wire:loading.attr="disabled">
                    {{ __('Cancel') }}
                </x-jet-secondary-button>

                <x-jet-button class="btn-primary"
                            wire:click="logoutOtherBrowserSessions"
                            wire:loading.attr="disabled">
                    {{ __('Log Out Other Browser Sessions') }}
                </x-jet-button>
            </x-slot>
        </x-jet-dialog-modal>


        {{--<div class="modal fade" id="defaultModalPrimary" tabindex="-1" role="dialog" aria-hidden="true" wire:model="confirmingLogout">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('Log Out Other Browser Sessions') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body m-3">
                        <p class="mb-0">{{ __('Please enter your password to confirm you would like to log out of your other browser sessions across all of your devices.') }}</p>
                        <div class="mt-4" x-data="{}" x-on:confirming-logout-other-browser-sessions.window="setTimeout(() => $refs.password.focus(), 250)">
                            <x-jet-input type="password" class="mt-1 block w-3/4"
                                         placeholder="{{ __('Password') }}"
                                         x-ref="password"
                                         wire:model.defer="password"
                                         wire:keydown.enter="logoutOtherBrowserSessions" />

                            <x-jet-input-error for="password" class="mt-2" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <x-jet-secondary-button wire:click="$toggle('confirmingLogout')" wire:loading.attr="disabled">
                            {{ __('Cancel') }}
                        </x-jet-secondary-button>

                        <x-jet-button class="btn-primary"
                                      wire:click="logoutOtherBrowserSessions"
                                      wire:loading.attr="disabled">
                            {{ __('Log Out Other Browser Sessions') }}
                        </x-jet-button>
                    </div>
                </div>
            </div>
        </div>--}}


    </x-slot>
</x-jet-action-section>
