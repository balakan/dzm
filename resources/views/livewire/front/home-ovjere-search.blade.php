<div class="container @if ( ! empty($search_results)) my-lg-0 @else my-5 my-lg-10 @endif" x-data="{ show: {{ empty($search_results) ? 'true' : 'false' }} }">
    <div class="row mb-4 mb-sm-3">
        <div class="col-lg-7 col-md-9 text-center text-sm-start">
            <h1 class="text-white lh-base">Pretraga mjernih uređaja</h1>
            @if (empty($search_results))
                <h2 class="h5 text-white fw-light" x-show.transition.scale.0.duration.10ms="show">Pretražite mjerila prema oznaci ovlaštenja ili serijskom broju mjerila...</h2>
            @endif
        </div>
    </div>
    <div class="row pb-lg-3 mb-5 mb-sm-3">
        <div class="col-lg-6 col-md-8">
            <div class="input-group input-group-lg flex-nowrap">
                <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" id="search-input" placeholder="Upišite pojam pretrage...">
                <button class="btn btn-primary btn-lg btn-red fs-base" type="submit"><i class="ci-search"></i></button>
            </div>
        </div>
    </div>

    <div class="row pb-lg-5 mb-4 mb-sm-5" x-show.transition.opacity.duration.400ms="!show">
        @if ( ! empty($search_results))
            <div class="col-sm-12 col-md-12">
                <div class="card" style="background: #033e7c;">
                    <h3 class="text-white lh-base">Rezultati pretrage</h3>
                    <hr style="color: rgba(255,255,255,.3);">
                    <div class="table-responsive">
                        <table class="table table-hover table-dark w-100" style="background: #033e7c; color:#fff">
                            <tbody>
                            @forelse($search_results as $ovjera)
                                <tr style="cursor: pointer;" {{--wire:click="link('{{ $ovjera->mjerilo->sn }}')"--}}>
                                    <td>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Mjerilo: </span>{{ $ovjera->mjerilo->sn }}, {{ $ovjera->mjerilo->tip->naziv }}, {{ $ovjera->mjerilo->tip->oznaka }}<br>

                                        {{--<span class="font-weight-light font-italic" style="color: darkgray;">Oznaka: </span> {{ \Illuminate\Support\Str::limit($ovjera->vrsta, 25) }} ---}}

                                        {{--@if ($ovjera->ovjernica)
                                            <span class="font-weight-light font-italic" style="color: darkgray;">Ovjernica: </span> {{ $ovjera->ovjernica }}<br>
                                        @endif--}}
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Datum ovjere/valjanosti: </span>{{ \Illuminate\Support\Carbon::make($ovjera->datum_ovjere)->format('d.m.Y') }} - {{ \Illuminate\Support\Carbon::make($ovjera->datum_valjanosti)->format('d.m.Y') }}<br>
                                        {{--<span class="font-weight-light font-italic" style="color: darkgray;">Mjeritelj: </span>{{ $ovjera->mjeritelj->broj }}, {{ $ovjera->mjeritelj->naziv }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Tijelo: </span>{{ \Illuminate\Support\Str::limit($ovjera->mjeritelj()->first()->tijelo->naziv, 30) }}, {{ $ovjera->mjeritelj()->first()->tijelo->city }}--}}
                                    </td>
                                    <td>
                                        @if ($ovjera->naljepnica)
                                            <span class="font-weight-light font-italic" style="color: darkgray;">Naljepnica: </span> {{ $ovjera->naljepnica }}<br>
                                        @endif
                                        @if ($ovjera->zig)
                                            <span class="font-weight-light font-italic" style="color: darkgray;">Žig: </span> {{ $ovjera->zig }}<br>
                                        @endif
                                        {{--<span class="font-weight-light font-italic" style="color: darkgray;">Vlasnik: </span>{{ $ovjera->vlasnik->naziv }}, {{ $ovjera->vlasnik->city }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Postavljanje: </span>{{ $ovjera->mjesto_postavljanja->city }}, {{ \Illuminate\Support\Str::limit($ovjera->mjesto_postavljanja->address, 30) }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Mjesto Ovjere: </span>{{ $ovjera->mjesto_ovjere->city }}, {{ \Illuminate\Support\Str::limit($ovjera->mjesto_ovjere->address, 30) }}--}}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">
                                        <label class="text-lg">Ne pronalazimo rezultate prema vašem upitu...</label>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!-- Pagination: with icons -->
                    {{ $search_results->links() }}
                </div>
            </div>
        @endif
    </div>

</div>