<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <h5 class="card-title mt-2">Lista ovjera <span class="ml-4 text-info">{{ number_format($ovjere->total(), 0, '', '.') }}</span></h5>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="input-group">
                            <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži zahtjeve...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary btn-search" type="button"><i class="align-middle fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <button class="btn btn-secondary btn-search btn-block btn-lg" wire:click="showFilterClicked({{ $show_filter }})" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample" type="button"><i class="align-middle fa fa-filter"></i> Filter</button>
                    </div>
                    {{--<div class="col-1">
                        <button class="btn btn-outline-info btn-block btn-lg" wire:click="changeListView('list')" type="button"><i class="align-middle fa fa-list"></i></button>
                    </div>
                    <div class="col-1">
                        <button class="btn btn-outline-info btn-block btn-lg" wire:click="changeListView('table')" type="button"><i class="align-middle fa fa-table"></i></button>
                    </div>--}}
                </div>


                {{--<div class="row">
                    <div class="col-md-8 my-3">
                        <div class="input-group">
                            <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži ovjere">
                        </div>
                    </div>
                    <div class="col-md-4 my-3">
                        <div class="row">
                            <div class="col-8">
                                <button class="btn btn-secondary btn-search btn-block btn-lg" wire:click="showFilterClicked({{ $show_filter }})" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample" type="button"><i class="align-middle fa fa-filter"></i> Prikaži Filtere</button>
                            </div>
                            <div class="col-2">
                                <button class="btn btn-outline-info btn-block btn-lg" wire:click="changeListView('list')" type="button"><i class="align-middle fa fa-list"></i></button>
                            </div>
                            <div class="col-2">
                                <button class="btn btn-outline-info btn-block btn-lg" wire:click="changeListView('table')" type="button"><i class="align-middle fa fa-table"></i></button>
                            </div>
                        </div>
                    </div>
                </div>--}}


                <div class="collapse {{ $show_filter ? 'show' : '' }} mt-3" id="collapseExample">
                    <div class="card bg-light">
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-12 col-md-7">
                                    <div class="row">
                                        <div class="col-12">
                                            Koristi se za tekstualnu pretragu.<span class="text-sm text-muted font-weight-light ml-4">Što više uključenih to će dulje trajati pretraga...</span>
                                            <hr class="mt-2 mb-4">
                                        </div>
                                        <div class="col-12 col-md-5">
                                            <div class="custom-control custom-switch mb-2">
                                                <input type="checkbox" class="custom-control-input" id="zahtjev" wire:model="zahtjev">
                                                <label class="custom-control-label" for="vrsta">Zahtjev</label>
                                            </div>
                                            <div class="custom-control custom-switch mb-2">
                                                <input type="checkbox" class="custom-control-input" id="mjerilo" wire:model="mjerilo">
                                                <label class="custom-control-label" for="mjerilo">Mjerilo</label>
                                            </div>
                                            {{--<div class="custom-control custom-switch mb-2">
                                                <input type="checkbox" class="custom-control-input" id="mjeritelj" wire:model="mjeritelj">
                                                <label class="custom-control-label" for="mjeritelj">Mjeritelj</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="ovlasteno_tijelo" wire:model="ovlasteno_tijelo">
                                                <label class="custom-control-label" for="ovlasteno_tijelo">Institucija</label>
                                            </div>--}}
                                        </div>
                                        <div class="col-12 col-md-7">
                                            <div class="custom-control custom-switch mb-2">
                                                <input type="checkbox" class="custom-control-input" id="vlasnik" wire:model="vlasnik">
                                                <label class="custom-control-label" for="vlasnik">Vlasnik mjerila</label>
                                            </div>
                                            <div class="custom-control custom-switch mb-2">
                                                <input type="checkbox" class="custom-control-input" id="mjesto_postavljanja" wire:model="mjesto_postavljanja">
                                                <label class="custom-control-label" for="mjesto_postavljanja">Mjesto postavljanja</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="mjesto_ovjere" wire:model="mjesto_ovjere">
                                                <label class="custom-control-label" for="mjesto_ovjere">Mjesto ovjere</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-5">
                                    <div class="row">
                                        <div class="col-12">
                                            Koristi se za svu pretragu.
                                            <hr class="mt-2 mb-4">
                                        </div>
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-4">Naljepnice:</div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input wire:model="naljepnice" name="naljepnice" type="radio" value="da" class="custom-control-input">
                                                        <span class="custom-control-label" style="padding-top: 2px;">DA</span>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input wire:model="naljepnice" name="naljepnice" type="radio" value="ne" class="custom-control-input">
                                                        <span class="custom-control-label" style="padding-top: 2px;">NE</span>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input wire:model="naljepnice" name="naljepnice" type="radio" value="sve" class="custom-control-input">
                                                        <span class="custom-control-label" style="padding-top: 2px;">SVE</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-4">Žigovi:</div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input name="zig" type="radio" class="custom-control-input" value="da" wire:model="zig">
                                                        <span class="custom-control-label" style="padding-top: 2px;">DA</span>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input name="zig" type="radio" class="custom-control-input" value="ne" wire:model="zig">
                                                        <span class="custom-control-label" style="padding-top: 2px;">NE</span>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input name="zig" type="radio" class="custom-control-input" value="sve" wire:model="zig">
                                                        <span class="custom-control-label" style="padding-top: 2px;">SVE</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="col-12">
                                            <div class="row">
                                                <div class="col-4">Ovjernice:</div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input name="ovjernica" type="radio" class="custom-control-input" value="da" wire:model="ovjernica">
                                                        <span class="custom-control-label" style="padding-top: 2px;">DA</span>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input name="ovjernica" type="radio" class="custom-control-input" value="ne" wire:model="ovjernica">
                                                        <span class="custom-control-label" style="padding-top: 2px;">NE</span>
                                                    </label>
                                                </div>
                                                <div class="col-2">
                                                    <label class="custom-control custom-radio">
                                                        <input name="ovjernica" type="radio" class="custom-control-input" value="sve" wire:model="ovjernica">
                                                        <span class="custom-control-label" style="padding-top: 2px;">SVE</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mt-4" @if($list_view != 'list') hidden @endif>
                    <div class="col-12 mb-2">
                        {{--<table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Info</th>
                                <th>Mjerilo i Tijela</th>
                                <th>Vlasnik i Lokacija</th>
                                <th style="width: 5%;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ovjere as $ovjera)
                                <tr style="cursor: pointer;">
                                    <td>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Oznaka: </span>{{ $ovjera->oznaka }}, {{ \Illuminate\Support\Str::limit($ovjera->vrsta, 25) }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Datumi: </span>{{ $ovjera->datum_prijave ? \Illuminate\Support\Carbon::make($ovjera->datum_prijave)->format('d.m.Y') : '' }} - {{ $ovjera->datum_ovjere ? \Illuminate\Support\Carbon::make($ovjera->datum_ovjere)->format('d.m.Y') : '' }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Naljepnica: </span>{!! $ovjera->naljepnica ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!} -
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Žig: </span>{!! $ovjera->zig ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!} -
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Ovjernica: </span>{!! $ovjera->ovjernica ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!}
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Mjerilo: </span>{{ $ovjera->mjerilo->sn }}, {{ $ovjera->mjerilo->tip->naziv }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Mjeritelj: </span>{{ $ovjera->mjeritelj->broj }}, {{ $ovjera->mjeritelj->naziv }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Tijelo: </span>{{ \Illuminate\Support\Str::limit($ovjera->mjeritelj()->first()->tijelo->naziv, 30) }}, {{ $ovjera->mjeritelj()->first()->tijelo->city }}
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Vlasnik: </span>{{ $ovjera->vlasnik->naziv }}, {{ $ovjera->vlasnik->city }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Postavljanje: </span>{{ $ovjera->mjesto_postavljanja->city }}, {{ \Illuminate\Support\Str::limit($ovjera->mjesto_postavljanja->address, 30) }}<br>
                                        <span class="font-weight-light font-italic" style="color: darkgray;">Mjesto Ovjere: </span>{{ $ovjera->mjesto_ovjere->city }}, {{ \Illuminate\Support\Str::limit($ovjera->mjesto_ovjere->address, 30) }}
                                    </td>
                                    <td class="text-right">
                                        <a href="{{ route('ovjere.show', ['ovjera' => $ovjera]) }}" class="text-success text-lg"><i class="align-middle fa fa-eye"></i></a><br>
                                        <a href="{{ route('ovjere.edit', ['ovjera' => $ovjera]) }}" class="text-primary text-lg"><i class="align-middle fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>--}}
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Oznaka</th>
                                <th>Zahtjev</th>
                                <th>Mjerilo</th>
                                <th>Mjeritelj</th>
                                <th>Datum ovjere/valjanosti</th>
                                <th>Ovjera</th>
                                <th class="text-center">Status</th>
                                <th class="text-right">Akcije</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($ovjere as $ovjera)
                                <tr style="cursor: pointer;">
                                    <td>{{ $ovjera->id }}</td>
                                    <td><a href="{{ route('zahtjevi.show', ['zahtjev' => $ovjera->zahtjev]) }}">{{ $ovjera->zahtjev->klasa }}</a></td>
                                    <td><a href="{{ route('mjerila', ['search' => $ovjera->mjerilo->sn]) }}">{{ $ovjera->mjerilo->sn }}</a>, {{ $ovjera->mjerilo->tip->naziv }}</td>
                                    <td><a href="{{ route('korisnici.show', ['korisnik' => $ovjera->zahtjev->mjeritelj]) }}">{{ $ovjera->zahtjev->mjeritelj->name }}</a></td>
                                    <td>
                                        {{ $ovjera->datum_ovjere ? \Illuminate\Support\Carbon::make($ovjera->datum_ovjere)->format('d.m.Y') : '' }} - {{ $ovjera->datum_valjanosti ? \Illuminate\Support\Carbon::make($ovjera->datum_valjanosti)->format('d.m.Y') : '' }}
                                    </td>
                                    <td>
                                        {!! $ovjera->naljepnica ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!}
                                    </td>
                                    <td class="text-center"><i class="{{ $ovjera->status ? 'text-success fa fa-check' : 'text-danger fa fa-times' }}"></i></td>
                                    <td class="text-right">
                                        <a href="{{ route('ovjere.show', ['ovjera' => $ovjera]) }}" class="btn btn-primary btn-view">Pogledaj</a>
                                        @if ($ovjera->zahtjev->status == 1 && (auth()->user()->can('edit-all-ovjera') || auth()->user()->can('edit-group-ovjera') || auth()->user()->can('edit-own-ovjera')))
                                            <a href="{{ route('ovjere.edit', ['ovjera' => $ovjera, 'zahtjev' => $ovjera->zahtjev]) }}" class="btn btn-primary btn-edit">Uredi</a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="text-center text-info py-4">Nema upisanih ovjera..!</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12">
                        {{ $ovjere->links() }}
                    </div>
                </div>
                <div class="row mb-2 mt-3" @if($list_view != 'table') hidden @endif>
                    @foreach ($ovjere as $ovjera)
                        <div class="col-md-4">
                            <div class="card border">
                                <div class="card-body p-3">
                                    <div class="table-responsive">
                                        {{--<table class="table table-sm table-striped table-bordered mb-0">
                                            <thead class="thead-custom">
                                            <tr>
                                                <th>Oznaka</th>
                                                <th>{{ $ovjera->oznaka }}, {{ \Illuminate\Support\Str::limit($ovjera->vrsta, 25) }}</th>
                                            </tr>
                                            </thead>
                                            <tbody class="thead-custom">
                                            <tr>
                                                <td>Mjeritelj</td>
                                                <td>{{ $ovjera->mjeritelj->broj }}, {{ $ovjera->mjeritelj->naziv }}</td>
                                            </tr>
                                            <tr>
                                                <td >Ovlašteno tijelo</td>
                                                <td>{{ \Illuminate\Support\Str::limit($ovjera->mjeritelj()->first()->tijelo->naziv, 30) }}<br>{{ $ovjera->mjeritelj()->first()->tijelo->city }}</td>
                                            </tr>
                                            <tr>
                                                <td>Mjerilo</td>
                                                <td>{{ $ovjera->mjerilo->sn }}, {{ $ovjera->mjerilo->tip->naziv }}</td>
                                            </tr>
                                            <tr>
                                                <td>Vlasnik mjerila</td>
                                                <td>{{ $ovjera->vlasnik->naziv }}, {{ $ovjera->vlasnik->city }}</td>
                                            </tr>
                                            <tr>
                                                <td>Mjesto ovjere</td>
                                                <td>{{ $ovjera->mjesto_ovjere->city }}<br>{{ \Illuminate\Support\Str::limit($ovjera->mjesto_ovjere->address, 30) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Mjesto postavljanja</td>
                                                <td>{{ $ovjera->mjesto_postavljanja->city }}<br>{{ \Illuminate\Support\Str::limit($ovjera->mjesto_postavljanja->address, 30) }}</td>
                                            </tr>
                                            <tr>
                                                <td>Naljepnica</td>
                                                <td>{!! $ovjera->naljepnica ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Žig</td>
                                                <td>{!! $ovjera->zig ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Ovjernica</td>
                                                <td>{!! $ovjera->ovjernica ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!}</td>
                                            </tr>
                                            <tr>
                                                <td>Datum prijave</td>
                                                <td>{{ $ovjera->datum_prijave ? \Illuminate\Support\Carbon::make($ovjera->datum_prijave)->format('d.m.Y') : '' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Datum ovjere</td>
                                                <td>{{ $ovjera->datum_ovjere ? \Illuminate\Support\Carbon::make($ovjera->datum_ovjere)->format('d.m.Y') : '' }}</td>
                                            </tr>
                                            </tbody>
                                        </table>--}}
                                    </div>
                                    <div class="float-right mt-3">
                                        <a class="btn btn-primary btn-edit" href="{{ route('ovjere.edit', ['ovjera' => $ovjera, 'zahtjev' => $ovjera->zahtjev]) }}">Uredi</a>
                                        <a class="btn btn-primary btn-view" href="{{ route('ovjere.show', ['ovjera' => $ovjera]) }}">Pogledaj</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12">
                        {{ $ovjere->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>