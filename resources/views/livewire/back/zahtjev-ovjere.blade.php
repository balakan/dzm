<div class="row">
    @foreach ($ovjere as $ovjera)
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <label class="form-label text-info">Info ovjere {{ $ovjera->id }}</label>
                            <hr class="mt-0">
                            <div class="row pl-2">
                                <div class="col-sm-12 col-md-4 font-weight-light">Mjerilo:</div>
                                <div class="col-sm-12 col-md-8">{{ $ovjera->mjerilo->sn }}</div>

                                <div class="col-sm-12 col-md-4 font-weight-light">Tip:</div>
                                <div class="col-sm-12 col-md-8">{{ $ovjera->mjerilo->tip->oznaka }} - {{ $ovjera->mjerilo->tip->brand }}</div>

                                <div class="col-sm-12 col-md-4 font-weight-light">Datum ovjere:</div>
                                <div class="col-sm-12 col-md-8">{{ now()->format('d.m.Y') }}</div>

                                <div class="col-sm-12 col-md-4 font-weight-light">Valjanost:</div>
                                <div class="col-sm-12 col-md-8">{{ \App\Models\Back\Catalog\Sifra::getValjanostMjerila($ovjera->mjerilo->sifra)->format('d.m.Y') }}..</div>

                                <div class="mt-3 col-sm-12 col-md-4 font-weight-light">Prijašnja ovjera:</div>
                                <div class="mt-3 col-sm-12 col-md-8">
                                    @if ($ovjera->last()->first())
                                        <a href="{{ route('ovjere.show', ['ovjera' => $ovjera->last()->first()]) }}">{{ $ovjera->last()->first()->id }}</a>
                                    @else
                                        Nema
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-2">
                                    <label class="form-label required" for="select-vrsta">Vrsta potvrde</label>
                                    <div class="input-group" wire:ignore>
                                        <select class="form-control form-control-lg select2" id="select-vrsta{{ $ovjera->id }}" data-toggle="select2">
                                            <option></option>
                                            <option value="N" {{ ($ovjera->naljepnica || $ovjera->mjerilo->sifra->naljepnica) ? 'selected' : '' }}>Naljepnica</option>
                                            <option value="Z" {{ ($ovjera->zig || ! $ovjera->mjerilo->sifra->naljepnica) ? 'selected' : '' }}>Žig</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label class="form-label required" for="input-vrsta">{{ $state[$ovjera->id]['vrsta_input_label'] }}</label>
                                    @if ($state[$ovjera->id]['vrsta'] == 'N')
                                        <input type="text" class="form-control form-control-lg" wire:model="state.{{ $ovjera->id }}.vrsta_input" id="input-vrsta{{ $ovjera->id }}" value="" placeholder="">
                                    @elseif ($state[$ovjera->id]['vrsta'] == 'Z')
                                        <br>
                                        <h3 class="font-weight-lighter mt-2 ml-3">HR {{ $ovjera->zahtjev->institucija->broj }}/{{ $ovjera->zahtjev->mjeritelj->data->broj }}</h3>
                                    @endif
                                </div>

                                <div class="col-sm-12 col-md-2">
                                    <label class="form-label" for="select-status">Ovjernica</label><br>
                                    <label class="css-control css-control-success css-switch mt-2">
                                        <input type="checkbox" class="css-control-input" disabled {{--checked=""--}}>
                                        <span class="css-control-indicator"></span> Ima ovjernicu
                                    </label>
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label class="form-label" for="select-status">Napomena</label>
                                    <a href="javascript:void(0);" class="btn btn-outline-secondary btn-block btn-lg shadow-sm" id="show-napomena{{ $ovjera->id }}">Dodaj Napomenu</a>
                                </div>

                                <div class="col-sm-12 col-md-2" wire:ignore>
                                    <label class="form-label" for="select-status">Status</label>
                                    <div class="input-group" wire:ignore>
                                        <select class="form-control form-control-lg select2" id="select-status{{ $ovjera->id }}" data-toggle="select2">
                                            {{--<option></option>--}}
                                            @foreach (config('settings.ovjera.statuses') as $key => $status)
                                                <option value="{{ $key }}" {{ (isset($ovjera) and $key == $ovjera->status) ? 'selected="selected"' : '' }}>{{ $status }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 mt-3 d-none" id="input-napomena{{ $ovjera->id }}">
                                    <label class="form-label" for="input-napomena">Napomena</label>
                                    <textarea class="form-control" rows="4" wire:model.defer="state.{{ $ovjera->id }}.napomena" placeholder="">{{ isset($ovjera) ? $ovjera->napomena : '' }}</textarea>
                                </div>
                            </div>

                            <div class="row mt-6" id="btn-row{{ $ovjera->id }}">
                                <div class="col-12 col-md-6"></div>

                                <div class="col-sm-6 col-md-4">

                                </div>

                                <div class="col-sm-6 col-md-2 text-right">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm" name="submitBtn" value="0" wire:click="ovjeri({{ $ovjera->id }})">
                                        {{--<i class="align-middle" data-feather="save">&nbsp;</i>--}} Snimi
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @if ($ovjere->count())
        <div class="col-12">
            <div class="row mt-2 mb-4">
                <div class="col-sm-12 col-md-9"></div>

                <div class="col-sm-12 col-md-3 text-right">
                    <button type="button" class="btn btn-primary btn-save-as shadow-sm" wire:click="ovjeriSve">
                        {{--<i class="align-middle" data-feather="save">&nbsp;</i>--}} Snimi Sve i Završi
                    </button>
                </div>
            </div>
        </div>
    @endif

    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Oznaka</th>
                                    <th>Zahtjev</th>
                                    <th>Mjerilo</th>
                                    <th>Mjeritelj</th>
                                    <th>Datum ovjere/valjanosti</th>
                                    <th>Ovjera</th>
                                    <th class="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($ovjere_ovjerene as $ovjera_ovjerena)
                                    <tr style="cursor: pointer;">
                                        <td>{{ $ovjera_ovjerena->id }}</td>
                                        <td><a href="{{ route('zahtjevi.show', ['zahtjev' => $ovjera_ovjerena->zahtjev]) }}">{{ $ovjera_ovjerena->zahtjev->klasa }}</a></td>
                                        <td><a href="{{ route('mjerila', ['search' => $ovjera_ovjerena->mjerilo->sn]) }}">{{ $ovjera_ovjerena->mjerilo->sn }}</a>, {{ $ovjera_ovjerena->mjerilo->tip->naziv }}</td>
                                        <td><a href="{{ route('korisnici.show', ['korisnik' => $ovjera_ovjerena->zahtjev->mjeritelj]) }}">{{ $ovjera_ovjerena->zahtjev->mjeritelj->name }}</a></td>
                                        <td>
                                            {{ $ovjera_ovjerena->datum_ovjere ? \Illuminate\Support\Carbon::make($ovjera_ovjerena->datum_ovjere)->format('d.m.Y') : '' }} <strong>/</strong> {{ $ovjera_ovjerena->datum_valjanosti ? \Illuminate\Support\Carbon::make($ovjera_ovjerena->datum_valjanosti)->format('d.m.Y') : '' }}
                                        </td>
                                        <td>@include('layouts.back.partials.naljepnica-ovjere', ['ovjera' => $ovjera_ovjerena])</td>
                                        <td class="text-center">@include('layouts.back.partials.ovjera-status', ['ovjera' => $ovjera_ovjerena])</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            @foreach ($ovjere as $ovjera)

            $('#select-vrsta{{ $ovjera->id }}').select2({
                placeholder: 'Ovjerna potvrda',
                theme: "bootstrap",
                minimumResultsForSearch: Infinity
            });

            $('#select-vrsta{{ $ovjera->id }}').on('change', (e) => {
                @this.vrstaSelected(e.currentTarget.value, {{ $ovjera->id }});
            });

            $('#select-status{{ $ovjera->id }}').select2({
                placeholder: 'Odaberite status',
                theme: "bootstrap",
                minimumResultsForSearch: Infinity
            });

            $('#select-status{{ $ovjera->id }}').on('change', (e) => {
                @this.statusSelected(e.currentTarget.value, {{ $ovjera->id }});
            });


            $('#show-napomena{{ $ovjera->id }}').on('click', (e) => {
                let has = $('#input-napomena{{ $ovjera->id }}').hasClass('d-none');

                if (has) {
                    $('#input-napomena{{ $ovjera->id }}').removeClass('d-none');
                    $('#btn-row{{ $ovjera->id }}').removeClass('mt-6');
                    $('#btn-row{{ $ovjera->id }}').addClass('mt-3');
                } else {
                    $('#input-napomena{{ $ovjera->id }}').addClass('d-none');
                    $('#btn-row{{ $ovjera->id }}').removeClass('mt-3');
                    $('#btn-row{{ $ovjera->id }}').addClass('mt-6');
                }
            });

            @endforeach

            document.addEventListener('livewire:load', function () {
                Livewire.hook('message.processed', (message, component) => {});
            });

            Livewire.on('success_alert', () => {
                successNotify();
            });

            Livewire.on('error_alert', () => {
                errorNotify();
            });

            Livewire.on('warning_alert', () => {
                errorNotify('Nisu ispunjeni svi potrebni podaci. Molim vas provjerite.');
            });
        </script>
    @endpush
</div>