<div class="row">
    <div class="col-12 col-lg-8">
        <div class="card">
            <div class="card-body pb-0">
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <h5 class="card-title mt-2">Lista institucija <span class="ml-4 text-info">{{ number_format($institucije->total(), 0, '', '.') }}</span></h5>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="input-group">
                            <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži instituciju...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary btn-search" type="button"><i class="align-middle fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <button class="btn btn-secondary btn-search btn-block btn-lg" wire:click="showFilterClicked({{ $show_filter }})" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample" type="button"><i class="align-middle fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                <div class="collapse {{ $show_filter ? 'show' : '' }} mt-3 mb-0" id="collapseExample">
                    <div class="card bg-light mb-0 border">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    Odaberite što pretražujete.
                                    <hr class="mt-2 mb-4">
                                </div>
                                <div class="col-12 col-md-2">
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="broj" wire:model="state.broj">
                                        <label class="custom-control-label" for="broj">Broj</label>
                                    </div>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="naziv" wire:model="state.naziv">
                                        <label class="custom-control-label" for="naziv">Naziv</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="korisnik" wire:model="state.korisnik">
                                        <label class="custom-control-label" for="korisnik">Korisnik</label>
                                    </div>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="city" wire:model="state.city">
                                        <label class="custom-control-label" for="city">Grad Institucije</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-5">
                                    <div class="row">
                                        <div class="col-12 mb-2">Status:</div>
                                        <div class="col-4">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="1" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Aktivni</span>
                                            </label>
                                        </div>
                                        <div class="col-5">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="0" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Neaktivni</span>
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="all" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Svi</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">Broj</th>
                            <th>Naziv</th>
                            <th>Grad</th>
                            <th class="text-center">Članova</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($institucije as $institucija)
                            <tr style="cursor: pointer;" wire:click="userSelected({{ $institucija->id }})">
                                <td class="text-center">{{ $institucija->broj }}</td>
                                <td class="font-weight-bold">{{ $institucija->naziv }}</td>
                                <td>{{ $institucija->city }}</td>
                                <td class="text-center">{{ $institucija->users()->count() }}</td>
                                <td class="text-center"><i class="{{ $institucija->status ? 'text-success fa fa-check' : 'text-danger fa fa-times' }}"></i></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center text-info py-4">Nema upisanih institucija..!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $institucije->links() }}
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">{{ ! empty($selected) ? $selected->naziv : 'Odaberite instituciju...' }}</h5>
            </div>
            <div class="card-body mt-0 pt-0">
                <table class="table table-sm my-4">
                    <tbody>
                    <tr>
                        <th>Naziv</th>
                        <td>{{ ! empty($selected) ? $selected->naziv : '' }}</td>
                    </tr>
                    <tr>
                        <th>Adresa</th>
                        <td>{{ ! empty($selected) ? $selected->address . ', ' . $selected->zip . ' ' . $selected->city : '' }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ ! empty($selected) ? $selected->email : '' }}</td>
                    </tr>
                    <tr>
                        <th>Tel</th>
                        <td>{{ ! empty($selected) ? $selected->phone : '' }}</td>
                    </tr>
                    <tr>
                        <th>Datum ovlaštenja</th>
                        <td>{{ ! empty($selected) ? ($selected->datum_ovlastenja_do ? \Illuminate\Support\Carbon::make($selected->datum_ovlastenja_do)->format('d.m.Y') : 'Neograničeno') : '' }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($selected)
                                @if($selected->status)
                                    <span class="badge badge-success">Aktivan</span>
                                @else
                                    <span class="badge badge-danger">Neaktivan</span>
                                @endif
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="row">
                    @if($selected)
                        <div class="col-6">
                            <a href="{{ route('institucije.show', ['institucija' => $selected]) }}" class="btn btn-info btn-view btn-block">Pogledaj</a>
                        </div>
                    @endif
                    @if($selected && (auth()->user()->can('edit-all-institucije') || auth()->user()->can('edit-own-institucije')))
                        <div class="col-6">
                            <a href="{{ route('institucije.edit', ['institucija' => $selected]) }}" class="btn btn-info btn-edit btn-block">Uredi</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        @if ( ! empty($selected) && (auth()->user()->can('view-all-korisnici') || auth()->user()->can('view-group-korisnici')))
            <div class="card">
                <div class="card-header pb-0">
                    <h5 class="card-title mb-0">Članovi <span class="text-info">{{ $selected->naziv }}</span></h5>
                </div>
                <div class="card-body mt-0 pt-0">
                    <table class="table table-sm my-4">
                        <tbody>
                        @forelse($selected->users()->get() as $user)
                            <tr>
                                <td><a href="{{ route('korisnici.show', ['korisnik' => $user]) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->uloga->title }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2" class="text-center">Institucija trenutno nema članova</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>

    @push('scripts')
        <script>
            document.addEventListener('livewire:load', function () {
                Livewire.hook('message.processed', (message, component) => {
                    $('#role-select').select2({
                        placeholder: "Odaberi ulogu...",
                        minimumResultsForSearch: Infinity
                    });

                    $('#role-select').on('change', function (e) {
                        var data = $('#role-select').select2("val");
                        @this.roleSelected(data);
                    });
                });
            });

            Livewire.on('success_alert', () => {
                successNotify();
            });

            Livewire.on('error_alert', () => {
                errorNotify();
            });
        </script>
    @endpush

</div>

