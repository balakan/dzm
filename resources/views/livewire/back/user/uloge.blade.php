<div>
    <div class="row">
        <div class="col-12 col-lg-7">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Lista uloga
                        <div class="card-actions float-right">
                            <div class="spinner-border spinner-border-sm text-info" role="status" wire:loading>
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </h5>
                </div>
                <div class="card-body pt-0">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th style="width:6%;">#</th>
                            <th>Naziv</th>
                            <th class="text-right" style="width:35%">Oznaka</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles['roles'] as $role)
                            <tr wire:click="roleSelected({{ $role['role']->id }})">
                                <td>{{ $loop->iteration }}.</td>
                                <td><span class="font-weight-bold">{{ $role['role']->title }}</span></td>
                                <td class="text-right">{{ $role['role']->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-5">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Lista ovlaštenja po ulozi</h5>
                </div>
                <div class="card-body">
                    <h4 class="mb-4">{{ $selected_role->title }}</h4>
                    <table class="table table-hover table-sm">
                        <tbody>
                        @forelse($selected_abilities as $ability)
                            <tr>
                                <td style="width:6%;">{{ $loop->iteration }}.<br>&nbsp;</td>
                                <td>{{ $ability->title }}<br><span class="text-sm font-weight-light">{{ $ability->name }}</span></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">Trenutno nema nikakvih ovlaštenja.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Lista svih ovlaštenja</h5>
                </div>
                <div class="card-body pt-0">
                    <table class="table table-hover table-sm table-striped">
                        <thead>
                        <tr>
                            <th style="width:1%;">#</th>
                            <th>Ovlaštenje</th>
                            <th class="text-center">Administracija</th>
                            <th class="text-center">Inspekcija</th>
                            <th class="text-center">Voditelj DZM</th>
                            <th class="text-center">Voditelj</th>
                            <th class="text-center">Mjeritelj DZM</th>
                            <th class="text-center">Mjeritelj</th>
                            <th class="text-center">Servis</th>
                            <th class="text-center">Public</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles['abilities'] as $ability)
                            <tr>
                                <td>{{ $loop->iteration }}.<br>&nbsp;</td>
                                <td><strong>{{ $ability->title }}</strong><br><span class="text-sm font-weight-light">{{ $ability->name }}</span></td>
                                <!-- ZAVOD -->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'zavod')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                                <!-- INSPEKCIJA -->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'inspekcija')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                                <!-- VODITELJ DZM -->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'voditelj-dzm')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                                <!-- VODITELJ-->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'voditelj')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                                <!-- MJERITELJ DZM -->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'mjeritelj-dzm')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                                <!-- MJERITELJ -->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'mjeritelj')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                                <!-- MJERITELJ -->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'mjeritelj-servis')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                                <!-- PUBLIC - JAVNA ULOGA -->
                                <td class="text-center">
                                    @foreach ($roles['roles'] as $role)
                                        @if ($role['role']->name == 'public')
                                            @if ($role['role']->hasAbility($ability))
                                                <input type="checkbox" class="form-checkbox" checked wire:click="disallowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @else
                                                <input type="checkbox" class="form-checkbox" wire:click="allowAbility({{ $role['role'] }}, {{ $ability }})">
                                            @endif
                                        @endif
                                    @endforeach<br>&nbsp;
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        Livewire.on('success_alert', () => {
            successNotify();
        });

        Livewire.on('error_alert', () => {
            errorNotify();
        });
    </script>
@endpush