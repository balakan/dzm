<div class="row">
    <div class="col-12 @can('view-all-korisnici') col-lg-7 @endcan">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Info</h5>
            </div>
            <div class="card-body mt-0 pt-0">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr>
                            <th>Ime </th>
                            <td>{{ $korisnik->profile->fname }}</td>
                        </tr>
                        <tr>
                            <th>Prezime</th>
                            <td>{{ $korisnik->profile->lname }}</td>
                        </tr>
                        <tr>
                            <th>Adresa</th>
                            <td>{{ $korisnik->profile->address }}</td>
                        </tr>
                        <tr>
                            <th>Poštanski broj</th>
                            <td>{{ $korisnik->profile->zip }}</td>
                        </tr>
                        <tr>
                            <th>Grad</th>
                            <td>{{ $korisnik->profile->city }}</td>
                        </tr>
                        <tr>
                            <th>Regija</th>
                            <td>{{ $korisnik->profile->region }}</td>
                        </tr>
                        <tr>
                            <th>Država</th>
                            <td>{{ $korisnik->profile->state }}</td>
                        </tr>
                        <tr>
                            <th>Telefon</th>
                            <td>{{ $korisnik->profile->phone }}</td>
                        </tr>
                        <tr>
                            <th>Bio</th>
                            <td>{{ $korisnik->profile->bio }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $korisnik->email }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @if($korisnik->status)
                                    <span class="badge badge-success">Aktivan</span>
                                @else
                                    <span class="badge badge-danger">Neaktivan</span>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @can('edit-korisnici-group')
        <div class="col-12 col-lg-5">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Uloga: <span class="text-info">{{ $korisnik->uloga->title }}</span></h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12"><label class="form-label">Promijeni korisniku ulogu</label></div>
                        <div class="col-9" wire:ignore>
                            <select class="form-control form-control-lg select2" id="role-select">
                                <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}" {{ (isset($role->name) and $korisnik->role == $role->name) ? 'selected="selected"' : '' }}>{{ $role->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-3">
                            <button wire:click="changeRole" class="btn btn-info btn-save btn-block">Snimi</button>
                        </div>
                    </div>
                </div>
            </div>

            @if ($show_institucija_input_window)
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4">
                                <label class="form-label">Odaberi instituciju korisniku</label>
                                @livewire('back.layout.search.institucija-search', ['institucija_id' => $institucija_id])
                            </div>
                            <div class="col-6 mb-4">
                                <label class="form-label">Broj ovlaštenja</label>
                                <input type="text" class="form-control form-control-lg" id="broj-input" wire:model="institucija_input_state.broj" value="{{ isset($institucija_input_state) ? $institucija_input_state['broj'] : old('broj') }}" placeholder="">
                            </div>
                            <div class="col-6 mt-4 text-center">
                                <label class="custom-control custom-checkbox mt-2">
                                    <input type="checkbox" class="custom-control-input" id="ispit" wire:model="institucija_input_state.ispit">
                                    <span class="custom-control-label"> Položen ispit</span>
                                </label>
                            </div>
                            <div class="col-12 mt-2">
                                <label class="form-label" for="input-datum">Upišite datum ovlaštenja do</label>
                                {{--<div class="form-group">
                                    <div class="input-group date" id="datum_ovlastenja_do" data-target-input="nearest">
                                        <input type="text" class="form-control form-control-lg datetimepicker-input"
                                               wire:model="datum_ovlastenja_do"
                                               wire:blur="setInstitucija(222)"
                                               id="datum-picker" data-target="#datum_ovlastenja_do"
                                               value="{{ isset($institucija) and $institucija->datum_ovlastenja_do ? \Illuminate\Support\Carbon::make($institucija->datum_ovlastenja_do)->format('d.m.Y') : old('datum_ovlastenja_do') }}" />
                                        <div class="input-group-append" data-target="#datum_ovlastenja_do" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>--}}
                                <input type="text" class="form-control form-control-lg @if ($date_error) is-invalid @endif"
                                       wire:model="institucija_input_state.datum_ovlastenja_do"
                                       wire:blur="checkDate">
                                @if ($date_error)
                                    <label class="small text-danger mt-0">Molimo vas da provjerite format upisanog datuma.</label>
                                @endif
                                <label class="small text-muted mt-1 mb-0"><strong class="mr-2">(Format primjer 31.05.2023).</strong> Za neograničeno trajanje ostavite prazno.</label>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        <div class="col-12">
            <div class="tab">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active" href="#grupe" data-toggle="tab" role="tab">Grupe</a></li>
                    {{--<li class="nav-item"><a class="nav-link" href="#zahtjevi" data-toggle="tab" role="tab">Zahtjevi</a></li>--}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="grupe" role="tabpanel">
                        @if ($show_group_window)
                            <h4 class="tab-title mb-4 mt-2">Lista dopuštenih grupa
                                <span class="float-right">
                                    <div class="dropdown show">
                                        <a href="#" data-toggle="dropdown" data-display="static"><i class="fa fa-th"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <button class="dropdown-item text-success" wire:click="allowAllGroups">Označi Sve</button>
                                            <button class="dropdown-item text-danger" wire:click="disallowAllGroups">Obriši Sve</button>
                                        </div>
                                    </div>
                                </span>
                            </h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Naziv</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($groups as $group)
                                        <tr>
                                            <td class="font-weight-bold">{{ $group->name }}</td>
                                            <td class="text-center">
                                                @if (in_array($group->id, $korisnik->grupe()->pluck('id')->toArray()))
                                                    <input type="checkbox" class="form-checkbox" checked wire:click="disallowGroup({{ $group->id }})">
                                                @else
                                                    <input type="checkbox" class="form-checkbox" wire:click="allowGroup({{ $group->id }})">
                                                @endif
                                            </td>
                                        </tr>
                                        @if($group->podgrupa)
                                            @foreach($group->podgrupa as $podgroup)
                                                <tr class="font-weight-lighter">
                                                    <td class="pl-5">{{ $podgroup->name }}</td>
                                                    <td class="text-center">
                                                        @if (in_array($podgroup->id, $korisnik->grupe()->pluck('id')->toArray()))
                                                            <input type="checkbox" class="form-checkbox" checked wire:click="disallowGroup({{ $podgroup->id }})">
                                                        @else
                                                            <input type="checkbox" class="form-checkbox" wire:click="allowGroup({{ $podgroup->id }})">
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <h4 class="tab-title">Nije potrebno namještati grupe. Uloga korisnika vidi sve grupe.</h4>
                        @endif
                    </div>
                    {{--<div class="tab-pane" id="zahtjevi" role="tabpanel">
                        <h4 class="tab-title">Another one</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor tellus eget condimentum rhoncus. Aenean massa. Cum sociis natoque
                           penatibus et magnis neque dis parturient montes, nascetur ridiculus mus.</p>
                        <p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                           eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
                    </div>--}}
                </div>
            </div>
        </div>

    @endcan
</div>

@push('scripts')
    <script>
        document.addEventListener('livewire:load', function () {
            $('#role-select').select2({
                placeholder: "Odaberi ulogu...",
                minimumResultsForSearch: Infinity
            });

            $('#role-select').on('change', function (e) {
                var data = $('#role-select').select2("val");
                @this.roleSelected(data);
            });

            /*$('#datum-picker').datetimepicker({
                format: 'DD.MM.YYYY'
            });*/

        });

        Livewire.on('success_alert', () => {
            successNotify();
        });

        Livewire.on('error_alert', () => {
            errorNotify();
        });
    </script>
@endpush