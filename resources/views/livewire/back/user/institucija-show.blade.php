<div class="row">
    <div class="col-12 @can('view-zavod') col-lg-7 @endcan">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">Info</h5>
            </div>
            <div class="card-body mt-0 pt-0">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr>
                            <th>Naziv </th>
                            <td>{{ $institucija->naziv }}</td>
                        </tr>
                        <tr>
                            <th>Broj</th>
                            <td>{{ $institucija->broj }}</td>
                        </tr>
                        <tr>
                            <th>Adresa</th>
                            <td>{{ $institucija->address }}</td>
                        </tr>
                        <tr>
                            <th>Poštanski broj</th>
                            <td>{{ $institucija->zip }}</td>
                        </tr>
                        <tr>
                            <th>Grad</th>
                            <td>{{ $institucija->city }}</td>
                        </tr>
                        <tr>
                            <th>Regija</th>
                            <td>{{ $institucija->region }}</td>
                        </tr>
                        <tr>
                            <th>Telefon</th>
                            <td>{{ $institucija->phone }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $institucija->email }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @if($institucija->status)
                                    <span class="badge badge-success">Aktivna</span>
                                @else
                                    <span class="badge badge-danger">Neaktivna</span>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @can('view-all-institucije')
        <div class="col-12 col-lg-5">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Korisnici: <span class="text-info">{{ $institucija->naziv }}</span></h5>
                </div>
                <div class="card-body pt-0">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th>Ime</th>
                            <th>Uloga</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($institucija->users()->get() as $user)
                            <tr>
                                <td><a href="{{ route('korisnici.show', ['korisnik' => $user]) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->uloga->title }}</td>
                                <td class="text-center"><i class="{{ $user->status ? 'text-success fa fa-check' : 'text-danger fa fa-times' }}"></i></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center pt-3">Institucija trenutno nema članova. <a href="{{ route('korisnici') }}">Dodaj...</a></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <a href="{{ route('zahtjevi', ['institucija' => $institucija]) }}" class="btn btn-info btn-view btn-block">Pogledaj Zahtjeve Institucije</a>
                </div>
            </div>
        </div>
    @endcan
</div>

@push('scripts')
    <script>
        document.addEventListener('livewire:load', function () {
            $('#role-select').select2({
                placeholder: "Odaberi ulogu...",
                minimumResultsForSearch: Infinity
            });

            $('#role-select').on('change', function (e) {
                var data = $('#role-select').select2("val");
                @this.roleSelected(data);
            });
        });

        Livewire.on('success_alert', () => {
            successNotify();
        });

        Livewire.on('error_alert', () => {
            errorNotify();
        });
    </script>
@endpush