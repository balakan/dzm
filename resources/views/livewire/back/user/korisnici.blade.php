<div class="row">
    <div class="col-12 col-lg-8">
        <div class="card">

            <div class="card-body pb-0">
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <h5 class="card-title mt-2">Lista korisnka <span class="ml-4 text-info">{{ number_format($users->total(), 0, '', '.') }}</span></h5>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="input-group">
                            <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži instituciju...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary btn-search" type="button"><i class="align-middle fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <button class="btn btn-secondary btn-search btn-block btn-lg" wire:click="showFilterClicked({{ $show_filter }})" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample" type="button"><i class="align-middle fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                <div class="collapse {{ $show_filter ? 'show' : '' }} mt-3 mb-0" id="collapseExample">
                    <div class="card bg-light mb-0 border">
                        <div class="card-body pt-3">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6 pt-2">Odaberite ulogu ili pretražite uključeno.</div>
                                        <div class="col-6 pb-2">
                                            <select class="form-control form-control-lg select2" id="role-select" wire:model="state.uloga">
                                                <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->name }}" {{--{{ (isset($role->name) and $korisnik->role == $role->name) ? 'selected="selected"' : '' }}--}}>{{ $role->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <hr class="mt-2 mb-4">
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="broj" wire:model="state.ime">
                                        <label class="custom-control-label" for="broj">Ime i/ili prezime</label>
                                    </div>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="naziv" wire:model="state.email">
                                        <label class="custom-control-label" for="naziv">Email</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    {{--<div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="korisnik" wire:model="state.uloga">
                                        <label class="custom-control-label" for="korisnik">Uloga</label>
                                    </div>--}}
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="city" wire:model="state.institucija">
                                        <label class="custom-control-label" for="city">Institucija</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <div class="col-12 mb-2">Status:</div>
                                        <div class="col-3">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="1" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Aktivni</span>
                                            </label>
                                        </div>
                                        <div class="col-4">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="0" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Neaktivni</span>
                                            </label>
                                        </div>
                                        <div class="col-5">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="all" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Svi</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Ime</th>
                            <th>Email</th>
                            <th>Br.Inst.</th>
                            <th>Institucija</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr style="cursor: pointer;" wire:click="userSelected({{ $user->id }})">
                                <td class="font-weight-bold">{{ $user->name }}</td>
                                <td class="font-weight-light">{{ $user->email }}</td>
                                <td class="font-weight-light">{{ $user->institucija ? $user->institucija->broj : '' }}</td>
                                <td class="font-weight-light">{{ $user->institucija ? $user->institucija->naziv : '' }}</td>
                                <td class="text-center"><i class="{{ $user->status ? 'text-success fa fa-check' : 'text-danger fa fa-times' }}"></i></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $users->links() }}
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">{{ ! empty($selected_user) ? $selected_user->name : 'Selektirajte korisnika...' }}</h5>
            </div>
            <div class="card-body mt-0 pt-0">
                {{--<div class="row no-gutters">
                    <div class="col-sm-3 col-xl-12 col-xxl-3 text-center">
                        <img src="{{ ! empty($selected_user) ? (! empty($selected_user->profile_photo_path) ? $selected_user->profile_photo_path : asset('img/avatars/avatar.jpeg')) : asset('img/avatars/avatar.jpeg') }}" width="80" height="80" class="rounded-circle mt-2" alt="{{ ! empty($selected_user) ? $selected_user->name : 'avatar' }}">
                    </div>
                </div>--}}
                <table class="table table-sm my-4">
                    <tbody>
                    <tr>
                        <th>Ime</th>
                        <td>{{ ! empty($selected_user) ? $selected_user->profile->fname.' '.$selected_user->profile->lname : '' }}</td>
                    </tr>
                    <tr>
                        <th>Uloga</th>
                        <td>{{ ! empty($selected_user) ? $selected_user->uloga->title : '' }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ ! empty($selected_user) ? $selected_user->email : '' }}</td>
                    </tr>
                    <tr>
                        <th>Mob</th>
                        <td>{{ ! empty($selected_user) ? $selected_user->profile->phone : '' }}</td>
                    </tr>
                    <tr>
                        <th>Grad</th>
                        <td>{{ ! empty($selected_user) ? $selected_user->profile->city : '' }}</td>
                    </tr>
                    {{--<tr>
                        <th>Status</th>
                        <td>
                            @if($selected_user)
                                @if($selected_user->status)
                                    <span class="badge badge-success">Aktivan</span>
                                @else
                                    <span class="badge badge-danger">Neaktivan</span>
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Institucija</th>
                        <td>
                            @if (! empty($selected_user) && $selected_user->institucija)
                                <a href="{{ route('institucije.show', ['institucija' => $selected_user->institucija]) }}">{{ $selected_user->institucija->naziv }}</a>
                            @endif
                        </td>
                    </tr>--}}
                    </tbody>
                </table>
                @if($selected_user)
                    <a href="{{ route('korisnici.show', ['korisnik' => $selected_user->id]) }}" class="btn btn-info btn-view btn-block">Detalji Korisnika</a>
                @endif
            </div>
        </div>

    </div>

    @push('scripts')
        <script>
            document.addEventListener('livewire:load', function () {
                Livewire.hook('message.processed', (message, component) => {
                    $('#role-select').select2({
                        placeholder: "Odaberi ulogu...",
                        minimumResultsForSearch: Infinity
                    });

                    $('#role-select').on('change', function (e) {
                        var data = $('#role-select').select2("val");
                        @this.filterRoleSelected(data);
                    });
                });
            });

            Livewire.on('success_alert', () => {
                successNotify();
            });

            Livewire.on('error_alert', () => {
                errorNotify();
            });
        </script>
    @endpush

</div>

