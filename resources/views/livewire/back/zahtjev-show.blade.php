<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <ul class="list-unstyled mb-0">
                            <li class="mb-1">Broj Zahtjeva:</li>
                            <li class="mb-1">Klasa:</li>
                            <li class="mb-1">Broj Insitucije:</li>
                            <li class="mb-1">Broj Mjeritelja:</li>
                            <li class="mb-1">Datum Pregleda:</li>
                            <li class="mb-1">Status:</li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled mb-0">
                            <li class="mb-1"><strong>HR-{{ $zahtjev->institucija->broj }}-{{ $zahtjev->id }}</strong></li>
                            <li class="mb-1">{{ $zahtjev->klasa }}</li>
                            <li class="mb-1"><strong>({{ $zahtjev->institucija->broj }})</strong> , {{ $zahtjev->institucija->naziv }}</li>
                            <li class="mb-1"><strong>({{ $zahtjev->mjeritelj->data->broj }})</strong> , {{ $zahtjev->mjeritelj->name }}</li>
                            <li class="mb-1">{{ $zahtjev->datum_pregleda ? \Illuminate\Support\Carbon::make($zahtjev->datum_pregleda)->format('d.m.Y.') : '' }}</li>
                            <li class="mb-1">@include('layouts.back.partials.badge-status', ['status' => $zahtjev->status])</li>
                        </ul>
                    </div>
                    <div class="col-md-2 text-right">
                        @if($zahtjev && $zahtjev->status == 1 && $zahtjev->ovjere->count())
                            @can('edit-own-zahtjev')
                                <div class="card-actions float-right">
                                    <button class="btn btn-outline-success" data-toggle="modal" data-target="#predaja-zahtjeva-modal">Predaj Zahtjev</button>
                                </div>
                            @endcan
                        @endif
                        @if($zahtjev && in_array($zahtjev->status, [2, 3]) && $zahtjev->ovjere->count())
                            <div class="card-actions float-right">
                                <a href="{{ route('zahtjev.print.lista', ['zahtjev' => $zahtjev]) }}" class="btn btn-outline-secondary">Print</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">
                    Ovjere prema zahtjevu<span class="ml-1 text-info">HR-{{ $zahtjev->institucija->broj }}-{{ $zahtjev->id }}</span>
                    <span class="float-right font-weight-lighter">{{ $zahtjev->ovjere->count() }} kom.</span>
                </h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Rb.</th>
                            <th>Šifra mjerila</th>
                            <th class="text-center">Vrsta</th>
                            <th>Serijski broj</th>
                            <th>Tip</th>
                            <th>Oznaka tipa</th>
                            <th>Mjesto Pregleda</th>
                            <th>Naljepnica / Žig</th>
                            <th class="text-center">Status</th>
                            @if (in_array($zahtjev->status, [1, 2]))
                                @if (auth()->user()->can('edit-all-ovjera') || auth()->user()->can('edit-group-ovjera'))
                                    <th class="text-center">Akcija</th>
                                @endif
                                @if (auth()->user()->can('edit-own-ovjera') && auth()->user()->id == $zahtjev->mjeritelj_id)
                                    <th class="text-center">Akcija</th>
                                @endif
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($ovjere as $ovjera)
                            <tr>
                                <td>{{ $loop->index + 1 }}.</td>
                                <td>{{ $ovjera->mjerilo->sifra->sifra }}</td>
                                <td class="text-center">{{ $ovjera->vrsta }}</td>
                                <td>{{ $ovjera->mjerilo->sn }}</td>
                                <td>{{ $ovjera->mjerilo->tip->naziv }}</td>
                                <td>{{ $ovjera->mjerilo->tip->oznaka }}</td>
                                <td>{{ $ovjera->mjesto_ovjere->city }}, <span class="font-weight-light">{{ $ovjera->mjesto_ovjere->address }}</span></td>
                                <td>
                                    @if ($ovjera->naljepnica) {{ $ovjera->naljepnica }} @endif
                                    @if ($ovjera->zig) {{ $ovjera->zig }} @endif
                                </td>
                                <td class="text-center">@include('layouts.back.partials.ovjera-status', ['ovjera' => $ovjera])</td>
                                @if (in_array($zahtjev->status, [1, 2]))
                                    @if (auth()->user()->can('edit-all-ovjera') || auth()->user()->can('edit-group-ovjera'))
                                        <td class="text-center">
                                            <a href="{{ route('ovjere.edit', ['ovjera' => $ovjera, 'zahtjev' => $ovjera->zahtjev]) }}" class="btn btn-primary btn-sm">Uredi</a>
                                        </td>
                                    @endif
                                    @if (auth()->user()->can('edit-own-ovjera') && auth()->user()->id == $zahtjev->mjeritelj_id)
                                        <td class="text-center">
                                            <a href="{{ route('ovjere.edit', ['ovjera' => $ovjera, 'zahtjev' => $ovjera->zahtjev]) }}" class="btn btn-primary btn-sm">Uredi</a>
                                        </td>
                                    @endif
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="9">Nema ovjera po zahtjevu <strong>{{ $zahtjev->id }}</strong></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $ovjere->links() }}
                <div class="row col mt-3">
                    <button class="btn btn-outline-secondary" data-toggle="modal" data-target="#napomena-zahtjeva-modal"><i class="fa fa-comment mr-2"></i>Dodaj Napomenu</button>
                    {{--@if ($zahtjev->ovjere()->count() && $zahtjev->status == 2)
                        <a href="{{ route('zahtjevi.ovjeri', ['zahtjev' => $zahtjev]) }}" class="btn btn-success ml-4">Ovjeri Zahtjev</a>
                    @endif--}}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="predaja-zahtjeva-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Predaja Zahtjeva</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <p class="mb-0">Nakon predaje zahtjeva više nećete biti u mogučnosti editirati zahtjev, niti ćete moći dodavati nove ovjere. Jeste li sigurni da želite predati zahtjev?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                    <button type="button" class="btn btn-success" wire:click="predajZahtjev()" data-dismiss="modal">Predaj Zahtjev</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="napomena-zahtjeva-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upiši Napomenu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <textarea wire:model.defer="napomena" class="form-control" rows="6"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                    <button type="button" class="btn btn-success" wire:click="napomenaZahtjeva()" data-dismiss="modal">Predaj Zahtjev</button>
                </div>
            </div>
        </div>
    </div>

</div>