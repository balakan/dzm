<div class="row">
    <div class="col-12 col-lg-8">
        <div class="card">
            <div class="card-body pb-0">
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <h5 class="card-title mt-2">Lista zahtjeva <span class="ml-4 text-info">{{ number_format($zahtjevi->total(), 0, '', '.') }}</span></h5>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="input-group">
                            <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži zahtjeve...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary btn-search" wire:click="clickSearch" type="button"><i class="align-middle fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <button class="btn btn-secondary btn-search btn-block btn-lg" wire:click="showFilterClicked({{ $show_filter }})" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample" type="button"><i class="align-middle fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                <div class="collapse {{ $show_filter ? 'show' : '' }} mt-3 mb-0" id="collapseExample">
                    <div class="card bg-light mb-0 border">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-md-4 pt-2">
                                            Odaberite što pretražujete.
                                        </div>
                                        <div class="col-lg-4 mb-2">
                                            <div class="input-group date" id="date_from" data-target-input="nearest">
                                                <input type="text" id="date_from_text" class="form-control form-control-lg datetimepicker-input" data-target="#date_from" value="" placeholder="Datum od"/>
                                                <div class="input-group-append" data-target="#date_from" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 mb-2">
                                            <div class="input-group date" id="date_to" data-target-input="nearest">
                                                <input type="text" id="date_to_text" class="form-control form-control-lg datetimepicker-input" data-target="#date_to" value="" placeholder="Datum do" />
                                                <div class="input-group-append" data-target="#date_to" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-2 mb-4">
                                </div>
                                <div class="col-12 col-md-2">
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="klasa" wire:model="state.klasa">
                                        <label class="custom-control-label" for="klasa">Klasa</label>
                                    </div>
                                    @can('search-oznaka-zahtjev')
                                        <div class="custom-control custom-switch mb-2">
                                            <input type="checkbox" class="custom-control-input" id="oznaka" wire:model="state.oznaka">
                                            <label class="custom-control-label" for="oznaka">Oznaka</label>
                                        </div>
                                    @endcan
                                </div>
                                <div class="col-12 col-md-4">
                                    @can('search-mjeritelj-zahtjev')
                                        <div class="custom-control custom-switch mb-2">
                                            <input type="checkbox" class="custom-control-input" id="mjeritelj" wire:model="state.mjeritelj">
                                            <label class="custom-control-label" for="mjeritelj">Mjeritelj</label>
                                        </div>
                                    @endcan
                                    @can('search-institucija-zahtjev')
                                        <div class="custom-control custom-switch mb-2">
                                            <input type="checkbox" class="custom-control-input" id="institucija" wire:model="state.institucija">
                                            <label class="custom-control-label" for="institucija">Institucija</label>
                                        </div>
                                    @endcan
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <div class="col-12 mb-2">Status:</div>
                                        <div class="col-3">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="1" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">U Izradi</span>
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="2" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Predan</span>
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="3" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Završen</span>
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="0" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Svi</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Rb.</th>
                            <th>Klasa</th>
                            <th class="text-center">Institucija/Mjeritelj</th>
                            <th class="text-center">Ovjera</th>
                            <th class="text-center">Datum pregleda</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($zahtjevi as $zahtjev)
                            <tr style="cursor: pointer;" wire:click="zahtjevSelected({{ $zahtjev->id }})">
                                <td class="font-weight-bold">{{ $zahtjev->id }}</td>
                                <td class="font-weight-bold">{{ $zahtjev->klasa }}</td>
                                <td class="text-center">{{ $zahtjev->institucija->broj }} / {{ $zahtjev->mjeritelj->data->broj }}</td>
                                <td class="text-center">{{ $zahtjev->ovjere()->count() }}</td>
                                <td class="text-center">{{ $zahtjev->datum_pregleda ? \Illuminate\Support\Carbon::make($zahtjev->datum_pregleda)->format('d.m.Y.') : '' }}</td>
                                <td class="text-center">
                                    @include('layouts.back.partials.badge-status', ['status' => $zahtjev->status])
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center text-info py-4">Nema upisanih zahtjeva..!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $zahtjevi->links() }}
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="card">
            <div class="card-header">
                @if($selected_zahtjev && $selected_zahtjev->status == 1 && $selected_zahtjev->ovjere->count()/* && auth()->user()->can('edit-group-zahtjev') && auth()->user()->can('edit-own-zahtjev')*/)
                    @can('edit-zahtjev-status')
                        <div class="card-actions float-right">
                            <button class="btn btn-outline-success" data-toggle="modal" data-target="#predaja-zahtjeva-modal">Predaj Zahtjev</button>
                        </div>
                    @endcan
                @endif
                <h5 class="card-title mb-1 mt-2">{{ $selected_zahtjev ? ($selected_zahtjev->klasa ?: $selected_zahtjev->oznaka ) : 'Odaberite zahtjev' }}</h5>
            </div>
            <div class="card-body mt-0 pt-0">
                <table class="table table-sm mb-5">
                    <tbody>
                    <tr>
                        <th style="min-width: 130px;">Br. Zahtjeva</th>
                        <td>{{ $selected_zahtjev ? $selected_zahtjev->id : '' }}</td>
                    </tr>
                    {{--<tr>
                        <th>Mjeritelj</th>
                        <td>{{ $selected_zahtjev ? $selected_zahtjev->mjeritelj->name : '' }}</td>
                    </tr>
                    <tr>
                        <th>Institucija</th>
                        <td>{{ $selected_zahtjev ? $selected_zahtjev->institucija->naziv : '' }}</td>
                    </tr>--}}
                    <tr>
                        <th>Datum upisa</th>
                        <td>{{ $selected_zahtjev ? \Illuminate\Support\Carbon::make($selected_zahtjev->created_at)->format('d.m.Y.') : '' }}</td>
                    </tr>
                    <tr>
                        <th>Datum predaje</th>
                        <td>{{ $selected_zahtjev && $selected_zahtjev->datum_predaje ? \Illuminate\Support\Carbon::make($selected_zahtjev->datum_predaje)->format('d.m.Y.') : '' }}</td>
                    </tr>
                    <tr>
                        <th>Datum pregleda</th>
                        <td>{{ $selected_zahtjev && $selected_zahtjev->datum_pregleda ? \Illuminate\Support\Carbon::make($selected_zahtjev->datum_pregleda)->format('d.m.Y.') : '' }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($selected_zahtjev)
                                @include('layouts.back.partials.badge-status', ['status' => $selected_zahtjev->status])
                            @endif
                        </td>
                    </tr>
                    {{--<tr>
                        <th>Mjesto ovjere</th>
                        <td>{{ $selected_zahtjev ? $selected_zahtjev->ovjere->count(): '' }}</td>
                    </tr>--}}
                    <tr>
                        <th>Napomena</th>
                        <td>{{ $selected_zahtjev ? $selected_zahtjev->napomena : '' }}</td>
                    </tr>
                    </tbody>
                </table>
                <div class="row">
                    @if($selected_zahtjev)
                        <div class="col-md-6 mb-3">
                            <a href="{{ route('zahtjevi.show', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-secondary btn-view btn-block">Prikaži zahtjev</a>
                        </div>

                        @can('edit-all-zahtjev')
                            <div class="col-md-6">
                                <a href="{{ route('zahtjevi.edit', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-primary btn-edit btn-block">Uredi</a>
                            </div>
                        @endcan
                        @can('edit-group-zahtjev')
                            @if (auth()->user()->isAn('voditelj'))
                                @if ($selected_zahtjev->status == 1)
                                    <div class="col-md-6">
                                        <a href="{{ route('zahtjevi.edit', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-primary btn-edit btn-block">Uredi</a>
                                    </div>
                                @endif
                            @else
                                <div class="col-md-6">
                                    <a href="{{ route('zahtjevi.edit', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-primary btn-edit btn-block">Uredi</a>
                                </div>
                            @endif
                            @if ($selected_zahtjev->status == 1 && auth()->user()->id == $selected_zahtjev->mjeritelj_id)
                                <div class="col-md-6">
                                    <a href="{{ route('ovjere.create', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-primary btn-new btn-block">Dodaj Ovjeru</a>
                                </div>
                            @endif
                        @endcan
                        @can('edit-own-zahtjev')
                            @if ($selected_zahtjev->status == 1)
                                <div class="col-md-6">
                                    <a href="{{ route('zahtjevi.edit', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-primary btn-edit btn-block">Uredi</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('ovjere.create', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-primary btn-new btn-block">Dodaj Ovjeru</a>
                                </div>
                            @endif
                            @if ($selected_zahtjev->status == 2 && ! auth()->user()->isAn('mjeritelj-servis'))
                                <div class="col-md-6">
                                    <a href="{{ route('zahtjevi.ovjeri', ['zahtjev' => $selected_zahtjev]) }}" class="btn btn-primary btn-save btn-block">Ovjeri Zahtjev</a>
                                </div>
                            @endif
                        @endcan
                    @endif
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $(() => {
                //
                $('#date_from').datetimepicker({
                    format: 'DD.MM.YYYY'
                });
                $('#date_from_text').on('input', (e) => {
                    let from = parseDate($('#date_from_text').val(), 'dd.mm.yyyy');

                    if (!moment(from).isSame(Date.now(), 'day') && localStorage.getItem('dmz_date_from') != $('#date_from_text').val()) {
                        localStorage.setItem('dmz_date_from', $('#date_from_text').val());
                        @this.fromSelected($('#date_from_text').val());
                    }
                });
                //
                $('#date_to').datetimepicker({
                    format: 'DD.MM.YYYY'
                });
                $('#date_to_text').on('input', (e) => {
                    let to = parseDate($('#date_to_text').val(), 'dd.mm.yyyy');

                    if (!moment(to).isSame(Date.now(), 'day') && localStorage.getItem('dmz_date_to') != $('#date_to_text').val()) {
                        localStorage.setItem('dmz_date_to', $('#date_to_text').val());
                        @this.toSelected($('#date_to_text').val());
                    }
                });
            });
        </script>

        <script>
            Livewire.on('success_alert', () => {
                successNotify();
            });

            Livewire.on('error_alert', () => {
                errorNotify();
            });
        </script>
    @endpush

    <div class="modal fade" id="predaja-zahtjeva-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Predaja Zahtjeva</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <p class="mb-0">Nakon predaje zahtjeva više nećete biti u mogučnosti editirati zahtjev, niti ćete moći dodavati nove ovjere. Jeste li sigurni da želite predati zahtjev?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
                    <button type="button" class="btn btn-success" wire:click="predajZahtjev()" data-dismiss="modal">Predaj Zahtjev</button>
                </div>
            </div>
        </div>
    </div>
</div>