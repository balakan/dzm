<div>
    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Shema boja</h5>
        </div>
        <div class="card-body">
            <span class="d-block text-muted mb-2">Izabarite temu za vaše admin sučelje.</span>
            <div class="row no-gutters text-center mx-n1 mb-2">
                <div class="col">
                    <label class="mx-1 d-block mb-1">
                        <input class="settings-scheme-label" type="radio" wire:model="theme" value="default" wire:change="changedColorScheme" {{ $theme == 'default' ? 'checked' : '' }}>
                        <div class="settings-scheme">
                            <div class="settings-scheme-theme settings-scheme-theme-default"></div>
                        </div>
                    </label>
                    Default
                </div>
                <div class="col">
                    <label class="mx-1 d-block mb-1">
                        <input class="settings-scheme-label" type="radio" wire:model="theme" value="colored" wire:change="changedColorScheme" {{ $theme == 'colored' ? 'checked' : '' }}>
                        <div class="settings-scheme">
                            <div class="settings-scheme-theme settings-scheme-theme-colored"></div>
                        </div>
                    </label>
                    Colored
                </div>

                <div class="col">
                    <label class="mx-1 d-block mb-1">
                        <input class="settings-scheme-label" type="radio" wire:model="theme" value="dark" wire:change="changedColorScheme" {{ $theme == 'dark' ? 'checked' : '' }}>
                        <div class="settings-scheme">
                            <div class="settings-scheme-theme settings-scheme-theme-dark"></div>
                        </div>
                    </label>
                    Dark
                </div>
                <div class="col">
                    <label class="mx-1 d-block mb-1">
                        <input class="settings-scheme-label" type="radio" wire:model="theme" value="light" wire:change="changedColorScheme" {{ $theme == 'light' ? 'checked' : '' }}>
                        <div class="settings-scheme">
                            <div class="settings-scheme-theme settings-scheme-theme-light"></div>
                        </div>
                    </label>
                    Light
                </div>

            </div>

        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Položaj bočne trake</h5>
        </div>
        <div class="card-body">
            <span class="d-block text-muted mb-2">Prebacite položaj bočne trake.</span>
            <div>
                <label>
                    <input class="settings-button-label" type="radio" wire:model="sidebar_position" wire:change="changeSidebarPosition" value="left">
                    <div class="settings-button">
                        Lijevo
                    </div>
                </label>
                <label>
                    <input class="settings-button-label" type="radio" wire:model="sidebar_position" wire:change="changeSidebarPosition" value="right">
                    <div class="settings-button">
                        Desno
                    </div>
                </label>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Ponašanje bočne trake</h5>
        </div>
        <div class="card-body">
            <span class="d-block text-muted mb-2">Promijenite ponašanje bočne trake.</span>
            <div>
                <label>
                    <input class="settings-button-label" type="radio" wire:model="sidebar_behavior" wire:change="changeSidebarBehavior" value="sticky">
                    <div class="settings-button">
                       Zaljepljeno
                    </div>
                </label>
                <label>
                    <input class="settings-button-label" type="radio" wire:model="sidebar_behavior" wire:change="changeSidebarBehavior" value="fixed">
                    <div class="settings-button">
                        Fiksno
                    </div>
                </label>
                <label>
                    <input class="settings-button-label" type="radio" wire:model="sidebar_behavior" wire:change="changeSidebarBehavior" value="compact">
                    <div class="settings-button">
                        Kompaktno
                    </div>
                </label>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Vidljivost bočne trake</h5>
        </div>
        <div class="card-body">
            <span class="d-block text-muted mb-2">Promijenite vidljivost bočne trake.</span>
            <div>
                <label>
                    <input class="settings-button-label" type="radio" wire:model="sidebar_visibility" wire:change="changeSidebarVisibility" value="default">
                    <div class="settings-button">
                        Vidljivo
                    </div>
                </label>
                <label>
                    <input class="settings-button-label" type="radio" wire:model="sidebar_visibility" wire:change="changeSidebarVisibility" value="collapsed">
                    <div class="settings-button">
                        Skriveno
                    </div>
                </label>
            </div>
        </div>
    </div>

</div>

@push('scripts')
    <script>
        //
        Livewire.on('success_alert', response => {
            if (response.tag === 'theme' || response.tag == 'sidebarVisibility'){
                window.location.reload(window.location.pathname);
            }
            // Set data attributes on body element
            document.body.dataset[response.tag] = response.value;

            successNotify();
        });
        //
        Livewire.on('error_alert', () => {
            errorNotify();
        });
    </script>
@endpush
