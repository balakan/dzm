<div class="card">
    <div class="card-header">
        <div class="card-actions float-right">
            <div class="dropdown show">
                <a href="#" data-toggle="dropdown" data-display="static">
                    <i class="align-middle" data-feather="more-horizontal"></i>
                </a>
            </div>
        </div>
        <h5 class="card-title mb-0">{{ __('Ažuriraj lozinku') }}</h5>
    </div>
    <div class="card-body">
        <form wire:submit.prevent="updateUserPassword">
            <div class="form-group">
                <x-jet-label for="currentPassword" value="{{ __('Trenutna lozinka') }}" />
                <x-jet-input id="currentPassword" type="password" wire:model.defer="state.current_password" />
                <x-jet-input-error for="fname" class="mt-2" />
            </div>
            <div class="form-group">
                <x-jet-label for="password" value="{{ __('Nova lozinka') }}" />
                <x-jet-input id="password" type="password" wire:model.defer="state.password" />
                <x-jet-input-error for="lname" class="mt-2" />
            </div>
            <div class="form-group">
                <x-jet-label for="passwordConfirmation" value="{{ __('Potvrdi lozinku') }}" />
                <x-jet-input id="passwordConfirmation" type="password" wire:model.defer="state.password_confirmation" />
                <x-jet-input-error for="lname" class="mt-2" />
            </div>

            <x-jet-button class="btn-primary mt-3">
                <i wire:loading class="fa fa-spinner fa-spin"></i>
                {{ __('Snimi') }}
            </x-jet-button>
        </form>

    </div>
</div>

@push('scripts')
    <script>
        window.livewire.on('password_saved', () => {
            successNotify();
        });
        window.livewire.on('password_error', () => {
            errorNotify();
        });
    </script>
@endpush
