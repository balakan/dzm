<div class="card">
    <div class="card-header">
        <div class="card-actions float-right">
            <div class="dropdown show">
                <a href="#" data-toggle="dropdown" data-display="static">
                    <i class="align-middle" data-feather="more-horizontal"></i>
                </a>
            </div>
        </div>
        <h5 class="card-title mb-0">Podaci o privatnom profilu</h5>
    </div>
    <div class="card-body">
        <form wire:submit.prevent="changeUserProfile">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <x-jet-label for="inputFirstName" value="{{ __('Ime') }}" />
                    <x-jet-input id="inputFirstName" type="text" wire:model.defer="fname" />
                    <x-jet-input-error for="fname" class="mt-2" />
                </div>
                <div class="form-group col-md-6">
                    <x-jet-label for="inputLastName" value="{{ __('Prezime') }}" />
                    <x-jet-input id="inputLastName" type="text" wire:model.defer="lname" />
                    <x-jet-input-error for="lname" class="mt-2" />
                </div>
            </div>
            <div class="form-group">
                <x-jet-label for="inputAddress" value="{{ __('Adresa') }}" />
                <x-jet-input id="inputAddress" type="text" wire:model.defer="address" />
                <x-jet-input-error for="address" class="mt-2" />
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <x-jet-label for="inputZip" value="{{ __('Poštanski broj') }}" />
                    <x-jet-input id="inputZip" type="text" wire:model.defer="zip" />
                    <x-jet-input-error for="zip" class="mt-2" />
                </div>
                <div class="form-group col-md-4">
                    <x-jet-label for="inputCity" value="{{ __('Grad') }}" />
                    <x-jet-input id="inputCity" type="text" wire:model.defer="city" />
                    <x-jet-input-error for="city" class="mt-2" />
                </div>
                <div class="form-group col-md-3">
                    <x-jet-label for="inputRegion" value="{{ __('Regija') }}" />
                    <x-jet-input id="inputRegion" type="text" wire:model.defer="region" />
                    <x-jet-input-error for="region" class="mt-2" />
                </div>
                <div class="form-group col-md-3">
                    <x-jet-label for="inputPhone" value="{{ __('Telefon') }}" />
                    <x-jet-input id="inputPhone" type="text" wire:model.defer="phone" />
                    <x-jet-input-error for="phone" class="mt-2" />
                </div>
            </div>


            @if($success_message)
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <div class="alert-icon">
                                <i class="fa fa-fw fa-check"></i>
                            </div>
                            <div class="alert-message">
                                {!! $success_message !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <x-jet-button class="btn-primary mt-3">
                <i wire:loading class="fa fa-spinner fa-spin"></i>
                {{ __('Snimi') }}
            </x-jet-button>
        </form>

    </div>
</div>
