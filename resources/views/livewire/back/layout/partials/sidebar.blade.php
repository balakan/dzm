<nav id="sidebar" class="sidebar {{ $setup->admin_sidebar_visibility }}">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="{{ route('dashboard') }}">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
                <path d="M19.4,4.1l-9-4C10.1,0,9.9,0,9.6,0.1l-9,4C0.2,4.2,0,4.6,0,5s0.2,0.8,0.6,0.9l9,4C9.7,10,9.9,10,10,10s0.3,0,0.4-0.1l9-4,C19.8,5.8,20,5.4,20,5S19.8,4.2,19.4,4.1z"/>
                <path d="M10,15c-0.1,0-0.3,0-0.4-0.1l-9-4c-0.5-0.2-0.7-0.8-0.5-1.3c0.2-0.5,0.8-0.7,1.3-0.5l8.6,3.8l8.6-3.8c0.5-0.2,1.1,0,1.3,0.5,c0.2,0.5,0,1.1-0.5,1.3l-9,4C10.3,15,10.1,15,10,15z"/>
                <path d="M10,20c-0.1,0-0.3,0-0.4-0.1l-9-4c-0.5-0.2-0.7-0.8-0.5-1.3c0.2-0.5,0.8-0.7,1.3-0.5l8.6,3.8l8.6-3.8c0.5-0.2,1.1,0,1.3,0.5,c0.2,0.5,0,1.1-0.5,1.3l-9,4C10.3,20,10.1,20,10,20z"/>
            </svg>

            <span class="align-middle mr-3">{{ config('app.name', 'Laravel') }} Admin</span>
        </a>

        <ul class="sidebar-nav">

            @if (\Bouncer::is(auth()->user())->notA('public'))
                <li class="sidebar-header">Navigacija</li>

                <li class="sidebar-item {{ request()->routeIs('dashboard') ? 'active' : '' }}">
                    <a class="sidebar-link" href="{{ route('dashboard') }}">
                        <i class="align-middle" data-feather="grid"></i> <span class="align-middle">Nadzorna ploča</span>
                    </a>
                </li>

                <li class="sidebar-item {{ request()->routeIs(['grupe', 'tipovi', 'sifre', 'grupe.*', 'tipovi.*', 'sifre.*']) ? 'active' : '' }}">
                    <a href="#catalog" data-toggle="collapse" class="sidebar-link {{ request()->routeIs(['grupe', 'tipovi', 'sifre', 'grupe.*', 'tipovi.*', 'sifre.*']) ? '' : 'collapsed' }}">
                        <i class="align-middle" data-feather="layers"></i> <span class="align-middle">Katalog</span>
                    </a>
                    <ul id="catalog" class="sidebar-dropdown list-unstyled collapse {{ request()->routeIs(['grupe', 'mjerila', 'tipovi', 'sifre', 'grupe.*', 'mjerila.*', 'tipovi.*', 'sifre.*']) ? 'show' : '' }}" data-parent="#sidebar">
                        <li class="sidebar-item {{ request()->routeIs(['grupe', 'grupe.*']) ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('grupe') }}">{{ __('Grupe') }}</a></li>
                        <li class="sidebar-item {{ request()->routeIs(['mjerila', 'mjerila.*']) ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('mjerila') }}">Mjerila</a></li>
                        <li class="sidebar-item {{ request()->routeIs(['sifre', 'sifre.*']) ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('sifre') }}">{{ __('Šifre') }}</a></li>
                        <li class="sidebar-item {{ request()->routeIs(['tipovi', 'tipovi.*']) ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('tipovi') }}">{{ __('Tipovi') }}</a></li>
                    </ul>
                </li>

                <li class="sidebar-item {{ request()->routeIs(['institucije', 'korisnici', 'institucije.*', 'korisnici.*']) ? 'active' : '' }}">
                    <a href="#users" data-toggle="collapse" class="sidebar-link {{ request()->routeIs(['institucije', 'korisnici', 'institucije.*', 'korisnici.*']) ? '' : 'collapsed' }}">
                        <i class="align-middle" data-feather="users"></i> <span class="align-middle">{{ __('Korisnici') }}</span>
                    </a>
                    <ul id="users" class="sidebar-dropdown list-unstyled collapse {{ request()->routeIs(['institucije', 'korisnici', 'institucije.*', 'korisnici.*']) ? 'show' : '' }}" data-parent="#sidebar">
                        {{--@can('view-zavod')
                            <li class="sidebar-item {{ request()->routeIs('zavod') ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('zavod') }}">Administracija</a></li>
                        @endcan
                        @can('view-inspekcija')
                            <li class="sidebar-item {{ request()->routeIs('inspekcija') ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('inspekcija') }}">Inspekcija</a></li>
                        @endcan
                        <li class="sidebar-item {{ request()->routeIs('ovlastena-tijela', 'ovlastena-tijela.*') ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('ovlastena-tijela') }}">Voditelji</a></li>
                        <li class="sidebar-item {{ request()->routeIs('mjeritelji', 'mjeritelji.*') ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('mjeritelji') }}">Mjeritelji</a></li>--}}

                        @if (auth()->user()->can('view-own-institucije') || auth()->user()->can('view-all-institucije'))
                            <li class="sidebar-item {{ request()->routeIs('institucije', 'institucije.*') ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('institucije') }}">Institucije</a></li>
                        @endif
                        <li class="sidebar-item {{ request()->routeIs('korisnici', 'korisnici.*') ? 'active' : '' }}"><a class="sidebar-link" href="{{ route('korisnici') }}">Korisnici</a></li>
                    </ul>
                </li>

                <li class="sidebar-item {{ request()->routeIs(['zahtjevi', 'zahtjevi.*']) ? 'active' : '' }}">
                    <a class="sidebar-link" href="{{ route('zahtjevi') }}">
                        <i class="align-middle" data-feather="clipboard"></i> <span class="align-middle">{{ __('Zahtjevi') }}</span>
                    </a>
                </li>

                <li class="sidebar-item {{ request()->routeIs(['ovjere', 'ovjere.*']) ? 'active' : '' }}">
                    <a class="sidebar-link" href="#">
                        <i class="align-middle" data-feather="check-circle"></i> <span class="align-middle">Ovjernice</span>
                    </a>
                </li>

                <li class="sidebar-item {{ request()->routeIs(['obracuni', 'obracuni.*']) ? 'active' : '' }}">
                    <a href="#report" data-toggle="collapse" class="sidebar-link {{ request()->routeIs(['obracuni', 'obracuni.*']) ? '' : 'collapsed' }}">
                        <i class="align-middle" data-feather="pie-chart"></i> <span class="align-middle">{{ __('Report') }}</span>
                    </a>
                    <ul id="report" class="sidebar-dropdown list-unstyled collapse {{ request()->routeIs(['obracuni', 'obracuni.*']) ? 'show' : '' }}" data-parent="#sidebar">
                        <li class="sidebar-item {{ request()->routeIs(['obracuni', 'obracuni.*']) ? 'active' : '' }}"><a class="sidebar-link" href="#{{--{{ route('obracuni') }}--}}">{{ __('Obračuni') }}</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="#{{--{{ route('obracuni') }}--}}">Izvještaji</a></li>
                    </ul>
                </li>
            @endif

            <li class="sidebar-header">
                {{ __('Administracija') }}
            </li>

            {{--<li class="sidebar-item">
                <a class="sidebar-link" href="#">
                    <i class="align-middle" data-feather="bell"></i> <span class="align-middle">{{ __('Notifikacije') }}</span>
                    <span class="badge badge-sidebar-primary">5</span>
                </a>
            </li>--}}

            <li class="sidebar-item {{ request()->routeIs('profile.show') ? 'active' : '' }}">
                <a class="sidebar-link" href="{{ route('profile.show') }}">
                    <i class="align-middle" data-feather="user"></i> <span class="align-middle">{{ __('Profil') }}</span>
                </a>
            </li>
            @can('*')
                <li class="sidebar-item {{ request()->routeIs('uloge') ? 'active' : '' }}">
                    <a class="sidebar-link" href="{{ route('uloge') }}">
                        <i class="align-middle" data-feather="lock"></i> <span class="align-middle">{{ __('Uloge i ovlaštenja') }}</span>
                    </a>
                </li>
            @endcan

        </ul>
    </div>
</nav>
