<nav class="navbar navbar-expand navbar-light navbar-bg">
    <a class="sidebar-toggle" @click="sidebarCollapsed = ! sidebarCollapsed">
        <i class="hamburger align-self-center"></i>
    </a>

    {{--<form class="d-none d-sm-inline-block">
        <div class="input-group input-group-navbar">
            <input type="text" class="form-control" placeholder="Pretraži..."  aria-label="Search">
            <div class="input-group-append">
                <button class="btn" type="button">
                    <i class="align-middle" data-feather="search"></i>
                </button>
            </div>
        </div>
    </form>--}}

    @if ($setup->admin_sidebar_visibility == 'collapsed')
        <ul class="navbar-nav ml-7" x-show.transition.duration.600ms="sidebarCollapsed">
            <li class="nav-item px-2">
                <a class="nav-link" href="{{ route('dashboard') }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('Nadzorna ploča') }}">
                    <i class="align-middle" data-feather="grid"></i>
                </a>
            </li>

            <li class="nav-item px-2 dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="catalog-drop" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="align-middle" data-feather="layers"></i> <span class="align-middle">{{ __('Katalog') }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-left dropdown-mega" aria-labelledby="catalog-drop">
                    <div class="d-md-flex align-items-start justify-content-start">
                        <div class="dropdown-mega-list">
                            {{--<div class="dropdown-header">{{ __('Katalog mjerila') }}</div>--}}
                            <a class="dropdown-item" href="{{ route('grupe') }}">{{ __('Grupe') }}</a>
                            <a class="dropdown-item" href="{{ route('mjerila') }}">Mjerila</a>
                            <a class="dropdown-item" href="{{ route('tipovi') }}">{{ __('Tipovi') }}</a>
                            <a class="dropdown-item" href="{{ route('sifre') }}">{{ __('Šifre') }}</a>
                        </div>
                    </div>
                </div>
            </li>

            <li class="nav-item px-2 dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="user-drop" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="align-middle" data-feather="users"></i> <span class="align-middle">{{ __('Korisnici') }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-left dropdown-mega" aria-labelledby="user-drop">
                    <div class="d-md-flex align-items-start justify-content-start">
                        <div class="dropdown-mega-list">
                            {{--<div class="dropdown-header">{{ __('Korisnici') }}</div>--}}
                            <a class="dropdown-item" href="{{ route('institucije') }}">Institucije</a>
                            <a class="dropdown-item" href="{{ route('korisnici') }}">Korisnici</a>
                        </div>
                    </div>
                </div>
            </li>

            <li class="nav-item px-2">
                <a class="nav-link" href="{{ route('zahtjevi') }}">
                    <i class="align-middle" data-feather="clipboard"></i> <span class="align-middle">{{ __('Zahtjevi') }}</span>
                </a>
            </li>

            <li class="nav-item px-2">
                <a class="nav-link" href="{{ route('ovjere') }}">
                    <i class="align-middle" data-feather="check-circle"></i> <span class="align-middle">{{ __('Ovjere') }}</span>
                </a>
            </li>

            <li class="nav-item px-2 dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="report-drop" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="align-middle" data-feather="pie-chart"></i> <span class="align-middle">{{ __('Report') }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-left dropdown-mega" aria-labelledby="report-drop">
                    <div class="d-md-flex align-items-start justify-content-start">
                        <div class="dropdown-mega-list">
                            {{--<div class="dropdown-header">{{ __('Report') }}</div>--}}
                            <a class="dropdown-item" href="{{ route('obracuni') }}">{{ __('Obračuni') }}</a>
                        </div>
                    </div>
                </div>
            </li>

            {{--<li class="nav-item px-2 dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="servicesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ __('Brze Poveznice') }}
                </a>
                <div class="dropdown-menu dropdown-menu-left dropdown-mega" aria-labelledby="servicesDropdown">
                    <div class="d-md-flex align-items-start justify-content-start">
                        <div class="dropdown-mega-list">
                            <div class="dropdown-header">{{ __('Brze Poveznice') }}</div>
                            <a class="dropdown-item" href="{{ route('dashboard') }}">{{ __('Nadzorna ploča') }}</a>
                            <a class="dropdown-item" href="{{ route('zahtjevi') }}">{{ __('Zahtjevi') }}</a>
                            <a class="dropdown-item" href="{{ route('ovjere') }}">{{ __('Ovjere') }}</a>
                            <a class="dropdown-item" href="{{ route('obracuni') }}">{{ __('Obračuni') }}</a>
                            <a class="dropdown-item" href="{{ route('profile.show') }}">{{ __('Profil') }}</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item px-2 dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="servicesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ __('Katalog mjerila') }}
                </a>
                <div class="dropdown-menu dropdown-menu-left dropdown-mega" aria-labelledby="servicesDropdown">
                    <div class="d-md-flex align-items-start justify-content-start">
                        <div class="dropdown-mega-list">
                            <div class="dropdown-header">{{ __('Katalog mjerila') }}</div>
                            <a class="dropdown-item" href="{{ route('grupe') }}">{{ __('Grupe') }}</a>
                            <a class="dropdown-item" href="{{ route('tipovi') }}">{{ __('Tipovi') }}</a>
                            <a class="dropdown-item" href="{{ route('sifre') }}">{{ __('Šifre') }}</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item px-2 dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="servicesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ __('Korisnici') }}
                </a>
                <div class="dropdown-menu dropdown-menu-left dropdown-mega" aria-labelledby="servicesDropdown">
                    <div class="d-md-flex align-items-start justify-content-start">
                        <div class="dropdown-mega-list">
                            <div class="dropdown-header">{{ __('Korisnici') }}</div>
                            <a class="dropdown-item" href="{{ route('zavod') }}">{{ __('Zavod') }}</a>
                            <a class="dropdown-item" href="{{ route('inspekcija') }}">{{ __('Inspekcija') }}</a>
                            <a class="dropdown-item" href="{{ route('ovlastena-tijela') }}">{{ __('Ovlaštena tijela') }}</a>
                            <a class="dropdown-item" href="{{ route('mjeritelji') }}">{{ __('Mjeritelji') }}</a>
                        </div>
                    </div>
                </div>
            </li>--}}
        </ul>
    @endif

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav navbar-align">
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-toggle="dropdown">
                    <i class="align-middle" data-feather="settings"></i>
                </a>

                @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                    <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
                        <img src="{{ asset(Auth::user()->profile_photo_url) }}" class="avatar img-fluid rounded-circle mr-1" alt="{{ Auth::user()->name }}" /> <span class="text-dark">{{ Auth::user()->name }}</span>
                    </a>
                @else
                    <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
                        <span class="text-dark">{{ Auth::user()->name }}</span>
                    </a>
                @endif

                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ route('index') }}" target="_blank"><i class="align-middle mr-1" data-feather="monitor"></i> {{ __('Početna stranica') }}</a>
                    <a class="dropdown-item" href="{{ route('profile.show') }}"><i class="align-middle mr-1" data-feather="user"></i> {{ __('Profile') }}</a>

                    @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
                        <a class="dropdown-item" href="{{ route('api-tokens.index') }}"><i class="align-middle mr-1" data-feather="pie-chart"></i> {{ __('API Tokens') }}</a>
                    @endif

                    <div class="dropdown-divider"></div>
                    <!-- Authentication -->
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">{{ __('Log Out') }}</a>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>