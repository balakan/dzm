<div class="mb-0">
    <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" name="zahtjev" id="zahtjev-input" placeholder="Unesite oznaku zahtjeva...">
    @if( ! empty($search_results))
        <div class="autocomplete" >
            <div id="myInputautocomplete-list" class="autocomplete-items">
                @foreach($search_results as $zahtjev)
                    <div wire:click="addZahtjev({{ $zahtjev->id }})">{{ $zahtjev->klasa }} <span class="font-weight-light float-right">{{ $zahtjev->mjeritelj->name }}</span></div>
                @endforeach
            </div>
        </div>
    @endif
</div>
