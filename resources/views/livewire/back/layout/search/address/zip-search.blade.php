<div class="mb-0">
    <label class="form-label" for="inputZip">PP.</label>
    <input type="search" wire:model.debounce.600ms="search" class="form-control form-control-lg" name="zip" id="inputZip" placeholder="..." autocomplete="off">
    @if( ! empty($search_results))
        <div class="autocomplete" >
            <div id="myInputautocomplete-list" class="autocomplete-items" style="height: 360px; overflow-y:auto;">
                @foreach($search_results as $data)
                    <div wire:click="addZip('{{ $data['BrojPu'] }}', '{{ $data['Naselje'] }}')">
                        <small class="font-weight-lighter">{{ $data['BrojPu'] }}</small>, {{ $data['Naselje'] }}<br>
                        <small class="font-weight-lighter">{{ $data['Zupanija'] }}</small>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>
