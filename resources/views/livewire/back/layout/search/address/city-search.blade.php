<div class="mb-0">
    <label class="form-label required" for="inputGrad">Grad</label>
    <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" name="city" id="city-input" placeholder="..." autocomplete="off">
    @if( ! empty($search_results))
        <div class="autocomplete" >
            <div id="myInputautocomplete-list" class="autocomplete-items">
                @foreach($search_results as $data)
                    <div wire:click="addCity('{{ $data['Naselje'] }}')">
                        <small class="font-weight-lighter">{{ $data['BrojPu'] }}</small>, {{ $data['Naselje'] }}<br>
                        <small class="font-weight-lighter">{{ $data['Zupanija'] }}</small>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>
