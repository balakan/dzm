<div class="mb-0">
    <label class="form-label" for="inputRegija">Regija</label>
    <input type="text" wire:model.debounce.300ms="search" class="form-control form-control-lg" name="region" id="inputRegija" placeholder="" autocomplete="off">
    {{--@if( ! empty($search_results))
        <div class="autocomplete" >
            <div id="myInputautocomplete-list" class="autocomplete-items">
                @foreach($search_results as $data)
                    <div wire:click="addCity('{{ $data['nazivPu'] }}')">
                        <small class="font-weight-lighter">{{ $data['brojPu'] }}</small>, {{ $data['nazivPu'] }}<br>
                        <small class="font-weight-lighter">{{ $data['zupanija'] }}</small>
                    </div>
                @endforeach
            </div>
        </div>
    @endif--}}
</div>
