<div class="mb-0 input-group">
    <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg @error('mjerilo_mjesto_postavljanja_id') is-invalid @enderror" placeholder="Dodaj mjesto postavljanja...">
    <input type="hidden" wire:model="mjesto_postavljanja_id" name="mjerilo_mjesto_postavljanja_id">
    <span class="input-group-append" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dodaj novo mjesto postavljanja">
        <a wire:click="viewAddWindow" class="btn btn-secondary btn-search"><i class="fa fa-user-plus pt-2"></i></a>
    </span>
    @if($show_add_window)
        <div class="autocomplete p-3" style="position:absolute; z-index:10; top:38px; background-color: #f6f6f6; border: 1px solid #d7d7d7;">
            <div class="row">
                <div class="mb-3 col-md-6">
                    <label class="form-label required" for="inputAdresa">Adresa</label>
                    <input type="text" class="form-control form-control-lg" id="inputAdresa" wire:model="address" value="" placeholder="">
                </div>
                <div class="mb-3 col-md-6">
                    <input type="hidden" id="zip-input" wire:model="zip">
                    @livewire('back.layout.search.address.zip-search', ['search' => $zip])
                </div>
                <div class="mb-3 col-md-6">
                    <input type="hidden" id="city-input" wire:model="city">
                    @livewire('back.layout.search.address.city-search', ['search' => $city])
                </div>
                <div class="mb-3 col-md-6">
                    <input type="hidden" id="region-input" wire:model="region">
                    @livewire('back.layout.search.address.region-search', ['search' => $region])
                </div>
            </div>
            <div class="row col-12">
                <button type="button" class="btn btn-primary btn-save" wire:click="storeMjestoPostavljanja">Pohrani mjesto postavljanja</button>
            </div>
        </div>
    @endif

    @if( ! empty($search_results))
        <div class="autocomplete" style="position:unset;">
            <div class="autocomplete-items">
                @foreach($search_results as $mjesto_postavljanja)
                    <div style="cursor: pointer;" wire:click="addMjestoPostavljanja({{ $mjesto_postavljanja->id }})">
                        {{ $mjesto_postavljanja->address }}<br>
                        <small class="font-weight-lighter">{{ $mjesto_postavljanja->zip }} {{ $mjesto_postavljanja->city }}</small><br>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
</div>
