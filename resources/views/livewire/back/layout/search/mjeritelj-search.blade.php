<div class="mb-0">
    <input type="search"
           wire:model.debounce.500ms="search"
           class="form-control form-control-lg @error('mjeritelj') is-invalid @enderror"
           id="mjeritelj-input"
           @if($disable_input) disabled @endif
           placeholder="Dodaj korisnika..."
           autocomplete="off">
    <input type="hidden" wire:model="mjeritelj_id" name="mjeritelj">
    @if( ! empty($search_results))
        <div class="autocomplete" >
            <div id="myInputautocomplete-list" class="autocomplete-items">
                @foreach($search_results as $user)
                    <div wire:click="addUser({{ $user->id }})">
                        {{ $user->name }} <span class="float-right text-info">{{ $user->data->broj }}</span>
                        {{--<button type="button" class="btn btn-primary btn-save btn-sm float-right" wire:click="addUser({{ $user->id }})">Dodaj</button>--}}
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    @error('mjeritelj')
    <label class="small text-danger">Mjeritelj je obavezan.</label>
    @enderror
</div>
