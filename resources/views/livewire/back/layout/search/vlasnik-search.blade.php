<div class="mb-0 input-group">
    <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg @error('mjerilo_vlasnik_id') is-invalid @enderror" placeholder="Dodaj vlasnika mjerila...">
    <input type="hidden" wire:model="vlasnik_id" name="mjerilo_vlasnik_id">
    <span class="input-group-append" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dodaj novog vlasnika">
        <a wire:click="viewAddWindow" class="btn btn-secondary btn-search"><i class="fa fa-user-plus pt-2"></i></a>
    </span>
    @if($show_add_window)
        <div class="autocomplete p-3" style="position:absolute; z-index:10; top:37px; background-color: #f6f6f6; border: 1px solid #d7d7d7;">
            <div class="row">
                <div class="mb-3 col-md-12">
                    <label class="form-label required" for="inputOznaka">Naziv ili ime vlasnika</label>
                    <input type="text" class="form-control form-control-lg @error('naziv') is-invalid @enderror" id="inputNaziv" wire:model="naziv" value="" placeholder="">
                    @error('naziv')
                    <label class="small text-danger">Naziv je obvezan.</label>
                    @enderror
                </div>
                <div class="mb-3 col-md-8">
                    <label class="form-label required" for="inputAdresa">Adresa</label>
                    <input type="text" class="form-control form-control-lg" id="inputAdresa" wire:model="address" value="" placeholder="">
                </div>
                <div class="mb-3 col-md-4">
                    <input type="hidden" id="zip-input" wire:model="zip">
                    @livewire('back.layout.search.address.zip-search', ['search' => $zip])
                </div>
                <div class="mb-3 col-md-6">
                    <input type="hidden" id="city-input" wire:model="city">
                    @livewire('back.layout.search.address.city-search', ['search' => $city])
                </div>

                <div class="mb-3 col-md-6">
                    <input type="hidden" id="region-input" wire:model="region">
                    @livewire('back.layout.search.address.region-search', ['search' => $region])
                </div>

                <div class="mb-3 col-md-6">
                    <label class="form-label" for="inputEmail">Email</label>
                    <input type="text" class="form-control form-control-lg" id="inputEmail" wire:model="email" value="" placeholder="">
                </div>
                <div class="mb-3 col-md-6">
                    <label class="form-label" for="inputTelefon">Telefon</label>
                    <input type="text" class="form-control form-control-lg" id="inputTelefon" wire:model="phone" value="" placeholder="">
                </div>
            </div>
            <div class="row my-3 col-12">
                <button type="button" class="btn btn-primary btn-save" wire:click="storeVlasnik">Pohrani vlasnika</button>
            </div>
        </div>
    @endif

    @if( ! empty($search_results))
        <div class="autocomplete" style="position:unset;">
            <div class="autocomplete-items">
                @foreach($search_results as $vlasnik)
                    <div style="cursor: pointer;" wire:click="addVlasnik({{ $vlasnik->id }})">
                        {{ $vlasnik->naziv }}<br>
                        <small class="font-weight-lighter">ADRESA: {{ $vlasnik->address }}, {{ $vlasnik->zip }} {{ $vlasnik->city }}</small><br>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @push('scripts')
        <script>
            Livewire.on('zip_added', (e) => {
                refreshInputs(e.data);
            });

            Livewire.on('city_added', (e) => {
                refreshInputs(e.data);
            });

            function refreshInputs(data) {
                $('#city-input').val(data.Naselje);
                $('#zip-input').val(data.BrojPu);
                $('#region-input').val(data.Zupanija);

                let city = document.getElementById('city-input');
                city.dispatchEvent(new Event('input'));

                let zip = document.getElementById('zip-input');
                zip.dispatchEvent(new Event('input'));

                let region = document.getElementById('region-input');
                region.dispatchEvent(new Event('input'));
            }

            Livewire.on('error_required_vlasnik_alert', (e) => {
                errorNotify(e.message);
            });
        </script>
    @endpush

</div>
