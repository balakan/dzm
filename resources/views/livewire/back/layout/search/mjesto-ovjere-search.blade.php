<div class="mb-0 input-group">
    <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg @error('mjerilo_mjesto_ovjere_id') is-invalid @enderror" placeholder="Dodaj mjesto ovjere...">
    <input type="hidden" wire:model="mjesto_ovjere_id" name="mjerilo_mjesto_ovjere_id">
    <span class="input-group-append" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dodaj novo mjesto ovjere">
        <a wire:click="viewAddWindow" class="btn btn-secondary btn-search"><i class="fa fa-user-plus pt-2"></i></a>
    </span>
    @if($show_add_window)
        <div class="autocomplete p-3" style="position:absolute; z-index:10; top:38px; background-color: #f6f6f6; border: 1px solid #d7d7d7;">
            <div class="row">
                <div class="mb-3 col-md-6">
                    <label class="form-label required" for="inputAdresa">Adresa</label>
                    <input type="text" class="form-control form-control-lg" id="inputAdresa" wire:model="address" value="" placeholder="">
                </div>
                <div class="mb-3 col-md-6">
                    <input type="hidden" id="zip-input" wire:model="zip">
                    @livewire('back.layout.search.address.zip-search', ['search' => $zip])
                </div>
                <div class="mb-3 col-md-6">
                    <input type="hidden" id="city-input" wire:model="city">
                    @livewire('back.layout.search.address.city-search', ['search' => $city])
                </div>
                <div class="mb-3 col-md-6">
                    <input type="hidden" id="region-input" wire:model="region">
                    @livewire('back.layout.search.address.region-search', ['search' => $region])
                </div>
            </div>
            <div class="row col-12">
                <button type="button" class="btn btn-primary btn-save" wire:click="storeMjestoOvjere">Pohrani mjesto ovjere</button>
            </div>
        </div>
    @endif

    @if( ! empty($search_results))
        <div class="autocomplete" style="position:unset;">
            <div class="autocomplete-items">
                @foreach($search_results as $mjesto_ovjere)
                    <div style="cursor: pointer;" wire:click="addMjestoOvjere({{ $mjesto_ovjere->id }})">
                        {{ $mjesto_ovjere->address }}<br>
                        <small class="font-weight-lighter">{{ $mjesto_ovjere->zip }} {{ $mjesto_ovjere->city }}</small><br>
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @push('scripts')
        <script>
            Livewire.on('zip_added', (e) => {
                refreshInputs(e.data);
                //@this.dataAdded(e.data);
            });

            Livewire.on('city_added', (e) => {
                refreshInputs(e.data);
                //@this.dataAdded(e.data);
            });

            function refreshInputs(data) {
                $('#city-input').val(data.Naselje);
                $('#zip-input').val(data.BrojPu);
                $('#region-input').val(data.Zupanija);

                let city = document.getElementById('city-input');
                city.dispatchEvent(new Event('input'));

                let zip = document.getElementById('zip-input');
                zip.dispatchEvent(new Event('input'));

                let region = document.getElementById('region-input');
                region.dispatchEvent(new Event('input'));
            }

            Livewire.on('error_required_mjesto_ovjere_alert', (e) => {
                errorNotify(e.message);
            });
        </script>
    @endpush

</div>
