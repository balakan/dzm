<div class="mb-0">
    <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg @error('institucija') is-invalid @enderror" name="institucija" placeholder="Upiši..." autocomplete="off">
    @if( ! empty($search_results))
        <div class="autocomplete" >
            <div id="myInputautocomplete-list" class="autocomplete-items">
                @foreach($search_results as $user)
                    <div class="font-weight-bold" wire:click="addUser('{{ $user->id }}')">{{ $user->naziv }} <small class="font-weight-lighter float-right">{{ $user->broj }}</small></div>
                @endforeach
            </div>
        </div>
    @endif
</div>
