<div class="mb-0 input-group">
    <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg @error('mjerilo_id') is-invalid @enderror" id="mjerilo-input" placeholder="Dodaj mjerilo..." autocomplete="off">
    <input type="hidden" wire:model="mjerilo_id" name="mjerilo_id">
    <span class="input-group-append" data-toggle="modal" data-target="#new-mjerilo-modal">
        <a href="javascript:void(0)" wire:click="viewAddWindow" class="btn btn-secondary btn-search"><i class="fa fa-plus pt-2"></i></a>
    </span>

    <div class="autocomplete p-3" @if( ! $show_add_window) hidden @endif style="position:absolute; z-index:10; top:38px; background-color: #f6f6f6; border: 1px solid #d7d7d7;">
        <div class="row" >
            <div class="mb-4 col-sm-12 col-md-6">
                <label class="form-label required" for="input-sn">Serijski broj</label>
                <input type="text" class="form-control form-control-lg @if (session()->has('sn')) is-invalid @endif" id="input-sn" wire:model.defer="new.sn" placeholder="">
                @if (session()->has('sn')) <label class="small text-danger">Serijski broj je obvezan...</label> @endif
            </div>

            <div class="col-sm-12 col-md-6">
                <label class="form-label" for="input-max">Max. Mjerenje</label>
                <input type="text" class="form-control form-control-lg" id="input-max" wire:model.defer="new.max_mjerenje" placeholder="">
            </div>

            <div class="mb-4 col-12" wire:ignore>
                <label class="form-label required" for="sifre-select">Šifra mjerila</label>
                <select class="form-control form-control-lg select2" id="sifre-select" data-toggle="select2" >
                    <option></option>
                    @foreach ($sifre as $sifra)
                        <option value="{{ $sifra->id }}">{{ $sifra->naziv }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3 col-12" wire:ignore>
                <label class="form-label required" for="tipovi-select">Tip mjerila</label>
                <select class="form-control form-control-lg select2" id="tipovi-select" data-toggle="select2" >
                    <option></option>
                    @foreach ($tipovi as $tip)
                        <option value="{{ $tip->id }}">{{ $tip->naziv }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-0 mt-4 col-md-12 text-right">
                <a href="javascript:void(0)" wire:click="makeNewMjerilo" class="btn btn-primary btn-save shadow-sm">
                    <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                </a>
            </div>
        </div>
    </div>

    @if( ! empty($search_results))
        <div class="autocomplete pt-3" style="position:unset; background-color: #f6f6f6;">
            <div id="myInputautocomplete-list" class="autocomplete-items" @if ($search_results->count() > 2) style="height: 360px; overflow-y:auto;" @endif>
                @forelse($search_results as $mjerilo)
                    <div style="cursor: pointer;" wire:click="addMjerilo('{{ $mjerilo->sn }}')">
                        {{ $mjerilo->sn }}<br>
                        <small class="font-weight-lighter">TIP: <strong>{{ $mjerilo->tip->naziv }}</strong> - GRUPA: <strong>{{ $mjerilo->grupa->name }}</strong></small><br>
                        <small class="font-weight-lighter">ŠIFRA: <strong>{{ $mjerilo->sifra->naziv }}</strong></small><br>
                    </div>
                @empty
                    <div style="cursor: pointer;" class="text-center bg-danger">
                        <small class="text-white font-size-lg">Ne postoji mjerilo prema upitu..!</small><br>
                    </div>
                @endforelse
            </div>
        </div>
    @endif

    @push('scripts')
        <script>
            document.addEventListener('livewire:load', function () {
                $('#sifre-select').select2({
                    placeholder: "Odaberi ili upiši šifu mjerila...",
                    theme: "bootstrap"
                });

                $('#sifre-select').on('change', function (e) {
                    var data = $('#sifre-select').select2("val");
                    @this.sifraSelected(data);
                });

                $('#tipovi-select').select2({
                    placeholder: "Odaberi ili upiši tip mjerila...",
                    theme: "bootstrap"
                });
                $('#tipovi-select').on('change', function (e) {
                    var data = $('#tipovi-select').select2("val");
                    @this.tipSelected(data);
                });
            });

            Livewire.on('success_alert', () => {
                successNotify();
            });

            Livewire.on('error_alert', (e) => {
                errorNotify(e.message);
            });
        </script>
    @endpush

</div>
