<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-actions float-right">
                    <div class="input-group">
                        <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži">
                        <span class="input-group-append">
                            <button class="btn btn-secondary btn-search" type="button"><i class="align-middle fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <h5 class="card-title">Lista tipova mjerila <span class="ml-4 text-info">{{ $tipovi->total() }}</span></h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Oznaka</th>
                            <th>Proizvođač</th>
                            <th>Tip Mjerila</th>
                            <th class="text-right">Uredi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($tipovi as $tip)
                            <tr>
                                <td class="font-weight-bold">{{ $tip->oznaka }}</td>
                                <td>{{ $tip->brand }}</td>
                                <td>{{ $tip->naziv }}</td>
                                <td class="text-right">
                                    @if ($tip->pdf != '')
                                        <a class="btn btn-success btn-view btn-sm" href="{{ url($tip->pdf) }}" target="_blank">PDF</a>
                                    @endif
                                    @can('edit-tipovi')
                                        <a class="btn btn-primary btn-edit btn-sm" href="{{ route('tipovi.edit', ['tip' => $tip]) }}">Uredi</a>
                                    @endcan
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center text-info py-4">Pretražite dostupne tipove mjerila..!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $tipovi->links() }}
            </div>
        </div>
    </div>
</div>
