<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-actions float-right">
                    <div class="input-group">
                        <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži">
                        <span class="input-group-append">
                            <button class="btn btn-secondary btn-search" type="button"><i class="align-middle fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <h5 class="card-title">Lista mjerila <span class="ml-4 text-info">{{ number_format($mjerila->total(), 0, '', '.') }}</span></h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Šifra</th>
                            <th>Sn.</th>
                            <th>Proizvođač</th>
                            {{--<th>Grupa</th>--}}
                            <th>Tip</th>
                            <th>Sl. Oznaka</th>
                            @if (auth()->user()->can('edit-mjerila') || auth()->user()->can('edit-group-mjerila') || auth()->user()->can('edit-own-mjerila'))
                                <th class="text-right">Akcije</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($mjerila as $mjerilo)
                            <tr>
                                <td class="font-weight-bold">{{ $mjerilo->sifra->sifra }}</td>
                                <td>{{ $mjerilo->sn }}</td>
                                <td>{{ $mjerilo->tip->brand }}</td>
                                {{--<td>{{ isset($mjerilo->grupa->name) ? \Illuminate\Support\Str::limit($mjerilo->grupa->name, 36) : '' }}</td>--}}
                                <td>{{ $mjerilo->tip->naziv }}</td>
                                <td>
                                    {{--@if($mjerilo->last_ovjera()->first())
                                        <a href="{{ route('ovjere.show', ['ovjera' => $mjerilo->last_ovjera()->first()]) }}">{{ $mjerilo->last_ovjera()->first()->id }}</a>
                                    @endif--}}
                                    {{ $mjerilo->tip->oznaka }}
                                </td>
                                @if (auth()->user()->can('edit-mjerila') || auth()->user()->can('edit-group-mjerila') || auth()->user()->can('edit-own-mjerila') || auth()->user()->can('view-mjerila'))
                                    <td class="text-right">
                                        @can('edit-mjerila')
                                            <a class="btn btn-primary btn-edit btn-sm" href="{{ route('mjerila.edit', ['mjerilo' => $mjerilo]) }}">Uredi</a>
                                        @endcan
                                            <a class="btn btn-primary btn-view btn-sm" href="{{ route('mjerila.show', ['mjerilo' => $mjerilo]) }}">Pogledaj</a>
                                    </td>
                                @endif
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center text-info py-4">Pretražite dostupna mjerila..!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $mjerila->links() }}
            </div>
        </div>
    </div>
</div>