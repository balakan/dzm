<div class="row">
    <div class="col-12 ">
        <div class="card">
            <div class="card-header">
                <div class="card-actions float-right">
                    <div class="input-group">
                        <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži">
                        <span class="input-group-append">
                            <button class="btn btn-secondary btn-search" type="button"><i class="align-middle fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <h5 class="card-title">Lista šifri <span class="ml-4 text-info">{{ $sifre->total() }}</span></h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">Šifra</th>
                            <th>Naziv</th>
                            <th class="text-center">Ovjerno razdoblje</th>
                            <th class="text-right">Tarifa</th>
                            <th class="text-center">Naljepnica</th>
                            @can('edit-sifre')
                                <th class="text-right">Uredi</th>
                            @endcan
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sifre as $sifra)
                            <tr>
                                <td class="text-center">{{ $sifra->sifra }}</td>
                                <td class="font-weight-bold">{{ $sifra->naziv }}</td>
                                <td class="text-center">{{ $sifra->ovjerno_razdoblje }}</td>
                                <td class="text-right">{{ number_format($sifra->tarifa, 2, ',', '.') }}</td>
                                <td class="text-center">@include('layouts.back.partials.naljepnica-sifre', ['naljepnica' => $sifra->naljepnica])</td>
                                @can('edit-sifre')
                                    <td class="text-right">
                                        <a class="btn btn-primary btn-edit btn-sm" href="{{ route('sifre.edit', ['sifra' => $sifra]) }}">Uredi</a>
                                    </td>
                                @endcan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $sifre->links() }}
            </div>
        </div>
    </div>
</div>