<div class="row">
    <div class="col-12 col-lg-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <h5 class="card-title mt-2">Lista mjeritelja <span class="ml-4 text-info">{{ number_format($mjeritelji->total(), 0, '', '.') }}</span></h5>
                    </div>
                    <div class="col-sm-12 col-md-5">
                        <div class="input-group">
                            <input type="search" wire:model.debounce.300ms="search" class="form-control form-control-lg" placeholder="Pretraži mjeritelje...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary btn-search" type="button"><i class="align-middle fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <button class="btn btn-secondary btn-search btn-block btn-lg" wire:click="showFilterClicked({{ $show_filter }})" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample" type="button"><i class="align-middle fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                <div class="collapse {{ $show_filter ? 'show' : '' }} mt-3 mb-0" id="collapseExample">
                    <div class="card bg-light mb-0 border">
                        <div class="card-body pt-3">
                            <div class="row">
                                <div class="col-12">
                                    Odaberite ulogu ili pretražite uključeno.
                                    <hr class="mt-2 mb-4">
                                </div>
                                <div class="col-12 col-md-3">
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="broj" wire:model="state.ime">
                                        <label class="custom-control-label" for="broj">Ime i/ili prezime</label>
                                    </div>
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="naziv" wire:model="state.email">
                                        <label class="custom-control-label" for="naziv">Email</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-3">
                                    {{--<div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="korisnik" wire:model="state.uloga">
                                        <label class="custom-control-label" for="korisnik">Uloga</label>
                                    </div>--}}
                                    <div class="custom-control custom-switch mb-2">
                                        <input type="checkbox" class="custom-control-input" id="city" wire:model="state.institucija">
                                        <label class="custom-control-label" for="city">Institucija</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <div class="col-12 mb-2">Status:</div>
                                        <div class="col-3">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="1" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Aktivni</span>
                                            </label>
                                        </div>
                                        <div class="col-4">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="0" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Neaktivni</span>
                                            </label>
                                        </div>
                                        <div class="col-5">
                                            <label class="custom-control custom-radio">
                                                <input wire:model="state.status" name="status" type="radio" value="all" class="custom-control-input">
                                                <span class="custom-control-label" style="padding-top: 2px;">Svi</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Ime mjeritelja</th>
                            <th class="text-center">Broj</th>
                            <th class="text-center">Datum ovlaštenja do</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mjeritelji as $mjeritelj)
                            <tr style="cursor: pointer;" wire:click="userSelected({{ $mjeritelj->id }})">
                                <td class="font-weight-bold">{{ $mjeritelj->name }}</td>
                                <td class="text-center">{{ $mjeritelj->data ? $mjeritelj->data->broj : '' }}</td>
                                <td class="text-center">{{ $mjeritelj->data && $mjeritelj->data->datum_ovlastenja_do ? \Illuminate\Support\Carbon::make($mjeritelj->data->datum_ovlastenja_do)->format('d.m.Y.') : '' }}</td>
                                <td class="text-center"><i class="{{ $mjeritelj->status ? 'text-success fa fa-check' : 'text-danger fa fa-times' }}"></i></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $mjeritelji->links() }}
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-4">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title mb-0">{{ ! empty($selected_user) ? $selected_user->name : 'Selektirajte mjeritelja...' }}</h5>
            </div>
            <div class="card-body mt-0 pt-0">
                <table class="table table-sm mb-5">
                    <tbody>
                    <tr>
                        <th>Ime</th>
                        <td>{{ ! empty($selected_user) ? $selected_user->name : '' }}</td>
                    </tr>
                    <tr>
                        <th>Broj</th>
                        <td>{{ ! empty($selected_user) && $selected_user->data ? $selected_user->data->broj : '' }}</td>
                    </tr>
                    <tr>
                        <th>Institucija</th>
                        <td>{{ ! empty($selected_user) && $selected_user->institucija ? $selected_user->institucija->naziv : '' }}</td>
                    </tr>
                    <tr>
                        <th>Ovlaštenje od</th>
                        <td>{{ ! empty($selected_user) && $selected_user->data && $selected_user->data->datum_ovlastenja_od ? \Illuminate\Support\Carbon::make($selected_user->data->datum_ovlastenja_od)->format('d.m.Y.') : '' }}</td>
                    </tr>
                    <tr>
                        <th>Ovlaštenje do</th>
                        <td>{{ ! empty($selected_user) && $selected_user->data && $selected_user->data->datum_ovlastenja_do ? \Illuminate\Support\Carbon::make($selected_user->data->datum_ovlastenja_do)->format('d.m.Y.') : '' }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($selected_user)
                                @if($selected_user->status)
                                    <span class="badge badge-success">Aktivan</span>
                                @else
                                    <span class="badge badge-danger">Neaktivan</span>
                                @endif
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>

                @if($selected_user && $selected_user->data)
                    <div class="row">
                        <div class="col-12">
                            <a href="{{ route('obracuni.show', ['user' => $selected_user]) }}" class="btn btn-primary btn-view btn-block">Pogledaj izvještaj</a>
                        </div>
                    </div>
                @elseif($selected_user && ! $selected_user->data)
                    <a href="{{ route('korisnici.show', ['korisnik' => $selected_user]) }}">Dodjelite korisniku instituciju</a>
                @endif
            </div>
        </div>
    </div>
</div>
