<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Državni zavod za mjeriteljstvo</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Državni zavod za mjeriteljstvo">
    <meta name="keywords" content="Državni zavod za mjeriteljstvo">
    <meta name="author" content="Državni zavod za mjeriteljstvo">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" media="screen" href="{{ asset('front/theme.css') }}">
</head>
<!-- Body-->
<body class="handheld-toolbar-enabled " style="background:#033e7c">
<!-- Sign in / sign up modal-->
<header class="bg-light shadow-sm navbar-sticky">
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="container"><a class="navbar-brand d-none d-sm-block flex-shrink-0 me-4 order-lg-1" href="#"><img src="{{ asset('front/mu_logo.png') }}" width="250" alt="Državni zavod za mjeriteljstvo"></a><a class="navbar-brand d-sm-none me-2 order-lg-1" href="#"><img src="{{ asset('front/mu_logo.png') }}" width="250" alt="Državni zavod za mjeriteljstvo"></a>
        </div>
    </div>
</header>

<section class=" py-5" style="background:#033e7c">
    <div class="pb-lg-5 mb-lg-3">
        <div class="container  my-lg-5">
            <div class="row mb-4 mb-sm-3">
                <div class="col-lg-7 col-md-9 text-center text-sm-start">
                    <h1 class="text-white lh-base">Pretraga mjernih uređaja</h1>
                    <h2 class="h5 text-white fw-light">Lorem Ipsum je jednostavno probni tekst koji se koristi u tiskarskoj i slovoslagarskoj industriji.  </h2>
                </div>
            </div>
            <div class="row pb-lg-2 mb-4 mb-sm-3">
                <div class="col-lg-6 col-md-8">
                    <div class="input-group input-group-lg flex-nowrap">
                        <input class="form-control rounded-start" type="text" placeholder="Upišite pojam pretrage">
                        <button class="btn btn-primary btn-lg btn-red fs-base"  type="submit" ><i class="ci-search  "></i></button>
                    </div>
                </div>
            </div>

            <div class="row pb-lg-5 mb-4 mb-sm-5">
                <div class="col-lg-12 col-md-12">
                    <div class="card" style="background: #033e7c;">

                        <h3 class="text-white lh-base">Rezultati pretrage</h3>
                        <div class="table-responsive">
                            <table class="table table-hover table-dark w-100" style="background: #033e7c;color:#fff">

                                <tbody>
                                <tr style="cursor: pointer;">
                                    <td>
                                        <span class="font-weight-light font-italic" >Oznaka: </span>YPI-1791-TT, dolore<br>
                                        <span class="font-weight-light font-italic" >Datumi: </span>03.05.2021 - 31.05.2021<br>
                                        <span class="font-weight-light font-italic" >Naljepnica: </span><span style="color:indianred;">NE</span> -
                                        <span class="font-weight-light font-italic" >Žig: </span><span style="color:limegreen;">DA</span> -
                                        <span class="font-weight-light font-italic" >Ovjernica: </span><span style="color:indianred;">NE</span>
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" >Mjerilo: </span>9RX-627-1021, Tip Aufderhar<br>
                                        <span class="font-weight-light font-italic" >Mjeritelj: </span>171-sW, Ottilie Abernathy<br>
                                        <span class="font-weight-light font-italic" >Tijelo: </span>Emard Inc, New Flavie
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" >Vlasnik: </span>Cronin PLC, Schmittborough<br>
                                        <span class="font-weight-light font-italic" >Postavljanje: </span>North Willis, 4621 Emmie Park
                                        East Juliana,...<br>
                                        <span class="font-weight-light font-italic" >Mjesto Ovjere: </span>Port Katelynstad, 41555 Clotilde Loaf Suite 707...
                                    </td>

                                </tr>
                                <tr style="cursor: pointer;">
                                    <td>
                                        <span class="font-weight-light font-italic" >Oznaka: </span>YPI-1791-TT, dolore<br>
                                        <span class="font-weight-light font-italic" >Datumi: </span>03.05.2021 - 31.05.2021<br>
                                        <span class="font-weight-light font-italic" >Naljepnica: </span><span style="color:indianred;">NE</span> -
                                        <span class="font-weight-light font-italic" >Žig: </span><span style="color:limegreen;">DA</span> -
                                        <span class="font-weight-light font-italic" >Ovjernica: </span><span style="color:indianred;">NE</span>
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" >Mjerilo: </span>9RX-627-1021, Tip Aufderhar<br>
                                        <span class="font-weight-light font-italic" >Mjeritelj: </span>171-sW, Ottilie Abernathy<br>
                                        <span class="font-weight-light font-italic" >Tijelo: </span>Emard Inc, New Flavie
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" >Vlasnik: </span>Cronin PLC, Schmittborough<br>
                                        <span class="font-weight-light font-italic" >Postavljanje: </span>North Willis, 4621 Emmie Park
                                        East Juliana,...<br>
                                        <span class="font-weight-light font-italic" >Mjesto Ovjere: </span>Port Katelynstad, 41555 Clotilde Loaf Suite 707...
                                    </td>

                                </tr>
                                <tr style="cursor: pointer;">
                                    <td>
                                        <span class="font-weight-light font-italic" >Oznaka: </span>YPI-1791-TT, dolore<br>
                                        <span class="font-weight-light font-italic" >Datumi: </span>03.05.2021 - 31.05.2021<br>
                                        <span class="font-weight-light font-italic" >Naljepnica: </span><span style="color:indianred;">NE</span> -
                                        <span class="font-weight-light font-italic" >Žig: </span><span style="color:limegreen;">DA</span> -
                                        <span class="font-weight-light font-italic" >Ovjernica: </span><span style="color:indianred;">NE</span>
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" >Mjerilo: </span>9RX-627-1021, Tip Aufderhar<br>
                                        <span class="font-weight-light font-italic" >Mjeritelj: </span>171-sW, Ottilie Abernathy<br>
                                        <span class="font-weight-light font-italic" >Tijelo: </span>Emard Inc, New Flavie
                                    </td>
                                    <td>
                                        <span class="font-weight-light font-italic" >Vlasnik: </span>Cronin PLC, Schmittborough<br>
                                        <span class="font-weight-light font-italic" >Postavljanje: </span>North Willis, 4621 Emmie Park
                                        East Juliana,...<br>
                                        <span class="font-weight-light font-italic" >Mjesto Ovjere: </span>Port Katelynstad, 41555 Clotilde Loaf Suite 707...
                                    </td>

                                </tr>



                                </tbody>
                            </table>
                        </div>
                                <!-- Pagination: with icons -->
                                <nav aria-label="Page navigation example" class="mb-3">
                                    <ul class="pagination">
                                        <li class="page-item">
                                            <a href="#" class="page-link" aria-label="Previous">
                                                <i class="ci-arrow-left"></i>
                                            </a>
                                        </li>
                                        <li class="page-item d-sm-none">
                                            <span class="page-link page-link-static">2 / 5</span>
                                        </li>
                                        <li class="page-item d-none d-sm-block">
                                            <a href="#" class="page-link">1</a>
                                        </li>
                                        <li class="page-item active d-none d-sm-block" aria-current="page">
                                          <span class="page-link">
                                            2
                                            <span class="visually-hidden">(current)</span>
                                          </span>
                                                                </li>
                                        <li class="page-item d-none d-sm-block">
                                            <a href="#" class="page-link">3</a>
                                        </li>
                                        <li class="page-item d-none d-sm-block">
                                            <a href="#" class="page-link">4</a>
                                        </li>
                                        <li class="page-item d-none d-sm-block">
                                            <a href="#" class="page-link">5</a>
                                        </li>
                                        <li class="page-item">
                                            <a href="#" class="page-link" aria-label="Next">
                                                <i class="ci-arrow-right"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                        </div>

                     </div>

            </div>


        </div>
    </div>
</section>
<footer class="bg-dark " style="position: fixed;left: 0;bottom: 0;width: 100%;z-index:1000">
    <div class="pt-5 bg-darker">
        <div class="container">
            <div class="d-md-flex justify-content-between pt-0">
                <div class="pb-5 fs-xs text-light opacity-50 text-center text-md-start">Copyright © 2021 Državni zavod za mjeriteljstvo. </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{ asset('front/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
