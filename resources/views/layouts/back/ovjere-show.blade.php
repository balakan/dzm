<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ back()->getTargetUrl() }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista ovjera">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Ovjera') }}
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1">

            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#ovjera" data-toggle="tab" role="tab">Ovjera</a></li>
                        <li class="nav-item"><a class="nav-link" href="#vlasnik" data-toggle="tab" role="tab">Vlasnik mjerila</a></li>
                        <li class="nav-item"><a class="nav-link" href="#mjesto_ovjere" data-toggle="tab" role="tab">Mjesto ovjere</a></li>
                        {{--<li class="nav-item"><a class="nav-link" href="#mjesto_postavljanja" data-toggle="tab" role="tab">Mjesto postavljanja</a></li>--}}
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ovjera" role="tabpanel">
                            <h5 class="card-title mb-4">Ovjera</h5>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <th>Oznaka</th>
                                        <td>{{ $ovjera->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Vrsta</th>
                                        <td>{{ $ovjera->vrsta }}</td>
                                    </tr>
                                    <tr>
                                        <th>Mjeritelj</th>
                                        <td>{{ $ovjera->zahtjev->mjeritelj->data->broj }}, <a href="{{ route('korisnici.show', ['korisnik' => $ovjera->zahtjev->mjeritelj]) }}">{{ $ovjera->zahtjev->mjeritelj->name }}</a></td>
                                    </tr>
                                    <tr>
                                        <th>Institucija</th>
                                        <td><a href="{{ route('institucije.show', ['institucija' => $ovjera->zahtjev->institucija]) }}">{{ $ovjera->zahtjev->institucija->naziv }}</a><br>{{ $ovjera->zahtjev->institucija->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>Mjerilo</th>
                                        <td>{{ $ovjera->mjerilo->sn }}, {{ $ovjera->mjerilo->tip->naziv }}</td>
                                    </tr>
                                    <tr>
                                        <th>Naljepnica</th>
                                        <td>{!! $ovjera->naljepnica ? '<span style="color:limegreen;">DA</span>, ' . $ovjera->broj_naljepnice : '<span style="color:indianred;">NE</span>' !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Žig</th>
                                        <td>{!! $ovjera->zig ? '<span style="color:limegreen;">DA</span>, ' . $ovjera->broj_ziga : '<span style="color:indianred;">NE</span>' !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Ovjernica</th>
                                        <td>{!! $ovjera->ovjernica ? '<span style="color:limegreen;">DA</span>, ' . $ovjera->ovjernica : '<span style="color:indianred;">NE</span>' !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Datum upisa</th>
                                        <td>{{ $ovjera->zahtjev ? \Illuminate\Support\Carbon::make($ovjera->zahtjev->created_at)->format('d.m.Y.') : '' }}</td>
                                    </tr>
                                    <tr>
                                        <th>Datum pregleda</th>
                                        <td>{{ $ovjera->zahtjev->datum_pregleda ? \Illuminate\Support\Carbon::make($ovjera->zahtjev->datum_pregleda)->format('d.m.Y.') : '' }}</td>
                                    </tr>
                                    <tr>
                                        <th>Napomena</th>
                                        <td>{{ $ovjera->napomena }}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="vlasnik" role="tabpanel">
                            <h5 class="card-title mb-4">Vlasnik mjerila</h5>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <th>Naziv</th>
                                        <td>{{ $ovjera->vlasnik->naziv }}</td>
                                    </tr>
                                    <tr>
                                        <th>Adresa</th>
                                        <td>{{ $ovjera->vlasnik->address }}</td>
                                    </tr>
                                    <tr>
                                        <th>Poštanski broj</th>
                                        <td>{{ $ovjera->vlasnik->zip }}</td>
                                    </tr>
                                    <tr>
                                        <th>Grad</th>
                                        <td>{{ $ovjera->vlasnik->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>Regija</th>
                                        <td>{{ $ovjera->vlasnik->region }}</td>
                                    </tr>
                                    <tr>
                                        <th>Telefon</th>
                                        <td>{{ $ovjera->vlasnik->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $ovjera->vlasnik->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>
                                            @if($ovjera->vlasnik->status)
                                                <span class="badge badge-success">Aktivan</span>
                                            @else
                                                <span class="badge badge-danger">Neaktivan</span>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="mjesto_ovjere" role="tabpanel">
                            <h5 class="card-title mb-4">Mjesto ovjere</h5>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <th>Adresa</th>
                                        <td>{{ $ovjera->mjesto_ovjere->address }}</td>
                                    </tr>
                                    <tr>
                                        <th>Poštanski broj</th>
                                        <td>{{ $ovjera->mjesto_ovjere->zip }}</td>
                                    </tr>
                                    <tr>
                                        <th>Grad</th>
                                        <td>{{ $ovjera->mjesto_ovjere->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>Regija</th>
                                        <td>{{ $ovjera->mjesto_ovjere->region }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{--<div class="tab-pane" id="mjesto_postavljanja" role="tabpanel">
                            <h5 class="card-title mb-4">Mjesto postavljanja</h5>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                    <tr>
                                        <th>Adresa</th>
                                        <td>{{ $ovjera->mjesto_postavljanja->address }}</td>
                                    </tr>
                                    <tr>
                                        <th>Poštanski broj</th>
                                        <td>{{ $ovjera->mjesto_postavljanja->zip }}</td>
                                    </tr>
                                    <tr>
                                        <th>Grad</th>
                                        <td>{{ $ovjera->mjesto_postavljanja->city }}</td>
                                    </tr>
                                    <tr>
                                        <th>Regija</th>
                                        <td>{{ $ovjera->mjesto_postavljanja->region }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Povijest ovjera</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Mjeritelj</th>
                                    <th class="text-center">Datum ovjere</th>
                                    <th class="text-center">Detalji</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($history as $item)
                                    <tr>
                                        <td class="font-weight-bold">{{ $ovjera->zahtjev->mjeritelj->name }}</td>
                                        <td class="text-center">{{ $item->datum_ovjere ? \Illuminate\Support\Carbon::make($item->datum_ovjere)->format('d.m.Y') : '' }}</td>
                                        <td class="text-center">
                                            <a class="btn btn-primary btn-view btn-sm" href="{{ route('ovjere.show', ['ovjera' => $item]) }}">Pogledaj</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="3">Trenutno nema povjesti ovjere mjerila.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        {{ $history->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        // Select2
        $(".select2").each(function() {
            $(this)
            .wrap("<div class=\"position-relative\"></div>")
            .select2({
                placeholder: "Pretraži korisnike",
                dropdownParent: $(this).parent()
            });
        })
    });
</script>
