<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('ovjere') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista ovjera">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Ovjera') }}
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1"></div>
        </div>
        @include('layouts.back.partials.session')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mb-0">Zahtjev {{ $zahtjev->id }} <span class="font-weight-light">od</span> {{ \Illuminate\Support\Carbon::make($zahtjev->created_at)->format('d.m.Y') }}</h4>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body mt-3">
                        <form action="{{ isset($ovjera) ? route('ovjere.update', ['ovjera' => $ovjera]) : route('ovjere.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ isset($ovjera) ? method_field('PATCH') : '' }}
                            <input type="hidden" name="zahtjev_id" value="{{ $zahtjev->id }}">
                            <div class="row">

                                <div class="mb-4 col-sm-12 col-md-2">
                                    <label class="form-label required" for="inputVrsta">Vrsta ovjere</label>
                                    <div class="input-group">
                                        <select class="form-control form-control-lg select2 @error('vrsta') is-invalid @enderror" name="vrsta" id="vrsta-select" data-toggle="select2">
                                            <option value="R" {{ isset($ovjera) && $ovjera->vrsta == 'R' ? 'selected' : '' }}>R</option>
                                            <option value="P" {{ isset($ovjera) && $ovjera->vrsta == 'P' ? 'selected' : '' }}>P</option>
                                            <option value="I" {{ isset($ovjera) && $ovjera->vrsta == 'I' ? 'selected' : '' }}>I</option>
                                            <option value="S" {{ isset($ovjera) && $ovjera->vrsta == 'S' ? 'selected' : '' }}>IIZM</option>
                                        </select>
                                    </div>
                                    @error('vrsta')
                                    <label class="small text-danger">Vrsta je obavezna.</label>
                                    @enderror
                                </div>

                                <div class="col-sm-12 col-md-4">
                                    <label class="form-label required" for="mjerilo">Serijski broj mjerila</label>
                                    @livewire('back.layout.search.mjerilo-search', ['mjerilo_id' => isset($ovjera) ? $ovjera->mjerilo_id : 0, 'zahtjev' => $zahtjev])
                                    @error('mjerilo_id')
                                    <label class="small text-danger">Mjerilo je obvezno.</label>
                                    @enderror
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label class="form-label required" for="inputVlasnikmjerila" >Vlasnik mjerila</label>
                                    @livewire('back.layout.search.vlasnik-search', ['vlasnik_id' => isset($ovjera) ? $ovjera->mjerilo_vlasnik_id : 0])
                                    @error('mjerilo_vlasnik_id')
                                    <label class="small text-danger">Vlasnik mjerila je obavezan.</label>
                                    @enderror
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label class="form-label required" for="inputmjerilo_mjesto_ovjere_id" >Mjesto ovjere</label>
                                    @livewire('back.layout.search.mjesto-ovjere-search', ['mjesto_ovjere_id' => isset($ovjera) ? $ovjera->mjerilo_mjesto_ovjere_id : 0])
                                    @error('mjerilo_mjesto_ovjere_id')
                                    <label class="small text-danger">Mjesto ovjere je obavezno.</label>
                                    @enderror
                                </div>

                                <div class="col-sm-12 col-md-9">
                                    <div class="row d-none" id="info-panel">
                                        <div class="col-12">
                                            <label class="form-label text-info">Info mjerila</label>
                                            <hr class="mt-0 mb-2">
                                            <p class="mb-0" id="info-data"></p>
                                        </div>
                                    </div>
                                </div>

                                {{--<div class="col-sm-12 col-md-5">
                                    <label class="form-label required" for="inputmjerilo_mjesto_postavljanja_id" >Mjesto postavljanja</label>
                                    @livewire('back.layout.search.mjesto-postavljanja-search', ['mjesto_postavljanja_id' => isset($ovjera) ? $ovjera->mjerilo_mjesto_postavljanja_id : 0])
                                    @error('mjerilo_mjesto_postavljanja_id')
                                    <label class="small text-danger">Mjesto postavljanja je obavezno.</label>
                                    @enderror
                                </div>--}}
                            </div>
                            <div class="row">
                                <div class="col-12 text-right mt-3">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm" name="submitBtn" value="0">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                    </button>
                                    @if ($zahtjev->status == 1)
                                        <button type="submit" class="btn btn-primary btn-save-as shadow-sm" name="submitBtn" value="1">
                                            <i class="align-middle" data-feather="save">&nbsp;</i> Snimi i otvori novu ovjeru
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Ovjere po zahtjevu {{ $zahtjev->id }}</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Oznaka</th>
                                <th>Mjerilo</th>
                                <th>Vlasnik</th>
                                <th class="text-center">Status</th>
                                <th class="text-right">Akcije</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($zahtjev->ovjere()->get() as $list_ovjera)
                                <tr>
                                    <td><a href="{{ route('ovjere.show', ['ovjera' => $list_ovjera]) }}">{{ $list_ovjera->id }}</a></td>
                                    <td><a href="{{ route('mjerila', ['search' => $list_ovjera->mjerilo->sn]) }}">{{ $list_ovjera->mjerilo->sn }}</a>, {{ $list_ovjera->mjerilo->tip->naziv }}</td>
                                    <td>{{ $list_ovjera->vlasnik->naziv }}</td>
                                    <td class="text-center">@include('layouts.back.partials.ovjera-status', ['ovjera' => $list_ovjera])</td>
                                    <td class="text-right">
                                        @if ( ! $list_ovjera->status)
                                            <a href="{{ route('ovjere.edit', ['ovjera' => $list_ovjera, 'zahtjev' => $list_ovjera->zahtjev]) }}" class="btn btn-primary btn-edit">Edit</a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="5">Za sada nema ovjera po zahtjevu <strong>{{ $zahtjev->oznaka }}</strong></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</x-app-layout>

<script>
    document.addEventListener("DOMContentLoaded", function() {

        $('#vrsta-select').select2({
            placeholder: 'Odaberite',
            theme: "bootstrap",
            minimumResultsForSearch: Infinity
        });

        //
        Livewire.on('userAdded', (e) => {
            $('#ot-input').val(e.mjeritelj.tijelo.naziv);
            $('#ot-input-hidden').val(e.mjeritelj.tijelo.naziv);
        });

        //
        Livewire.on('mjeriloAdded', (e) => {
            successNotify('Mjerilo dodano!');

            let mjerilo = e.mjerilo;
            let datum_ovjere = mjerilo.last.datum_ovjere ? new Date(mjerilo.last.datum_ovjere).toLocaleDateString('hr-HR') : 'Nema';
            let datum_valjanosti = mjerilo.last.datum_valjanosti ? new Date(mjerilo.last.datum_valjanosti).toLocaleDateString('hr-HR') : 'Nema';
            let ovjera_text = '';

            if (mjerilo.last) {
                ovjera_text = 'Zadnja ovjera: <b class="text-primary">' + datum_ovjere + '</b><br>Valjanost do: <b class="text-primary">' + datum_valjanosti + '</b>';
            } else {
                ovjera_text = 'Mjerilo nije ovjeravano.';
            }

            let mjerilo_text = "Mjerilo: <b>" + mjerilo.sn + ' - ' + mjerilo.tip.naziv + ', ' + mjerilo.tip.brand + '</b><br>';
            let sifra_text = "Šifra: <b>" + mjerilo.sifra.naziv + '</b><br>';

            $('#info-panel').removeClass('d-none');
            $('#info-data').append(mjerilo_text + sifra_text + ovjera_text);
        });

        //
        Livewire.on('vlasnikStored', (e) => {
            successNotify('Novi vlasnik dodan!');
        });

        Livewire.on('mjestoOvjereStored', (e) => {
            successNotify('Novo mjesto ovjere dodano!');
        });

        Livewire.on('mjestoPostavljanjaStored', (e) => {
            successNotify('Novo mjesto postavljanja dodano!');
        });

        Livewire.on('errorStoring', (e) => {
            errorNotify('Whoops!... Nešto je pošlo krivo sa snimanjem podataka. Greška je poslana administratoru. Molimo pokušajte ponovo.')
        });
    });
</script>
