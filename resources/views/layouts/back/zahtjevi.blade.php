<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Zahtjevi') }}</h3>
            </div>
            @can('create-zahtjev')
                <div class="col-auto ml-auto text-right mt-n1">
                    <a href="{{ route('zahtjevi.create') }}" class="btn btn-primary btn-new shadow-sm">
                        <i class="align-middle" data-feather="plus-circle">&nbsp;</i> Novi Zahtjev
                    </a>
                </div>
            @endcan
        </div>
        @livewire('back.zahtjevi')
    </div>
</x-app-layout>
