<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Uloge i Ovlaštenja') }}</h3>
            </div>
        </div>

        @livewire('back.user.uloge')

    </div>
</x-app-layout>