<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('institucije') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Povratak na listu institucija">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    Institucija <small>{{ isset($institucija) ? $institucija->naziv : '' }}</small>
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1"></div>
        </div>

        @include('layouts.back.partials.session')

        <div class="row">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body mt-3">
                        <form action="{{ isset($institucija) ? route('institucije.update', ['institucija' => $institucija]) : route('institucije.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ isset($institucija) ? method_field('PATCH') : '' }}
                            <div class="row">
                                <div class="mb-4 col-12 col-md-4">
                                    <label class="form-label required" for="inputNaziv">Naziv</label>
                                    <input type="text" class="form-control form-control-lg @error('naziv') is-invalid @enderror" id="inputNaziv" name="naziv" value="{{ isset($institucija) ? $institucija->naziv : old('naziv') }}" placeholder="">
                                    @error('naziv')
                                    <label class="small text-danger">Naziv je obvezan.</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-12 col-md-2">
                                    <label class="form-label required" for="inputBroj">Broj ovlaštenja</label>
                                    <input type="text" class="form-control form-control-lg @error('broj') is-invalid @enderror" id="inputBroj" name="broj" value="{{ isset($institucija) ? $institucija->broj : old('broj') }}" placeholder="">
                                    @error('broj')
                                    <label class="small text-danger">Broj je obavezan.</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-12 col-md-3">
                                    <label class="form-label" for="inputTelefon">Telefon</label>
                                    <input type="text" class="form-control form-control-lg" id="inputTelefon" name="phone" value="{{ isset($institucija) ? $institucija->phone : old('phone') }}" placeholder="">
                                </div>
                                <div class="mb-3 col-12 col-md-3">
                                    <label class="form-label" for="inputEmail">Email</label>
                                    <input type="text" class="form-control form-control-lg" id="inputEmail" name="email" value="{{ isset($institucija) ? $institucija->email : old('email') }}" placeholder="">
                                </div>
                                {{--<div class="mb-3 col-md-12" >
                                    <label class="form-label" for="group-select" >Glavni (nadređeni) voditelj</label>
                                    <select class="form-control form-control-lg select2" name="parent_id" id="group-select" data-toggle="select2">
                                        <option value="0"></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        @foreach ($groups as $group)
                                            <option value="{{ $group->id }}" {{ (isset($institucija->parent_id) and $group->id == $institucija->parent_id) ? 'selected="selected"' : '' }}>{{ $group->naziv }}</option>
                                        @endforeach
                                    </select>
                                </div>--}}
                                <div class="mb-4 col-12 col-md-4">
                                    @livewire('back.layout.search.address.city-search', ['search' => isset($institucija) ? $institucija->city : old('city')])
                                </div>
                                <div class="mb-3 col-12 col-md-2">
                                    @livewire('back.layout.search.address.zip-search', ['search' => isset($institucija) ? $institucija->zip : old('zip')])
                                </div>
                                <div class="col-12 col-md-6"></div>
                                <div class="mb-4 col-12 col-md-4">
                                    <label class="form-label" for="inputAdresa">Adresa</label>
                                    <input type="text" class="form-control form-control-lg" id="inputAdresa" name="address" value="{{ isset($institucija) ? $institucija->address : old('address') }}" placeholder="">
                                </div>
                                <div class="mb-3 col-12 col-md-5">
                                    @livewire('back.layout.search.address.region-search', ['search' => isset($institucija) ? $institucija->region : old('region')])
                                </div>


                                <div class="mb-3 col-12 col-md-6">
                                    <label class="form-label required" for="inputdatum_ovlastenja_od">Datum ovlaštenja od</label>
                                    <div class="form-group">
                                        <div class="input-group date" id="datum_ovlastenja_od" data-target-input="nearest">
                                            <input type="text" class="form-control form-control-lg datetimepicker-input" name="datum_ovlastenja_od" data-target="#datum_ovlastenja_od" value="{{ isset($institucija) ? \Illuminate\Support\Carbon::make($institucija->datum_ovlastenja_od)->format('d.m.Y') : old('datum_ovlastenja_od') }}" />
                                            <div class="input-group-append" data-target="#datum_ovlastenja_od" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label" for="inputdatum_ovlastenja_do">Datum ovlaštenja do</label>
                                    <div class="form-group">
                                        <div class="input-group date" id="datum_ovlastenja_do" data-target-input="nearest">
                                            <input type="text" class="form-control form-control-lg datetimepicker-input" name="datum_ovlastenja_do" data-target="#datum_ovlastenja_do" value="{{ isset($institucija) and $institucija->datum_ovlastenja_do ? \Illuminate\Support\Carbon::make($institucija->datum_ovlastenja_do)->format('d.m.Y') : old('datum_ovlastenja_do') }}" />
                                            <div class="input-group-append" data-target="#datum_ovlastenja_do" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-4 col-md-6">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" name="status" class="custom-control-input" id="Status" @if (isset($institucija) and $institucija->status) checked @endif>
                                        <label class="custom-control-label" for="Status">Status </label>
                                    </div>
                                </div>

                                <div class="mt-2 col-md-12">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(() => {
        $('#group-select').select2({
            placeholder: "Odaberi krovno ovlašteno tijelo...",
            theme: "bootstrap"
        });

        $('#datum_ovlastenja_do').datetimepicker({
            format: 'DD.MM.YYYY'
        });

        $('#datum_ovlastenja_od').datetimepicker({
            format: 'DD.MM.YYYY'
        });
    });
</script>
