<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>Institucije</h3>
            </div>
            @can('create-institucije')
                <div class="col-auto ml-auto text-right mt-n1">
                    <a href="{{ route('institucije.create') }}" class="btn btn-primary btn-create shadow-sm">
                        <i class="align-middle" data-feather="plus-circle"></i>&nbsp;&nbsp;Nova Institucija
                    </a>
                </div>
            @endcan
        </div>
        @livewire('back.user.institucije')
    </div>
</x-app-layout>
