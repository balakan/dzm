<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('institucije') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista...">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    Institucija <span class="text-info">{{ $institucija->naziv }}</span>
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1">
                @can('edit-all-institucije' || 'edit-own-institucije')
                    <a href="{{ route('institucije.edit', ['institucija' => $institucija]) }}" class="btn btn-primary btn-edit">
                        <i class="fa fa-edit">&nbsp;</i> Uredi Instituciju
                    </a>
                @endcan
            </div>
        </div>
        @livewire('back.user.institucija-show', ['institucija' => $institucija])
    </div>
</x-app-layout>

