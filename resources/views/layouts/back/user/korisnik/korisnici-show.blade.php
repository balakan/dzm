<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('korisnici') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista Ovlaštenih Tijela">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Korisnik') }} <small>{{ $korisnik->name }}</small>
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1">
            </div>
        </div>
        @livewire('back.user.korisnik-show', ['korisnik' => $korisnik])
    </div>
</x-app-layout>

