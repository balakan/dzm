<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Korisnici') }}</h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1">
            </div>
        </div>
        @livewire('back.user.korisnici')
    </div>
</x-app-layout>
