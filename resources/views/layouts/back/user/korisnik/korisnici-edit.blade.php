<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('korisnici') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista Mjeritelja">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                   Korisnik Pero Perić
                </h3>
            </div>

        </div>

        <div class="row">
            <div class="col-md-3 col-xl-2">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Postavke</h5>
                    </div>

                    <div class="list-group list-group-flush" role="tablist">
                        <a class="list-group-item list-group-item-action active" data-toggle="list" href="#account" role="tab">
                            Korisnički podaci
                        </a>
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#password" role="tab">
                            Lozinka
                        </a>
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#privacy" role="tab">
                            Privatnost i sigurnost
                        </a>
                        <a class="list-group-item list-group-item-action" data-toggle="list" href="#theme" role="tab">
                           Postavke
                        </a>

                    </div>
                </div>
            </div>

            <div class="col-md-9 col-xl-10">
                <div class="tab-content">
                    <div class="tab-pane active" id="account" role="tabpanel" id="account" >
                    <div class="card">
                        <div class="card-header">

                            <h5 class="card-title mb-0"> Korisnički podaci</h5>
                        </div>
                        <div class="card-body">
                            <form >
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <x-jet-label for="inputFirstName" value="{{ __('Ime') }}" />
                                        <x-jet-input id="inputFirstName" type="text" wire:model.defer="fname" />
                                        <x-jet-input-error for="fname" class="mt-2" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <x-jet-label for="inputLastName" value="{{ __('Prezime') }}" />
                                        <x-jet-input id="inputLastName" type="text" wire:model.defer="lname" />
                                        <x-jet-input-error for="lname" class="mt-2" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <x-jet-label for="inputAddress" value="{{ __('Adresa') }}" />
                                    <x-jet-input id="inputAddress" type="text" wire:model.defer="address" />
                                    <x-jet-input-error for="address" class="mt-2" />
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <x-jet-label for="inputZip" value="{{ __('Poštanski broj') }}" />
                                        <x-jet-input id="inputZip" type="text" wire:model.defer="zip" />
                                        <x-jet-input-error for="zip" class="mt-2" />
                                    </div>
                                    <div class="form-group col-md-3">
                                        <x-jet-label for="inputCity" value="{{ __('Grad') }}" />
                                        <x-jet-input id="inputCity" type="text" wire:model.defer="city" />
                                        <x-jet-input-error for="city" class="mt-2" />
                                    </div>
                                    <div class="form-group col-md-3">
                                        <x-jet-label for="inputRegion" value="{{ __('Regija') }}" />
                                        <x-jet-input id="inputRegion" type="text" wire:model.defer="region" />
                                        <x-jet-input-error for="region" class="mt-2" />
                                    </div>
                                    <div class="form-group col-md-3">
                                        <x-jet-label for="inputState" value="{{ __('Država') }}" />
                                        <x-jet-input id="inputState" type="text" wire:model.defer="state" />
                                        <x-jet-input-error for="state" class="mt-2" />
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                    </button>
                                </div>


                            </form>

                        </div>
                    </div>
                    </div>

                    <div class="tab-pane fade" id="password" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-actions float-right">
                                    <div class="dropdown show">
                                        <a href="#" data-toggle="dropdown" data-display="static">
                                            <i class="align-middle" data-feather="more-horizontal"></i>
                                        </a>
                                    </div>
                                </div>
                                <h5 class="card-title mb-0">{{ __('Promjeni lozinku') }}</h5>
                            </div>
                            <div class="card-body">
                                <form >
                                    <div class="form-group">
                                        <x-jet-label for="currentPassword" value="{{ __('Trenutna lozinka') }}" />
                                        <x-jet-input id="currentPassword" type="password"  />
                                        <x-jet-input-error for="fname" class="mt-2" />
                                    </div>
                                    <div class="form-group">
                                        <x-jet-label for="password" value="{{ __('Nova lozinka') }}" />
                                        <x-jet-input id="password" type="password"  />
                                        <x-jet-input-error for="lname" class="mt-2" />
                                    </div>
                                    <div class="form-group">
                                        <x-jet-label for="passwordConfirmation" value="{{ __('Potvrdi novu lozinku') }}" />
                                        <x-jet-input id="passwordConfirmation" type="password"  />
                                        <x-jet-input-error for="lname" class="mt-2" />
                                    </div>

                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary btn-save shadow-sm">
                                            <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>


                    </div>

                    <div class="tab-pane fade" id="privacy" role="tabpanel">

                    </div>

                    <div class="tab-pane fade" id="theme" role="tabpanel">
                        @livewire('back.settings.theme.color-scheme')
                    </div>


                </div>
            </div>
        </div>

    </div>
</x-app-layout>
