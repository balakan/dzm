@if($status == 1)
    <span class="badge badge-info">U izradi</span>
@elseif($status == 2)
    <span class="badge badge-warning">Predan</span>
@elseif($status == 3)
    <span class="badge badge-success">Završen</span>
@else
    <span class="badge badge-info">U izradi</span>
@endif