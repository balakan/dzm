@if($ovjera->status == 1)
    <span class="badge badge-success">Prihvaćen</span>
@elseif($ovjera->status == 2)
    <span class="badge badge-danger">Odbijen</span>
@elseif($ovjera->status == 3)
    <span class="badge badge-soft-secondary">Odustao</span>
@else
    <span class="badge badge-soft-secondary">Nema</span>
@endif