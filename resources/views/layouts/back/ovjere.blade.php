<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Ovjere') }}</h3>
            </div>
            @can('create-ovjera')
                <div class="col-auto ml-auto text-right mt-n1">
                    <button data-toggle="modal" data-target="#new-ovjera-modal" class="btn btn-primary btn-new shadow-sm">
                        <i class="align-middle" data-feather="plus-circle">&nbsp;</i> Nova Ovjera
                    </button>
                </div>
            @endcan
        </div>

        @livewire('back.ovjere', ['mjeritelj' => $mjeritelj, 'zahtjev' => $zahtjev])

    </div>

    <!-- Vlasnik mjerila modal -->
    <div class="modal fade" id="new-ovjera-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Unesite zahtjev nove ovjere</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body m-3">
                    <div class="row" >
                        <div class="mb-3 col-md-12">
                            @livewire('back.layout.search.zahtjev-search')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
