<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('zahtjevi') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista zahtjeva">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Zahtjev') }}
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1"></div>
        </div>
        @include('layouts.back.partials.session')
        <div class="row">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body mt-3">
                        <form action="{{ isset($zahtjev) ? route('zahtjevi.update', ['zahtjev' => $zahtjev]) : route('zahtjevi.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ isset($zahtjev) ? method_field('PATCH') : '' }}
                            <div class="row">
                                <div class="mb-3 col-12 col-md-2">
                                    <label class="form-label" for="inputOznaka">Klasa ili oznaka</label>
                                    <input type="text" class="form-control form-control-lg" id="inputOznaka" name="klasa" value="{{ isset($zahtjev) ? $zahtjev->klasa : '' }}" placeholder="">
                                </div>

                                <div class="mb-3 col-12 col-md-4">
                                    <label class="form-label required" for="inputMjeritelj_id" >Mjeritelj</label>
                                    @livewire('back.layout.search.mjeritelj-search', ['mjeritelj_id' => isset($zahtjev) ? $zahtjev->mjeritelj_id : 0])
                                </div>

                                <div class="mb-3 col-12 col-md-3">
                                    <label class="form-label" for="inputdatum_pregleda_predlozeni">Predloženi datum pregleda</label>
                                    <div class="form-group mb-0">
                                        <div class="input-group date" id="datum_pregleda_predlozeni" data-target-input="nearest">
                                            <input type="text"
                                                   class="form-control form-control-lg datetimepicker-input @error('datum_pregleda') is-invalid @enderror"
                                                   name="datum_pregleda"
                                                   data-target="#datum_pregleda_predlozeni"
                                                   value="{{ isset($zahtjev) && $zahtjev->datum_pregleda ? \Illuminate\Support\Carbon::make($zahtjev->datum_pregleda)->format('d.m.Y') : '' }}" />
                                            <div class="input-group-append" data-target="#datum_pregleda_predlozeni" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    @error('datum_pregleda')
                                    <label class="small text-danger">Datum pregleda je obavezan.</label>
                                    @enderror
                                </div>

                                <div class="mb-3 col-12 col-md-3">
                                    <label class="form-label" for="status-select">Status</label>
                                    <div class="input-group">
                                        <select class="form-control form-control-lg select2 @error('status') is-invalid @enderror" name="status" id="status-select" data-toggle="select2">
                                            @if( ! auth()->user()->can('edit-zahtjev-status'))
                                                @if(isset($zahtjev))
                                                    <option value="{{ $zahtjev->status }}" selected="selected">{{ \App\Models\Helper::resolveStatusNameById($zahtjev->status) }}</option>
                                                @else
                                                    <option value="1" selected="selected">{{ \App\Models\Helper::resolveStatusNameById(1) }}</option>
                                                @endif
                                            @else
                                                @foreach (/*config('settings.zahtjev.statuses')*/$statuses as $key => $status)
                                                    <option value="{{ $key }}" {{ (isset($zahtjev) and $key == $zahtjev->status) ? 'selected="selected"' : ($key == 1 ? 'selected="selected"' : '') }}>{{ $status }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @error('status')
                                    <label class="small text-danger">Status je obavezan.</label>
                                    @enderror
                                </div>

                                <div class="mb-3 col-md-12">
                                    <label class="form-label" for="inputnapomena">Napomena</label>
                                    <textarea class="form-control" rows="5" name="napomena" placeholder="">{{ isset($zahtjev) ? $zahtjev->napomena : '' }}</textarea>
                                </div>

                                <div class="mt-4 col-md-12">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm" name="submitBtn" value="0">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                    </button>
                                    <button type="submit" class="btn btn-primary btn-save-as shadow-sm ml-2" name="submitBtn" value="1">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi i idi na Ovjere Zahtjeva
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(() => {

        $('#status-select').select2({
            placeholder: "Odaberi status...",
            theme: "bootstrap",
            minimumResultsForSearch: Infinity
        });

        $('#datum_pregleda_predlozeni').datetimepicker({
            format: 'DD.MM.YYYY'
        });

        Livewire.on('userAdded', (e) => {
            $('#ot-input').val(e.mjeritelj.institucija.naziv);
            $('#ot-input-hidden').val(e.mjeritelj.institucija.naziv);
        });

    });
</script>
