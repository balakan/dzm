<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Tipovi mjerila') }}</h3>
            </div>
            @can('create-tipovi')
                <div class="col-auto ml-auto text-right mt-n1">
                    <a href="{{ route('tipovi.create') }}" class="btn btn-primary btn-new shadow-sm">
                        <i class="align-middle" data-feather="plus-circle"></i> Novi tip mjerila
                    </a>
                </div>
            @endcan
        </div>
        @include('layouts.back.partials.session')

        @livewire('back.catalog.tipovi')

    </div>
</x-app-layout>
