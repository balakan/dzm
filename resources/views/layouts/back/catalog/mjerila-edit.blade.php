<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('mjerila') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista tipova mjerila">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    Mjerilo
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1"></div>
        </div>
        @include('layouts.back.partials.session')
        <div class="row">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body mt-3">
                        <form action="{{ isset($mjerilo) ? route('mjerila.update', ['mjerilo' => $mjerilo]) : route('mjerila.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ isset($mjerilo) ? method_field('PATCH') : '' }}
                            @if (isset($mjerilo))
                                <input type="hidden" name="id" value="{{ $mjerilo->id }}">
                            @endif
                            <div class="row">
                                <div class="mb-3 col-sm-12 col-md-2">
                                    <label class="form-label required" for="input-sn">Serijski broj</label>
                                    <input type="text" class="form-control form-control-lg @error('sn') is-invalid @enderror" id="input-sn" name="sn" value="{{ isset($mjerilo) ? $mjerilo->sn : old('sn') }}" placeholder="">
                                    @error('sn')
                                    <label class="small text-danger">Serijski broj je obvezan...</label>
                                    @enderror
                                </div>

                                <div class="mb-3 col-sm-12 col-md-2">
                                    <label class="form-label" for="input-max">Max. Mjerenje</label>
                                    <input type="text" class="form-control form-control-lg" id="input-max" name="max_mjerenje" value="{{ isset($mjerilo) ? $mjerilo->max_mjerenje : old('max_mjerenje') }}" placeholder="">
                                </div>

                                <div class="mb-3 col-sm-12 col-md-4">
                                    <label class="form-label required" for="sifre-select">Šifra mjerila</label>
                                    <select class="form-control form-control-lg select2 @error('sifra_id') is-invalid @enderror" id="sifre-select" name="sifra_id" data-toggle="select2" >
                                        <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        @foreach ($sifre as $sifra)
                                            <option value="{{ $sifra->id }}" {{ (isset($mjerilo->sifra_id) and $sifra->id == $mjerilo->sifra_id) ? 'selected="selected"' : old('sifra_id') }}>{{ $sifra->naziv }}</option>
                                        @endforeach
                                    </select>
                                    @error('sifra_id')
                                    <label class="small text-danger">Šifra mjerila je obvezna...</label>
                                    @enderror
                                </div>

                                <div class="mb-3 col-sm-12 col-md-4">
                                    <label class="form-label required" for="tipovi-select">Tip mjerila</label>
                                    <select class="form-control form-control-lg select2 @error('tip_id') is-invalid @enderror" id="tipovi-select" name="tip_id" data-toggle="select2" >
                                        <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        @foreach ($tipovi as $tip)
                                            <option value="{{ $tip->id }}" {{ (isset($mjerilo->tip_id) and $tip->id == $mjerilo->tip_id) ? 'selected="selected"' : old('tip_id') }}>{{ $tip->naziv }}</option>
                                        @endforeach
                                    </select>
                                    @error('tip_id')
                                    <label class="small text-danger">Tip mjerila je obvezan...</label>
                                    @enderror
                                </div>

                                <div class="mb-3 mt-3 col-md-12">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(() => {
        $('#sifre-select').select2({
            placeholder: "Odaberi ili upiši šifu mjerila...",
            theme: "bootstrap"
        });
        $('#tipovi-select').select2({
            placeholder: "Odaberi ili upiši tip mjerila...",
            theme: "bootstrap",
            tags: true
        });
    });
</script>
