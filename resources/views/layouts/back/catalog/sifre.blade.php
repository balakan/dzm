<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Šifre mjerila') }}</h3>
            </div>
            @can('create-sifre')
                <div class="col-auto ml-auto text-right mt-n1">
                    <a href="{{ route('sifre.create') }}" class="btn btn-primary btn-new shadow-sm">
                        <i class="align-middle" data-feather="plus-circle">&nbsp;</i> Nova šifra
                    </a>
                </div>
            @endcan
        </div>
        @include('layouts.back.partials.session')

        @livewire('back.catalog.sifre')

    </div>
</x-app-layout>
