<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Grupe mjerila') }}</h3>
            </div>
            @can('create-grupe')
                <div class="col-auto ml-auto text-right mt-n1">
                    <a href="{{ route('grupe.create') }}" class="btn btn-primary btn-new shadow-sm">
                        <i class="align-middle" data-feather="plus-circle">&nbsp;</i> Nova grupa
                    </a>
                </div>
            @endcan
        </div>
        @include('layouts.back.partials.session')
        <div class="row">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Lista grupa i podgrupa</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Naziv</th>
                                <th class="text-center">Krovna Kategorija</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Redosljed</th>
                                @can('edit-grupe')
                                    <th class="text-center">Uredi</th>
                                @endcan
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($groups as $group)
                                <tr>
                                    <td class="font-weight-bold">{{ $group->name }}</td>
                                    <td class="text-center"><i class="align-middle {{ $group->status ? 'text-success' : '' }}" data-feather="{{ $group->status ? 'check' : '' }}"></i></td>
                                    <td class="text-center"><i class="align-middle {{ $group->status ? 'text-success' : 'text-danger' }}" data-feather="{{ $group->status ? 'check' : 'triangle' }}"></i></td>
                                    <td class="text-center">{{ $group->sort_order }}</td>
                                    @can('edit-grupe')
                                        <td class="text-center">
                                            <a class="btn btn-primary btn-sm" href="{{ route('grupe.edit', ['grupa' => $group]) }}">Uredi</a>
                                        </td>
                                    @endcan
                                </tr>
                                @if($group->podgrupa)
                                    @foreach($group->podgrupa as $podgroup)
                                        <tr class="font-weight-lighter">
                                            <td class="pl-5">{{ $podgroup->name }}</td>
                                            <td></td>
                                            <td class="text-center"><i class="align-middle {{ $podgroup->status ? 'text-success' : 'text-danger' }}" data-feather="{{ $podgroup->status ? 'check' : 'triangle' }}"></i></td>
                                            <td class="text-center">{{ $podgroup->sort_order }}</td>
                                            @can('edit-grupe')
                                                <td class="text-center">
                                                    <a class="btn btn-primary btn-edit btn-sm" href="{{ route('grupe.edit', ['grupa' => $podgroup]) }}">Uredi</a>
                                                </td>
                                            @endcan
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</x-app-layout>

