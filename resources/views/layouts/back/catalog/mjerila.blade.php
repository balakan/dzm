<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Mjerila') }}</h3>
            </div>
            @can('create-mjerila')
                <div class="col-auto ml-auto text-right mt-n1">
                    <a href="{{ route('mjerila.create') }}" class="btn btn-primary btn-new shadow-sm">
                        <i class="align-middle" data-feather="plus-circle"></i> Novo Mjerilo
                    </a>
                </div>
            @endcan
        </div>
        @include('layouts.back.partials.session')

        @if (auth()->user()->can('search-mjerila'))
            @livewire('back.catalog.mjerila', ['search' => $search ?: ''])
        @endif

    </div>
</x-app-layout>
