<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('tipovi') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista tipova mjerila">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Tip') }}
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1"></div>
        </div>
        @include('layouts.back.partials.session')
        <div class="row">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body mt-3">
                        <form action="{{ isset($tip) ? route('tipovi.update', ['tip' => $tip]) : route('tipovi.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ isset($tip) ? method_field('PATCH') : '' }}
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputNaziv">Tip mjerila</label>
                                    <input type="text" class="form-control form-control-lg @error('naziv') is-invalid @enderror" id="inputNaziv" name="naziv" value="{{ isset($tip) ? $tip->naziv : '' }}" placeholder="">
                                    @error('naziv')
                                    <label class="small text-danger">Upis tipa je obvezan...</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputOznaka">Oznaka tipa</label>
                                    <input type="text" class="form-control form-control-lg @error('oznaka') is-invalid @enderror" id="inputOznaka" name="oznaka" value="{{ isset($tip) ? $tip->oznaka : '' }}" placeholder="">
                                    @error('oznaka')
                                    <label class="small text-danger">Oznaka tipa je obvezna...</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputBrand">Proizvođač</label>
                                    <select class="form-control form-control-lg select2 @error('brand') is-invalid @enderror" id="brand-select" name="brand" data-toggle="select2" >
                                        <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        @foreach ($brands as $brand)
                                            <option value="{{ $brand }}" {{ (isset($tip->brand) and $brand == $tip->brand) ? 'selected="selected"' : '' }}>{{ $brand }}</option>
                                        @endforeach
                                    </select>

                                    @error('brand')
                                    <label class="small text-danger">Proizvođač tipa je obvezan...</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputGroup_id" >Grupa kojoj tipna oznaka pripada</label>
                                    <div class="input-group mb-0">
                                        <select class="form-control form-control-lg select2 @error('group_id') is-invalid @enderror" id="group-select" name="group_id" data-toggle="select2" >
                                            <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                            @foreach ($groups as $group)
                                                <option value="{{ $group->id }}" {{ (isset($tip->group_id) and $group->id == $tip->group_id) ? 'selected="selected"' : '' }}>{{ $group->name }}</option>
                                                @if($group->podgrupa)
                                                    @foreach($group->podgrupa as $podgroup)
                                                        <option value="{{ $podgroup->id }}" {{ (isset($tip->group_id) and $podgroup->id == $tip->group_id) ? 'selected="selected"' : '' }}>&nbsp;&nbsp;&nbsp;&nbsp;{{ $podgroup->name }}</option>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @error('group_id')
                                    <label class="small text-danger">Grupa je obvezna...</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6"></div>
                                <div class="mb-3 mt-3 col-12 col-md-6">
                                    <label class="form-label mb-3" for="inputGroup_id" >Odaberi PDF tipne oznake</label><br>
                                    <input type="file" name="pdf" class="@error('pdf') is-invalid @enderror"><br>
                                    @error('pdf')
                                    <label class="small text-danger mt-2">Provjerite dokument. <br>Mora biti PDF i ne veći od 10 MB...</label>
                                    @enderror
                                    @if (isset($tip->group_id) and $tip->pdf != '')
                                        <label class="mt-3 font-weight-bold"><a href="{{ url($tip->pdf) }}">Postojeći PDF</a></label>
                                    @endif
                                </div>
                                <div class="mb-3 col-md-12">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(() => {
        $('#group-select').select2({
            placeholder: "Odaberi grupu...",
            theme: "bootstrap"
        });
        $('#brand-select').select2({
            placeholder: "Odaberi ili upiši novog proizvođača...",
            theme: "bootstrap",
            tags: true
        });
    });
</script>
