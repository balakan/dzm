<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('sifre') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista šifri">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Šifra') }}
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1">

            </div>
        </div>
        @include('layouts.back.partials.session')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Uredi</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ isset($sifra) ? route('sifre.update', ['sifra' => $sifra]) : route('sifre.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ isset($sifra) ? method_field('PATCH') : '' }}
                            <div class="row" >

                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputNaziv">Naziv</label>
                                    <input type="text" class="form-control form-control-lg @error('naziv') is-invalid @enderror" id="inputNaziv" name="naziv" value="{{ isset($sifra) ? $sifra->naziv : '' }}" placeholder="">
                                    @error('naziv')
                                    <label class="small text-danger">Naziv šifre je obvezan.</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputSifra">Šifra</label>
                                    <input type="text" class="form-control form-control-lg @error('sifra') is-invalid @enderror" id="inputSifra" name="sifra" value="{{ isset($sifra) ? $sifra->sifra : '' }}" placeholder="">
                                    @error('sifra')
                                    <label class="small text-danger">Šifra je obvezna.</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputOvjernoRazdoblje">Ovjerno razdoblje</label>
                                    <input type="text" class="form-control form-control-lg @error('ovjerno_razdoblje') is-invalid @enderror" id="inputOvjernoRazdoblje" name="ovjerno_razdoblje" value="{{ isset($sifra) ? $sifra->ovjerno_razdoblje : '' }}" placeholder="">
                                    @error('ovjerno_razdoblje')
                                    <label class="small text-danger">Ovjerno razdoblje je obvezno.</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label class="form-label required" for="inputTarifa">Tarifa</label>
                                    <input type="text" class="form-control form-control-lg @error('tarifa') is-invalid @enderror" id="inputTarifa" name="tarifa" value="{{ isset($sifra) ? number_format($sifra->tarifa, 2, ',', '') : '' }}" placeholder="">
                                    @error('tarifa')
                                    <label class="small text-danger">Tarifa je obvezna.</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6" >
                                    <label class="form-label required" for="inputGroup_id" >Grupa mjerila kojoj šifra pripada</label>
                                    <div class="input-group mb-0">
                                        <select class="form-control form-control-lg select2 @error('group_id') is-invalid @enderror" id="group-select" name="group_id" data-toggle="select2">
                                            <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                            @foreach ($groups as $group)
                                                <option value="{{ $group->id }}" {{ (isset($sifra->group_id) and $group->id == $sifra->group_id) ? 'selected="selected"' : '' }}>{{ $group->name }}</option>
                                                @if($group->podgrupa)
                                                    @foreach($group->podgrupa as $podgroup)
                                                        <option value="{{ $podgroup->id }}" {{ (isset($sifra->group_id) and $podgroup->id == $sifra->group_id) ? 'selected="selected"' : '' }}>&nbsp;&nbsp;&nbsp;&nbsp;{{ $podgroup->name }}</option>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @error('group_id')
                                    <label class="small text-danger">Grupa je obvezna.</label>
                                    @enderror
                                </div>
                                <div class="mb-3 col-md-6 pl-4" style="padding-top: 36px;">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" name="naljepnica" class="custom-control-input" id="naljepnica-switch" @if (isset($sifra) and $sifra->naljepnica) checked @endif>
                                        <label class="custom-control-label" for="naljepnica-switch"> Naljepnica</label>
                                    </div>
                                </div>
                                <div class="mb-3 mt-3 col-md-12">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" name="status" class="custom-control-input" id="Status" @if (isset($sifra) and $sifra->status) checked @endif>
                                        <label class="custom-control-label" for="Status"> Status</label>
                                    </div>
                                </div>
                                <div class="mb-3 mt-3 col-md-12">
                                    <button type="submit" class="btn btn-primary btn-save shadow-sm">
                                        <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    $(() => {
        $('#group-select').select2({
            placeholder: "Odaberi grupu...",
            theme: "bootstrap"
        });
    });
</script>
