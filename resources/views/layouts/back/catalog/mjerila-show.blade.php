<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('mjerila') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista mjerila">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    Mjerilo {{ $mjerilo->sn }}
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1"></div>
        </div>
        @include('layouts.back.partials.session')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title mt-0 mb-3">Informacije o mjerilu</h5>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="row">
                                    <div class="col-md-3">
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-1">Proizvođač:</li>
                                            <li class="mb-1">Tip:</li>
                                            <li class="mb-1">Šifra:</li>
                                            <li class="mb-1">Tipna Oznaka:</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-9">
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-1"><strong>{{ $mjerilo->tip->brand }}</strong></li>
                                            <li class="mb-1">{{ $mjerilo->tip->naziv }}</li>
                                            <li class="mb-1"><strong>({{ $mjerilo->sifra->sifra }})</strong>, {{ $mjerilo->sifra->naziv }}</li>
                                            <li class="mb-1">{{ $mjerilo->tip->oznaka }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3"></div>
                            <div class="col-sm-12 col-md-3"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Ovjere mjerila {{ $mjerilo->sn }}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center">Vrsta</th>
                                    <th class="text-center">Br.Institucije</th>
                                    <th class="text-center">Br.Mjeritelja</th>
                                    <th>Br.Ovjerne oznake</th>
                                    <th>Datum ovjere/valjanosti</th>
                                    <th>Vlasnik</th>
                                    <th>Mjesto ovjere</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($ovjere as $ovjera)
                                    <tr>
                                        {{--<td><a href="{{ route('ovjere.show', ['ovjera' => $ovjera]) }}">{{ $ovjera->id }}</a></td>--}}
                                        <td class="text-center">{{ $ovjera->vrsta }}</td>
                                        <td class="text-center">{{ $ovjera->zahtjev->institucija->broj }}</td>
                                        <td class="text-center">{{ $ovjera->zahtjev->mjeritelj->data->broj }}</td>
                                        <td>{{ $ovjera->naljepnica ?: $ovjera->zig }}</td>
                                        <td>
                                            {{ $ovjera->datum_ovjere ? \Illuminate\Support\Carbon::make($ovjera->datum_ovjere)->format('d.m.Y') : '' }} - {{ $ovjera->datum_valjanosti ? \Illuminate\Support\Carbon::make($ovjera->datum_valjanosti)->format('d.m.Y') : '' }}
                                        </td>
                                        <td>{{ $ovjera->vlasnik->naziv }}</td>
                                        <td>{{ $ovjera->mjesto_ovjere->city }}, {{ $ovjera->mjesto_ovjere->address }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="5">Za sada nema ovjera po mjerilu <strong>{{ $mjerilo->sn }}</strong></td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(() => {

    });
</script>
