<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('grupe') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista grupa">
                        <i class="align-middle" data-feather="arrow-left-circle"></i>
                    </a>
                    {{ __('Grupa') }} <span class="font-weight-lighter">{{ isset($grupa) ? $grupa->name : '' }}</span>
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1">

            </div>
        </div>

        @include('layouts.back.partials.session')

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Uredi</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ isset($grupa) ? route('grupe.update', ['grupa' => $grupa]) : route('grupe.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{ isset($grupa) ? method_field('PATCH') : '' }}
                            <div class="row" x-data="{ show: {{ (isset($grupa) and $grupa->top) ? 'false' : 'true' }} }">
                                <div class="col-12">
                                    <div class="mb-3">
                                        <label class="form-label required" for="inputName">Naziv grupe</label>
                                        <input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror" id="inputName" name="name" value="{{ isset($grupa) ? $grupa->name : '' }}" placeholder="">
                                        @error('name')
                                        <label class="small text-danger">Naslov stranice je obvezan...</label>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label" for="inputDescription" >Opis</label>
                                        <textarea class="form-control" rows="6" name="description" value="{{ isset($grupa) ? $grupa->description : '' }}" placeholder=""></textarea>
                                    </div>

                                    <div class="mb-3" x-show.transition.opacity.duration.300ms="show">
                                        <label class="form-label" for="inputParent_id" >Odaberi krovna grupa</label>
                                        <select class="form-control form-control-lg select2" id="group-select" name="parent_id" data-toggle="select2">
                                            @foreach ($parents as $id => $name)
                                                <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                <option value="{{ $id }}" {{ (isset($grupa->parent_id) and $id == $grupa->parent_id) ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="mb-3">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="top" class="custom-control-input" id="Top" @if (isset($grupa) and $grupa->top) checked @endif x-on:change="show = !show">
                                            <label class="custom-control-label" for="Top">Ovo je krovna grupa</label>
                                        </div>
                                    </div>

                                    <div class="mb-3 ">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="status" class="custom-control-input" id="Status" @if (isset($grupa) and $grupa->status) checked @endif>
                                            <label class="custom-control-label" for="Status">Status </label>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label" for="inputSortOrder">Redosljed</label>
                                        <input type="text" class="form-control form-control-lg" id="inputSortOrder" name="sort_order" value="{{ isset($grupa) ? $grupa->sort_order : '' }}" placeholder="">
                                    </div>

                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary btn-save shadow-sm">
                                            <i class="align-middle" data-feather="save">&nbsp;</i> Snimi
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</x-app-layout>

<script>
    $(() => {
        $('#group-select').select2({
            placeholder: "Odaberi krovnu grupu..."
        });
    });
</script>
