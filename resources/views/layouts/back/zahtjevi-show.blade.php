<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('zahtjevi') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista zahtjeva">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Zahtjev') }} <span class="ml-2 text-info">HR-{{ $zahtjev->institucija->broj }}-{{ $zahtjev->id }}</span>
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1">
                @if ($zahtjev->status == 1 && auth()->user()->can('create-ovjera') && auth()->user()->id == $zahtjev->mjeritelj_id)
                    <a href="{{ route('ovjere.create', ['zahtjev' => $zahtjev]) }}" class="btn btn-primary btn-new btn-block">Dodaj Ovjeru</a>
                @endif
                @if ($zahtjev->ovjere()->count() && $zahtjev->status == 2 && auth()->user()->can('ovjera-zahtjev') && auth()->user()->id == $zahtjev->mjeritelj_id)
                    <a href="{{ route('zahtjevi.ovjeri', ['zahtjev' => $zahtjev]) }}" class="btn btn-success">Ovjeri Zahtjev</a>
                @endif
            </div>
        </div>
        @livewire('back.zahtjev-show', ['zahtjev' => $zahtjev])
    </div>
</x-app-layout>
