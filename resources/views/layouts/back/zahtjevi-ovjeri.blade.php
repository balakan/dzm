<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ back()->getTargetUrl() }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Nazad">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    Zahtjev {{ $zahtjev->klasa }} <span class="font-weight-light">od</span> {{ \Illuminate\Support\Carbon::make($zahtjev->created_at)->format('d.m.Y') }} - Ovjere
                </h3>
            </div>
            <div class="col-auto ml-auto text-right mt-n1"></div>
        </div>
        @include('layouts.back.partials.session')

        @livewire('back.zahtjev-ovjere', ['zahtjev' => $zahtjev])

    </div>
</x-app-layout>

<script>
    document.addEventListener("DOMContentLoaded", function() {


        Livewire.on('userAdded', (e) => {
            $('#ot-input').val(e.mjeritelj.tijelo.naziv);
            $('#ot-input-hidden').val(e.mjeritelj.tijelo.naziv);
        });

        Livewire.on('mjeriloAdded', (e) => {
            successNotify('Mjerilo dodano!');
        });

        Livewire.on('vlasnikStored', (e) => {
            successNotify('Novi vlasnik dodan!');
        });

        Livewire.on('mjestoOvjereStored', (e) => {
            successNotify('Novo mjesto ovjere dodano!');
        });

        Livewire.on('mjestoPostavljanjaStored', (e) => {
            successNotify('Novo mjesto postavljanja dodano!');
        });

        Livewire.on('errorStoring', (e) => {
            errorNotify('Whoops!... Nešto je pošlo krivo sa snimanjem podataka. Greška je poslana administratoru.')
        });
    });
</script>
