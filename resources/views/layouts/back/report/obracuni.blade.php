<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Obračuni') }}</h3>
            </div>
        </div>

        @livewire('back.report.obracuni')

    </div>
</x-app-layout>