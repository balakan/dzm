<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>
                    <a href="{{ route('obracuni') }}" class="shadow-sm mr-2" data-toggle="tooltip" data-placement="top" title="Lista ovjera">
                        <i class="align-middle" data-feather="arrow-left-circle">&nbsp;</i>
                    </a>
                    {{ __('Obračun za mjeritelja:') }} <span class="font-weight-lighter text-info">{{ $user->name }}</span>
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Godišnji graf mjeritelja</h5>
                    </div>
                    <div class="card-body">
                        <div class="chart mb-3">
                            <canvas id="chartjs-bar"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Lista mjesečnih iznosa</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-sm table-hover">
                            <thead>
                            <tr>
                                <th>Mjesec</th>
                                <th class="text-right">Iznos</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($months_array as $month)
                                <tr style="cursor: pointer;">
                                    <td>{{ $month['title'] }}</td>
                                    <td class="text-right">{{ number_format($month['value'], 2) }} kn</td>
                                </tr>
                            @endforeach
                            <tr class="font-weight-bold" style="height: 56px;">
                                <td>Ukupno</td>
                                <td class="text-right">{{ number_format($total, 2) }} kn</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            document.addEventListener("DOMContentLoaded", function(dom) {
                let months_data = JSON.parse('{{ $months }}'.replace(/&quot;/g,'"'));
                let months_names = [];
                let months_values = [];
                let top = 0;
                let step_size = 100;

                for (let i = 0; i < months_data.length; i++) {
                    months_names.push(months_data[i].title + '.');
                    months_values.push(months_data[i].value);
                }

                for (let i = 0; i < months_values.length; i++) {
                    if (months_values[i] > top) {
                        top = months_values[i];
                    }
                }

                if (top > 10000) {
                    step_size = 2000;
                }
                if (top < 10000 && top > 4000) {
                    step_size = 1000;
                }
                if (top < 4000 && top > 1000) {
                    step_size = 500;
                }

                new Chart(document.getElementById("chartjs-bar"), {
                    type: "bar",
                    data: {
                        labels: months_names,
                        datasets: [{
                            label: "Last year",
                            backgroundColor: window.theme.primary,
                            borderColor: window.theme.primary,
                            hoverBackgroundColor: window.theme.primary,
                            hoverBorderColor: window.theme.primary,
                            data: months_values,
                            barPercentage: 1,
                            categoryPercentage: .5
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                gridLines: {
                                    display: false
                                },
                                stacked: false,
                                ticks: {
                                    stepSize: step_size
                                }
                            }],
                            xAxes: [{
                                stacked: false,
                                gridLines: {
                                    color: "transparent"
                                }
                            }]
                        }
                    }
                });
            });
        </script>
    @endpush
</x-app-layout>