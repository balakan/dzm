@extends('layouts.front.layout.app')

@section('content')
    <div class="page-title-overlap  pt-0">
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="order-lg-1 mb-3 mb-lg-0 pt-lg-0">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-light flex-lg-nowrap justify-content-center justify-content-lg-start">
                        <li class="breadcrumb-item"><a class="text-nowrap" href="{{ route('index') }}"><i class="ci-home"></i>Home</a></li>
                        <li class="breadcrumb-item text-nowrap active" aria-current="page">{{ $ovjera->mjerilo->sn }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <section class="container mb-3 pb-3">
        <div class="bg-light shadow-lg rounded-3 overflow-hidden">
            <div class="row">
                <!-- Content-->
                <section class="col-lg-12">
                    <div class="p-5 ">
                        <h4 class="card-title mb-4">Ovjera</h4>
                        <div class="table-responsive">
                            <table class="table table-striped table-white ">
                                <tbody>
                                <tr>
                                    <th class="col-3">Vrsta</th>
                                    <td>{{ $ovjera->vrsta == 'R' ? 'Redovna' : ($ovjera->vrsta == 'P' ? 'Prva' : 'Izvanredna') }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Mjerilo</th>
                                    <td>{{ $ovjera->mjerilo->sn }}, {{ $ovjera->mjerilo->tip->naziv }}</td>
                                </tr>
                                @if ($ovjera->broj_naljepnice != '')
                                    <tr>
                                        <th class="col-3">Naljepnica</th>
                                        <td><span style="color:limegreen;">DA</span>, {{ $ovjera->broj_naljepnice }}</td>
                                    </tr>
                                @endif
                                @if ($ovjera->broj_ziga != '')
                                    <tr>
                                        <th class="col-3">Žig</th>
                                        <td><span style="color:limegreen;">DA</span>, {{ $ovjera->broj_ziga }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th class="col-3">Datum ovjere</th>
                                    <td>{{ \Illuminate\Support\Carbon::make($ovjera->datum_ovjere)->format('d.m.Y') }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Datum valjanosti</th>
                                    <td>{{ \Illuminate\Support\Carbon::make($ovjera->datum_valjanosti)->format('d.m.Y') }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Napomena</th>
                                    <td>{{ $ovjera->mjesto_ovjere->napomena }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <h4 class="card-title mb-4 mt-4">Vlasnik mjerila</h4>
                        <div class="table-responsive">
                            <table class="table table-striped table-white">
                                <tbody>
                                <tr>
                                    <th class="col-3">Naziv</th>
                                    <td>{{ $ovjera->vlasnik->naziv }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Adresa</th>
                                    <td>{{ $ovjera->vlasnik->address }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Poštanski broj</th>
                                    <td>{{ $ovjera->vlasnik->zip }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Grad</th>
                                    <td>{{ $ovjera->vlasnik->city }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <h4 class="card-title mb-4 mt-4">Mjesto ovjere</h4>
                        <div class="table-responsive">
                            <table class="table table-striped table-white">
                                <tbody>
                                <tr>
                                    <th class="col-3">Adresa</th>
                                    <td>{{ $ovjera->mjesto_ovjere->address }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Poštanski broj</th>
                                    <td>{{ $ovjera->mjesto_ovjere->zip }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Grad</th>
                                    <td>{{ $ovjera->mjesto_ovjere->city }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <h4 class="card-title mb-4 mt-4">Mjesto postavljanja</h4>
                        <div class="table-responsive">
                            <table class="table table-striped table-white">
                                <tbody>
                                <tr>
                                    <th class="col-3">Adresa</th>
                                    <td>{{ $ovjera->mjesto_postavljanja->address }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Poštanski broj</th>
                                    <td>{{ $ovjera->mjesto_postavljanja->zip }}</td>
                                </tr>
                                <tr>
                                    <th class="col-3">Grad</th>
                                    <td>{{ $ovjera->mjesto_postavljanja->city }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </section>
@endsection
