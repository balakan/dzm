<div class="navbar navbar-expand-lg navbar-dark">
    <div class="container"><a class="navbar-brand d-none d-sm-block flex-shrink-0 me-4 order-lg-1" href="#"><img src="{{ asset('front/mu_logo.png') }}" width="250" alt="Državni zavod za mjeriteljstvo"></a><a class="navbar-brand d-sm-none me-2 order-lg-1" href="#"><img src="{{ asset('front/mu_logo.png') }}" width="250" alt="Državni zavod za mjeriteljstvo"></a>
        <!-- Toolbar-->
        <div class="navbar-toolbar d-flex align-items-center order-lg-3">
            <div class="navbar-tool dropdown ms-2"><a class="navbar-tool-icon-box border dropdown-toggle me-2" href="register"> <div class="navbar-tool-icon-box"><i class="navbar-tool-icon ci-user"></i></div></a>

                @auth
                    {{--<form method="POST" action="{{ route('logout') }}" class="d-xl-none">
                        @csrf
                        <a class="navbar-tool-icon-box border d-xl-none" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();"> <div class="navbar-tool-icon-box"><i class="navbar-tool-icon ci-sign-out"></i></div></a>
                    </form>--}}
                @else
                    <a class="navbar-tool-icon-box border d-xl-none me-2" href="{{ route('login') }}"> <div class="navbar-tool-icon-box"><i class="navbar-tool-icon ci-unlocked"></i></div></a>
                    @if (Route::has('register'))
                        <a class="dropdown-item d-flex align-items-center d-xl-none me-2" href="{{ route('register') }}"><i class="ci-user opacity-60 me-2"></i>Registracija</a>
                    @endif
                @endauth

                <div class="dropdown-menu dropdown-menu-end">
                    <div style="min-width: 14rem;">
                        <h6 class="dropdown-header">Korisnički račun</h6>
                        @auth
                            <a class="dropdown-item d-flex align-items-center" href="{{ route('dashboard') }}"><i class="ci-home opacity-60 me-2"></i>Korisnički račun</a>
                        @else
                            <a class="dropdown-item d-flex align-items-center" href="{{ route('login') }}"><i class="ci-unlocked opacity-60 me-2"></i>Prijava</a>
                            @if (Route::has('register'))
                                <a class="dropdown-item d-flex align-items-center" href="{{ route('register') }}"><i class="ci-user opacity-60 me-2"></i>Registracija</a>
                            @endif
                        @endauth
                        {{--<div class="dropdown-divider"></div>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a class="dropdown-item d-flex align-items-center" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();"><i class="ci-sign-out opacity-60 me-2"></i>Odjava</a>
                        </form>--}}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>