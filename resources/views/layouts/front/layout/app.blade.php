<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Državni zavod za mjeriteljstvo</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Državni zavod za mjeriteljstvo">
    <meta name="keywords" content="Državni zavod za mjeriteljstvo">
    <meta name="author" content="Državni zavod za mjeriteljstvo">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" media="screen" href="{{ asset('front/theme.css') }}">
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
</head>
<!-- Body-->
<body class="handheld-toolbar-enabled " style="background:#033e7c">
<!-- Sign in / sign up modal-->
<header class="bg-light shadow-sm navbar-sticky">
    @include('layouts.front.layout.partials.navbar')
</header>

<section class=" py-5" style="background:#033e7c">
    <div class="pb-lg-5 mb-lg-3">
        @yield('content')
    </div>
</section>

<footer class="bg-dark" style="position: fixed;left: 0;bottom: 0;width: 100%;z-index:1000">
    <div class="pt-4 bg-darker">
        <div class="container">
            <div class="d-md-flex justify-content-between pt-0">
                <div class="pb-4 fs-xs text-light opacity-50 text-center text-md-start">Copyright © 2021 Državni zavod za mjeriteljstvo. </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('front/bootstrap.bundle.min.js') }}"></script>
@livewireScripts
@stack('scripts')

</body>
</html>
