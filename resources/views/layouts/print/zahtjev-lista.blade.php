<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="invoice.css">
    <style>
        .tb-all {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
        }
        .tb-row {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 10px;
        }
        .tb-bold {
            font-weight: bold;
        }
    </style>
</head>
<body style="padding: .1rem 1rem">

<h3 style="text-align: center; margin-bottom: 20px; /*text-transform: uppercase;*/">Zahtjev Državnom zavodu za mjeriteljstvo</h3>

<table class="table" style="margin-bottom: 27px; font-size: 14px;">
    <tbody>
    <tr>
        <td>Broj Zahtjeva:</td>
        <td class="tb-bold">{{ $data['zahtjev']['br'] }}</td>
    </tr>
    <tr>
        <td>Klasa:</td>
        <td class="tb-bold">{{ $data['zahtjev']['klasa'] }}</td>
    </tr>
    <tr>
        <td>Broj Insitucije:</td>
        <td class="tb-bold">{{ $data['zahtjev']['institucija'] }}</td>
    </tr>
    <tr>
        <td>Broj Mjeritelja:</td>
        <td class="tb-bold">{{ $data['zahtjev']['mjeritelj'] }}</td>
    </tr>
    <tr>
        <td>Datum Pregleda: &nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="tb-bold">{{ $data['zahtjev']['pregled'] }}</td>
    </tr>
    <tr>
        <td>Status:</td>
        <td class="tb-bold">{{ $data['zahtjev']['status'] }}</td>
    </tr>
    </tbody>
</table>

<h5>Ovjere prema zahtjevu {{ $data['zahtjev']['br'] }}</h5>

<table class="table tb-all" style="width: 100%;">
    <thead>
    <tr>
        <th class="tb-all">Rb.</th>
        <th class="tb-all">Šifra mjerila</th>
        <th class="tb-all">Vrsta</th>
        <th class="tb-all">Serijski broj</th>
        <th class="tb-all">Tip</th>
        <th class="tb-all">Oznaka tipa</th>
        <th class="tb-all">Mjesto Pregleda</th>
        <th class="tb-all">Naljepnica / Žig</th>
        {{--<th class="tb-all">Status</th>--}}
    </tr>
    </thead>
    <tbody>
    @forelse ($data['ovjere'] as $ovjera)
        <tr>
            <td class="tb-row">{{ $ovjera['rb'] }}.</td>
            <td class="tb-row">{{ $ovjera['sifra'] }}</td>
            <td class="text-center tb-row">{{ $ovjera['vrsta'] }}</td>
            <td class="tb-row">{{ $ovjera['sb'] }}</td>
            <td class="tb-row">{{ $ovjera['tip'] }}</td>
            <td class="tb-row">{{ $ovjera['oznaka'] }}</td>
            <td class="tb-row">{{ $ovjera['mjesto'] }}</td>
            <td class="tb-row">{{ $ovjera['nalj'] }}</td>
            {{--<td class="tb-row">{{ $ovjera['status'] }}</td>--}}
        </tr>
    @empty
        <tr>
            <td class="text-center" colspan="7">Nema ovjera po zahtjevu <strong>{{ $data['zahtjev']['id'] }}</strong></td>
        </tr>
    @endforelse
    </tbody>
</table>

</body>
</html>