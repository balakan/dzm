<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - Admin</title>

    <link rel="canonical" href="#" />
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&display=swap" rel="stylesheet">

    <link class="js-stylesheet" href="{{ $setup->admin_color_scheme == 'dark' ? asset('css/dark.css') : asset('css/light.css') }}" rel="stylesheet">

    @livewireStyles

</head>

<body data-theme="{{ $setup->admin_color_scheme }}" data-layout="{{ $setup->admin_layout }}" data-sidebar-position="{{ $setup->admin_sidebar_position }}" data-sidebar-behavior="{{ $setup->admin_sidebar_behavior }}">

<div class="wrapper" x-data="{ sidebarCollapsed: {{ $setup->admin_sidebar_visibility == 'collapsed' ? 'true' : 'false' }} }">
    @livewire('back.layout.partials.sidebar')

    <div class="main">
        @livewire('back.layout.partials.topbar')

        <main class="content">
            {{ $slot }}
        </main>

        @livewire('back.layout.partials.footer')

    </div>

</div>

@stack('modals')

@livewireScripts

<script src="{{ asset('js/app.js') }}"></script>

<script>
    function parseDate(input, format) {
        format = format || 'dd.mm.yyyy'; // default format
        var parts = input.match(/(\d+)/g),
            i = 0, fmt = {};
        // extract date-part indexes from the format
        format.replace(/(yyyy|dd|mm)/g, function(part) { fmt[part] = i++; });

        return new Date(parts[fmt['yyyy']], parts[fmt['mm']]-1, parts[fmt['dd']]);
    }
</script>

@stack('scripts')

</body>
</html>
