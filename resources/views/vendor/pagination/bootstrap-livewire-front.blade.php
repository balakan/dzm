@if ($paginator->hasPages())
    <div class="row mt-4">
        <div class="col-md-6">
            <nav aria-label="Page navigation example" class="mb-3">
                <ul class="pagination">
                    @if ($paginator->onFirstPage())
                        <li class="page-item" aria-disabled="true" aria-label="@lang('pagination.previous')">
                            <span class="page-link" aria-hidden="true"><i class="ci-arrow-left"></i></span>
                        </li>
                    @else
                        <li class="page-item"><a class="page-link" href="javascript:void(0)" wire:click="previousPage" aria-label="@lang('pagination.previous')"><i class="ci-arrow-left"></i></a></li>
                    @endif

                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                                <li class="page-item d-none d-sm-block disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                        <li class="page-item d-none d-sm-block active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                                @else
                                        <li class="page-item d-none d-sm-block"><a class="page-link" href="javascript:void(0)" wire:click="gotoPage({{ $page }})">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    @if ($paginator->hasMorePages())
                            <li class="page-item"><a class="page-link" href="javascript:void(0)" wire:click="nextPage" aria-label="@lang('pagination.next')"><i class="ci-arrow-right"></i></a></li>
                    @else
                            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                                <span class="page-link" aria-hidden="true"><i class="ci-arrow-right"></i></span>
                            </li>
                    @endif
                </ul>
            </nav>
        </div>

        <div class="col-md-6 text-right">
            <p class="text-sm leading-5" style="color: whitesmoke; padding-top: 9px;">
                Prikazano
                <span class="font-weight-bold">{{ $paginator->firstItem() }}</span>
                do
                <span class="font-weight-bold">{{ $paginator->lastItem() }}</span>
                od
                <span class="font-weight-bold">{{ $paginator->total() }}</span>
                rezultata
            </p>
        </div>
    </div>
@endif