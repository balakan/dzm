<button {{ $attributes->merge(['type' => 'button', 'class' => 'btn btn-lg']) }}>
    {{ $slot }}
</button>
