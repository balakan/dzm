<button {{ $attributes->merge(['type' => 'submit', 'class' => 'btn btn-lg']) }}>
    {{ $slot }}
</button>
