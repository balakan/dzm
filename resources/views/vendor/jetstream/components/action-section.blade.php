<div class="card" {{ $attributes }}>
    <x-jet-section-title>
        <x-slot name="title">{{ $title }}</x-slot>
    </x-jet-section-title>

    <div class="card-body">
        <div class="row">
            {{ $content }}
        </div>
    </div>
</div>
