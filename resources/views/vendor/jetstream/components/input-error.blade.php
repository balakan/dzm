@props(['for'])

@error($for)
    <p {{ $attributes->merge(['class' => 'small text-danger']) }}>{{ $message }}</p>
@enderror
