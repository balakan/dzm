<button {{ $attributes->merge(['type' => 'button', 'class' => 'btn btn-lg btn-secondary']) }}>
    {{ $slot }}
</button>
