@props(['active'])

@php
$classes = ($active ?? false)
            ? 'active'
            : '';
@endphp

<a {{ $attributes->merge(['class' => 'sidebar-link' . $classes]) }}>
    {{ $slot }}
</a>