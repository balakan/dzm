@props(['submit'])

<div {{ $attributes->merge(['class' => 'tab-pane fade']) }}>
    <div class="card">
        <x-jet-section-title>
            <x-slot name="title">{{ $title }}</x-slot>
            {{--<x-slot name="description">{{ $description }}</x-slot>--}}
        </x-jet-section-title>

        <div class="card-body">
            <form wire:submit.prevent="{{ $submit }}">
                <div class="row {{ isset($actions) ? '' : '' }}">
                    {{ $form }}
                </div>

                @if (isset($actions))
                    {{ $actions }}
                @endif
            </form>
        </div>
    </div>
    {{ $slot }}
</div>
