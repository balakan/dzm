@props(['active'])

@php
$classes = ($active ?? false)
            ? 'active'
            : '';
@endphp

<li {{ $attributes->merge(['class' => 'sidebar-item' . $classes]) }}>
    {{ $slot }}
</li>