<x-auth-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <a href="{{ url('/') }}"><i class="fa fa-4x fa-balance-scale text-info mb-3"></i></a>
            <h1 class="h2">Republika Hrvatska</h1>
            <p class="lead">
                Državni zavod za mjeriteljstvo
            </p>
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="form-group">
                <x-jet-label for="password" value="{{ __('Lozinka') }}" />
                <x-jet-input id="password" type="password" name="password" required autocomplete="current-password" />
            </div>

            <x-jet-label for="remember_me" class="custom-control custom-checkbox">
                <x-jet-checkbox id="remember_me" name="remember" />
                <span class="custom-control-label">{{ __('Zapamti me') }}</span>
            </x-jet-label>

            <div class="text-center mt-4">
                <x-jet-button class="btn-primary">
                    {{ __('Log in') }}
                </x-jet-button>
            </div>

            <div class="text-center mt-4">
                @if (Route::has('password.request'))
                    <a class="" href="{{ route('password.request') }}">
                        {{ __('Zaboravio sam lozinku?') }}
                    </a>
                @endif
            </div>
        </form>
    </x-jet-authentication-card>
</x-auth-layout>
