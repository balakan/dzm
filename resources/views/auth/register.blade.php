<x-auth-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <a href="{{ url('/') }}"><i class="fa fa-4x fa-balance-scale text-info mb-3"></i></a>
            <h1 class="h2">Republika Hrvatska</h1>
            <p class="lead">
                Državni zavod za mjeriteljstvo
            </p>
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group">
                <x-jet-label for="name" value="{{ __('Ime') }}" />
                <x-jet-input id="name" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="form-group">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="form-group">
                <x-jet-label for="password" value="{{ __('Lozinka') }}" />
                <x-jet-input id="password" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="form-group">
                <x-jet-label for="password_confirmation" value="{{ __('Potvrda lozinke') }}" />
                <x-jet-input id="password_confirmation" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <x-jet-label class="custom-control custom-checkbox">
                    <x-jet-checkbox name="terms" id="terms"/>
                    <span class="custom-control-label">
                        {!! __('Pristajem na :terms_of_service i :privacy_policy', [
                                    'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="">'.__('Uvjere Uporabe').'</a>',
                                    'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="">'.__('Policu Privatnosti').'</a>',
                            ]) !!}
                    </span>
                </x-jet-label>
            @endif

            <div class="text-center mt-4">
                <x-jet-button class="btn-primary">
                    {{ __('Registriraj') }}
                </x-jet-button>
            </div>

            <div class="text-center mt-3">
                <a class="" href="{{ route('login') }}">
                    {{ __('Već sam registriran?') }}
                </a>
            </div>
        </form>

    </x-jet-authentication-card>
</x-auth-layout>
