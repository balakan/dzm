<x-app-layout>
    <div class="container-fluid p-0">
        <div class="row mb-2 mb-xl-3">
            <div class="col-auto d-none d-sm-block">
                <h3>{{ __('Dashboard') }}</h3>
            </div>

            <div class="col-auto ml-auto text-right mt-n1">
                {{--<button class="btn btn-primary shadow-sm">
                    <i class="align-middle" data-feather="refresh-cw">&nbsp;</i>
                </button>--}}
            </div>
        </div>

        @if(\Bouncer::is(auth()->user())->an('public'))
            <div class="row col-12">
                <h4>Molimo vas da ispunite svoje podatke do kraja na <br><a href="{{ route('profile.show') }}">Vašem Profilu...</a></h4>
            </div>
        @else
            @if (auth()->user()->can('*') || auth()->user()->institucija)
                {{--<div class="row">
                    <div class="col-12 col-sm-6 col-xxl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <h3 class="mb-2">{{ $zahtjevi->total() }}</h3>
                                        <p class="mb-2">Ukupno Zahtjeva</p>
                                        <div class="mb-0">
                                            <a href="{{ route('zahtjevi') }}">Pogledaj listu...</a>
                                        </div>
                                    </div>
                                    <div class="d-inline-block ml-3">
                                        <div class="stat">
                                            <i class="align-middle text-success" data-feather="clipboard"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xxl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <h3 class="mb-2">{{ $ovjere->total() }}</h3>
                                        <p class="mb-2">Ukupno Ovjera</p>
                                        <div class="mb-0">
                                            <a href="{{ route('ovjere') }}">Pogledaj listu...</a>
                                        </div>
                                    </div>
                                    <div class="d-inline-block ml-3">
                                        <div class="stat">
                                            <i class="align-middle text-success" data-feather="check-circle"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xxl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <h3 class="mb-2">{{ $mjerila->total() }}</h3>
                                        <p class="mb-2">Ukupno Mjerila</p>
                                        <div class="mb-0">
                                            <a href="{{ route('mjerila') }}">Pogledaj listu...</a>
                                        </div>
                                    </div>
                                    <div class="d-inline-block ml-3">
                                        <div class="stat">
                                            <i class="align-middle text-success" data-feather="layers"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xxl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <h3 class="mb-2">{{ number_format($total, 2) }} kn</h3>
                                        <p class="mb-2">Ukupan promet</p>
                                        <div class="mb-0">
                                            <a href="{{ route('obracuni') }}">Pogledaj listu...</a>
                                            --}}{{--<span class="badge badge-soft-success mr-2"> <i class="mdi mdi-arrow-bottom-right"></i> +8.65% </span>
                                            <span class="text-muted">Since last week</span>--}}{{--
                                        </div>
                                    </div>
                                    <div class="d-inline-block ml-3">
                                        <div class="stat">
                                            <i class="align-middle text-info" data-feather="dollar-sign"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('dashboard') }}" method="get">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h5 class="card-title"><a href="{{ route('zahtjevi') }}"><i class="align-middle text-secondary" data-feather="clipboard"></i> Lista zahtjeva</a></h5>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group date" id="date_from" data-target-input="nearest">
                                                <input type="text" name="date_from" class="form-control form-control-lg datetimepicker-input" data-target="#date_from" value="{{ request()->query('date_from') ?: null }}" placeholder="Datum od"/>
                                                <div class="input-group-append" data-target="#date_from" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group date" id="date_to" data-target-input="nearest">
                                                <input type="text" name="date_to" class="form-control form-control-lg datetimepicker-input" data-target="#date_to" value="{{ request()->query('date_to') ?: null }}" placeholder="Datum do" />
                                                <div class="input-group-append" data-target="#date_to" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1"><button type="submit" class="btn btn-block btn-lg btn-outline-secondary"><i class="fa fa-search"></i></button></div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Rb.</th>
                                            <th>Klasa</th>
                                            <th>Institucija/Mjeritelj</th>
                                            <th class="text-center">Ovjera</th>
                                            <th>Mjesto pregleda</th>
                                            <th class="text-center">Datum predaje</th>
                                            <th class="text-center">Datum pregleda</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($zahtjevi as $zahtjev)
                                            <tr style="cursor: pointer;">
                                                <td class="font-weight-bold"><a href="{{ route('zahtjevi.show', ['zahtjev' => $zahtjev]) }}">{{ $zahtjev->id }}</a></td>
                                                <td>{{ $zahtjev->klasa }}</td>
                                                <td>{{ $zahtjev->institucija->broj }} / {{ $zahtjev->mjeritelj->data->broj }}</td>
                                                <td class="text-center">{{ $zahtjev->ovjere->count() }}</td>
                                                <td>{{ $zahtjev->ovjere->first()->mjesto_ovjere->city }}, <span class="font-weight-light">{{ $zahtjev->ovjere->first()->mjesto_ovjere->address }}</span></td>
                                                <td class="text-center">{{ $zahtjev->datum_predaje ? \Illuminate\Support\Carbon::make($zahtjev->datum_predaje)->format('d.m.Y.') : '' }}</td>
                                                <td class="text-center">{{ $zahtjev->datum_pregleda ? \Illuminate\Support\Carbon::make($zahtjev->datum_pregleda)->format('d.m.Y.') : '' }}</td>
                                                <td class="text-center">
                                                    @include('layouts.back.partials.badge-status', ['status' => $zahtjev->status])
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $zahtjevi->links() }}
                            </div>
                        </div>
                    </div>

                    {{--<div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title"><a href="{{ route('ovjere') }}"><i class="align-middle text-secondary" data-feather="check-circle"></i> Lista ovjera</a></h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Oznaka</th>
                                            <th>Zahtjev</th>
                                            <th>Mjerilo</th>
                                            <th>Ovjera</th>
                                            <th class="text-center">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($ovjere as $ovjera)
                                            <tr style="cursor: pointer;">
                                                <td>{{ $ovjera->id }}</td>
                                                <td><a href="{{ route('zahtjevi.show', ['zahtjev' => $ovjera->zahtjev]) }}">{{ $ovjera->zahtjev->klasa }}</a></td>
                                                <td><a href="{{ route('mjerila', ['search' => $ovjera->mjerilo->sn]) }}">{{ $ovjera->mjerilo->sn }}</a>, {{ $ovjera->mjerilo->tip->naziv }}</td>
                                                <td>
                                                    {!! $ovjera->naljepnica ? '<span style="color:limegreen;">DA</span>' : '<span style="color:indianred;">NE</span>' !!}
                                                </td>
                                                <td class="text-center"><i class="{{ $ovjera->status ? 'text-success fa fa-check' : 'text-danger fa fa-times' }}"></i></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $ovjere->links() }}
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title"><a href="{{ route('mjerila') }}"><i class="align-middle text-secondary" data-feather="layers"></i> Lista mjerila</a></h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Serijski br.</th>
                                            <th>Proizvođač</th>
                                            <th>Grupa</th>
                                            <th>Tip</th>
                                            <th>Zadnja ovjera</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($mjerila as $mjerilo)
                                            <tr>
                                                <td class="font-weight-bold">{{ $mjerilo->sn }}</td>
                                                <td>{{ $mjerilo->tip->brand }}</td>
                                                <td>{{ isset($mjerilo->grupa->name) ? \Illuminate\Support\Str::limit($mjerilo->grupa->name, 36) : '' }}</td>
                                                <td>{{ $mjerilo->tip->naziv }}</td>
                                                <td>
                                                    @if($mjerilo->last_ovjera()->first())
                                                        <a href="{{ route('ovjere.show', ['ovjera' => $mjerilo->last_ovjera()->first()]) }}">{{ $mjerilo->last_ovjera()->first()->id }}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $mjerila->links() }}
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title">Godišnji graf</h5>
                            </div>
                            <div class="card-body">
                                <div class="chart mb-3">
                                    <canvas id="chartjs-bar"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>--}}

                </div>
            @else
                <h3>Molimo as da kontaktirate administratora da vas pravilno odobri za korištenje DZM aplikacije.</h3>
            @endif

        @endif
    </div>

    @push('scripts')
        <script>
            $(() => {
                //
                $('#date_from').datetimepicker({
                    format: 'DD.MM.YYYY'
                });
                //
                $('#date_to').datetimepicker({
                    format: 'DD.MM.YYYY'
                });
            });
        </script>

        {{--<script>
            document.addEventListener("DOMContentLoaded", function(dom) {
                let months_data = JSON.parse('{{ $months }}'.replace(/&quot;/g,'"'));
                let months_names = [];
                let months_values = [];
                let top = 0;
                let step_size = 100;

                for (let i = 0; i < months_data.length; i++) {
                    months_names.push(months_data[i].title + '.');
                    months_values.push(months_data[i].value);
                }

                for (let i = 0; i < months_values.length; i++) {
                    if (months_values[i] > top) {
                        top = months_values[i];
                    }
                }

                if (top > 10000) {
                    step_size = 2000;
                }
                if (top < 10000 && top > 4000) {
                    step_size = 1000;
                }
                if (top < 4000 && top > 1000) {
                    step_size = 500;
                }

                new Chart(document.getElementById("chartjs-bar"), {
                    type: "bar",
                    data: {
                        labels: months_names,
                        datasets: [{
                            label: "Last year",
                            backgroundColor: window.theme.primary,
                            borderColor: window.theme.primary,
                            hoverBackgroundColor: window.theme.primary,
                            hoverBorderColor: window.theme.primary,
                            data: months_values,
                            barPercentage: 1,
                            categoryPercentage: .5
                        }]
                    },
                    options: {
                        maintainAspectRatio: false,
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                gridLines: {
                                    display: false
                                },
                                stacked: false,
                                ticks: {
                                    stepSize: step_size
                                }
                            }],
                            xAxes: [{
                                stacked: false,
                                gridLines: {
                                    color: "transparent"
                                }
                            }]
                        }
                    }
                });
            });
        </script>--}}
    @endpush
</x-app-layout>
