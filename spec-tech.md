# Tehnička specifikacija
### Aplikacija Državnog zavoda za mjeriteljstvo

---
**Naziv aplikacije** DZM Admin  
**Korisnik** Državni zavod za mjeriteljstvo  
**Razvoj** AGmedia d.o.o.  
**Privremeni URL** http://mapp.selectpo.lin48.host25.com/

## Hardverski i softverski zahtjevi za instalaciju DZM Admin

### Hardverski zahtjevi

- Linux web server
- 40 GB prostora
- 8 GB dodijeljene RAM memorije
- min 2 CPU jezgre

### Softverski zahtjevi

- Linux web server
- PHP verzija 7.3+
- MySql 5.6+
- cPanel administracija
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension


## Postavke servera

Aplikacija zahtjeva samostalni web site unutar servera.  
Ukoliko se želi koristiti HTTPS pristup (preporučljivo), potrebno je dodijeliti ispravan certifikat (IIS kompatibilan) u protivnom će se postaviti samo-potpisani certifikat  
Domenski račun pod kojim je pokrenut application pool mora imati slijedeća prava:  

- čitanje foldera u kojem se aplikacija nalazi
- čitanje i pisanje u folderu za pohranu datoteka
- db_owner prava u bazi podataka

## Postavke baze podataka  

- recovery model: full
- default collation: utf_general_ci
